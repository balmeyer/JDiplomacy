[recipient]
${account.email}
[subject]
[JDiplomacy] Confirmation d'inscription
[body]
Bonjour ${account.login},

Pour confirmer votre inscription au jeu JDiplomacy, merci de cliquer sur ce lien :

${confirmurl}

---
http://www.jdiplomacy.net
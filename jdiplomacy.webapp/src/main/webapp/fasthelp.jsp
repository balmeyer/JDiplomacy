<h1>Moving</h1>
<li>Left-Click  > Drag and Drop</li>

<h1>Moving overseas</h1>
<li>Right-Click (or Control + Click) > Drag and Drop</li>
<br/>
<h1>Support</h1>
<li>Click to select supporting unit</li>
<li>Right-Click on supported unit (or Shift + Click)</li>
<li>Drag and Drop to add support</li>
<br/>
<h1>Convoy</h1>
<li>Double-Click to select fleet (or Control + Click).</li>
<li>Drag and drop to add convoy.</li>
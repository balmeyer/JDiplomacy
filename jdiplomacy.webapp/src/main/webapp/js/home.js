var content = new Array();
var contentload = new Array();
var current = "";

$(document).ready(function() {
	//$("#waiticon").hide();

});

function setPanel(id){
	var div = '<img src="images/ajax-loader.gif"/>';
	
	//save current
	if (current != "" && contentload[current] != "1"){
		contentload[current] = "1";
		content[current] = $("#content").html();
	}
	
	current = id;
	
	if (contentload[id] == "1"){
		var data = content[id];
		//$("#content").remove();
		$("#content").html(data);
		return;
	}
	
	$("#waiticon").append(div);
	$("#content").load('/content/panel' + id + '.html', ajaxOk(id));
	
}

function ajaxOk(id){
	$("#waiticon").remove();
	
}
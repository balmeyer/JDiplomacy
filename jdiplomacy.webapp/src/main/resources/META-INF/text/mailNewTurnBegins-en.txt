[Recipient]
${account.email}
[Subject]
[JDiplomacy] A new turn has just started !
[Body]
Hello ${account.login},

A new round has just started in the following game :

Title : ${game.title}
Id : ${gameid}


---------------------------
http://www.jdiplomacy.net

(to stop receiving this email, disable the option in the JDiplomacy "options" Panel)
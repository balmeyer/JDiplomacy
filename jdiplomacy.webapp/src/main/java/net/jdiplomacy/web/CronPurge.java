package net.jdiplomacy.web;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.PendingOrder;
import net.jdiplomacy.server.entities.Token;
import net.jdiplomacy.server.entities.Tour;

/**
 * Purge data
 * @author JBVovau
 *
 */
public class CronPurge extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5433074537399051585L;

	@Override
	public void doGet(HttpServletRequest req  , 
			HttpServletResponse res) throws IOException {
		

		this.purgeOldToken();
	
		this.purgeOldPendingOrder();
		
		this.purgeOldAccount();
	}
	
	/**
	 * Purge all token connection
	 */
	private void purgeOldToken(){
		//----------------------------------------------
		//purge old TOKEN
		EntityManager em = DataManager.createEntityManager();
		
		Query q = em.createQuery("select t from Token t");
		
		Calendar limitdate = Calendar.getInstance();
		limitdate.add(Calendar.HOUR, -48);
		
		int nbpurge = 0;
		
		for (Object t : q.getResultList()){
			
			Token token = (Token) t;
			
			Calendar old = Calendar.getInstance();
			old.setTime(token.getDateCreation());
			
			//too old ?
			if (old.before(limitdate)){
				//delete
				em.remove(token);
				DataManager.commit(em);
				nbpurge++;
			}
			
		}
		
		em.close();
		System.out.println("purged token = " + nbpurge);
	
	}
	
	private void purgeOldPendingOrder(){
		
		//limit date
		Calendar limitdate = Calendar.getInstance();
		limitdate.add(Calendar.HOUR, -100); //almost 4 days
		
		EntityManager em = DataManager.createEntityManager();
		
		List<Tour> list = em.createQuery("select t from Tour t where t.finished = true")
			.getResultList();
		
		for (Tour t : list){
			
			if (!t.isFinished()) continue;
			
			Calendar old = Calendar.getInstance();
			old.setTime(t.getStartDate());
			
			if (old.before(limitdate)){
				//load PendingTour
				for(PendingOrder po : t.getPendingOrders(em)){
					em.remove(po);
					DataManager.commit(em);
				}
			}
		}
		
		em.close();
		
		
	}
	
	private void purgeOldAccount(){
		EntityManager em = DataManager.createEntityManager();
		
		List<Account> acs = em.createQuery("select a from Account a where a.confirmed = false")
		.getResultList();
		
		Calendar limitdate = Calendar.getInstance();
		limitdate.add(Calendar.HOUR, 24 * -7); // 7 days
		
		for(Account ac : acs){
			if (ac.isConfirmed()) continue;
			
			Calendar date = Calendar.getInstance();
			date.setTime(ac.getDateCrea());
			
			if (date.before(limitdate)){
				//delete
				em.remove(ac);
				DataManager.commit(em);
			}
		}
		
		
		em.close();
	}
}

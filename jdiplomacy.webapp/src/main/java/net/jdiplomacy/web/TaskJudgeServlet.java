package net.jdiplomacy.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jdiplomacy.judge.JudgeEngine;
import net.jdiplomacy.judge.JudgeResult;
import net.jdiplomacy.judge.Phase;
import net.jdiplomacy.language.TextMail;
import net.jdiplomacy.language.TextType;
import net.jdiplomacy.server.command.ListGamesCommand;
import net.jdiplomacy.server.command.Tools;
import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.Game;
import net.jdiplomacy.server.entities.Inscription;
import net.jdiplomacy.server.entities.MyCache;
import net.jdiplomacy.server.entities.PendingOrder;
import net.jdiplomacy.server.entities.Tour;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

public class TaskJudgeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1825338364337017711L;

	@Override
	public void doGet(HttpServletRequest req  , 
			HttpServletResponse res) throws IOException {
		
		Date now = Calendar.getInstance().getTime();
		
		//fetch current order
		if (req.getParameter("idtour") == null) return;
		Long id = Long.valueOf(req.getParameter("idtour"));
		
		//force end tour ?
		boolean force = req.getParameter("force") != null;
		
		MyCache.remove("xmlorders" + String.valueOf(id));
		
		EntityManager em = DataManager.createEntityManager();
		Tour t = em.find(Tour.class, id);
		
		if (t == null) return;
		
		//remove from cache
		MyCache.remove("xmltour" + String.valueOf(t.getKeygame().getId()) 
				+ "-" + String.valueOf(id));
		
		@SuppressWarnings("unchecked")
		List<PendingOrder> list = em.createQuery("select po from PendingOrder po "
				+ " where po.keytour = :key" ).setParameter("key", t.getKey())
				.getResultList();
		
		if (!force && list.size() != 7) {//TODO order not complete 
			throw new IllegalStateException("tour not completed : id = " + t.getKey().getId());
		}
		
		//build orders and positions
		StringBuilder myorders = new StringBuilder();
		String positions = t.getStartPosition().getValue();
		List<Object> orders = new ArrayList<Object>(100);
		for(PendingOrder po : list){
			myorders.append(po.getCountry());
			myorders.append(" : ");
			if (po.getOrders() != null & po.getOrders().getValue() != null){
				for (String o : po.getOrders().getValue().split(";")){
					orders.add(o);
					myorders.append(o);
					myorders.append(" ; ");
				}
			}
			myorders.append("\r\n");
		}
		
		//phase
		
		JudgeEngine je = new JudgeEngine();
		JudgeResult jr = je.judge(positions.split(";"), orders.toArray(), t.getPhase());
	

		
		//next phase
		
		//test if next phase already exists...
		int nextPhaseId = getNextPhaseId(t.getPhaseID(), jr.getNextPhase());
		List<Tour> alreadyExist = em.createQuery("select t from Tour t where t.keygame = :game "
				+ " and t.phase = :phase").setParameter("game", t.getKeygame())
				.setParameter("phase", nextPhaseId).setMaxResults(1).getResultList();
		if (alreadyExist.size() > 0){
			throw new IllegalStateException("tour already exists for phase :" + nextPhaseId);
		}
		
		//old tour
		t.setOrders(new Text(jr.getTextOrders(true)));
		t.setFinished(true);
		DataManager.commit(em);
		
		//set everyone has played
        List<Inscription> ins = em.createQuery("select i from Inscription i "
        		+ " where i.keygame = :game")
        		.setParameter("game", t.getKeygame())
        		.getResultList();
        
        for(Inscription i : ins){
        	i.setHasplayed(false);
        	DataManager.commit(em);
        	//empty "my list" cache
        	MyCache.remove(ListGamesCommand.CACHE_LIST_MY_GAMES + i.getKeyAccount().toString());
        }
        
		
        //test if game finished
        if (jr.getNextPhase().equals(Phase.gameOver)){
        	Game g = em.find(Game.class, t.getKeygame());
        	g.setFinished(true);
        	g.setEndDate(now);
        	
        	//new list
        	List<Integer> winlist = new ArrayList<Integer>();
        	for (net.jdiplomacy.judge.game.board.Country c : jr.getWinList()){
        		winlist.add(c.getIndex());
        	}
        	
        	g.setWinlist(winlist);
        	DataManager.commit(em);
        	
        	//empty cache
        	MyCache.remove(ListGamesCommand.CACHE_ALL_GAMES);
        	MyCache.remove(ListGamesCommand.CACHE_LIST_OPEN_GAMES);
        }
        
        
		Tour newtour = new Tour();
		newtour.setKeygame(t.getKeygame());
		newtour.setStartDate(now);
		newtour.setStartPosition(new Text(jr.getFinalPosition()));
		newtour.setPhaseID(nextPhaseId);
		newtour.setFinished(false);
				
		em.persist(newtour);
		DataManager.commit(em);
		em.close();
		
	
		this.sendMail(t.getKeygame());
	}
	
	private int getNextPhaseId(int phase ,Phase newPhase){
        int year = (phase - 1) / 5;
        year = year + 1901;
        int lastphase = ((phase - 1) % 5);
        
        int newphase = lastphase + 1;
        

        	switch(newphase){
        	case 1:
        		if (newPhase != Phase.disbandSpring) newphase++;
        		break;
        	case 3:
        		if (newPhase != Phase.disbandFall) newphase++;
        		break;
        	}
        	
        	//adujust ?
        	if (newphase == 4 && newPhase != Phase.adjust) newphase++;
        	
            if (newphase >4){
            	year++;
            	newphase = 0;
            } 
        
        
        int id = (year - 1901) * 5;
        id = id + newphase + 1;

        return id;
	}
	
	/**
	 * send email when turn completed
	 * @param game
	 */
	private void sendMail(Key keygame){
		EntityManager em = DataManager.createEntityManager();
		
		List<Inscription> listIns = em.createQuery("select i from Inscription i "
				+ " where i.keygame = :key").setParameter("key", keygame)
				.getResultList();
		
		Game game = null;
		
		for(Inscription ins : listIns){
			//load Account
			Account ac = ins.getAccount(em);
			Object op = ac.getConfig("maileachtour");
			if (op != null && op.toString().equals("yes")){
				
				TextMail tm = new TextMail(ac,TextType.mailNewTurnBegins);
				
				if(game == null) game = em.find(Game.class, keygame);
				tm.addData(game);
				tm.addData("gameid",game.getKey().getId());
				
				Tools.sendMail(tm);
				
			}
		}
		
		//TODO test finished
		
		
		em.close();
	}
}

package net.jdiplomacy.web;

import java.io.IOException;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jdiplomacy.server.command.Tools;

/**
 * Use for mailing
 * @author Jean-Baptiste Vovau
 *
 */
public class TaskMailing  extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2027645113998072123L;

	@Override
	public void doPost(HttpServletRequest req  , 
			HttpServletResponse res) throws IOException {
		
		Properties props = new Properties(); 
        Session session = Session.getDefaultInstance(props, null); 
        MimeMessage message = null;
        

		try {
			message = new MimeMessage(session, req.getInputStream());
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			throw new IOException ("erreur recup message", e);
		}

		
		if (message == null) return;
		
			try {
				Tools.sendMail(message.getSubject(), 
						((MimeMultipart)message.getContent())
						.getBodyPart(0).getContent().toString()
						, "vovau@jdiplomacy.net");
				
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				throw new IOException ("erreur envoi mail", e);
			}

		
	}
	
}

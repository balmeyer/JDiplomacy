package net.jdiplomacy.web;


import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;
import com.google.appengine.api.taskqueue.TaskOptions.Method;


import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.Game;
import net.jdiplomacy.server.entities.PendingOrder;
import net.jdiplomacy.server.entities.Tour;


public class CronCheckGames  extends HttpServlet{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7086118520645195880L;

	@Override
	public void doGet(HttpServletRequest req  , 
			HttpServletResponse res) throws IOException {
		
		
		//check limited games
		EntityManager em = DataManager.createEntityManager();
		
		Query q = em.createQuery("select g from Game g where g.started = true and g.minutePerTour > 0");
		
		List<Game> list = q.getResultList();
		
		for (Game g : list){
			/*
			System.out.println("Game : " + g.getKey().getId() 
					+ ", started = " + g.isStarted()
					+ ", minute = " + g.getMinutePerTour());*/
			
			if (!g.isStarted()) continue;
			
			int minute  = g.getMinutePerTour();
			
			if (minute == 0) continue;
			
			//last tour
			List<Tour> lastTourList = em.createQuery("select t from Tour t "
					+ " where t.keygame = :key order by t.startDate desc ")
					.setParameter("key", g.getKey())
					.setMaxResults(1)
					.getResultList();
			
			if (lastTourList.size() == 0) continue;
			
			//get last tour
			Tour t = lastTourList.get(0);
			System.out.println("test tour :" + t.getKey().getId());
			
			//get pending order
			List<PendingOrder> listPO = em.createQuery("select po from PendingOrder po "
					+ " where po.keytour = :keytour").setParameter("keytour", t.getKey())
					.getResultList();
			
			//no order yet !
			if (listPO.size() == 0) {
				System.out.println("NO ORDER YET !");
				continue;
			}
			
			//fetch min order
			Calendar mindate = Calendar.getInstance();
			for (PendingOrder po : listPO){
				Calendar dateCompare = Calendar.getInstance();
				dateCompare.setTime(po.getCreationDate());
				
				if (mindate.after(dateCompare)){
					mindate = dateCompare;
				}
			}
			//date min
			System.out.println("date min :" + mindate.getTime());
			
			//substract date
			Calendar now = Calendar.getInstance();
			mindate.add(Calendar.MINUTE, minute);
			
			System.out.println("now : " + now.getTime() 
					+ ", limit date :" + mindate.getTime());
			//time out !
			if (now.after(mindate)){
				System.out.println("ask for judge !!");
				Queue queue = QueueFactory.getDefaultQueue();
            	queue.add(
            			withUrl("/tasks/judge")
            			.method(Method.GET)
            			.param("force", "1")
            			.param("idtour", String.valueOf(t.getKey().getId()))
            			);
			}
		}
		
		
		em.close();
		
	}
	
}

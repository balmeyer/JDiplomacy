package net.jdiplomacy.web;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jdiplomacy.judge.JudgeEngine;
import net.jdiplomacy.judge.JudgeResult;
import net.jdiplomacy.judge.Phase;


public class JudgeServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest req  , 
			HttpServletResponse res) throws IOException {
		
		String positions = req.getParameter("positions");
		String orders = req.getParameter("orders");
		String textPhase = req.getParameter("phase");
		
		if (positions == null) positions = "";
		if (orders == null) orders = "";
		
		//parse the current phase
		Phase phase = this.parsePhase(textPhase);
		
		JudgeEngine engine = new JudgeEngine();
		JudgeResult result = engine.judge(positions, orders, phase);
		
		try {
		JudgeResult.toXmlStream(result, res.getOutputStream());
		}catch (Exception ex){
			//log("crashed" , ex);
			res.getWriter().write("BAD");
		}
		
	}
	
	private Phase parsePhase(String phase){
		
		if (phase == null || phase.trim().length() == 0) return Phase.movementSpring;
		
		//classic
		if (phase.toLowerCase().equals("spring")) return Phase.movementSpring;
		if (phase.toLowerCase().equals("fall")) return Phase.movementFall;
		if (phase.toLowerCase().equals("disbandSpring")) return Phase.disbandSpring;
		if (phase.toLowerCase().equals("disbandFall")) return Phase.disbandFall;
		if (phase.toLowerCase().equals("adjust")) return Phase.adjust;
		
		//
		if (phase.toLowerCase().equals("move")) return Phase.movementSpring;
		if (phase.toLowerCase().equals("disband")) return Phase.disbandSpring;
		
		throw new IllegalArgumentException("phase unkwown : " + phase);

	}
	
}

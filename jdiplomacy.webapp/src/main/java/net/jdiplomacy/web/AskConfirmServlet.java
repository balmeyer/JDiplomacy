package net.jdiplomacy.web;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jdiplomacy.language.TextMail;
import net.jdiplomacy.language.TextType;
import net.jdiplomacy.server.command.Tools;
import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.DataManager;


public class AskConfirmServlet extends HttpServlet {



	/**
	 * 
	 */
	private static final long serialVersionUID = -8273489524689912187L;

	private static final String CONFIRM_PAGE = "http://www.jdiplomacy.net/diplomacy/confirm";
	
	@Override
	public void doGet(HttpServletRequest req  , 
			HttpServletResponse res) throws IOException {
		
        
		Long id = Long.parseLong(req.getParameter("id"));
		
		EntityManager em = DataManager.createEntityManager();
		
		Account ac = em.find(Account.class, id);
		
		//confirm link
        StringBuilder sb = new StringBuilder();
        sb.append(CONFIRM_PAGE);
        sb.append("?id=");
        sb.append(ac.getChecktoken().toString());
		
		TextMail tm = new TextMail(ac, TextType.mailAskConfirmation);
		tm.addData("confirmurl", sb.toString());
		
        Tools.sendMail(tm);
        
        System.out.println(tm.getFragment("body"));

	}
	
}

package net.jdiplomacy.web;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.jdiplomacy.server.handler.HttpDispatcher;



public class DiplomacyServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3357052064323636259L;

	private HttpDispatcher dispatcher = new HttpDispatcher();

	@Override
	public void doGet(HttpServletRequest req  , 
			HttpServletResponse res) throws IOException {
		
		
		
		Map<String ,String> map = new HashMap<String,String>();
		
		Enumeration<String> e = req.getParameterNames();
		while(e.hasMoreElements()){
			String key = e.nextElement().toString();
			map.put(key,req.getParameter(key));
		}
		
		res.setHeader("Content-Type", "text.xml");
		dispatcher.dispatch(req.getPathInfo(), map, res.getOutputStream());
		
		
			

	}
	
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.command;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.Country;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.Game;
import net.jdiplomacy.server.entities.Inscription;
import net.jdiplomacy.server.entities.MyCache;
import net.jdiplomacy.server.entities.PendingOrder;
import net.jdiplomacy.server.entities.Tour;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

/**
 *
 * @author Jean-Baptiste
 */
@CommandType(page="orders")
public class OrdersCommand extends AuthorizedCommand{

    @Override
	public void perform(Map<String, String> map, OutputStream output) throws IOException, NotAuthorizedException {

        EntityManager em = DataManager.createEntityManager();
        
        Account ac = getAccount(map,em);
        String action = map.get("action");
        String strIdTour = map.get("idtour");

        Date now = Calendar.getInstance().getTime();

        
        Long idTour = Long.parseLong(strIdTour);

        Tour tour = null;
        
        //find in cache
        tour = (Tour) MyCache.get("tour" + strIdTour);
        
        if (tour == null) {
        	tour = em.find(Tour.class, idTour);
        	MyCache.put("tour" + strIdTour, tour);
        }
        

        //send new orders
        if (action != null && action.equals("send")){
        	        	
        	//empty order cache
        	MyCache.remove("xmlorders" + strIdTour);
        	//empty "my games" list (containing order status)
        	MyCache.remove(ListGamesCommand.CACHE_LIST_MY_GAMES + ac.getKey().toString());
        	
            //get current pending order
            List<PendingOrder> listOfOrders = em.createQuery("select o from PendingOrder o "
                    + " where o.keytour = :keytour")
                    .setParameter("keytour", tour.getKey())
                    .getResultList();

            //map : country TO pendingOrder
            Map<Integer,PendingOrder> countryToPendingOrders = new HashMap<Integer,PendingOrder>(7);
            for(PendingOrder po : listOfOrders){
                countryToPendingOrders.put(po.getCountry() , po);
            }

            //new orders received
            Map<Country,String> countryToNewOrders = buildCountryOrders(map ,em);
            
            for(Country c : countryToNewOrders.keySet()){
                String textOrders = countryToNewOrders.get(c);

                //no new order for this country
                //if (textOrders == null) continue;
                
                //modify or create
                PendingOrder po = countryToPendingOrders.get(c.getIndex());
                if (po == null){
                    po = new PendingOrder();
                    po.setAccount(ac);
                    po.setCountry(c);
                    po.setCreationDate(now);
                    po.setTour(tour);
                    //tour.getPendingOrders().add(po);
                    po.setOrders(new Text(textOrders));
                    em.persist(po);
                    DataManager.commit(em);
                    countryToPendingOrders.put(c.getIndex(), po);
                    //set has played
                    List<Inscription> ins = em.createQuery("select i from Inscription i "
                    		+ " where i.keygame = :game and i.keyaccount = :user")
                    		.setParameter("game", tour.getKeygame())
                    		.setParameter("user", ac.getKey())
                    		.getResultList();
                    
                    for(Inscription i : ins){
                    	i.setHasplayed(true);
                    }
                    DataManager.commit(em);
                } else{
                	//order should not be modified...
                }
                
                

            }

            
            //em.refresh(tour);
            if (countryToPendingOrders.size() == 7){
            	Queue q = QueueFactory.getDefaultQueue();
            	q.add(
            			withUrl("/tasks/judge")
            			.method(Method.GET)
            			.param("idtour", String.valueOf(idTour))
            			);
            }
        }

        
        //test cache
        Document cachedDoc = (Document) MyCache.get("xmlorders" + strIdTour);
        if (cachedDoc != null){
        	flushDoc(cachedDoc, output);
        	return;
        }

        //find in database
        Game game = em.find(Game.class, tour.getKeygame());
        Element pending = getPendingOrdersElement(em , tour , game);
        
        Element root = new Element("pendingOrders");
        Document doc = new Document(root);
        root.addContent(pending);

        flushDoc(doc, output);
        em.close();
        
        //keep in cache
        MyCache.put("xmlorders" + strIdTour, doc);
    }


    @SuppressWarnings("unchecked")
	public static Element getPendingOrdersElement(EntityManager em ,Tour t ,Game game ){
    	//calculate delay
    	String minuteDelay = "0";
    	if (game.getMinutePerTour() > 0){
    		//calculate delay
    		List<PendingOrder> listPO = null;
    		

    		listPO = em.createQuery("select po from PendingOrder po "
    				+ " where po.keytour = :keytour").setParameter("keytour", t.getKey())
    				.getResultList();
		
    		
    		if (listPO.size() >0) {
	    		//fetch date min
	    		Calendar min = Calendar.getInstance();
	    		for(PendingOrder po : listPO){
	    			Calendar current = Calendar.getInstance();
	    			current.setTime(po.getCreationDate());
	    			if (min.after(current)){
	    				min = current;
	    			}
	    		}
	    		//min date found
	    		Calendar now = Calendar.getInstance();
	    		long dif = now.getTimeInMillis() - min.getTimeInMillis();
	    		//in minute ?
	    		long minute = dif / 60000L;
	    		minuteDelay = String.valueOf(minute);
    		}
    	}
    	
    	//set element
        Element pending = new Element("pending");
        pending.setAttribute("idtour", String.valueOf(t.getKey().getId()));
        pending.setAttribute("delay", minuteDelay); //TODO tour delay
        

    	Collection<PendingOrder> list = t.getPendingOrders(em);

        
        for (PendingOrder po : list) {
            Element order = new Element("order");
            pending.addContent(order);
            order.setAttribute("country", String.valueOf(po.getCountry()));
            order.setAttribute("date", Tools.format(po.getCreationDate()));
            order.setAttribute("delay", "10"); //TODO Country  delay
            order.addContent(po.getOrders().getValue());
        }
        return pending;
    }


    private Map<Country,String> buildCountryOrders(Map<String,String> map, EntityManager em){
        Map<Country,String> co = new HashMap<Country,String>();

        for(String key : map.keySet()){
            if (key.startsWith("country")){

                String strIndex = key.replace("country[", "").replace("]", "");
                Country c = Country.getCountry(em, Integer.parseInt(strIndex));
                co.put(c, map.get(key));

            }

        
        }

        return co;
    }

    
    private static void flushDoc(Document doc, OutputStream output) throws IOException {
        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(Format.getPrettyFormat());
        outputter.output(doc, output);
        output.close();
    }
    
}

package net.jdiplomacy.server.command;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;


import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.Conversation;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.Game;
import net.jdiplomacy.server.entities.MyCache;

import org.jdom.CDATA;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.google.appengine.api.datastore.Text;

@CommandType(page = "conversation")
public class ConversationCommand extends AuthorizedCommand {

	private static final String CACHE_KEY = "CONVERSATION-";
	
	@Override
	public void perform(Map<String, String> map, OutputStream output)
			throws IOException, NotAuthorizedException {

		String action = map.get("action");

		if (action == null)
			return;

		if (action.equalsIgnoreCase("send")) {
			this.methodSend(map, output);
		}

		if (action.equalsIgnoreCase("list")) {
			this.methodList(map, output);
		}
		output.close();
	}

	/**
	 * Create new new conversation
	 * 
	 * @throws NotAuthorizedException
	 * @throws IOException
	 */
	private void methodSend(Map<String, String> map, OutputStream output)
			throws NotAuthorizedException, IOException {
		// data
		String text = map.get("text");
		String gameid = map.get("game");
		String replyto = map.get("parent");
		String filter = map.get("filter");

		if (gameid == null || text == null)
			return;

		//flush cache
		MyCache.remove(CACHE_KEY + gameid);
		
		// get account
		EntityManager em = DataManager.createEntityManager();
		Account ac = getAccount(map, em);

		Game game = em.find(Game.class, Long.parseLong(gameid));

		if (game == null)
			return;

		// fetch parent
		Conversation parent = null;
		if (replyto != null) {
			Long cid = Long.parseLong(replyto);
			parent = em.find(Conversation.class, cid);
		}

		// create conversation
		Conversation conv = new Conversation();
		conv.setKeyaccount(ac.getKey());
		if (parent != null)
			conv.setParent(parent.getKey());
		conv.setText(new Text(text));
		conv.setWriteDate(Calendar.getInstance().getTime());
		conv.setKeygame(game.getKey());
		conv.setAuthor(ac.getLogin());

		// filter
		if (filter != null) {
			conv.getPrivateDest().clear();
			for (String f : filter.split(",")) {
				conv.getPrivateDest().add(f);
			}
		}

		DataManager.persist(em, conv);

		// XML
		Element root = new Element("conversation");
		Document doc = new Document(root);

		Element elem = new Element("send");
		elem.setAttribute("id", String.valueOf(conv.getKey().getId()));
		root.addContent(elem);

		XMLOutputter outputter = new XMLOutputter();
		outputter.output(doc, output);

		em.close();
	}

	/**
	 * List conversation
	 * @param map
	 * @param output
	 * @throws NotAuthorizedException
	 * @throws IOException
	 */
	private void methodList(Map<String, String> map, OutputStream output)
			throws NotAuthorizedException, IOException {
		// data
		String gameid = map.get("game");
		//String lastconv = map.get("from");

		if (gameid == null)
			return;

		//test cache
		Document cached = (Document) MyCache.get(CACHE_KEY + gameid);
		if (cached != null){
			this.flushDoc(cached, output);
			return;
		}
		
		
		// get account
		EntityManager em = DataManager.createEntityManager();
		Account ac = getAccount(map, em);

		Game game = em.find(Game.class, Long.parseLong(gameid));

		if (game == null)
			return;

		// conversation list
		List<Conversation> list = em
				.createQuery(
						"select c from Conversation c "
								+ " where c.keygame = :game order by c.writeDate")
				.setParameter("game", game.getKey())
				.getResultList();

		// XML
		Document doc = new Document();
		Element root = new Element("conversation");
		root.setAttribute("game", String.valueOf(game.getKey().getId()));
		doc.addContent(root);

		for (Conversation c : list) {
			
			if (c.getAuthor() == null)
				continue;

			Element conv = new Element("conv");
			conv.setAttribute("id", String.valueOf(c.getKey().getId()));
			conv.setAttribute("date", Tools.format(c.getWriteDate()));

			if (c.getParent() != null) {
				conv.setAttribute("parent", String.valueOf(c.getParent()
						.getId()));
			}

			Element author = new Element("author");
			author.setAttribute("name", c.getAuthor());
			author
					.setAttribute("id", String.valueOf(c.getKeyaccount()
							.getId()));
			conv.addContent(author);

			// exclude list ?
			if (c.getPrivateDest().size() > 0) {
				Element filter = new Element("restricted");
				conv.addContent(filter);
				for (String f : c.getPrivateDest()) {
					Element user = new Element("user");
					user.setAttribute("id", f);
					filter.addContent(user);
				}
			}
			// BODY
			Element body = new Element("body");
			body.addContent(new CDATA(c.getText().getValue()));
			conv.addContent(body);
			
			root.addContent(conv);
		}

		em.close();


		flushDoc(doc, output);
		MyCache.put(CACHE_KEY + gameid, doc); //keep

	}
	
	
	private void flushDoc(Document doc , OutputStream output) throws IOException{
		XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
		outputter.output(doc, output);
		output.close();
	}
}

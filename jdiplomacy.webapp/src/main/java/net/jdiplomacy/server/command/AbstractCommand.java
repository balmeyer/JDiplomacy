/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.command;

import java.io.IOException;
import java.io.OutputStream;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Text;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author Jean-Baptiste
 */
public abstract class AbstractCommand implements Command{

    protected void flushError(OutputStream os, int id , String message) throws IOException {
        Document doc = new Document();

        Element root = new Element("reply");

        Element error = new Element("error");

        error.setAttribute("id", String.valueOf(id));
        error.addContent(new Text(message));

        root.addContent(error);

        doc.addContent(root);

        XMLOutputter outputter = new XMLOutputter();

        outputter.output(doc, os);

        os.close();
    
    }

}

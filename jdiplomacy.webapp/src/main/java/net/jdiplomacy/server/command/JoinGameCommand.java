/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.server.command;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import net.jdiplomacy.language.TextMail;
import net.jdiplomacy.language.TextType;
import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.Country;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.Game;
import net.jdiplomacy.server.entities.GameCountry;
import net.jdiplomacy.server.entities.Inscription;
import net.jdiplomacy.server.entities.MyCache;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.google.appengine.api.datastore.Key;

/**
 *
 * @author Jean-Baptiste Vovau
 */
@CommandType(page = "joingame")
public class JoinGameCommand extends AuthorizedCommand {

    @Override
	public void perform(Map<String, String> map, OutputStream output) throws IOException, NotAuthorizedException {
        EntityManager em = DataManager.createEntityManager();

        Account ac = getAccount(map, em);

        long gameid = Long.parseLong(map.get("idgame"));


        Game game = em.find(Game.class, gameid);

        doInscription(game.getKey(), ac.getKey());

        //XML
        Document doc = new Document();
        Element root = new Element("joingame");
        Element ok = new Element("ok");
        ok.setAttribute("gameid", String.valueOf(game.getKey().getId()));
        ok.setAttribute("place", String.valueOf(game.getFreePlace()));

        root.addContent(ok);
        doc.addContent(root);

        em.close();

        XMLOutputter outputter = new XMLOutputter();
        outputter.output(doc, output);
        output.close();


    }

    public static void doInscription(Key keyGame , Key keyaccount) {

        Date now = Calendar.getInstance().getTime();
        EntityManager em = DataManager.createEntityManager();
        
        //refresh
        Game game = em.find(Game.class,keyGame);

        List<Inscription> testInscription = em.createQuery("select i from Inscription i "
                + " where i.keyaccount = :keyaccount and i.keygame = :keygame")
                .setParameter("keyaccount", keyaccount)
                .setParameter("keygame", game.getKey()).getResultList();

        if (testInscription.size() > 0) {
            //em.close();
        	System.out.println("already suscribed !");
            return ;
        }



        Inscription inscr = new Inscription();
        inscr.setKeyAccount(keyaccount);
        inscr.setGame(game);
        inscr.setCreationDate(now);
        inscr.setHasplayed(false);
        //game.getInscriptions(em).add(inscr);

        em.persist(inscr);
        DataManager.commit(em);
        
        em.close();
        
        em = DataManager.createEntityManager();
        game = em.find(Game.class, keyGame);

        int count = em.createQuery("select i from Inscription i where "
                + " i.keygame = :keygame").setParameter("keygame", game.getKey()).getResultList().size();

        game.setFreePlace(game.getCountPlayer() - count);

        if (game.getFreePlace() == 0) {
            //start game !
            game.setStarted(true);
            game.setStartDate(now);
            DataManager.commit(em);
            //TODO start game
            List<Inscription> listInscr = em.createQuery("select i from Inscription i"
                    + " where i.keygame = :keygame").setParameter("keygame", game.getKey()).getResultList();
            CountryDispatcher cd = new CountryDispatcher(game.getCountPlayer());

            //Mail to send
            List<Account> accountToMail = new ArrayList<Account>();
            
            for (Inscription ins : listInscr) {
            	Account acc = ins.getAccount(em);
                cd.moveToNextPlayer();
                for (int country : cd.getCountriesForPlayer()) {
                    GameCountry gc = new GameCountry();
                
                    gc.setAccount(acc);
                    gc.setCountry(Country.getCountry(em, country));
                    gc.setGame(game);
                    gc.setScore(0);
                    gc.setLastPlayDate(now);
                    em.persist(gc);
                    DataManager.commit(em);
                    //game.getGameCountries(em).add(gc);

                }
                //
                if (!accountToMail.contains(acc)){
                	accountToMail.add(acc);
                }
            }
            
            //send mail
            for(Account acc : accountToMail){
            	TextMail tm = new TextMail(acc, TextType.mailNewGame);
            	tm.addData("gameid", game.getKey().getId());
            	tm.addData("gametitle",game.getTitle());
            	tm.addData(game);
            	Tools.sendMail(tm);
            	//remove "my games" in cache
            	MyCache.remove(ListGamesCommand.CACHE_LIST_MY_GAMES + acc.getKey().toString());
            }
            
            
            //empty open list cache
            MyCache.remove(ListGamesCommand.CACHE_LIST_OPEN_GAMES);
            MyCache.remove(ListGamesCommand.CACHE_ALL_GAMES);
        }
        
        
        //test reload

        DataManager.commit(em);
        em.close();

    }

    /**
     * DIspatch countries for players when game starts
     */
    private static class CountryDispatcher {

        private List<List<Integer>> countryToPlayer;
        private int index;

        public CountryDispatcher(int nbplayer) {

            this.countryToPlayer = new ArrayList<List<Integer>>(7);
            this.index = -1;

            switch (nbplayer) {
                case 7:
                    this.addPlayerAndCountry(0);
                    this.addPlayerAndCountry(1);
                    this.addPlayerAndCountry(2);
                    this.addPlayerAndCountry(3);
                    this.addPlayerAndCountry(4);
                    this.addPlayerAndCountry(5);
                    this.addPlayerAndCountry(6);
                    break;
                case 6:
                    this.addPlayerAndCountry(0);
                    this.addPlayerAndCountry(1);
                    this.addPlayerAndCountry(2);
                    this.addPlayerAndCountry(3);
                    this.addPlayerAndCountry(5);
                    this.addPlayerAndCountry(6);
                    break;
                case 5:
                    this.addPlayerAndCountry(0);
                    this.addPlayerAndCountry(1);
                    this.addPlayerAndCountry(2);
                    this.addPlayerAndCountry(5);
                    this.addPlayerAndCountry(6);
                    break;
                case 4:
                    //PLAYER 1 : england
                    addPlayerAndCountry(1);
                    //player 2 : AUSTRIA - FRANCE
                    addPlayer();
                    addCountry(0);
                    addCountry(2);
                    //player 3 : GERMANY TURKEY
                    addPlayer();
                    addCountry(3);
                    addCountry(6);
                    //PLAYER 4 : ITALY / RUSSIA
                    addPlayer();
                    addCountry(4);
                    addCountry(5);
                    break;
                case 3:
                    //PLAYER 1 : england - GERMANY - austria
                    addPlayer();
                    addCountry(0);
                    addCountry(1);
                    addCountry(3);

                    //player 2 : RUSSIA - ITALY
                    addPlayer();
                    addCountry(4);
                    addCountry(5);

                    //player 3 : FRANCE  TURKEY
                    addPlayer();
                    addCountry(2);
                    addCountry(6);
                    break;
                case 2:
                    //ENGLAND FRANCE RUSSIA
                    addPlayer();
                    addCountry(1);
                    addCountry(2);
                    addCountry(5);
                    //AUSTRIA GERMANY TURKEY
                    addPlayer();
                    addCountry(0);
                    addCountry(3);
                    addCountry(6);
                    break;
                case 1:
                    this.addPlayer();
                    this.addCountry(0);
                    this.addCountry(1);
                    this.addCountry(2);
                    this.addCountry(3);
                    this.addCountry(4);
                    this.addCountry(5);
                    this.addCountry(6);
            }

        }

        public boolean moveToNextPlayer() {
            if (++this.index < this.countryToPlayer.size()) {
                return true;
            }
            return false;
        }

        public List<Integer> getCountriesForPlayer() {
            return this.countryToPlayer.get(this.index);
        }

        /**
         * Add a player slot with the country
         * @param country
         */
        private void addPlayerAndCountry(int country) {
            this.addPlayer();
            this.addCountry(country);
        }

        /**
         * Add a new player slot
         */
        private void addPlayer() {
            List<Integer> countries = new ArrayList<Integer>(4);
            this.countryToPlayer.add(countries);
        }

        /**
         * Add a new country to the current player slot
         */
        private void addCountry(int country) {
            List<Integer> countries = this.countryToPlayer.get(
                    this.countryToPlayer.size() - 1);

            countries.add(country);
        }
    }
}

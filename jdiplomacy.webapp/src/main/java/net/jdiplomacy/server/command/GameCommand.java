/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.server.command;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;


import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.Game;
import net.jdiplomacy.server.entities.GameCountry;
import net.jdiplomacy.server.entities.Tour;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author Jean-Baptiste Vovau
 */
@CommandType(page = "game")
public class GameCommand extends AuthorizedCommand {

    @Override
	public void perform(Map<String, String> map, OutputStream output) throws IOException, NotAuthorizedException {
        EntityManager em = DataManager.createEntityManager();

        Account myAccount = getAccount(map,em);
        Long idgame = Long.parseLong(map.get("idgame"));

        Game game = em.find(Game.class, idgame);


        Document doc = new Document();

        //root element
        Element root = new Element("game");
        root.setAttribute("id", String.valueOf(idgame));
        doc.addContent(root);

        //users
        Element users = new Element("users");
        root.addContent(users);

        for (GameCountry gc : game.getGameCountries(em)) {
        	Account a = gc.getAccount(em);
            Element country = new Element("country");
            users.addContent(country);
            country.setAttribute("index", String.valueOf(gc.getCountryIndex()));
            country.setAttribute("name", gc.getCountry(em).getName());
            country.setAttribute("userid", String.valueOf(a.getKey().getId()));
            country.setAttribute("username", a.getLogin());

        }

        //tours
        Element tours = new Element("tours");
        root.addContent(tours);

        List<Tour> list = em.createQuery("select t from Tour t where t.keygame = :keygame order by t.startDate")
        	.setParameter("keygame", game.getKey()).getResultList();

        Tour lastTour = null;

        for (Tour t : list) {
            Element tour = new Element("tour");
            tours.addContent(tour);
            tour.setAttribute("id", String.valueOf(t.getKey().getId()));
            tour.setAttribute("idphase", String.valueOf(t.getPhaseID()));
            tour.setAttribute("finished", t.isFinished() ? "1" : "0");
            tour.setAttribute("discovered", "1");
            //start position
            Element position = new Element("position");
            position.addContent(t.getStartPosition().getValue());
            tour.addContent(position);
            //orders
            Element orders = new Element("orders");
            if (t.getOrders() != null){
            orders.addContent(t.getOrders().getValue());
            }
            tour.addContent(orders);

            lastTour = t;
        }

        //pending orders

        Element pending = OrdersCommand.getPendingOrdersElement(em , lastTour , game);
        root.addContent(pending);

        //winlist
        if (game.getWinlist() != null && game.getWinlist().size() > 0){
        	int pos = 1;
        	Element winlist = new Element("winlist");
        	for(int i : game.getWinlist()){
        		//Country c = Country.get(i);
        		Element country = new Element("winner");
        		country.setAttribute("index", String.valueOf(i));
        		country.setAttribute("position", String.valueOf(pos++));
        		//country.setAttribute("name",c.getGameName());
        		winlist.addContent(country);
        	}
        	
        	root.addContent(winlist);
        }
        
        //save last acceded game

        myAccount.setLastgame(game);
        DataManager.commit(em);

        em.close();

        //flush result
        XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
        outputter.output(doc, output);
        output.close();
    }
}

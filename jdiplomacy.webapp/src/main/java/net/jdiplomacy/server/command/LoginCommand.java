/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.command;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;

import net.jdiplomacy.language.TextMail;
import net.jdiplomacy.language.TextType;
import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.MyCache;
import net.jdiplomacy.server.entities.Token;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Text;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;


/**
 *
 * @author Jean-Baptiste
 */

@CommandType(page="login")
public class LoginCommand extends AbstractCommand {

    @Override
	public void perform(Map<String, String> map , OutputStream output) throws IOException {

        //account found
        Account ac = null;

        String login = map.get("login");
        String password = map.get("password");
        String language = map.get("lang");
        String action = map.get("action");

        int errornumber = 0;
        
        //password ?
        if (action != null && action.equals("askpassword")){
        	this.askPassword(login, output);
        	return;
        }
        
        
        EntityManager em = DataManager.createEntityManager();

        ac = (Account) MyCache.get("login-" + login);
        
        if (ac == null){

	        //look the login
	        List<Account> list = em.createQuery("select a from Account a where a.login = :login")
	                .setParameter("login", login)
	                .getResultList();
	
	        if (list.size() != 1){
	            errornumber = 1;
	        } else {
	            ac = list.get(0);
	            
	            MyCache.put("login-" + login, ac);
	

	        }

        }
        
        if (ac != null){
	        if (!ac.getPassword().equals(password)){
	            errornumber = 2;
	        } else{
	        	//confirm ?
	        	if (!ac.isConfirmed()){
	        		errornumber = 3;
	        	}
	        }
        }
        
        //Token
        Token token = null;
        if (errornumber == 0){
        	        	
            UUID uuid = UUID.randomUUID();
            token = new Token();
            token.setIdAccount(ac.getKey().getId());
            token.setToken(uuid.toString());
            token.setDateCreation(Calendar.getInstance().getTime());
            DataManager.persist(em, token);
            

            //save language
            //System.out.println("langue : " + language + ", current : " + ac.getLanguage());
            if (language != null && !language.equals(ac.getLanguage())){
            	//TODO remove console message
            	System.out.println("langue change " + language);
            	Account acToChange = em.find(Account.class, ac.getKey());
            	acToChange.setLanguage(language);
            	DataManager.commit(em);
            }
            
        }

        

        //REPLY
        Document doc = new Document();
        Element elem = new Element("login");
        Element reply = null;

        String strLastGame = "0";
        if (ac != null && ac.getLastgame(em) != null){
            strLastGame = String.valueOf(ac.getLastgame(em).getKey().getId());
        }

        if (errornumber > 0){
            reply = new Element("error");
            reply.addContent(new Text(String.valueOf(errornumber)));
        } else {
            reply = new Element("user");
            reply.setAttribute("id",String.valueOf(ac.getKey().getId()));
            reply.setAttribute("name", ac.getLogin());
            reply.setAttribute("lastgame",strLastGame);
            reply.setAttribute("token",token.getToken()); 
        }


        doc.addContent(elem);
        elem.addContent(reply);

        XMLOutputter outputter = new XMLOutputter();
        outputter.output(doc,output);
        output.close();
        
        em.close();
    }

    /**
     * Send password
     * @param login
     * @param output
     * @throws IOException 
     */
    private void askPassword(String login, OutputStream output) throws IOException{
    	EntityManager em = DataManager.createEntityManager();
    	
    	int error = 0;
    	String msg = "";
    	
    	List<Account> l = em.createQuery("select a from Account a where a.login=:login")
    		.setParameter("login", login)
    		.setMaxResults(1)
    		.getResultList();
    	
    	if (l.size() > 0){
    		String email = l.get(0).getEmail();
    		String password = l.get(0).getPassword();
    		
    		TextMail tm = new TextMail(l.get(0),TextType.mailRecallPassword);
    		tm.addData("password", password);
    		Tools.sendMail(tm);
    		error = 0;
    		msg = "ok";
    	} else {
    		error = 1 ;
    		msg = "login not found";
    	}
    	
    	System.out.println(msg);
    	Document doc = new Document();
    	Element elem = new Element("password");
    	
    	doc.addContent(elem);
    	Element reply = new Element("msg");
    	reply.setAttribute("id",String.valueOf(error));
    	reply.addContent(msg);
    	elem.addContent(reply);
    	
    	XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
    	outputter.output(doc, output);
    	output.close();
    	
    	
    	em.close();
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.server.command;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;

import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.DataManager;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Text;
import org.jdom.output.XMLOutputter;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

/**
 *
 * @author Jean-Baptiste
 */
@CommandType(page = "createaccount")
public class CreateAccountCommand implements Command {

    @Override
	public void perform(Map<String, String> map, OutputStream output) throws IOException {


        String login = map.get("login");
        String password = map.get("password");
        String email = map.get("email");
        String language = map.get("lang");

        //test if all data are provided
        if (login.length() == 0 || password.length() == 0 || email.length() == 0) {

            this.flushError(output, -1, "champ manquant");
            return;
        }

        //test email unicity

        EntityManager em = DataManager.createEntityManager();

        int count = em.createQuery("select a from Account a where a.email = :email")
                .setParameter("email", email.toLowerCase())
                .getResultList().size();
        if (count > 0) {
            this.flushError(output, 1, "email en double");
            return;
        }

        //test login unicity
        count = em.createQuery("select a from Account a where a.login = :login")
                .setParameter("login", login)
                .getResultList().size();
        if (count > 0) { 
            this.flushError(output, 2, "login en double");
            return;
        }


        //insert account
        Account newaccount = new Account();
        newaccount.setDateCrea(Calendar.getInstance().getTime());
        newaccount.setEmail(email.toLowerCase());
        newaccount.setLogin(login);
        newaccount.setPassword(password);
        newaccount.setLanguage(language);
        newaccount.setConfig("newsletter", "yes");
        newaccount.setConfig("maileachtour","yes");
        //token to validate by email
        newaccount.setChecktoken(UUID.randomUUID().toString());
        newaccount.setConfirmed(false);

        DataManager.persist(em, newaccount);

        em.close();

        //send mail
        
        Queue queue = QueueFactory.getDefaultQueue();

        queue.add(withUrl("/tasks/askconfirm")
        		.method(Method.GET).param("id", String.valueOf(
        				newaccount.getKey().getId())));
        				

        
        //reply
        Document doc = new Document();
        Element elem = new Element("account");
        Element ok = new Element("id");
        ok.setAttribute("id", String.valueOf(newaccount.getKey().getId()));

        doc.addContent(elem);
        elem.addContent(ok);

        XMLOutputter outputter = new XMLOutputter();
        outputter.output(doc, output);

    }

    private void flushError(OutputStream os, int id, String message) throws IOException {
        Document doc = new Document();

        Element root = new Element("reply");

        Element error = new Element("error");

        error.setAttribute("id", String.valueOf(id));
        error.addContent(new Text(message));

        root.addContent(error);

        doc.addContent(root);

        XMLOutputter outputter = new XMLOutputter();

        outputter.output(doc, os);

        os.close();

    }
}

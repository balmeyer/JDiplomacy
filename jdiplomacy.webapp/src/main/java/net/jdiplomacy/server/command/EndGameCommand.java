/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.command;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;

import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.Country;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.Game;
import net.jdiplomacy.server.entities.GameCountry;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author Jean-Baptiste Vovau
 */
@CommandType(page="endgame")
public class EndGameCommand extends AuthorizedCommand {

    @Override
	public void perform(Map<String, String> map, OutputStream output) throws IOException, NotAuthorizedException {

        Date now = Calendar.getInstance().getTime();

        EntityManager em = DataManager.createEntityManager();
        Account ac = this.getAccount(map, em);

        Long idgame = Long.parseLong(map.get("idgame"));
        String [] results = map.get("result").split(";");

        List<Result> list = new ArrayList<Result>(7);
        for(String s : results){
            Result r = new Result(s , em);
            list.add(r);
        }

        //when one player has many countries
        Map<Account,Integer> playerToScore = new HashMap<Account,Integer>(7);
        Map<Country,Integer> countryToScore = new HashMap<Country,Integer>(7);
        
        //count best score result
        for(Result r : list){
            int myscore = 0;
            int actual;
            
            //test same player
            if (playerToScore.containsKey(r.player)){
                actual = playerToScore.get(r.player);
                myscore = Math.max(actual, r.supply);
            }

            playerToScore.put(r.player, myscore);
            countryToScore.put(r.country,r.supply);

        }

        //apply score
        Game game = em.find(Game.class, idgame);
        int nbpayer = game.getCountPlayer();

        for(GameCountry gc : game.getGameCountries(em)){
            int score = countryToScore.get(gc.getCountry(em));
            gc.setScore(score);
        }


        game.setEndDate(now);

        game.setFinished(true);

        DataManager.commit(em);

        em.close();


        //send XML reply
        Element elem = new Element("ok");
        Document doc = new Document(elem);
        Element ended = new Element("gameEnded");
        elem.addContent(ended);
        ended.setAttribute("id", String.valueOf(game.getKey().getId()));

        XMLOutputter outputter = new XMLOutputter();
        outputter.output(doc, output);
        output.close();
    }



    private class Result{

        public Account player;

        public Country country;

        public int supply;

        public Result(String result , EntityManager em){
            String [] tokens = result.split(":");

            this.player = em.find(Account.class,Long.parseLong(tokens[0]));
            this.country = Country.getCountry(em, Integer.parseInt(tokens[1]));
            this.supply = Integer.parseInt(tokens[2]);
        }
        
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.command;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 *
 * @author Jean-Baptiste
 */
public interface Command {

    public void perform(Map<String,String> map, OutputStream output) 
            throws IOException , NotAuthorizedException;


}

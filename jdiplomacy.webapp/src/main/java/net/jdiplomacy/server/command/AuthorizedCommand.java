/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.server.command;

import java.util.Map;

import javax.persistence.EntityManager;

import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.MyCache;
import net.jdiplomacy.server.entities.Token;


/**
 *
 * @author Jean-Baptiste Vovau
 */
public abstract class AuthorizedCommand implements Command {


    protected Account getAccount(Map<String, String> map, EntityManager em) throws NotAuthorizedException {

        String token = map.get("token");

        if (token == null) {
            throw new NotAuthorizedException();
        }
        
        Token t = null;
        
		t = (Token) MyCache.get(token);
		
     
		if (t == null){
			t = em.find(Token.class, token);
			if (t != null) MyCache.put(token, t);
		}

        if (t == null) {
            throw new NotAuthorizedException();
        }

        Account ac = em.find(Account.class, t.getIdAccount());

        return ac;

    }
}

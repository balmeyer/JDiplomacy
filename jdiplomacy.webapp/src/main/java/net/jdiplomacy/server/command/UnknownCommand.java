/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.command;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Text;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public class UnknownCommand implements Command {

    @Override
	public void perform(Map<String, String> map, OutputStream output) throws IOException {
        Element elem = new Element("reply");

        Element error = new Element("error");
        error.addContent(new Text("Unknown command"));

        elem.addContent(error);
        Document doc = new Document(elem);

        XMLOutputter outputter = new XMLOutputter();

        outputter.output(doc, output);
        output.close();
    }

}

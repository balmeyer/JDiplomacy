package net.jdiplomacy.server.command;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.MyCache;


@CommandType(page="confirm")
public class ConfirmCommand  implements Command{

	@Override
	public void perform(Map<String, String> map, OutputStream output)
			throws IOException, NotAuthorizedException {
		
		String token = map.get("id");
		
		if (token == null) return;
		
		EntityManager em = DataManager.createEntityManager();
		
		List<Account> acs = em.createQuery("select a from Account a" 
				+ " where a.checktoken = :token and a.confirmed = false")
				.setParameter("token", token)
				.getResultList();
		
		if (acs.size() > 0){
			
			Account ac = acs.get(0);
			ac.setConfirmed(true);
			DataManager.commit(em);
			
			BufferedWriter writer = new BufferedWriter(
					new OutputStreamWriter(output));
			
			writer.write("THANK YOU ! Account confimed.");
			writer.close();
			output.close();
			
			MyCache.remove("login-" + ac.getLogin());
			
			Tools.sendMail("[JDiplomacy] nouvel inscrit !", 
					ac.getEmail() + " > " + ac.getLogin(), "vovau@jdiplomacy.net");
		}
		em.close();
	}

}

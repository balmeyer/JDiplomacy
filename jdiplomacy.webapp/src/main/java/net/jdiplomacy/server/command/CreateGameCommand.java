/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.server.command;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.persistence.EntityManager;


import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.Game;
import net.jdiplomacy.server.entities.MyCache;
import net.jdiplomacy.server.entities.Tour;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.google.appengine.api.datastore.Text;

/**
 *
 * @author Jean-Baptiste Vovau
 */
@CommandType(page = "creategame")
public class CreateGameCommand extends AuthorizedCommand {

    private static final String FIRST_POSITION = "0:A:Vie;0:A:Bud;0:F:Tri;1:A:Lvp;1:F:Lon;1:F:Edi;"
            + "2:A:Par;2:A:Mar;2:F:Bre;3:A:Mun;3:A:Ber;3:F:Kie;4:A:Rom;4:A:Ven;"
            + "4:F:Nap;5:A:Mos;5:A:War;5:F:Stp-S;5:F:Sev;6:A:Con;6:A:Smy;6:F:Ank;"
            + "0:S:Bud;0:S:Tri;0:S:Vie;1:S:Edi;1:S:Lvp;1:S:Lon;2:S:Bre;2:S:Mar;"
            + "2:S:Par;3:S:Ber;3:S:Mun;3:S:Kie;4:S:Nap;4:S:Rom;4:S:Ven;5:S:Mos;"
            + "5:S:Sev;5:S:StP;5:S:War;6:S:Ank;6:S:Con;6:S:Smy;";

    @Override
	public void perform(Map<String, String> map, OutputStream output) throws IOException, NotAuthorizedException {

        EntityManager em = DataManager.createEntityManager();

        Account ac = getAccount(map , em);

        int minute = Integer.parseInt(map.get("minutes"));
        int nbplayer = Integer.parseInt(map.get("player"));
        String title = map.get("title");

        Date now = Calendar.getInstance().getTime();

        Game game = new Game();
        game.setCountPlayer(nbplayer);
        game.setFreePlace(nbplayer);
        game.setCreationDate(now);
        game.setFinished(false);
        game.setMinutePerTour(minute);
        game.setOwner(ac);
        game.setTitle(title);

        em.persist(game);
        DataManager.commit(em);

        
        Tour tour = new Tour();
        tour.setKeygame(game.getKey());
        tour.setPhaseID(1);
        tour.setStartPosition(new Text(CreateGameCommand.FIRST_POSITION));
        //tour.setOrders("");
        tour.setFinished(false);
        tour.setStartDate(now);
       
        em.persist(tour);
        
        DataManager.commit(em);
        
        em.close();
        em = null;
        
        
        JoinGameCommand.doInscription(game.getKey(), ac.getKey());
        
        //empty open list cache
        MyCache.remove(ListGamesCommand.CACHE_LIST_OPEN_GAMES);
        MyCache.remove(ListGamesCommand.CACHE_ALL_GAMES);       
       
   
        //XML
        Document doc = new Document();
        XMLOutputter outputter = new XMLOutputter();

        Element elem = new Element("creagame");
        Element ok = new Element("ok");

        ok.setAttribute("gameid", String.valueOf(game.getKey().getId()));
        ok.setAttribute("freeplace",String.valueOf(game.getFreePlace()));
        ok.setAttribute("tourid",String.valueOf(tour.getKey()));
        
        doc.addContent(elem);
        elem.addContent(ok);
        outputter.output(doc,output);
        output.close();


    }
}

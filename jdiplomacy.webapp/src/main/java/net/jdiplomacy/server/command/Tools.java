/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.command;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;

import net.jdiplomacy.language.TextMail;
import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.Inscription;
import net.jdiplomacy.server.entities.PendingOrder;
import net.jdiplomacy.server.entities.Tour;

import com.google.appengine.api.datastore.Key;

/**
 *
 * @author Jean-Baptiste
 */
public class Tools {

    private static final DateFormat convertDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private Tools(){}

    public static String format(Date date){
        return convertDateFormat.format(date);
    }

   
    
    public static boolean sendMail(TextMail tm){
    	return sendMail(
    			tm.getFragment(TextMail.FRAGMENT_SUBJECT),
    			tm.getFragment(TextMail.FRAGMENT_BODY),
    			tm.getFragment(TextMail.FRAGMENT_RECIPIENT)
    			);
    	
    }
    
    /**
     * 
     * @param subject
     * @param body
     * @param to
     * @return
     */
    public static boolean sendMail(String subject, String body, String to){
    	MailMessage msg = new Tools.MailMessage();
    	msg.body = body;
    	msg.dest = to;
    	msg.from = "noreply@jdiplomacy.net";
    	msg.subject = subject;
    	
    	return sendMail(msg);
    }
    
    public static boolean sendMail(MailMessage msg){
    	 Properties props = new Properties();
         Session session = Session.getDefaultInstance(props, null);
         try{
         Message m = new MimeMessage(session);
         m.setFrom(new InternetAddress(msg.from));
         m.addRecipient(RecipientType.TO, new InternetAddress(msg.dest));
         
         m.setSubject(msg.subject);
         m.setHeader("Content-Type", "text/plain; charset=utf-8");
         m.setText(msg.body);
         
         /*
         MimeBodyPart plainPart = new MimeBodyPart();
         plainPart.setContent(msg.body, "text");
         Multipart alt = new MimeMultipart("alternative");
         alt.addBodyPart(plainPart);
         m.setContent(alt); */

         Transport.send(m);
         
         
         } catch (AddressException aex){
        	 aex.printStackTrace();
        	 return false;
         } catch (MessagingException msex){
        	 msex.printStackTrace();
        	 return false;
         }
         return true;
    }
    
    public static class MailMessage{
    	public String subject ;
    	public String body;
    	public String dest;
    	public String from = "noreply@jdiplomacy.net";
    }
    
    /**
     * Not used
     * @param userkey
     */
    @Deprecated
    public static void setLimitedStatusTour(Key userkey){
    	EntityManager em = DataManager.createEntityManager();
    	Account user = em.find(Account.class, userkey);
    	
    	//get inscription list
    	List<Inscription> list = em.createQuery("select i from Inscription i "
    			+ " where i.keyaccount = :account")
    			.setParameter("account", user.getKey())
    			.getResultList();
    	
    	for(Inscription ins : list){
    		//for each inscription : fetch tour
    		Key keygame = ins.getKeyGame();
    		
    		List<Tour> listLastTour = em.createQuery("select t from Tour t"
    				+ " where t.keygame = :key and t.finished = false")
    				.setParameter("key", keygame).getResultList();
    		
    		//last tour found
    		Tour last = null;
    		if (listLastTour.size() >0) last = listLastTour.get(0);
    		
    		if(last != null){
    			//fetch pending orders
    			List<PendingOrder> listPO = em.createQuery("select po from PendingOrder po "
    					+ " where po.keytour = :tour and po.keyaccount = :user")
    					.setParameter("tour", last.getKey())
    					.setParameter("user", user.getKey())
    					.getResultList();
    			
    			ins.setHasplayed(listPO.size() > 0);
    			DataManager.commit(em);
    		}
    	}
    	
    	em.close();
    }
    
}

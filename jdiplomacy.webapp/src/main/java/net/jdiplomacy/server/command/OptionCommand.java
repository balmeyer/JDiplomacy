package net.jdiplomacy.server.command;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import javax.persistence.EntityManager;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.DataManager;

@CommandType(page="options")
public class OptionCommand extends AuthorizedCommand {

	@Override
	public void perform(Map<String, String> map, OutputStream output)
			throws IOException, NotAuthorizedException {
		
		
		String action = map.get("action");
		if (action == null) action ="list";
		
		EntityManager em = DataManager.createEntityManager();
		
		Account ac = this.getAccount(map, em);
		
		//Account ac = em.find(Account.class, test.getKey());
		//warm up (detached error else)
		ac.getConfig("test");
		
		
		for(String key : map.keySet()){
			if (key.startsWith("key:")){
				String value = map.get(key);
				String name = key.substring(key.indexOf(":") + 1);
				
				//save config
				ac.setConfig(name, value);
			}
		}
		
		DataManager.commit(em);
		
		//list options
        Element root = new Element("options");
        Document doc = new Document(root);
        
        Map<String,String> options = ac.getOptions();
        
        for(String key : options.keySet()){
        	Element elm = new Element("option");
        	elm.setAttribute("key",key);
        	elm.setAttribute("value",options.get(key));
        	root.addContent(elm);
        }
        

        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(Format.getPrettyFormat());
        outputter.output(doc, output);
        output.close();
        em.close();
	}

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.server.command;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.Game;
import net.jdiplomacy.server.entities.Inscription;
import net.jdiplomacy.server.entities.MyCache;

import org.jdom.CDATA;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author Jean-Baptiste Vovau
 */
@CommandType(page = "listgames")
public class ListGamesCommand extends AuthorizedCommand {

	public static final String CACHE_LIST_OPEN_GAMES = "listopengames";
	public static final String CACHE_LIST_MY_GAMES = "mygames";
	public static final String CACHE_ALL_GAMES = "allgames";
	
    @Override
	public void perform(Map<String, String> map, OutputStream output) throws IOException,
            NotAuthorizedException {

                //LOAD RESULT
        EntityManager em = DataManager.createEntityManager();

        Account ac = getAccount(map,em);



        String sql = "select g from Game g";
        List<Game> list = null;

        boolean fetchInscription = map.get("my") != null;

        //cached "my games" ?
        if (map.get("my") != null){
        	Document cached = (Document) MyCache.get(CACHE_LIST_MY_GAMES + ac.getKey().toString());
        	if (cached != null){
        		flushDoc(cached, output);
        		return;
        	}
        }
        
        
        if (map.get("all") != null || map.get("my") != null) {

            boolean juststarted = false;
            sql = "select i from Inscription i where i.keyaccount = :keyaccount";
            if (map.get("my") != null) {
                juststarted = true;
            }
            List<Inscription> inscrs = em.createQuery(sql).setParameter("keyaccount", ac.getKey()).getResultList();

            list = new ArrayList<Game>(20);
            for (Inscription i : inscrs) {
            	//System.out.println("inscription : " + i.getId());
            	if (i.getGame(em) == null) continue;
                if (!juststarted || i.getGame(em).isStarted())
                list.add(i.getGame(em));
            }
        }

        //open game : test in cache
        if (map.get("open") != null) {
            sql = "select g from Game g where g.started = false";
            Document cached = (Document) MyCache.get(CACHE_LIST_OPEN_GAMES);
            if (cached != null) {
            	flushDoc(cached , output);
            	return;
            }
        }

        if (map.get("idgame") != null) {
            Long id = Long.parseLong(map.get("idgame"));
            Game g = em.find(Game.class, id);
            list = new ArrayList<Game>(1);
            list.add(g);
            fetchInscription = true;
        }

        boolean showUsers = map.get("users") != null;

        if (map.get("fastjoin") != null){
        	sql = "select g from Game g where g.started = false order by g.freePlace";
        }


        if (list == null) {
            list = em.createQuery(sql).getResultList();
        }


        //XML REPLY
        Element games = new Element("games");
        Document doc = new Document(games);

        for (Game game : list) {
        	
        	//check if started
        	if (game.isStarted()){
        		
        	}
        	
        	//get inscription list
        	boolean hasplayed = false;
        	if (fetchInscription){
        	List<Inscription> inscrList = em.createQuery("select i from Inscription i "
        			+ " where i.keyaccount = :account and i.keygame = :game")
        			.setParameter("account", ac.getKey())
        			.setParameter("game", game.getKey())
        			.getResultList();
        	
        		for(Inscription ins : inscrList){
        			hasplayed = ins.isHasplayed();
        			break;
        		}
        	}
        	
            Element e = new Element("game");

            String creadate = Tools.format(game.getCreationDate());
            String startdate = null;
            if (game.getStartDate() != null) {
                startdate = Tools.format(game.getStartDate());
            }
            String enddate = null;
            if (game.getEndDate() != null) {
                enddate = Tools.format(game.getEndDate());
            }

            e.addContent(new Element("id").addContent(String.valueOf(game.getKey().getId())));
            e.addContent(new Element("started").addContent(game.isStarted() ? "1" : "0"));
            e.addContent(new Element("finished").addContent(game.isFinished() ? "1" : "0"));
            e.addContent(new Element("hasplayed").addContent(hasplayed ? "1" : "0"));
            e.addContent(new Element("title").addContent(new CDATA(game.getTitle())));
            e.addContent(new Element("minute").addContent(String.valueOf(game.getMinutePerTour())));
            e.addContent(new Element("player").addContent(String.valueOf(game.getCountPlayer())));
            e.addContent(new Element("place").addContent(String.valueOf(game.getFreePlace())));
            e.addContent(new Element("creadate").addContent(creadate));
            e.addContent(new Element("startdate").addContent(startdate));
            e.addContent(new Element("enddate").addContent(enddate));
            e.addContent(new Element("owner").addContent(String.valueOf(game.getOwner(em).getKey().getId())));

            if (showUsers) {
                Element users = new Element("users");

                for (Inscription ins : game.getInscriptions(em)) {
                	Account a = ins.getAccount(em);
                    Element user = new Element("user");
                    user.setAttribute("login", a.getLogin());
                    user.setAttribute("id", String.valueOf(a.getKey().getId()));
                    users.addContent(user);
                }
                e.addContent(users);
            }


            games.addContent(e);
        }

        flushDoc(doc,output);
        em.close();
        
        //---------------------------------
        //keep in cache
        if (map.get("open") != null) {
        	MyCache.put(CACHE_LIST_OPEN_GAMES, doc);
        }
        
        //my games
        if (map.get("my") != null){
        	MyCache.put(CACHE_LIST_MY_GAMES + ac.getKey().toString() , doc);
        }
        
        //all games
        if (map.get("all") != null){
        	MyCache.remove(CACHE_ALL_GAMES);
        }

    }
    
    
    private static void flushDoc(Document doc, OutputStream output) throws IOException{
        //Output
        XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
        outputter.output(doc, output);
        output.close();
    }
}

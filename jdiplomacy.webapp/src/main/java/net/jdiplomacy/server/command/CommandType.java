/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.command;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)

/**
 *
 * @author Jean-Baptiste
 */
public @interface CommandType {

    public String page();


}

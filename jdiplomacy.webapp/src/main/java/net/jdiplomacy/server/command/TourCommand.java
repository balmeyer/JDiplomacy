/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.server.command;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;


import net.jdiplomacy.server.entities.Account;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.Game;
import net.jdiplomacy.server.entities.MyCache;
import net.jdiplomacy.server.entities.Tour;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author Jean-Baptiste
 */
@CommandType(page = "tour")
public class TourCommand extends AuthorizedCommand {

    @Override
	@SuppressWarnings("unchecked")
	public void perform(Map<String, String> map, OutputStream output) throws IOException, NotAuthorizedException {

        EntityManager em = DataManager.createEntityManager();

        Account ac = getAccount(map,em);

        Long idgame = Long.parseLong(map.get("game"));
        Long idtour = 0L ;
        
        if (map.get("fromtour") != null) {
        	idtour = Long.parseLong(map.get("fromtour"));
        }
        

        //if (idtour == 0) idtour = 1L;
        Game game = em.find(Game.class, idgame);

        Date now = Calendar.getInstance().getTime();
        Date min = null;

        //Game game = em.find(Game.class, idgame);
        Tour current = null;
        if (idtour > 0L){
        	current = em.find(Tour.class, idtour);
        }
        
        Calendar mindate = Calendar.getInstance();
        mindate.set(Calendar.YEAR, 1800);
        min = mindate.getTime();
        
        if (current != null){
        	min = current.getStartDate();
        }
        
        //test if xml doc is already in cache
        Document cached = (Document) MyCache.get("xmltour" + String.valueOf(idgame) 
        		+ "-" + String.valueOf(idtour));
        if (cached != null){
        	flushDoc(cached, output);
        	return;
        }

        List<Tour> listTours = em.createQuery("select t from Tour t where  "
        		+ " t.keygame = :keygame and t.startDate > :date order by t.startDate")
        		.setParameter("keygame", game.getKey())
        		.setParameter("date", min)
        		.getResultList();

        //result
        List<Tour> newtours = new ArrayList<Tour>();
        
        if (listTours.size() > 0){
        	//add the last tour for refreshing orders
        	if (current != null) newtours.add(current);
        	newtours.addAll(listTours);
        }

        //XML
        
        
        Element elem = new Element("tours");
        Document doc = new Document(elem);
        
        for (Tour t : newtours){
        	
        	//if (t.getId().getId() <= idtour) continue;
        	
        	Element tour = new Element("tour");
        	tour.setAttribute("id", String.valueOf(t.getKey().getId()));
        	tour.setAttribute("idphase", String.valueOf(t.getPhaseID()));
            tour.setAttribute("finished", t.isFinished() ? "1" : "0");
            tour.setAttribute("discovered", "1");
            
        	Element positions = new Element("position");
        	positions.addContent(t.getStartPosition().getValue());
        	tour.addContent(positions);
        	
        	Element orders = new Element("orders");
        	if (t.getOrders() != null){
        	orders.addContent(t.getOrders().getValue());
        	}
        	tour.addContent(orders);
        	
        	elem.addContent(tour);
        }
        
        flushDoc(doc,output);
        
        //keep in cache
        MyCache.put("xmltour" + String.valueOf(idgame) 
        		+ "-" + String.valueOf(idtour) , doc);
    }
    
    
    private static void flushDoc(Document doc, OutputStream output) throws IOException{
        XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
        outputter.output(doc, output);
        output.close();
    }
}

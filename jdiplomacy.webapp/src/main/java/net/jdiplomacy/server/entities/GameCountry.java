/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.google.appengine.api.datastore.Key;

/**
 *
 * @author Jean-Baptiste Vovau
 */
@Entity
public class GameCountry implements Serializable {
    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Key key;

    @Column
    private Key keygame;

    @Column
    private int indexcountry;

    @Column
    private Key keyaccount;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPlayDate;

    @Column
    private int score;

    public Key getId() {
        return key;
    }

    public void setId(Key id) {
        this.key = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GameCountry)) {
            return false;
        }
        GameCountry other = (GameCountry) object;
        if ((this.getId() == null && other.getId() != null) 
        		|| (this.getId() != null && !this.key.equals(other.key))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jdiplomacy.server.dal.GameCountry[id=" + getId() + "]";
    }

    /**
     * @return the game
     */
    public Game getGame(EntityManager em) {
        return em.find(Game.class, this.keygame);
    }

    /**
     * @param game the game to set
     */
    public void setGame(Game game) {
        this.keygame = game.getKey();
    }

    /**
     * @return the country
     */
    public Country getCountry(EntityManager em) {
        return (Country) em.createQuery("select c from Country c where c.index = :index")
        .setParameter("index", this.indexcountry).getSingleResult();
    }

    public int getCountryIndex(){
    	return this.indexcountry;
    }
    
    /**
     * @param country the country to set
     */
    public void setCountry(Country country) {
        this.indexcountry = country.getIndex();
    }

    /**
     * @return the account
     */
    public Account getAccount(EntityManager em) {
        return em.find(Account.class, this.keyaccount);
    }

    /**
     * @param account the account to set
     */
    public void setAccount(Account account) {
        this.keyaccount = account.getKey();
    }

    /**
     * @return the lastPlayDate
     */
    public Date getLastPlayDate() {
        return lastPlayDate;
    }

    /**
     * @param lastPlayDate the lastPlayDate to set
     */
    public void setLastPlayDate(Date lastPlayDate) {
        this.lastPlayDate = lastPlayDate;
    }

    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }

}

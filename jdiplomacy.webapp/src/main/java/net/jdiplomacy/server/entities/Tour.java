/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.jdiplomacy.judge.Phase;


import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

/**
 *
 * @author Jean-Baptiste Vovau
 */
@Entity
public class Tour implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7460294987909782975L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Key id;

    //@Column
    //private Key keygame;

    @Column(nullable=false)
    private int phase;

    @Column(nullable=false)
    private boolean finished;

    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastOrderDate;

    @Column
    private Text startPosition;

    @Column(updatable=true)
    private Text orders;

    @Column
    private Key keygame;

    
    public Key getKey() {
        return id;
    }

    public void setKey(Key id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getKey() != null ? getKey().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tour)) {
            return false;
        }
        Tour other = (Tour) object;
        if ((this.getKey() == null && other.getKey() != null) || (this.getKey() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jdiplomacy.server.dal.Tour[id=" + getKey() + "]";
    }

    /**
     * @return the game
     */
    /*
    public Game getGame(EntityManager em) {
        return em.find(Game.class, this.keygame);
    }*/

    /**
     * @param game the game to set
     */
    /*
    public void setGame(Game game) {
        this.keygame = game.getKey();
    }*/

    /**
     * @return the phase
     */
    public int getPhaseID() {
        return phase;
    }

    public Phase getPhase(){
    	int p = ((getPhaseID() - 1) % 5);
    	
    	Phase phase = null;
    	
    	switch(p){
    		case 0:
    			phase = Phase.movementSpring;
    		break;
    		case 1:
    			phase = Phase.disbandSpring;
    			break;
    		case 2:
    			phase = Phase.movementFall;
    			break;
    		case 3:
    			phase = Phase.disbandFall;
    			break;
    		case 4:
    			phase = Phase.adjust;
    			break;
    			default:
    				//for debug only
    				throw new IllegalArgumentException("unknwon phase number : " + p);    				

    		
    	}
    	
    	return phase;
    }
    
    /**
     * @param phase the phase to set
     */
    public void setPhaseID(int phase) {
        this.phase = phase;
    }

    /**
     * @return the finished
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * @param finished the finished to set
     */
    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the lastOrderDate
     */
    public Date getLastOrderDate() {
        return lastOrderDate;
    }

    /**
     * @param lastOrderDate the lastOrderDate to set
     */
    public void setLastOrderDate(Date lastOrderDate) {
        this.lastOrderDate = lastOrderDate;
    }

    /**
     * @return the startPosition
     */
    public Text getStartPosition() {
        return startPosition;
    }

    /**
     * @param startPosition the startPosition to set
     */
    public void setStartPosition(Text startPosition) {
        this.startPosition = startPosition;
    }

    /**
     * @return the orders
     */
    public Text getOrders() {
        return orders;
    }

    /**
     * @param orders the orders to set
     */
    public void setOrders(Text orders) {
        this.orders = orders;
    }

    /**
     * @return the pendingOrders
     */
    public Collection<PendingOrder> getPendingOrders(EntityManager em) {
        return em.createQuery("select po from PendingOrder po where po.keytour = :key")
        .setParameter("key", this.getKey())
        .getResultList();
    }

	public void setKeygame(Key keygame) {
		if (keygame == null) throw new IllegalArgumentException("keygame cannot be null");
		this.keygame = keygame;
	}

	public Key getKeygame() {
		return keygame;
	}



}

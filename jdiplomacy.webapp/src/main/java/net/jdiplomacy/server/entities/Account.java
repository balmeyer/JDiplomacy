/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.google.appengine.api.datastore.Key;

/**
 *
 * @author Jean-Baptiste
 */
@Entity
public class Account implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Key id;

    @Column(length=500,nullable=false)
    private String email;

    @Column(length=500,nullable=false)
    private String login;

    @Column(length=50)
    private String password;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCrea;

    @ManyToOne(optional=true)
    private Key keylastgame;

    @Column(length=3,updatable=true)
    private String language = "en";
    
    @Column
    private String checktoken;
    
    @Column(nullable=true)
    private boolean confirmed;
    
    @Column
    private List<String> configUser;
    
    public Key getKey() {
        return id;
    }

    public void setKey(Key id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Account)) {
            return false;
        }
        Account other = (Account) object;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return "[id=" + id + "]" + this.login;

    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the dateCrea
     */
    public Date getDateCrea() {
        return dateCrea;
    }

    /**
     * @param dateCrea the dateCrea to set
     */
    public void setDateCrea(Date dateCrea) {
        this.dateCrea = dateCrea;
    }

    /**
     * @return the lastgame
     */
    public Game getLastgame(EntityManager em) {
    	if (this.keylastgame == null) return null;
        return em.find(Game.class, this.keylastgame);
    }

    /**
     * @param lastgame the lastgame to set
     */
    public void setLastgame(Game lastgame) {
        this.keylastgame = lastgame.getKey();
    }

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLanguage() {
		return language;
	}

	public void setChecktoken(String checktoken) {
		this.checktoken = checktoken;
	}

	public String getChecktoken() {
		return checktoken;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public Object getConfig(String key){
		if (this.configUser == null){
			this.configUser = new ArrayList<String>();
		}
		String result = null;
		
		for(String t : configUser){
			if (t.toLowerCase().startsWith(key.toLowerCase() + "=")){
				result = t.substring(t.indexOf("=") + 1);
				break;
			}
		}
		
		return result;
	}
	
	public void setConfig(String key, String value){
		if (this.configUser == null){
			this.configUser = new ArrayList<String>();
		}
		
		String result = null;
		
		for(int i = 0 ; i < this.configUser.size() ; i++){
			if (this.configUser.get(i).toLowerCase()
					.startsWith(key.toLowerCase() + "=")){
				this.configUser.remove(i);
				break;
			}
		}
		
		this.configUser.add(key.toLowerCase() + "=" + value);
		
	}
	
	public Map<String,String> getOptions(){
		if (this.configUser == null){
			this.configUser = new ArrayList<String>();
		}
		
		Map<String,String> map = new HashMap<String,String>();
		for (String t : this.configUser){
			String key = t.substring(0 , t.indexOf("="));
			String val = t.substring(t.indexOf("=") + 1);
			map.put(key, val);
		}
		
		return map;
		
	}
	
}

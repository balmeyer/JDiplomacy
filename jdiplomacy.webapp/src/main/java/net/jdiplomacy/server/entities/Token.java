/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Jean-Baptiste Vovau
 */
@Entity
public class Token implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    private String token;

    @Column
    private long idAccount;


    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(nullable=false)
    private Date dateCreation ;


    @Override
    public int hashCode() {
        int hash = 0;
        hash = token.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Token)) {
            return false;
        }
        Token other = (Token) object;
        if ((this.token == null && other.token != null)
                || (this.token != null && !this.token.equals(other.token))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jdiplomacy.server.dal.Token[id=" + token + "]";
    }

    /**
     * @return the account
     */
    public long getIdAccount() {
        return this.idAccount;
    }

    /**
     * @param account the account to set
     */
    public void setIdAccount(long idaccount) {
        this.idAccount = idaccount;
        
    }

    /**
     * @return the dateCreation
     */
    public Date getDateCreation() {
        return dateCreation;
    }

    /**
     * @param dateCreation the dateCreation to set
     */
    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

}

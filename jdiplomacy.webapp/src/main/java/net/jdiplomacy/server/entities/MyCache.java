package net.jdiplomacy.server.entities;

import java.util.HashMap;
import java.util.Map;

import javax.cache.Cache;
import javax.cache.CacheException;
import javax.cache.CacheManager;

import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

/**
 * Easy caching
 * @author Jean-Baptiste Vovau
 *
 */
public class MyCache {

	/**
	 * Returns object in cache
	 * @param key
	 * @return
	 */
	public static Object get(Object key){
		Cache cache = getCache();
		
		if (cache != null) return cache.get(key);
		
		return null;
	}
	
	/**
	 * Save object in cache
	 * @param key
	 * @param value
	 */
	public static void put(Object key, Object value){
		Cache cache = getCache();
		
		if (cache != null) cache.put(key, value);
	}
	
	public static void remove(Object key){
		Cache cache = getCache();
		if (cache != null) cache.remove(key);
	}
	
	private static Cache getCache(){
		//find in cache first
        Cache cache = null;
        Map<Object,Object> props = new HashMap<Object,Object>();
        props.put(GCacheFactory.EXPIRATION_DELTA, 3600); //one our
        try {
			cache = CacheManager.getInstance().getCacheFactory().createCache(props);
		} catch (CacheException e) {
			e.printStackTrace();
		}
		
		return cache;
	}
	
}

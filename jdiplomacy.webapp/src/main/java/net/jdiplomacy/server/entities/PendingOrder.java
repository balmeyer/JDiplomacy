/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

/**
 *
 * @author Jean-Baptiste
 */
@Entity
public class PendingOrder implements Serializable {
    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Key id;

    @Column
    private Key keytour;

    @Column
    private int idcountry;

    @Column
    private Key keyaccount;

    @Column
    private Text orders;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date creationDate;

    public Key getId() {
        return id;
    }

    public void setId(Key id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PendingOrder)) {
            return false;
        }
        PendingOrder other = (PendingOrder) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jdiplomacy.server.dal.PendingOrder[id=" + getId() + "]";
    }

    /**
     * @return the tour
     */
    public Tour getTour(EntityManager em , Key key) {
        return em.find(Tour.class , key);
    }

    /**
     * @param tour the tour to set
     */
    public void setTour(Tour tour) {
        this.keytour = tour.getKey();
    }

    /**
     * @return the country
     */
    public int getCountry() {
        return idcountry;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(Country country) {
        this.idcountry = country.getIndex();
    }

    /**
     * @return the account
     */
    public Account getAccount(EntityManager em , Key key) {
        return em.find(Account.class , key);
    }

    /**
     * @param account the account to set
     */
    public void setAccount(Account account) {
        this.keyaccount = account.getKey();
    }

    /**
     * @return the orders
     */
    public Text getOrders() {
        return orders;
    }

    /**
     * @param orders the orders to set
     */
    public void setOrders(Text orders) {
        this.orders = orders;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

}

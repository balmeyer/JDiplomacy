package net.jdiplomacy.server.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

@Entity
public class Conversation {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Key key;
	
	@Column
	private Key parent;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date writeDate;
	
	@Column
	private Text text;
	
	@Column
	private Key keyaccount;
	
	@Column
	private String author;
	
	@Column
	private Key keygame;
	
	@Column
	private List<String> restrictedUsers;

	/**
	 * @param key the key to set
	 */
	public void setKey(Key key) {
		this.key = key;
	}

	/**
	 * @return the key
	 */
	public Key getKey() {
		return key;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(Key parent) {
		this.parent = parent;
	}

	/**
	 * @return the parent
	 */
	public Key getParent() {
		return parent;
	}

	/**
	 * @param writeDate the writeDate to set
	 */
	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	/**
	 * @return the writeDate
	 */
	public Date getWriteDate() {
		return writeDate;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(Text text) {
		this.text = text;
	}

	/**
	 * @return the text
	 */
	public Text getText() {
		return text;
	}

	/**
	 * @param keyaccount the keyaccount to set
	 */
	public void setKeyaccount(Key keyaccount) {
		this.keyaccount = keyaccount;
	}

	/**
	 * @return the keyaccount
	 */
	public Key getKeyaccount() {
		return keyaccount;
	}

	/**
	 * @param forbidden the forbidden to set
	 */
	public void setPrivateDest(List<String> privatedest) {
		this.restrictedUsers = privatedest;
	}

	/**
	 * @return the forbidden
	 */
	public List<String> getPrivateDest() {
		if (restrictedUsers == null) restrictedUsers = new ArrayList<String>();
		return restrictedUsers;
	}

	/**
	 * @param keygame the keygame to set
	 */
	public void setKeygame(Key keygame) {
		this.keygame = keygame;
	}

	/**
	 * @return the keygame
	 */
	public Key getKeygame() {
		return keygame;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.entities;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean-Baptiste
 */
public class DataManager {

	static{
		checkCountries();
	}
	
    private static final String PERSISTENCE_UNIT_NAME = "JDiplomacyPU";

    private static EntityManagerFactory emf;

    /** No instance */
    private DataManager(){}

    /**
     *
     * Create new EntityManager
     *
     * @return
     */
    public static EntityManager createEntityManager(){
        EntityManager em = getEntityManagerFactory().createEntityManager();
    
        //em.setFlushMode(FlushModeType.COMMIT);
        
        return em;
    }


    /**
     * Persist a determinate object
     * @param obj
     */


    public static void persist(EntityManager em , Object obj){
        em.getTransaction().begin();
        em.persist(obj);
        em.getTransaction().commit();
        //em.refresh(obj);
    }

    public static void commit(EntityManager em){
        em.getTransaction().begin();
        em.flush();
        em.getTransaction().commit();
        

    }

    /**
     * Get entity manager Factory
     * @return
     */
    private static EntityManagerFactory getEntityManagerFactory(){
        if (emf == null){
            emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        }
        return emf;
    }

    /**
     * Create countries when server starts if those entities don't exist.
     */
    private static void checkCountries(){
        EntityManager em = DataManager.createEntityManager();

        //derby returns Integer
        //MySql returns Long
        
        List list = em.createQuery("select c from Country c")
                .getResultList();

        if (list.size() == 0){
            em.persist(new Country(0,"Autria"));
            DataManager.commit(em);
            em.persist(new Country(1,"England"));
            DataManager.commit(em);
            em.persist(new Country(2,"France"));
            DataManager.commit(em);
            em.persist(new Country(3,"Germany"));
            DataManager.commit(em);
            em.persist(new Country(4,"Italy"));
            DataManager.commit(em);
            em.persist(new Country(5,"Russia"));
            DataManager.commit(em);
            em.persist(new Country(6,"Turkey"));

            DataManager.commit(em);
        }

        em.close();
    }


    
}

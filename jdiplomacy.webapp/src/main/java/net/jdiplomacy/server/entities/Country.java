/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.appengine.api.datastore.Key;

/**
 *
 * @author Jean-Baptiste Vovau
 */
@Entity
public class Country implements Serializable {
    private static long serialVersionUID = 1L;

    /**
     * Returns country from the 0-6 index.
     */
    public static Country getCountry(EntityManager em , int country){
        return (Country)em.createQuery("select c from Country c where c.index = :index")
        .setParameter("index", country).getSingleResult();
    }

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Key key;

    @Column
    private int index;
    
    @Column(length=20,nullable=false)
    private String name;

    public Country(){}

    public Country(int number, String name){
        this.setIndex(number);
        this.setName(name);
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key id) {
        this.key = id;
    }

    public int getIndex(){
    	return this.index;
    }
    
    public void setIndex(int i){
    	this.index = i;
    }
    
    @Override
    public int hashCode() {
        return getIndex();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Country)) {
            return false;
        }
        Country other = (Country) object;
        if (other == null) return false;

        return this.index == other.index;
    }

    @Override
    public String toString() {
        return "[id=" + getIndex() + "]" + this.getName();

    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

}

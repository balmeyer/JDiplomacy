/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.google.appengine.api.datastore.Key;

/**
 *
 * @author Jean-Baptiste Vovau
 */
@Entity
public class Inscription implements Serializable {
    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Key id;

    @ManyToOne(optional=false)
    private Key keygame;

    @Column
    private Key keyaccount;

    @Column(nullable=false)
    private boolean hasplayed = false;

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public Key getId() {
        return id;
    }

    public void setId(Key id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Inscription)) {
            return false;
        }
        Inscription other = (Inscription) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jdiplomacy.server.dal.Inscription[id=" + getId() + "]";
    }

    /**
     * @return the game
     */
    public Game getGame(EntityManager em) {
    	if (this.keygame == null) return null;
    	
        return em.find(Game.class, this.keygame);
    }

    public Key getKeyGame(){
    	return this.keygame;
    }
    /**
     * @param game the game to set
     */
    public void setGame(Game game) {
        this.keygame = game.getKey();
    }

    /**
     * @return the account
     */
    public Account getAccount(EntityManager em) {
        return em.find(Account.class, this.keyaccount);
    }

    public Key getKeyAccount(){
    	return this.keyaccount;
    }
    
    /**
     * @param account the account to set
     */
    public void setKeyAccount(Key ac) {
        this.keyaccount = ac;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the hasplayed
     */
    public boolean isHasplayed() {
        return hasplayed;
    }

    /**
     * @param hasplayed the hasplayed to set
     */
    public void setHasplayed(boolean hasplayed) {
        this.hasplayed = hasplayed;
    }

}

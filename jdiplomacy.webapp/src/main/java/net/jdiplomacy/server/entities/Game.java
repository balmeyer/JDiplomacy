/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.server.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

import com.google.appengine.api.datastore.Key;

/**
 *
 * @author Jean-Baptiste Vovau
 */
@Entity
public class Game implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Key key;

    @Column(length=500)
    private String title;

    @Column
    private int minutePerTour;

    @Column
    private boolean started;

    @Column
    private boolean finished;

    @Column
    private boolean hasplayed;

    @Column
    private int countPlayer;

    @Column
    private int freePlace;
    
    @Column
    private List<Integer> winlist;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date creationDate;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date startDate;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date endDate;

    @Column
    private Key keyowner;

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    @Override
    public int hashCode() {
        return this.key.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Game)) {
            return false;
        }
        Game other = (Game) object;
        if ((this.key == null && other.key != null) || (this.key != null && !this.key.equals(other.key))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jdiplomacy.server.dal.Game[id=" + this.key + "]";
    }

    /**
     * @return the finished
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * @param finished the finished to set
     */
    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    /**
     * @return the hasplayed
     */
    public boolean isHasplayed() {
        return hasplayed;
    }

    /**
     * @param hasplayed the hasplayed to set
     */
    public void setHasplayed(boolean hasplayed) {
        this.hasplayed = hasplayed;
    }

    /**
     * @return the countPlayer
     */
    public int getCountPlayer() {
        return countPlayer;
    }

    /**
     * @param countPlayer the countPlayer to set
     */
    public void setCountPlayer(int countPlayer) {
        this.countPlayer = countPlayer;
    }

    /**
     * @return the freePlace
     */
    public int getFreePlace() {
        return freePlace;
    }

    /**
     * @param freePlace the freePlace to set
     */
    public void setFreePlace(int freePlace) {
        this.freePlace = freePlace;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the owner
     */
    public Account getOwner(EntityManager em) {
        return em.find(Account.class , this.keyowner);
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(Account owner) {
        this.keyowner = owner.getKey();
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the minutePerTour
     */
    public int getMinutePerTour() {
        return minutePerTour;
    }

    /**
     * @param minutePerTour the minutePerTour to set
     */
    public void setMinutePerTour(int minutePerTour) {
        this.minutePerTour = minutePerTour;
    }

    /**
     * @return the started
     */
    public boolean isStarted() {
        return started;
    }

    /**
     * @param started the started to set
     */
    public void setStarted(boolean started) {
        this.started = started;
    }

    /**
     * @return the inscriptions
     */
    public List<Inscription> getInscriptions(EntityManager em) {
        
    	return em.createQuery("select i from Inscription i where "
    			+ " i.keygame = :keygame")
    			.setParameter("keygame", this.getKey())
    			.getResultList();
    }


    /**
     * @return the gameCountries
     */
    public List<GameCountry> getGameCountries(EntityManager em) {
        
    	return em.createQuery("select g from GameCountry g where "
    			+ " g.keygame = :keygame")
    			.setParameter("keygame", this.getKey())
    			.getResultList();
    }

	/**
	 * @param winlist the winlist to set
	 */
	public void setWinlist(List<Integer> winlist) {
		this.winlist = winlist;
	}

	/**
	 * @return the winlist
	 */
	public List<Integer> getWinlist() {
		return winlist;
	}



}

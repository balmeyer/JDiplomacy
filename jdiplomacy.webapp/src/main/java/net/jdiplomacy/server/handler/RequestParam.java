
package net.jdiplomacy.server.handler;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Build request parameters from client URL
 * @author Jean-Baptiste
 */
public class RequestParam {

    private String page;
    private Map<String, String> properties;

    public RequestParam(String link) throws UnsupportedEncodingException {

        this.properties = new HashMap<String, String>();

        int point = link.indexOf("?");

        if (point < 0) {
            this.page = link;
            return;
        }

        this.page = link.substring(0, point - 1);
        if (this.page.startsWith("/")) {
            this.page = page.replace("/", "");
        }
        String params = link.substring(point + 1);

        String[] args = params.split("&");

        for (String pair : args) {
            String[] pairKeyValue = pair.split("=");

            String key = pairKeyValue[0];
            String value = null;
            if (pairKeyValue.length > 1) {
                value = pairKeyValue[1];
                //decode
                value = URLDecoder.decode(value, "UTF-8");
            }

            this.properties.put(key, value);
        }

        //add the page property
        this.properties.put("page",this.page);
    }

    /**
     * @return the page
     */
    public String getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * @return the properties
     */
    public Map<String, String> getProperties() {
        return properties;
    }
}

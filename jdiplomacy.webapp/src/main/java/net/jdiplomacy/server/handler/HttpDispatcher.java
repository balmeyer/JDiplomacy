/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.server.handler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.jdiplomacy.server.command.Command;
import net.jdiplomacy.server.command.CommandType;
import net.jdiplomacy.server.command.ConfirmCommand;
import net.jdiplomacy.server.command.ConversationCommand;
import net.jdiplomacy.server.command.CreateAccountCommand;
import net.jdiplomacy.server.command.CreateGameCommand;
import net.jdiplomacy.server.command.EndGameCommand;
import net.jdiplomacy.server.command.GameCommand;
import net.jdiplomacy.server.command.JoinGameCommand;
import net.jdiplomacy.server.command.ListGamesCommand;
import net.jdiplomacy.server.command.LoginCommand;
import net.jdiplomacy.server.command.NotAuthorizedException;
import net.jdiplomacy.server.command.OptionCommand;
import net.jdiplomacy.server.command.OrdersCommand;
import net.jdiplomacy.server.command.TourCommand;
import net.jdiplomacy.server.command.UnknownCommand;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

/**
 * Dispatch Request provided by the HttpRequestHandler
 * @author Jean-Baptiste Vovau
 */
public class HttpDispatcher {

    //map page (as "php page") to command
    private Map<String, Class> pageToCommand;
    //default command when nothing is recognized
    private Command unknowncommand;

    public void dispatch(String page , Map<String,String> map, OutputStream stream)
    throws IOException{
    	
        try {
            Command cmd = getCommand(page);
            cmd.perform(map, stream);
        } catch (NotAuthorizedException naex) {
            flushNotAuthorized(stream);
        }

    }
    
    /**
     * Find the right command regarding given RequestParam, execute the command
     * which sends back a XML to the given outputstream
     *
     * @param param
     * @param stream
     * @throws IOException
     */
    public void dispatch(RequestParam param, OutputStream stream)
            throws IOException {

        try {
            Command cmd = getCommand(param.getPage());
            cmd.perform(param.getProperties(), stream);
        } catch (NotAuthorizedException naex) {
            flushNotAuthorized(stream);
        }

    }

    /**
     * Provide Command object depending on page name.
     * @param page
     * @return
     */
    private Command getCommand(String page) {

        if (pageToCommand == null) {
            buildPageToCommand();
        }
        
        if (page == null) return unknowncommand;

        if (page.contains(".")) {
            page = page.substring(0, page.indexOf("."));
        }

        if (page.startsWith("/")){
        	page = page.substring(1);
        }
        
        if (pageToCommand.containsKey(page)) {
            Class<Command> clazz = pageToCommand.get(page);

            try {
                Command instance = clazz.newInstance();
                return instance;
            } catch (InstantiationException ex) {
                Logger.getLogger(HttpDispatcher.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(HttpDispatcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //nothing found
        return unknowncommand;
    }

    private void buildPageToCommand() {
        pageToCommand = new HashMap<String, Class>();
        unknowncommand = new UnknownCommand();

        Class[] array = new Class[]{
            LoginCommand.class,
            CreateAccountCommand.class,
            ListGamesCommand.class,
            CreateGameCommand.class,
            JoinGameCommand.class,
            GameCommand.class,
            OrdersCommand.class,
            TourCommand.class,
            EndGameCommand.class,
            ConfirmCommand.class,
            ConversationCommand.class,
            OptionCommand.class
        };


        for (Class<Command> c : array) {

            CommandType ct = c.getAnnotation(CommandType.class);

            pageToCommand.put(ct.page(), c);


        }






    }

    private void flushNotAuthorized(OutputStream os) throws IOException {
        Element elem = new Element("reply");
        Element error = new Element("error");

        error.setAttribute("id", "0");
        error.addContent("not authorized, please log in");

        Document doc = new Document(elem);
        elem.addContent(error);

        XMLOutputter outputter = new XMLOutputter();

        outputter.output(doc, os);
        os.close();
    }
}

package net.jdiplomacy.language;

/**
 * Text type
 * @author Jean-Baptiste Vovau
 *
 */
public enum TextType {

	mailNewGame,
	mailAskConfirmation,
	mailAccountConfirmed,
	mailNewTurnBegins,
	mailRecallPassword
	
}

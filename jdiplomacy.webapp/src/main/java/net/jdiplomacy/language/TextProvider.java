package net.jdiplomacy.language;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Provide text 
 * @author Jean-Baptiste Vovau
 *
 */
public  class TextProvider {

	private static final Map<String,String> codeToText = new HashMap<String,String>();
	
	/**
	 * Returns associated textS
	 * @param tt
	 * @return
	 */
	public static String getPattern(TextType tt, String language){
		
		String code = "/META-INF/text/" + tt.toString() + "-" + language + ".txt";
		
		String result = codeToText.get(code);
		
		if (result == null){
			URL u = TextProvider.class.getResource(code);
			
			//not found ? try english version
			if (u == null){
				code = code.replace("-" + language, "-en");
				u = TextProvider.class.getResource(code);
			}
			
			try {
				//Read resource
				BufferedReader reader = new BufferedReader(new InputStreamReader(u.openStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;
				do {
					line = reader.readLine();
					
					if (line != null){
						sb.append(line);
						sb.append("\r\n");
					}
				} while (line != null);
				
				//save resource
				result = sb.toString();
				codeToText.put(code,result);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		return result;
	
	}
	
	
	
}

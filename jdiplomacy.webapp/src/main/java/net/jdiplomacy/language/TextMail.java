package net.jdiplomacy.language;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import net.jdiplomacy.server.entities.Account;

/**
 * Internationalized mail
 * @author JBVovau
 *
 */
public class TextMail {

	public static final String FRAGMENT_SUBJECT = "subject";
	public static final String FRAGMENT_BODY = "body";
	public static final String FRAGMENT_RECIPIENT = "recipient";
	
	
	private final Map<String, Object> namedData = new HashMap<String,Object>();
	private final Map<String,String> namedFragment = new HashMap<String,String>();
	
	private String pattern = null;
	
	public TextMail(Account ac, TextType tt){
		this.pattern = TextProvider.getPattern(tt, ac.getLanguage());
		this.addData(ac);
	}
	
	public void addData(Object obj){

		try {
		for (Method m : obj.getClass().getDeclaredMethods()){
			//no argument for this method
			if (m.getParameterTypes().length > 0) continue;
			
			if (m.getName().startsWith("get")){
				if (m.getParameterTypes().length >0) continue;
				
				
				
				String name = m.getName().substring(3);
				name = obj.getClass().getSimpleName() + "." + name;
				System.out.println("fecth :" + name);
				
				//TODO replace
				if (name.equals("Game.Winlist")) continue;
				if (name.equals("Account.Options")) continue;
				Object value = m.invoke(obj, null);
				if (value != null) this.addData(name.toLowerCase(),value);
			}
			
		}
		
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void addData(String key , Object value){
		this.namedData.put(key,value);
		//this.buildPattern();
	}
	
	/**
	 * Returns the fragment name
	 * @param name
	 * @return
	 */
	public String getFragment(String name){
		if (this.namedFragment.size() == 0) this.buildPattern();
		return this.namedFragment.get(name);
	}
	
	private void buildPattern(){
		//build new pattern
		StringBuilder sb = new StringBuilder(this.pattern);
		
		//replace variables
		for(String key : this.namedData.keySet()){
			this.replaceVar(sb, key, this.namedData.get(key));
		}
		
		//split fragment
		this.namedFragment.clear();
		String [] lines = sb.toString().split("\r\n");
		StringBuilder frag = new StringBuilder();
		String currentName = null;
		for(String line : lines){
			if (line.startsWith("[") && line.endsWith("]")){
				String fragname = line.replace("[","").replace("]","").toLowerCase();
				
				if (currentName != null && frag.length() > 0 ){
					//add last fragment
					this.namedFragment.put(currentName, frag.toString());
				}
				frag.setLength(0);
				currentName = fragname;
			} else {
				frag.append(line);
				frag.append("\r\n");
			}
		}
		
		if (frag.length() > 0){
			this.namedFragment.put(currentName, frag.toString());
		}
	}
	
	private void replaceVar(StringBuilder sb , String key, Object value){
		String varname = "${" + key.toLowerCase() + "}";
		String strValue = "";
		
		if (value != null) strValue = value.toString();
		
		int start = sb.toString().toLowerCase().indexOf(varname);
		while (start>=0){
			int end = start + varname.length() ;
			
			
			sb.replace(start, end, strValue);
			
			start = sb.toString().toLowerCase().indexOf(varname);
		}
		
	}
}

package net.jdiplomacy.tools;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import net.jdiplomacy.server.entities.Conversation;
import net.jdiplomacy.server.entities.DataManager;
import net.jdiplomacy.server.entities.Game;

/**
 * Stats on usages
 * @author JBVovau
 *
 */
public class UsageStats {

	private Map<String,Integer> dateToTour = new HashMap<String,Integer>();
	private Map<String,Integer> dateToConversation = new HashMap<String,Integer>();
	
	private int nbUsers;
	private int nbOpenGames;
	
	public static UsageStats getInstance(){
		UsageStats stats = new UsageStats();
		
		EntityManager em = DataManager.createEntityManager();
		
		//exam tour
		/*
		List<Tour> list = em.createQuery("select t from Tour t").getResultList();
		
		for(Tour t : list){
			String codeDate = getCodeDate(t.getStartDate());
			stats.addStatTour(codeDate);
		}*/
		
		//exam conversation
		
		/*
		List<Conversation> conv = em.createQuery("select c from Conversation c").getResultList();
		
		for (Conversation c : conv){
			String codeDate = getCodeDate(c.getWriteDate());
			stats.addStatConversation(codeDate);
		}*/
		
		
		
		//number of user
		List<Conversation> users = em.createQuery("select a from Account a").getResultList();
		int nbUser = users.size();
		stats.setNbUsers(nbUser);
		
		List<Game> games = em.createQuery("select g from Game g where g.started = true and g.finished = false")
		.getResultList();
		int nbOpenGames = games.size();
		stats.setNbOpenGames(nbOpenGames);
		
		em.close();
		
		return stats;
	}
	
	
	public void addStatTour(String codeDate){
		int value = 0;
		if (dateToTour.containsKey(codeDate)){
			value = dateToTour.get(codeDate);
		}
		value++;
		dateToTour.put(codeDate,value);
		
	}
	
	public void addStatConversation(String codeDate){
		int value = 0;
		if (dateToConversation.containsKey(codeDate)){
			value = dateToConversation.get(codeDate);
		}
		value++;
		dateToConversation.put(codeDate,value);
		
	}
	
	
	public String tmpGetHtml(){
		
		StringBuilder sb = new StringBuilder();
		
		addHtml("CONV",dateToConversation, sb);
		addHtml("TOUR",dateToTour,sb);
		
		return sb.toString();
	}
	
	private void addHtml(String mapName, Map<?,?> map, StringBuilder sb){
		for(Object key : map.keySet()){
			
			Object value = map.get(key);
			sb.append("<li>");
			sb.append(mapName);
			sb.append(key);
			sb.append(" \t : ");
			sb.append(value);
			sb.append("</li>");
			
		}
		
		
	}
	
	private static String getCodeDate(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(c.get(Calendar.YEAR));
		sb.append('-');
		sb.append(c.get(Calendar.MONTH) + 1);
		sb.append('-');
		sb.append(c.get(Calendar.DAY_OF_MONTH) + 1);
		
		return sb.toString();
	}


	public void setNbUsers(int nbUsers) {
		this.nbUsers = nbUsers;
	}


	public int getNbUsers() {
		return nbUsers;
	}


	public void setNbOpenGames(int nbOpenGames) {
		this.nbOpenGames = nbOpenGames;
	}


	public int getNbOpenGames() {
		return nbOpenGames;
	}
	
}

[Recipient]
${account.email}
[Subject]
[JDiplomacy] Un nouveau tour vient de commencer !
[Body]
Bonjour ${account.login},

Un nouveau tour vient juste de commencer dans la partie suivante :

Titre : ${game.title}
Id : ${gameid}


---------------------------
http://www.jdiplomacy.net

(pour ne plus recevoir cet email, désactiver l'option dans le panel "options" du jeu)
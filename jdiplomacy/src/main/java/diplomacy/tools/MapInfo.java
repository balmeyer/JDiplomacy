package diplomacy.tools;

import java.net.URL;

import diplomacy.game.Game;

/**
 * Information about a map
 * @author JBVovau
 *
 */
public class MapInfo {

	private final static MapInfo instance = new MapInfo();
	
	public String type;
	public String theme;
	
	
	private MapInfo(){}
	
	/**
	 * Returns single instance of map info
	 * @return
	 */
	public static MapInfo getInstance(){
		return instance;
	}
	
	/**
	 *  
	 * @returnurl of XML file
	 */
	public URL getMapXml(){
		//TODO implements real map loading
		return Game.class.getClassLoader().getResource("map.xml");
	}
	
}

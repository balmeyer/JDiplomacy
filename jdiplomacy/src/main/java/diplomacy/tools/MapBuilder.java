package diplomacy.tools;

import java.net.MalformedURLException;
import java.net.URL;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import diplomacy.Main;
import diplomacy.access.exception.AccessException;
import diplomacy.game.Game;
import diplomacy.game.Unit;
import diplomacy.game.board.Country;
import diplomacy.game.board.Province;
import diplomacy.game.board.Region;
import diplomacy.game.board.Supply;
import diplomacy.graph.Graph;
import diplomacy.graph.GraphNode;
import diplomacy.xml.DocumentLoader;

/**
 * Build maps and themes
 * @author JBVovau
 *
 */
public class MapBuilder {

	private static URL mapImageUrl ;
	
	//tmp : used for start position
    private static final String FIRST_POSITION = "0:A:Vie;0:A:Bud;0:F:Tri;1:A:Lvp;1:F:Lon;1:F:Edi;"
        + "2:A:Par;2:A:Mar;2:F:Bre;3:A:Mun;3:A:Ber;3:F:Kie;4:A:Rom;4:A:Ven;"
        + "4:F:Nap;5:A:Mos;5:A:War;5:F:Stp-S;5:F:Sev;6:A:Con;6:A:Smy;6:F:Ank;"
        + "0:S:Bud;0:S:Tri;0:S:Vie;1:S:Edi;1:S:Lvp;1:S:Lon;2:S:Bre;2:S:Mar;"
        + "2:S:Par;3:S:Ber;3:S:Mun;3:S:Kie;4:S:Nap;4:S:Rom;4:S:Ven;5:S:Mos;"
        + "5:S:Sev;5:S:StP;5:S:War;6:S:Ank;6:S:Con;6:S:Smy;";
	
    private MapBuilder(){}
    
    /**
     * Build map from mapinfo
     * @param info
     */
    public static void loadMap(){
    	serialize(MapInfo.getInstance().getMapXml());
    }
    
	public static URL getMapImageLocation(){
		//return Game.class.getClassLoader().getResource("Images/map.png");
		return mapImageUrl;
	}
    
	/*
	 * COUNTRY
	 * PROVINCE (short / long)
	 * REGION (short / position / type)
	 * GRAPH (army / fleet)
	 * SUPPLY ( province / position) 
	 * UNIT (type / start position)
	 */
	
	private static void unserializeFromGame(){
		
		XmlMap xmap = new XmlMap();
		
		//map type
		xmap.newSection("meta");
		Node nMap = xmap.addToSection("board");
		xmap.attr(nMap, "type", "standard");
		
		//add country
		xmap.newSection("countries");
		for (Country c : Country.getCountries()){
			Node n = xmap.addToSection("country");
			xmap.attr(n,"name", c.getGameName());
			xmap.attr(n, "index", c.getIndex());
		}
		
		//add provinces
		xmap.newSection("provinces");
		for(Province p : Province.getProvinceList()){
			Node n = xmap.addToSection("p");
			xmap.attr(n, "short", p.getShortName());
			xmap.attr(n, "name", p.getName());
			
		}
		
		//add regions
		xmap.newSection("regions");
		for(Region r : Region.getRegions()){
			Node n = xmap.addToSection("r");
			xmap.attr(n, "name", r.getName());
			xmap.attr(n, "x", r.getX());
			xmap.attr(n, "y", r.getY());
			xmap.attr(n,"type", r.getClass().getSimpleName());
		}
		
		//graph for army
		xmap.newSection("graph");
		for(GraphNode g : Unit.getGraph(Unit.ARMY).getNodes()){
			Node n = xmap.addToSection("g");
			xmap.attr(n, "name", g.getName());
			xmap.attr(n, "type", "A");
			StringBuilder sb = new StringBuilder();
			for(GraphNode a : g.getSuccessors()){
				sb.append(a.getName());
				sb.append(',');
			}
			sb.setLength(sb.length() - 1);
			xmap.attr(n,"around",sb.toString());
		}
		
		//graph for fleet
		for(GraphNode g : Unit.getGraph(Unit.SHIP).getNodes()){
			Node n = xmap.addToSection("g");
			xmap.attr(n, "name", g.getName());
			xmap.attr(n, "type", "F");
			StringBuilder sb = new StringBuilder();
			for(GraphNode a : g.getSuccessors()){
				sb.append(a.getName());
				sb.append(',');
			}
			sb.setLength(sb.length() - 1);
			xmap.attr(n,"around",sb.toString());
		}
		
		//Supply
		xmap.newSection("supplies");
		for(Supply s : Supply.getSupplies()){
			Country c = s.getOriginalOwner();
			
			Node n = xmap.addToSection("s");
			xmap.attr(n, "place", s.getProvince().getShortName());
			xmap.attr(n, "x", s.getX());
			xmap.attr(n,"y", s.getY());
			if (c != null) xmap.attr(n, "owner", c.getIndex());
			
		}
		
		
		//units
		xmap.newSection("units");
		String [] items = FIRST_POSITION.split(";");
		for(String item : items){
			String [] w = item.split(":");
			int country = Integer.parseInt(w[0]);
			String type = w[1];
			String place = w[2];
			Node n = xmap.addToSection("u");
			xmap.attr(n, "country", country);
			xmap.attr(n,"type",type);
			xmap.attr(n, "place", place);
		}
		
		DocumentLoader.writeXmlFile(xmap.getDocument(),"map.xml");
		
	}
	
	
	private static void serialize(URL url){
		
		Document doc = null;
		
		try {
			doc = DocumentLoader.load(url);
		} catch (AccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//MAP
		NodeList list = doc.getElementsByTagName("image");
		if (list.getLength() <1) throw new IllegalArgumentException("Image node not found");
		Node nimg = list.item(0);
		String path = getAttr(nimg,"path");
		if (path.startsWith("http:")){
			try {
				mapImageUrl = new URL(path);
			} catch (MalformedURLException e) {
				Main.log(e);
			}
		} else {
			mapImageUrl = MapBuilder.class.getClassLoader().getResource(path);
		}
		
		//Country
		Country.init();
		list = doc.getElementsByTagName("country");
		for (int i = 0 ; i < list.getLength() ; i++){
			Node n = list.item(i);
			String name = getAttr(n,"name");
			String index = getAttr(n,"index");
			Country c = Country.build(name, Integer.valueOf(index));
		}
		
		//Province
		Province.init();
		list = doc.getElementsByTagName("p");
		for (int i = 0 ; i < list.getLength(); i++){
			Node n = list.item(i);
			String shortName = getAttr(n,"short");
			String name = getAttr(n,"name");
			Province.build(shortName, name);
		}
		
		//Regions
		Region.init();
		list = doc.getElementsByTagName("r");
		for (int i = 0 ; i < list.getLength(); i++){
			Node n = list.item(i);
			String name = getAttr(n,"name");
			String type = getAttr(n,"type");
			String x = getAttr(n,"x");
			String y = getAttr(n,"y");
			Region.build(name, type, Integer.parseInt(x), Integer.parseInt(y));
		}
		
		//graph
		Graph gArmy = new Graph();
		Graph gShip = new Graph();
		list = doc.getElementsByTagName("g");
		for(int i = 0 ; i < list.getLength() ; i++){
			Node n = list.item(i);
			String name = getAttr(n,"name");
			String around = getAttr(n,"around");
			String type = getAttr(n,"type");
			
			if (type.equalsIgnoreCase("A")){
				gArmy.addNode(name, around);
			} else {
				gShip.addNode(name, around);
			}
		}
		
		//supplies
		Supply.init();
		list = doc.getElementsByTagName("s");
		for(int i = 0 ; i < list.getLength() ; i++){
			Node n = list.item(i);
			String owner = getAttr(n,"owner");
			String place = getAttr(n,"place");
			String x = getAttr(n,"x");
			String y = getAttr(n,"y");
			
			Country c = null;
			if (owner != null) c = Country.get(Integer.parseInt(owner));
			
			Supply.create(Province.get(place), 
					c, 
					Integer.parseInt(x),
					Integer.parseInt(y)
					);
		}
		

		
		
        Region.completeGraph(gArmy);
        Region.completeGraph(gShip);
        Unit.setGraph(gArmy, Unit.ARMY);
        Unit.setGraph(gShip, Unit.SHIP);
	}

	private static String getAttr(Node node , String key){
		Node n = node.getAttributes().getNamedItem(key);
		if( n != null){
			return n.getNodeValue();
		}
		return null;
	}
	

	/**
	 * 
	 * @author JBVovau
	 *
	 */
	private static class XmlMap{
		
		private Document doc;
		private Node root;
		private Node currentSection = null;
		
		public XmlMap(){
			this.doc = DocumentLoader.create();
			//root
			root = this.doc.createElement("map");
			doc.appendChild(root);
		}
		
		/**
		 * Returns DOM Document generated by class
		 * @return
		 */
		public Document getDocument(){
			return this.doc;
		}
		
		/**
		 * Add a new list to Document ("regions", "provinces", "units", etc.)
		 * @param name
		 * @return
		 */
		public Node newSection(String name){
			Node n = this.doc.createElement(name);
			this.currentSection = n;
			this.root.appendChild(n);
			return n; 
		}
		
		public Node addToSection(String elemName){
			Node e = this.doc.createElement(elemName);
			this.currentSection.appendChild(e);
			return e;
		}
		
		public void attr(Node node , String key, String value){
			Node attr = this.doc.createAttribute(key);
			attr.setNodeValue(value);
			node.getAttributes().setNamedItem(attr);
		}
		
		public void attr(Node node , String key, int value){
			attr(node, key, String.valueOf(value));
		}
		

	}
	
}

package diplomacy.graph;

import diplomacy.game.board.Province;
import diplomacy.game.board.Region;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Un sommet d'un graphe. Contenu dans un objet Graph. <br>
 * <br>
 * 
 * Un graphe possède des successeurs qu'on peut it�rer avec la m�thode <br>
 * getSuccessors() </b>
 * 
 * @see diplomacy.graph.Graph
 *  
 */
public class GraphNode {

	private Region region;

	private String name;

	private Map<String,GraphNode> succ;

    //régions autour du noeud
    private Collection<Region> regionsAround;

    //provinces autour du noeud
    private Collection<Province> provincesAround;

	public GraphNode(String name) {
		this.name = name.toLowerCase();
		this.succ = new HashMap<String,GraphNode>();
	}

	/**
	 * retourne les successeurs sous forme d'Iterateur
	 */
	public Collection<GraphNode> getSuccessors() {
		if (succ == null ) this.succ = new HashMap<String,GraphNode>();
		
		return succ.values();
	}

    /**
     * Retourne les régions voisines
     * @return
     */
    public Collection<Region> getRegionsAround(){
        if (this.regionsAround == null){
            this.regionsAround = new ArrayList<Region>(getSuccessors().size());
            for(GraphNode node : getSuccessors()){
                this.regionsAround.add(node.getRegion());
            }
        }

        return this.regionsAround;
    }

    /**
     * Retourne les provinces tout autour du noeud
     * @return
     */
    public Collection<Province> getProvincesAround(){
        if (this.provincesAround == null){
            this.provincesAround = new ArrayList<Province>(8);
            for(Region r : getRegionsAround()){
                this.provincesAround.add(r.getProvince());
            }
        }

        return this.provincesAround;
    }

	/**
	 * Ajoute un sommet successeur à celui-ci.
	 */
	public void addSuccessor(GraphNode node) {
		if (!this.succ.containsValue(node)) {

			this.succ.put(node.getName().toLowerCase(), node);
			//référence croisée : graphe non orienté.
			node.addSuccessor(this);
		}
	}

	/**
	 *  
	 */
	public boolean isNear(GraphNode node) {
		if (node == null) {
			return false;
		}

		boolean result = false;
		
		if (succ.containsValue(node)) return true;
		
		//A partir de la région, teste les régions équivalente
		Iterator<Region> regions = node.getRegion().getProvince().getRegions().iterator();
		while (regions.hasNext()){
			Region r = regions.next();
			result = succ.containsKey(r.getName().toLowerCase());
			if (result) break;
		}
		return result;

	}

	public boolean isNear(String n) {
		GraphNode node = succ.get(n.toLowerCase());
		return (isNear(node));
	}

	public Region getRegion() {
		return this.region;
	}

	public void setRegion(Region region) {
		assert(region!= null);

		this.region = region;
	}

	public String getName() {
		return this.name;
	}

    @Override
	public String toString(){
		return getName();
	}
	
}

package diplomacy.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import diplomacy.game.board.Region;

/**
 * Permet d'explorer les graphes avec un objet Graph. Plusieurs algorithmes de
 * parcours doivent y figurer. Le parcours s'effectue entre les sommets reli�s
 * entre eux.
 *  
 */
public class GraphExplorer {

	//declarations
	private Graph graph;

	private GraphNode currentNode = null;

	private Map<GraphNode,String> etat;

	private GraphStack pile;

	//Gestion des sommets actifs / non actifs
	private Collection<GraphNode> activated;

	//liste des sommets explor�s
	private List<GraphNode> explorationList;

	/**
	 * le constructeur prend en argument le graphe � explorer.
	 */
	public GraphExplorer(Graph graph) {
		this.activated = new ArrayList<GraphNode>(30);
		this.etat = new HashMap<GraphNode,String>();

		this.graph = graph;
		//départ par défaut
		this.startAt(graph.getFirstNode());

	}

	/**
	 * Indique à quel sommet on commence l'exploration du graphe. Indispensable
	 * pour commencer l'exploration.
	 */
	public void startAt(String shortName) {
		currentNode = graph.getNode(shortName);
		this.startAt(currentNode);
	}

	public void startAt(GraphNode node) {
		currentNode = node;
		this.reset();
		this.pile = new GraphStack();
		pile.push(currentNode);
	}

	/**
	 * Utilis� pour n'autoriser que certains sommets (convoi)
	 * 
	 * @param node
	 */
	public void addOnly(GraphNode node) {
		this.activated.add(node);
	}

	public void addOnly(Region r) {
		GraphNode node = this.graph.getNode(r.getName());
		this.addOnly(node);
	}

	/**
	 * Existe-t-il un prochain sommet dans le parcours ?
	 */
	public boolean hasNext() {

		return (!pile.isEmpty());
	}

	/**
	 * Retourne le prochain sommet du parcours du graphe.
	 */
	public GraphNode next() {

		//liste de l'exploration
		if (this.explorationList == null) {
			explorationList = new ArrayList<GraphNode>(36);
		}

		if (!pile.isEmpty()) {
			currentNode = pile.pop();
			visit(currentNode);
			//ajoute le noeud � la liste
			explorationList.add(currentNode);

			//remplit la pile
			Iterator<GraphNode> it = currentNode.getSuccessors().iterator();
			while (it.hasNext()) {
				GraphNode n = it.next();
				if (isFree(n)) {
					open(n);
					pile.push(n);
				}
			}
			return currentNode;
		} else {
			return null;
		}

	}

	/**
	 * R�initialise le parcours du graphe. On peut aussi utiliser startAt() pour
	 * choisir un autre sommet.
	 */
	public void reset() {
		//vide la liste des sommets autoris�s
		//this.activated.clear();

		etat.clear();
		//rentre les sommets et leur état
		Iterator<GraphNode> it = graph.getNodes().iterator();
		GraphNode gn;
		while (it.hasNext()) {
			gn = it.next();
			//associe à chaque sommet une valeur : 0 pour libre
			etat.put(gn, "0");
		}

		explorationList = null;
	}

	/**
	 * Le sommet de d�part peut-il joindre le sommet donn�e ? On doit utiliser
	 * startAt() avant.
	 * 
	 * @see diplomacy.graph.areLinked
	 */
	public boolean canJoin(String name) {
		GraphNode but = graph.getNode(name);

		if (but == null)
			return false;

		GraphNode n;
		boolean resultat = false;
		while (hasNext()) {
			n = next();
			resultat = (n == but);
			if (resultat)
				break;
		}
		return resultat;
	}

	/**
	 * Existe-t-il un chemin entre les deux sommets ?
	 */
	public boolean areLinked(String node1, String node2) {
		this.startAt(node1);
		return canJoin(node2);
	}

	/**
	 * Retourne la liste des sommets explor�s
	 * @return
	 */
	public List<GraphNode> getExplorationList(){
		if (explorationList == null) explorationList = new ArrayList<GraphNode>(36);
		
		return this.explorationList;
	}
	
	/**
	 *  
	 */
	private void open(GraphNode node) {
		etat.put(node, "1");
	}

	/**
	 *  
	 */
	private void visit(GraphNode node) {
		etat.put(node, "2");
	}

	/**
	 *  
	 */
	private boolean isFree(GraphNode node) {
		//gestion des sommets autoris�s
		if (this.activated.size() > 0) {
			if (!activated.contains(node))
				return false;
		}

		String s = etat.get(node);
		return (s.equals("0"));
	}

}

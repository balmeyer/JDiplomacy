/*
 * Created on 19 avr. 2007
 *
 *
 */
package diplomacy.gui.leftpanel;

import diplomacy.game.Board;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import diplomacy.game.Game;
import diplomacy.game.GameListener;
import diplomacy.game.Tour;
import diplomacy.order.Order;
import diplomacy.order.OrderSet;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public class OrderTextPanel extends JPanel implements ActionListener, GameListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = -983616841266728381L;
	private OrderSet set;
    private JTextField txtOrder;
    private JListOrder listeOrdre;

    public OrderTextPanel() {


        //text
        txtOrder = new JTextField();
        txtOrder.setMaximumSize(new Dimension(500, 20));
        //BoxLayout box = new BoxLayout(this,BoxLayout.Y_AXIS);
        this.setLayout(new BorderLayout());

        //ajoute la liste des ordres
        this.listeOrdre = new JListOrder();
        this.add(txtOrder, BorderLayout.NORTH);
        //this.add(Box.createHorizontalStrut(2));
        this.add(listeOrdre, BorderLayout.CENTER);
        //this.add(Box.createVerticalGlue());


        //a l'�coute d'un nouvel ordre
        txtOrder.addActionListener(this);
    }

    /**
     * Attribue le jeu
     * @param game
     */
    public void setGame(Game game) {
        //a l'Ecoute du jeu
        game.addGameListener(this);
        this.listeOrdre.setTour(game.getCurrentTour());

    }

    @Override
	public void actionPerformed(ActionEvent e) {
        if (this.set != null) {
            Board board = this.set.getTour().getBoard();
            String ordre = txtOrder.getText();
            Order o = Order.create(board, ordre);
            set.addOrder(o);
        }
        txtOrder.setText("");
    }

    /**
     * Ecoute du changement de tour
     * @param tour
     */
    @Override
    public void tourChanged(Tour tour) {
        this.listeOrdre.setTour(tour);
        this.set = tour.getOrderSet();
    }

    @Override
    public void tourAdded(Tour tour) {
    }

}

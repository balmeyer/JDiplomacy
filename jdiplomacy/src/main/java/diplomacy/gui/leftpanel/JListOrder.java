/*
 * Created on 9 avr. 2007
 *
 *
 */
package diplomacy.gui.leftpanel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Iterator;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import diplomacy.game.Tour;
import diplomacy.gui.MainGameInterface;
import diplomacy.gui.ImageProvider;
import diplomacy.order.Order;
import diplomacy.order.OrderSet;
import diplomacy.order.OrderSetListener;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 * 
 *  
 */
public class JListOrder extends JList implements ListCellRenderer,
        OrderSetListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2060644361100968077L;
	private static final Font MYFONT_BOLD = new Font("ARIAL", Font.PLAIN, 14);
    private static final Font MYFONT_PLAIN = new Font("ARIAL", Font.PLAIN, 14);
    
    public JListOrder() {
        this.setCellRenderer(this);
    }

    public void setTour(Tour tour) {
        this.tour = tour;
        this.setModel(new OrderListModel(tour.getOrderSet()));
        if (!tour.isFinished()) {
            tour.getOrderSet().addOrderSetListener(this);
        }
    }

    /**
     * Mod�le de valeurs pour la liste des ordres
     * 
     * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
     * 
     *  
     */
    private class OrderListModel implements ListModel {

        private OrderSet set;

        public OrderListModel(OrderSet orderSet) {
            this.set = orderSet;
        }

        /*
         * (non-Javadoc)
         * 
         * @see javax.swing.ListModel#getSize()
         */
        @Override
		public int getSize() {
            if (set == null) {
                return 0;
            }
            return set.getOrders().size();
        }

        /*
         * (non-Javadoc)
         * 
         * @see javax.swing.ListModel#getElementAt(int)
         */
        @Override
		public Object getElementAt(int arg0) {
            Iterator<Order> it = set.getOrders().iterator();
            int i = 0;
            Order o = null;
            while (it.hasNext() && i <= arg0) {
                o = it.next();
                if (i == arg0) {
                    break;
                }
                o = null;
                i++;
            }
            return o;
        }

        /*
         * (non-Javadoc)
         * 
         * @see javax.swing.ListModel#addListDataListener(javax.swing.event.ListDataListener)
         */
        @Override
		public void addListDataListener(ListDataListener arg0) {
           
        }

        /*
         * (non-Javadoc)
         * 
         * @see javax.swing.ListModel#removeListDataListener(javax.swing.event.ListDataListener)
         */
        @Override
		public void removeListDataListener(ListDataListener arg0) {
            
        }
    }
    private Tour tour;

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList,
     *      java.lang.Object, int, boolean, boolean)
     */
    @Override
	public Component getListCellRendererComponent(JList arg0, Object arg1,
            int position, boolean focused, boolean selected) {
        //Ordre
        Order o = (Order) arg1;
        //Panel contenant le label
        JPanel panel = new JPanel();
        panel.setMinimumSize(new Dimension(400, 16));
        panel.setMaximumSize(new Dimension(600, 16));
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

        //icone d'Etat
        JLabel statut = new JLabel(new ImageIcon(ImageProvider
                .getInstance().get("Images/c6_s.gif")));

        JLabel label = new JLabel(o.getText());
        
        if (o.getCountry() != null) {
            label.setIcon(new ImageIcon(o.getCountry().getSmallFlag()));
        }
        //fonte : selon le type d'ordre
        if (o.getOrderType() == Order.ORDRE_MOUVEMENT && o.getActionRegion().equals(o.getEndRegion())) {
            //ordre neutre
            label.setFont(MYFONT_PLAIN);
            label.setForeground(Color.GRAY);
        } else {
            label.setFont(MYFONT_BOLD);
        }
        panel.add(statut);
        panel.add(Box.createHorizontalStrut(2));
        panel.add(label);
        panel.add(Box.createHorizontalGlue());

        //Etat
        if (o.isJudged()) {
            //ICONE SUCCES
            if (o.isSuccess()) {
                statut.setIcon(new ImageIcon(ImageProvider.getInstance().get("Images/c4_s.gif")));
            } else {
                //ICONE ECHEC
                statut.setIcon(new ImageIcon(ImageProvider.getInstance().get("Images/c0_s.gif")));
                label.setForeground(Color.RED);
            }
        }

        //Couleur de fond du paneau
        if (selected) {
            panel.setBackground(Color.ORANGE);
        } else {

            //Couleur de fond selon nombre pair / impair
            if (position % 2 == 0) {
                panel.setBackground(Color.WHITE);
            } else {
                panel.setBackground(MainGameInterface.COLOR_LIST_GREY);
            }
        }
        return panel;
    }

    /*
     * (non-Javadoc)
     * 
     * @see diplomacy.order.OrderSetListener#orderAdded(diplomacy.order.Order)
     */
    @Override
	public void orderAdded(Order o) {
        //this.updateUI();
        this.setModel(new OrderListModel(tour.getOrderSet()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see diplomacy.order.OrderSetListener#orderRemoved(diplomacy.order.Order)
     */
    @Override
	public void orderRemoved(Order o) {
        //this.updateUI();
        this.setModel(new OrderListModel(tour.getOrderSet()));
    }
}

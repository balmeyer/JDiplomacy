/*
 * Created on 22 avr. 2007
 *
 *
 */
package diplomacy.gui.leftpanel;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JList;
import javax.swing.JPanel;

import diplomacy.game.Game;
import diplomacy.game.Tour;
import javax.swing.JScrollPane;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public class HistoryPanel extends JPanel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1468055657716936526L;
	private Game game;
    private JListTour tourList;

    public HistoryPanel() {
        
        this.setLayout(new BorderLayout());
        tourList = new JListTour();
        JScrollPane pane = new JScrollPane(tourList);
        this.add(pane, BorderLayout.CENTER);
        this.tourList.addMouseListener(new HistoryMouseListener());
    }

    public void setGame(Game game){
        this.game = game;
        this.tourList.setGame(game);
    }
    
    /**
     * Ecouteur des évenements de la souris pour la liste
     * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
     *
     *
     */
    private class HistoryMouseListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent me) {
            if (me.getClickCount() > 1) {
                JList list = (JList) me.getComponent();
                Tour tour = (Tour) list.getSelectedValue();
                game.restoreTour(tour);

            }
        }
    }
}

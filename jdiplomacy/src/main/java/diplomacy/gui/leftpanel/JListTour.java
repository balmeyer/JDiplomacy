/*
 * Created on 22 avr. 2007
 *
 *
 */
package diplomacy.gui.leftpanel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import diplomacy.game.Game;
import diplomacy.game.GameListener;
import diplomacy.game.Tour;
import diplomacy.gui.MainGameInterface;
import diplomacy.gui.ImageProvider;

import javax.swing.border.LineBorder;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public class JListTour extends JList implements ListCellRenderer, GameListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 135569286482059047L;
	private Game game;
//    private static final Color COULEUR_SPRING = new Color(200, 255, 130);
//    private static final Color COULEUR_SPRING_DISBAND = new Color(90, 220, 50);
//    private static final Color COULEUR_FALL = new Color(255, 235, 170);
//    private static final Color COULEUR_FALL_DISBAND = new Color(250, 210, 70);
//    private static final Color COULEUR_FALL_AJUST = new Color(220, 220, 240);
    private static final Font MYFONT = new Font("ARIAL", Font.PLAIN, 14);

    public JListTour() {
        this.setCellRenderer(this);
    }

    public void setGame(Game game){
        this.game = game;
        this.setModel(new TourListModel(game));
        this.game.addGameListener(this);
    }

    @Override
    public void tourChanged(Tour tour) {
        this.setModel(new TourListModel(game));
    }

    @Override
    public void tourAdded(Tour tour) {
    }


    /* (non-Javadoc)
     * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
     */
    @Override
	public Component getListCellRendererComponent(JList arg0, Object o,
            int position, boolean focused, boolean selected) {

        //nouveau panel
        JPanel panel = new JPanel();
        Tour tour = (Tour) o;
        StringBuilder texte = new StringBuilder();

        JLabel lab = new JLabel(texte.toString());

        //tour en cours
        if (tour == tour.getGame().getCurrentTour()) {

            panel.setBackground(Color.YELLOW);
            panel.setBorder(new LineBorder(Color.RED, 1, true));
        } else {
            lab.setIcon(new ImageIcon(ImageProvider.getInstance().get("Images/c5_s.gif")));

            Color color = (position % 2 == 0) ? Color.WHITE : MainGameInterface.COLOR_LIST_GREY;
            panel.setBackground(color);
        }

        //ICONE SAISON
        switch (tour.getPhase()) {
            case Tour.PHASE_SPRING_DISBAND:
            case Tour.PHASE_SPRING:
                lab.setIcon(new ImageIcon(ImageProvider.getInstance().get("Images/spring.png")));
                break;
            case Tour.PHASE_FALL:
                lab.setIcon(new ImageIcon(ImageProvider.getInstance().get("Images/fall.png")));
                break;
            case Tour.PHASE_FALL_DISBAND:
                lab.setIcon(new ImageIcon(ImageProvider.getInstance().get("Images/fall.png")));
                break;
            case Tour.PHASE_FALL_REAJUST:
                lab.setIcon(new ImageIcon(ImageProvider.getInstance().get("Images/fall.png")));

                break;
        }


        //tour découvert
        if (tour.isDiscovered()) {
            //texte.append("[vu] ");
        }

        //texte du tour
        texte.append(tour.toString());


        //selection + focused
        if (focused && selected) {
            panel.setBackground(Color.ORANGE);
        }

        lab.setText(texte.toString());
        lab.setFont(MYFONT);
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(lab);
        panel.add(Box.createHorizontalGlue());


        return panel;
    }

    /**
     * Modèle pour la liste des tours
     */
    private class TourListModel implements ListModel {

        private Game game;

        public TourListModel(Game game) {
            this.game = game;
        }

        /* (non-Javadoc)
         * @see javax.swing.ListModel#getSize()
         */
        @Override
		public int getSize() {
            
            return game.getTours().size();
        }

        /* (non-Javadoc)
         * @see javax.swing.ListModel#getElementAt(int)
         */
        @Override
		public Object getElementAt(int n) {
            
            return game.getTours().get(game.getTours().size() - n - 1);
        }

        /* (non-Javadoc)
         * @see javax.swing.ListModel#addListDataListener(javax.swing.event.ListDataListener)
         */
        @Override
		public void addListDataListener(ListDataListener arg0) {
            
        }

        /* (non-Javadoc)
         * @see javax.swing.ListModel#removeListDataListener(javax.swing.event.ListDataListener)
         */
        @Override
		public void removeListDataListener(ListDataListener arg0) {
            
        }
    }
}

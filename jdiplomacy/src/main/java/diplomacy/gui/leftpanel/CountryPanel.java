/*
 * Created on 25 avr. 2007
 *
 *
 */
package diplomacy.gui.leftpanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import diplomacy.Main;
import diplomacy.game.Game;
import diplomacy.game.GameListener;
import diplomacy.game.Tour;
import diplomacy.game.User;
import diplomacy.game.board.Country;
import diplomacy.gui.ImageProvider;
import diplomacy.language.Language;
import diplomacy.order.CountryOrders;
import diplomacy.order.OrderFetcherListener;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public class CountryPanel extends JPanel implements GameListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7997120228311284275L;
	private boolean hasplayed = false;
    private static final String fontType = Font.MONOSPACED;

    public CountryPanel(Game game, Country c) {
        this.country = c;
        this.game = game;

        //dimension
        this.setPreferredSize(new Dimension(200, 60));

        //bord
        this.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
        this.setBorder(BorderFactory.createTitledBorder(c.getDisplayName()));
        
       
        //Drapeaux
        JButton btFlag = new JButton(new ImageIcon(c.getBigFlag()));
        btFlag.setBounds(8, 22, 36, 36);

        //nom de l'utilisateur
        String userName = "";
        User user = this.game.getUser(c);
        if (user != null) {
            userName = user.getUserName();
        }
        JLabel lbUsername = new JLabel(userName);
        lbUsername.setBounds(46, 20, 120, 20);
        lbUsername.setFont(new Font(fontType, Font.PLAIN, 14));

        //couleur de fond : en fonction du joueur
        if (!Main.getUser().equals(user)) {
            this.setBackground(Color.LIGHT_GRAY);
        }

        //Label statut de l'ordre
        
        this.lbStatutOrdre = new JLabel(
                Language.getInstance().get("countrypanel.waiting"),
                new ImageIcon(ImageProvider.getInstance().get("Images/c0_s.gif")),
                SwingConstants.LEFT);
        lbStatutOrdre.setBounds(8, 54, 120, 20);
        lbStatutOrdre.setFont(new Font(fontType, Font.ITALIC, 12));

        //number of units
        lbNbUnit = new JLabel(
                "0",
                new ImageIcon(c.getSmallUnitImage()),
                SwingConstants.RIGHT);
        lbNbUnit.setBounds(160, 22, 42, 20);
        lbNbUnit.setFont(new Font(fontType,Font.BOLD,16));

        //nombre de depot

        lbNbSupply = new JLabel(
                "0",
                new ImageIcon(c.getSupplyImage()),
                SwingConstants.RIGHT);
        lbNbSupply.setBounds(160, 54
        		, 42, 20);
        lbNbSupply.setFont(new Font(fontType,Font.BOLD,16));

        this.setLayout(null);
        this.add(btFlag);
        //this.add(lbCountryName);
        this.add(lbUsername);
        this.add(lbStatutOrdre);
        this.add(lbNbSupply);
        this.add(lbNbUnit);

        //a l'écoute des ordres
        this.game.getOrderFetcher().addOrderFetcherListener(new FetchListener());
        this.game.addGameListener(this);
        this.tourChanged(game.getCurrentTour());
        
        
        //game over : winner ?
        this.checkGameOver();
    }


    /**
     * Ecoute le changement de tour
     */
    @Override
    public void tourChanged(Tour tour) {
        //Mise à jour des unités et des dépots

        int nbSupply = tour.getBoard().countSupply(country);
        int nbUnit = tour.getBoard().getUnits(country).size();

        this.lbNbSupply.setText(String.valueOf(nbSupply));
        this.lbNbUnit.setText(String.valueOf(nbUnit));
        this.checkHasPlayed();
    }

    @Override
    public void tourAdded(Tour tour) {
        //Replace tous les indicateurs en attente
        hasplayed = false;
        this.checkHasPlayed();
    }

    private void checkGameOver(){
    	if (this.game.getGameInfo().getWinlist() == null) return;
    	
    	List<Country> winlist = this.game.getGameInfo().getWinlist();
    	
    	if (winlist.size() >0 && winlist.get(0).equals(this.country)){
    		this.setBorder(BorderFactory.createLineBorder(Color.yellow));

    	}
    	
    }
    
    private void checkHasPlayed(){

        String myImage = null;
        String myText = null;

        if (hasplayed){
            myImage = "Images/c4_s.gif";
            myText = "countrypanel.ok";
        } else{
            myImage = "Images/c0_s.gif";
            myText = "countrypanel.waiting";
        }

                        lbStatutOrdre.setIcon(
                        new ImageIcon(
                        ImageProvider.getInstance().get(myImage)));
                lbStatutOrdre.setText(Language.getInstance().get(myText));
    }

    
    /**
     * A l'écoute des ordres
     */
    private class FetchListener implements OrderFetcherListener {

        /**
         * Nouvel ordre reçu de l'objet OrderFetcher
         */
        @Override
        public void newOrders(CountryOrders co) {
            if (country.equals(co.getCountry())) {
                hasplayed = true;
                checkHasPlayed();
            }
        }

        @Override
		public void remainingTime(int minutes) {
            //NOTHING
        }
        /* (non-Javadoc)
         * @see diplomacy.order.OrderFetcherListener#newTourPersited(diplomacy.game.Tour)
         */
    }
    private Country country;
    private Game game;
    private JLabel lbStatutOrdre,  lbNbSupply,  lbNbUnit;

}

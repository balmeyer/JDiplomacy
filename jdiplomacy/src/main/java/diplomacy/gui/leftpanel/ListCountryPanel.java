/*
 * Created on 25 avr. 2007
 *
 *
 */
package diplomacy.gui.leftpanel;

import java.awt.BorderLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import diplomacy.game.Game;
import diplomacy.game.board.Country;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public class ListCountryPanel extends JPanel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 749831172404653098L;

	public ListCountryPanel() {

    }
    
    public void setGame(Game game){
        this.removeAll();
        
        Box box = new Box(BoxLayout.Y_AXIS);

        //ajoute tous les pays
        this.setLayout(new BorderLayout());

        for (int i = 0; i < 7; i++) {
            CountryPanel cp = new CountryPanel(game, Country.get(i));
            box.add(cp);
        }

        box.add(Box.createVerticalGlue());

        //scroll
        JScrollPane sp = new JScrollPane(box);

        this.add(sp, BorderLayout.CENTER);
    }
}

/*
 * Created on 9 mai 2007
 *
 *
 */
package diplomacy.gui.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import diplomacy.game.Game;
import diplomacy.game.GameListener;
import diplomacy.game.Tour;
import diplomacy.gui.ImageProvider;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public class ActionNextTour extends AbstractAction implements GameListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5149427566021767175L;
	private Game game;

    public ActionNextTour() {
        super("", ImageProvider.getInstance().getIcon(ImageProvider.IconType.arrowRight));
        this.setEnabled(false);
    }

    public void setGame(Game game) {
        this.game = game;
        if (game != null) {
            this.game.addGameListener(this);
            //currentTourChanged(this.game.getCurrentTour());
            this.checkNextTour();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Tour tour =this.game.getCurrentTour().getNextTour();
        if (tour != null)
            this.game.restoreTour(tour);
    }

    /* (non-Javadoc)
     * @see diplomacy.game.GameListener#currentTourChanged(diplomacy.game.Tour)
     */
    @Override
    public void tourChanged(Tour tour) {
        this.checkNextTour();
    }

    @Override
    public void tourAdded(final Tour tour) {
        this.checkNextTour();
    }


    private void checkNextTour(){
        Tour tour = this.game.getCurrentTour();
        this.setEnabled(tour.getNextTour() != null);
    }
}

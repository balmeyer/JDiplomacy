package diplomacy.gui.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import diplomacy.gui.FrameDocument;
import diplomacy.gui.ImageProvider;

/**
 * Action for showing Help Frame
 * @author Jean-Baptiste Vovau
 *
 */
public class ActionHelp extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7771137701669185763L;

	public ActionHelp(){
		super("Help !",
				ImageProvider.getInstance()
				.getIcon(ImageProvider.IconType.interrogation));
		
		
	}
	
	/**
	 * Show Help
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run(){
				final JFrame fhelp = FrameDocument.get(FrameDocument.DocType.help);
				fhelp.setVisible(true);
			}
		});
		
	}

}

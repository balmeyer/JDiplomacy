/*
 * Created on 21 mars 2004
 *
 * Vovau Jean-Baptiste - balmeyer@dev.java.net
 */
package diplomacy.gui.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import diplomacy.game.Game;
import diplomacy.game.GameListener;
import diplomacy.game.Tour;
import diplomacy.gui.ImageProvider;
import diplomacy.language.Language;
import diplomacy.order.CountryOrders;
import diplomacy.order.OrderFetcherListener;
import diplomacy.order.OrderSet;

/**
 * 
 * Created on 21 mars 2004.
 * 	@author Jean-Baptiste Vovau - balmeyer@dev.java.net
 *	@version 1.0 
 */

//=========================================================
public final class ActionValidate extends AbstractAction
        implements GameListener, OrderFetcherListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7202719584163311490L;
	private Game currentGame;

    public ActionValidate() {
        super(Language.getInstance().get("form.validate"),
                ImageProvider.getInstance().getIcon(ImageProvider.IconType.greenOk));


        this.setSwingEnabled(false);

    }

    public void setGame(Game game) {
        this.currentGame = game;

        //A l'écoute du jeu
        currentGame.addGameListener(this);
        currentGame.getOrderFetcher().addOrderFetcherListener(this);
        this.testActionEnabled();
    }

    /**
     * Envoi des ordres du joueur
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
    	

    	
        this.setSwingEnabled(false);
        Tour tour = currentGame.getCurrentTour();
        
    	//order ?
    	if (tour.getOrderSet().getOrders().size() == 0){
    		//ask confirmation
    		int n = JOptionPane.showConfirmDialog(null,
    				Language.getInstance().get("form.confirmorder")
    				, "", 
    				JOptionPane.YES_NO_OPTION);
    		if (n != JOptionPane.YES_OPTION) {
    			setSwingEnabled(true);
    			return;
    		}
    	}
        
        if (!tour.isFinished()) {
            sendThreadOrder(tour.getOrderSet());
        }
    }

    /**
     * 
     * @param b
     */
    private void setSwingEnabled(final boolean b) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                setEnabled(b);
            }
        });
    }

    private void sendThreadOrder(final OrderSet set){
    	Thread th = new Thread(new Runnable() {
    		@Override
			public void run(){
    			currentGame.getOrderFetcher().sendUserOrder(set);
    		}
    	});
    	
    	th.setDaemon(true);
    	th.setPriority(Thread.MIN_PRIORITY);
    	th.start();
    }
    
    @Override
    public void tourChanged(Tour tour) {
        this.testActionEnabled();
    }

    @Override
    public void tourAdded(final Tour tour) {
        this.testActionEnabled();
    }

    @Override
    public void newOrders(CountryOrders c) {
        this.testActionEnabled();
    }

    @Override
    public void remainingTime(int minutes) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Test si les ordres peuvente être enregistrés
     */
    private void testActionEnabled() {
        if (this.currentGame == null
                || this.currentGame.getGameInfo().isFinished()) {
            this.setSwingEnabled(false);
            return;
        }

        if (this.currentGame.getGameInfo().isHasplayed()){
        	this.setSwingEnabled(false);
        	return;
        }
        
        Tour tour = this.currentGame.getCurrentTour();

        if (tour == null) {
            this.setSwingEnabled(false);
            return;
        }

        if (tour != null && tour.getNextTour() != null){
            this.setSwingEnabled(false);
            return;
        }


        boolean canModify = tour.canModify();

        setSwingEnabled(canModify);
    }
}

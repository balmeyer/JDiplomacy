/*
 * Created on 24 mars 2004
 *
 * Vovau Jean-Baptiste - balmeyer@dev.java.net
 */
package diplomacy.gui.action;

import java.awt.event.ActionEvent;
import javax.swing.*;

import diplomacy.gui.*;
import diplomacy.gui.ImageProvider.IconType;

/**
 * 
 * Created on 24 mars 2004.
 * 	@author Jean-Baptiste Vovau - balmeyer@dev.java.net
 *	@version 1.0 
 */
public class ActionZoom extends AbstractAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8126121014763966968L;
	public ActionZoom(MainGameInterface jf, boolean zoomAgrandit) {
		super("Zoom", ImageProvider.getInstance().getIcon(
                (zoomAgrandit) ? IconType.zoomMore : IconType.zoomLess)
         );


		this.agrandit = zoomAgrandit;
		this.myMap = jf;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		myMap.changeZoom(this.agrandit);
	}
	
	private MainGameInterface myMap;
	private boolean agrandit = false;
}

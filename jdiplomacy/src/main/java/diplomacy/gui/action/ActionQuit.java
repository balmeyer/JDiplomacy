/*
 * Created on 24 mars 2004
 *
 * Vovau Jean-Baptiste - balmeyer@dev.java.net
 */
package diplomacy.gui.action;

import diplomacy.language.Language;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 * 
 * Created on 24 mars 2004.
 * 	@author Jean-Baptiste Vovau - balmeyer@dev.java.net
 *	@version 1.0 
 */
/**
	Represente l'op�ration quitter...
*/
public class ActionQuit extends AbstractAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6204868466747881498L;
	/** le constructeur */
	public ActionQuit() {
		super(Language.getInstance().get("form.quit"));
	}
	/** cette m�thode fait quitter le jeu */
	@Override
	public void actionPerformed(ActionEvent e) {
		System.exit(0);
	}
}

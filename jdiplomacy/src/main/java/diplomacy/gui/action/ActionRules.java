package diplomacy.gui.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import diplomacy.gui.FrameDocument;
import diplomacy.gui.ImageProvider;

/**
 * Action for showing Rules
 * @author Jean-Baptiste Vovau
 *
 */
public class ActionRules extends AbstractAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6653450344340009139L;

	public ActionRules(){
		super("Rules",
				ImageProvider.getInstance()
				.getIcon(ImageProvider.IconType.rules));
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {

		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run(){
				final JFrame frules = FrameDocument.get(FrameDocument.DocType.rules);
				frules.setVisible(true);
			}
		});
		
	}

}

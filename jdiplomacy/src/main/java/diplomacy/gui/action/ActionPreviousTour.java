/*
 * Created on 9 mai 2007
 *
 *
 */
package diplomacy.gui.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import diplomacy.game.Game;
import diplomacy.game.GameListener;
import diplomacy.game.Tour;
import diplomacy.gui.ImageProvider;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public class ActionPreviousTour extends AbstractAction implements GameListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3192977559588618268L;
	private Game game;

    public ActionPreviousTour() {
        super("", ImageProvider.getInstance().getIcon(ImageProvider.IconType.arrowLeft));
        this.setEnabled(false);
    }

    public void setGame(Game game) {
        this.game = game;
        if (this.game != null) {
            this.game.addGameListener(this);
            tourChanged(this.game.getCurrentTour());
        }
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent arg0) {
        Tour tour = game.getCurrentTour().getPreviousTour();
        if (tour != null){
            game.restoreTour(tour);
        }
    }

    /* (non-Javadoc)
     * @see diplomacy.game.GameListener#currentTourChanged(diplomacy.game.Tour)
     */
    @Override
    public void tourChanged(Tour tour) {
        
        this.setEnabled(tour.getPreviousTour() != null);
    }

    @Override
    public void tourAdded(final Tour tour) {
        //this.currentTourChanged(tour);
    }

}

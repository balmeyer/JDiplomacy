/*
 * Created on 27 mai 2007
 *
 *
 */
package diplomacy.gui;

import java.awt.Color;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;

import diplomacy.game.Game;

/**
 * 
 * Panel contenant un scrollPane pour la carte et un panneau de superposition
 * pour les infos.
 * 
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public final class ScrollMap extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8747866927955960189L;

    private JScrollPane scr;
    private InfoPanel infoPanel;

	/**
	 * Construction à partir du panel de carte
	 * @param map
	 */
	public ScrollMap(JPanel map){
		super(true);
		//this.setLayout(new BorderLayout());

        this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));

        //panel d'information
        this.infoPanel = new InfoPanel();
        this.add(this.infoPanel);
                //, BorderLayout.NORTH);

        this.add(Box.createVerticalGlue());

        this.scr = new JScrollPane(map);
        this.scr.setAutoscrolls(true);
        this.scr.getVerticalScrollBar().setUnitIncrement(16);
        this.scr.getHorizontalScrollBar().setUnitIncrement(16);
        this.scr.getViewport().setScrollMode(JViewport.BLIT_SCROLL_MODE);
        this.scr.setWheelScrollingEnabled(true);
		this.add(scr); //, BorderLayout.CENTER);

		
		JPanel pp = new JPanel();
		pp.setBounds(5, 5 , 200, 200);
		pp.setBackground(new Color(0.7f,0.7f,0.7f,0.7f));
		//map.add(pp);
		
        this.add(Box.createVerticalGlue());
	}

    public void displayMessage(String msg){
        this.infoPanel.displayMessage(msg);
    }

    /**
     * 
     * @return
     */
    public JScrollPane getScrollPane(){
        return this.scr;
    }


    public void setGame(Game game){
        this.infoPanel.setGame(game);
    }




}

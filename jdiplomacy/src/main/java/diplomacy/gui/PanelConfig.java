package diplomacy.gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import diplomacy.access.GameOptions;
import diplomacy.access.exception.AccessException;
import diplomacy.language.Language;

public class PanelConfig extends JPanel {

	
	/**
	 * Configuration panel
	 */
	private static final long serialVersionUID = 5263170446091794768L;

	//send a mail when tour begins
	private JCheckBox jcbMailEachTour ;
	
	//newsletter
	private JCheckBox jcbNewsletter;
	
	public PanelConfig(){
		
		//panel email
		Border etched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		JPanel panelMail = new JPanel();
		panelMail.setBorder(BorderFactory.createTitledBorder(etched, "emails"));
		panelMail.setBounds(20,55,400,200);
		panelMail.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		this.jcbMailEachTour = new JCheckBox(Language.getInstance()
				.get("form.options.maileachtour"));
		this.jcbNewsletter = new JCheckBox(Language.getInstance()
				.get("form.options.newsletter"));
		
		panelMail.add(jcbMailEachTour);
		panelMail.add(jcbNewsletter);
		

		this.setLayout(null);
		
		Icon saveicon = ImageProvider.getInstance().getIcon(ImageProvider.IconType.greenOk);
		JButton btnSave = new JButton(Language.getInstance()
				.get("form.options.save"),saveicon);
		
		btnSave.setBounds(20,20,160,30);
		btnSave.addActionListener(
				new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e){
						save();
					}
				});
		
		this.add(panelMail);
		this.add(btnSave);
		
		
		Runnable loader = new Runnable(){
			@Override
			public void run(){
				loadOptions();
			}
		};
		/*
		Thread th = new Thread(loader);
		th.start();*/
		SwingUtilities.invokeLater(loader);
	}
	
	/**
	 * Update options from server
	 */
	private final void loadOptions(){
		try {
			
			GameOptions.getInstance().load();
			
			SwingUtilities.invokeLater(new Runnable(){
				@Override
				public void run(){
					updateUIOptions();
				}
			});
		} catch (AccessException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update ui
	 */
	private final void updateUIOptions(){
		this.jcbMailEachTour.setSelected(getNotNullValue("maileachtour").equals("yes"));
		this.jcbNewsletter.setSelected(!getNotNullValue("newsletter").equals("no"));
	}
	
	private final void save(){
		
		String optionEachTour = (this.jcbMailEachTour.isSelected()) ? "yes": "no";
		String optionNewsletter = (this.jcbNewsletter.isSelected()) ? "yes": "no";
		
		GameOptions.getInstance().setOption("maileachtour", optionEachTour);
		GameOptions.getInstance().setOption("newsletter", optionNewsletter);
		
		Runnable loader = new Runnable(){
			@Override
			public void run(){
				try {
					GameOptions.getInstance().save();
				} catch (AccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		
		Thread th = new Thread(loader);
		th.start();
	}
	
	/**
	 * Return empty string if option is null
	 * @param key
	 * @return
	 */
	private String getNotNullValue(String key){
		String val = GameOptions.getInstance().getOption(key);
		
		if (val == null) return "";
		return val;
	
	}
	
	
}

package diplomacy.gui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import diplomacy.game.board.Region;

public class ProvinceNameImage {

	private static Map<Region,ProvinceNameImage> cache =
		new HashMap<Region,ProvinceNameImage>();
	
	public static ProvinceNameImage get(Region r){
		//image in cache
		ProvinceNameImage pni = cache.get(r);
		if (pni != null) return pni;
		
		//build image
		pni = new ProvinceNameImage();
		Image img = new BufferedImage(220, 30,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = ((BufferedImage) img).createGraphics();

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		
		//		
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(r.getName());
		sb.append("] ");
		sb.append(r.getProvince().getName());
		
		//composite
        g.setComposite(AlphaComposite.getInstance(
                AlphaComposite.SRC_OVER, 0.7f));
		

		//text metrics
		Font font = new Font(Font.MONOSPACED, Font.BOLD ,14);
		FontMetrics fm = g.getFontMetrics(font);
		Rectangle2D rect = fm.getStringBounds(sb.toString(), g);
		
		int width = (int) Math.round(rect.getWidth());
		int height = (int) Math.round(rect.getHeight());
		
		//shadow rect
		g.setColor(Color.darkGray);
		g.fillRoundRect(1,1, width + 9, height + 1, 4, 4);
		
		//draw rect
		g.setColor(Color.yellow);
		g.fillRoundRect(0,0, width + 8, height, 4, 4);
		
		//drawtext
		g.setColor(Color.black);
		g.setFont(font);
		g.drawString(sb.toString(), 4, 13);
		
		
		
		//finish
		pni.img = img;
		pni.x = r.getX() - (int) Math.round(rect.getWidth()/2);
		pni.y = r.getY() + 16;
		
		cache.put(r, pni);
		return pni;
	}
	
	public Image img;
	public int x = 5;
	public int y = 5;
	
	private ProvinceNameImage(){}
	
}

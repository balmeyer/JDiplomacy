/*
 * FrameUserLogin.java
 *
 * Created on 4 octobre 2007, 20:12
 */

package diplomacy.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import diplomacy.Main;
import diplomacy.access.exception.AccessException;
import diplomacy.game.User;
import diplomacy.language.Language;

/**
 * Prompt Login and Password from user. First inteface to appear in the application.
 * 
 * @author Jean-Baptiste Vovau
 */
public class FrameUserLogin extends javax.swing.JFrame {


	private static final long serialVersionUID = -494492437752856717L;
	
	/** Creates new form FrameUserLogin */
	public FrameUserLogin() {
		
		//init components
		initComponents();

		
		//build combobox for language
		List<String> listCombo = new ArrayList<String>();
		String defaultLanguage = null;
		Map<String, String> map = Language.getInstance().getCodeToLibelle();
		for (String code : map.keySet()) {
			String libelle = code + " - " + map.get(code);
			listCombo.add(libelle);
			if (code.equals(Language.getInstance().getLanguage())) {
				defaultLanguage = libelle;
			}
		}
		this.comboLangue
				.setModel(new DefaultComboBoxModel(listCombo.toArray()));
		this.comboLangue.setSelectedItem(defaultLanguage);

		//window start position
		Rectangle bounds = this.getBounds();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		bounds.x = (dim.width - bounds.width) / 2;
		bounds.y = (dim.height - bounds.height) / 2;
		this.setBounds(bounds);
	}


	/**
	 * Init all swing components
	 */
	private void initComponents() {

		
		
		jbValidate = new javax.swing.JButton();
		jbCancel = new javax.swing.JButton();
		labMessage = new javax.swing.JLabel();
		labNewAccount = new javax.swing.JLabel();
		panelForm = new javax.swing.JPanel();
		labLogin = new javax.swing.JLabel();
		tbLogin = new javax.swing.JTextField();
		labPassword = new javax.swing.JLabel();
		tbPassword = new javax.swing.JPasswordField();
		comboLangue = new javax.swing.JComboBox();
		labForgotPassword = new javax.swing.JLabel();

		
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setTitle("JDiplomacy");
		setLocationByPlatform(true);
		setResizable(false);
		setUndecorated(false);

		jbValidate.setIcon(ImageProvider.getInstance().getIcon(ImageProvider.IconType.greenOk));
		jbValidate.setText(Language.getInstance().get("form.validate"));
		jbValidate.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jbValidateActionPerformed(evt);
			}
		});

		jbCancel.setFont(new java.awt.Font("Tahoma", 1, 12));
		jbCancel.setIcon(ImageProvider.getInstance().getIcon(ImageProvider.IconType.cancel));
		jbCancel.setText(Language.getInstance().get("form.cancel"));
		jbCancel.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				//exit
				System.exit(0);
			}
		});

		ImageIcon logo = ImageProvider.getInstance().getIcon(ImageProvider.IconType.bigLogo);

		labMessage.setFont(new java.awt.Font("Tahoma", 2, 18));
		labMessage.setForeground(new java.awt.Color(102, 102, 102));
		labMessage.setText(Language.getInstance().get("form.login.welcome"));

		labNewAccount.setFont(new java.awt.Font("Tahoma", Font.PLAIN, 12)); 
		labNewAccount.setIcon(ImageProvider.getInstance().getIcon(ImageProvider.IconType.newAccount));
		labNewAccount.setText(Language.getInstance().get("form.newaccount"));
		labNewAccount.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				labNewAccountMouseClicked(evt);
			}
		});

		labForgotPassword.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent evt){
				labForgotPasswordMouseClicked(evt);
			}
		});
		
		panelForm.setBorder(BorderFactory.createEtchedBorder());

		labLogin.setFont(new java.awt.Font("Tahoma", 1, 14));
		labLogin.setText(Language.getInstance().get("form.login"));

		tbLogin.setFont(new java.awt.Font("Tahoma", 1, 14));

		labPassword.setFont(new java.awt.Font("Tahoma", 1, 14));
		labPassword.setText(Language.getInstance().get("form.password"));

		tbPassword.setFont(new java.awt.Font("Tahoma", 1, 14));

		comboLangue.setFont(new java.awt.Font("Tahoma", 0, 14));
		comboLangue.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
				"FR - Francais", "EN - English" }));


		labForgotPassword.setFont(new Font("Tahoma",Font.PLAIN,12));
		labForgotPassword.setIcon(ImageProvider.getInstance().getIcon(ImageProvider.IconType.interrogation));
		labForgotPassword.setText(Language.getInstance().get(
				"form.forgotpassword"));
		labForgotPassword.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				labForgotPasswordMouseClicked(evt);
			}
		});

		//form panel
		panelForm.setLayout(null);
		labLogin.setBounds(10,10,150,20);
		labPassword.setBounds(10,40,150,20);
		tbLogin.setBounds(130, 10, 210, 25);
		tbPassword.setBounds(130,40,210,25);
		this.comboLangue.setBounds(130,70,210,25);
		panelForm.add(labLogin);
		panelForm.add(labPassword);
		panelForm.add(tbLogin);
		panelForm.add(tbPassword);
		panelForm.add(this.comboLangue);
		
		getContentPane().removeAll();
		getContentPane().setLayout(null);
		
		//position
		
		this.lbLogo = new JLabel(logo);
		this.lbLogo.setBounds(0,0,380,100);
		this.labMessage.setBounds(10,85,360,30);
		panelForm.setBounds(10,110, 360, 105);
		//panelForm.setPreferredSize(new Dimension(330,200));
		
		this.jbValidate.setBounds(10,220,180,30);
		this.jbCancel.setBounds(190,220,180,30);
		
		this.labNewAccount.setBounds(10,250,250,30);
		this.labForgotPassword.setBounds(200,250,250,30);
		
		getContentPane().add(lbLogo);
		getContentPane().add(labMessage);
		getContentPane().add(panelForm);
		getContentPane().add(this.jbValidate);
		getContentPane().add(this.jbCancel);
		getContentPane().add(this.labNewAccount);
		getContentPane().add(this.labForgotPassword);
		getContentPane().setBackground(Color.white);
		
		//dimension
		this.setMinimumSize(new Dimension(386,320));
		
	}


	/**
	 * Action performed when connection
	 * @param evt
	 */
	private void jbValidateActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jbValidateActionPerformed

		this.doBtnEnable(false);
		this.startConnection(this.tbLogin.getText(), this.tbPassword
				.getPassword());

	}

	/**
	 * Try to connect to JDiplomacy server.
	 * @param login
	 * @param password
	 */
	private void startConnection(final String login, final char[] password) {

		//
		Runnable run = new Runnable() {

			@Override
			public void run() {
				try {
					doMessage("form.login.connecting");
					//choose language
					String l = comboLangue.getSelectedItem().toString()
							.substring(0, 2);
					Language.getInstance().setLanguage(l);
					User user = Main.getDBAccess().login(login, password);


					

					// game start
					Main.startMainInterface(user);
					dispose();
				} catch (final AccessException ae) {
					// access expcetion
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							doMessage(ae.getMessage());
						}
					});

					doBtnEnable(true);
				}
			}

		};

		
		Thread th = new Thread(run);
		th.start();
		
		//SwingUtilities.invokeLater(run);

	}

	/** Create new account */
	private void labNewAccountMouseClicked(java.awt.event.MouseEvent evt) {

		//save current language
		String l = comboLangue.getSelectedItem().toString()
				.substring(0, 2);
		Language.getInstance().setLanguage(l);
		
		FrameNewAccount fc = new FrameNewAccount();

		// centre la fenêtre
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		double x = (screen.getWidth() - fc.getWidth()) / 2;
		double y = (screen.getHeight() - fc.getHeight()) / 2;
		fc.setBounds((int) x, (int) y, fc.getWidth(), fc.getHeight());

		fc.setVisible(true);
	}

	/**
	 * 
	 * @param evt
	 */
	private void labForgotPasswordMouseClicked(java.awt.event.MouseEvent evt) {

		//send email
		String login = this.tbLogin.getText();
		if (login == null || login.length() == 0){
			this.doMessage("form.login.nologin");
			return;
		}
		
		try {
			Main.getDBAccess().askPassword(login);
			this.doMessage("form.login.passwordsent");
		} catch (AccessException e) {
			this.doMessage("form.login.badlogin");
		}
		
	}

	
	
	
	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new FrameUserLogin().setVisible(true);
			}
		});
	}

	private void doBtnEnable(final boolean enable) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				jbValidate.setEnabled(enable);
			}
		});
	}

	private void doMessage(String code) {
		labMessage.setVisible(true);
		labMessage.setText(Language.getInstance().get(code));
	}

	private JLabel lbLogo;
	private javax.swing.JComboBox comboLangue;
	private javax.swing.JButton jbCancel;
	private javax.swing.JButton jbValidate;
	private JLabel labForgotPassword;
	private javax.swing.JLabel labLogin;
	private javax.swing.JLabel labMessage;
	private javax.swing.JLabel labNewAccount;
	private javax.swing.JLabel labPassword;
	private javax.swing.JPanel panelForm;
	private javax.swing.JTextField tbLogin;
	private javax.swing.JPasswordField tbPassword;
	// End of variables declaration//GEN-END:variables
}
package diplomacy.gui;

import java.awt.Image;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

import diplomacy.Config;
import diplomacy.game.Board;
import diplomacy.game.Unit;
import diplomacy.game.board.Country;
import diplomacy.game.board.Supply;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 * 
 * 
 */
public final class ImageProvider {

	private Image[] imageUnit;
	private Map<URL, ImageIcon> urlToIcon; //caching icons

	private static ImageProvider instance; //single instance

	public static ImageProvider getInstance() {
		if (instance == null)
			instance = new ImageProvider();
		return instance;
	}

	/**
	 * Build
	 */
	private ImageProvider() {
		imageUnit = new Image[50];

		// Charge les armees big
		for (int i = 0; i < 7; i++) {
			imageUnit[i] = get("Images/a" + i + "_b.gif");
		}

		// charge les flottes big
		for (int i = 0; i < 7; i++) {
			imageUnit[i + 7] = get("Images/f" + i + "_b.gif");
		}

		// charge les army small
		for (int i = 0; i < 7; i++) {
			imageUnit[i + 14] = get("Images/a" + i + "_s.gif");
		}

		// charge les flottes small
		for (int i = 0; i < 7; i++) {
			imageUnit[i + 21] = get("Images/f" + i + "_s.gif");
		}

		// charge les centres big
		for (int i = 0; i < 7; i++) {
			imageUnit[i + 28] = get("Images/c" + i + "_b.gif");
		}

		// Charge les drappeaux BIG et SMALL
		for (int i = 0; i < 7; i++) {
			imageUnit[i + 35] = get("Images/flag" + i + "_b.gif");
			imageUnit[i + 42] = get("Images/flag" + i + "_s.gif");
		}

	}

	public Image get(Unit unit) {
		// calcul d'apres le pays
		int index = unit.getCountry().getIndex();
		// calcul d'apres le type
		if (unit.getType() == Unit.SHIP) {
			index += 7;
		}
		index += 0;
		return findImage(index);
	}

	public Image get(Board board, Supply supply) {

		int index = board.getOwner(supply).getIndex() + (28);
		return findImage(index);
	}

	public Image get(Country country) {
		int index = country.getIndex() + 35;
		return findImage(index);
	}

	private Image findImage(int index) {
		if (index < 0 || index >= imageUnit.length) {
			return null;
			// throw new ArrayIndexOutOfBoundsException("Image introuvable !");
		}
		return imageUnit[index];
	}



	public Image get(IconType type){
		String path = Config.get(String.valueOf(type));
		
		return get(path);
	}
	
	/**
	 * Provide ImageIcon from relative path
	 * @param path
	 * @return
	 */
	private ImageIcon getIcon(String path) {
		return new ImageIcon(get(path));
	}

	public ImageIcon getIcon(IconType type){
		String path = Config.get(String.valueOf(type));
		
		if (path == null){
			throw new IllegalArgumentException("Image type not found : " + type);
		}
		
		return getIcon(path);
	}
	
	public Image get(String path){
		URL url = ImageProvider.class.getClassLoader().getResource(path);
		return get(url);
	}
	
	
	public Image get(URL url){
		return loadIcon(url).getImage();
	}
	
	private ImageIcon loadIcon(String chemin) {

		URL url = ImageProvider.class.getClassLoader().getResource(chemin);
		return loadIcon(url);
	}

	private ImageIcon loadIcon(URL url){
		
		//cache
		if (this.urlToIcon == null){
			this.urlToIcon = new HashMap<URL,ImageIcon>();
		}
		
		ImageIcon icon = this.urlToIcon.get(url);
		
		if (icon == null){
			icon = new ImageIcon(url);
			this.urlToIcon.put(url,icon);
		}
		
		
		return icon;
	}
	
	/**
	 * Type of icon
	 * @author Jean-Baptiste Vovau
	 *
	 */
	public enum IconType{
		arrowLeft,
		arrowRight,
		greenOk,
		cancel,
		bigLogo,
		newAccount,
		interrogation,
		appIcon,
		zoomMore,
		zoomLess,
		mailSend,
		rules,
		waiting,
		running,
		ended,
		hasPlayed,
		hasNotPlayed
	}
	
}
package diplomacy.gui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

import diplomacy.gui.map.MapPanel;

/**
 * Message to display in the map container.
 * 
 * @author JBVovau
 *
 */
public final class MapMessage {

	private static final Color DEFAULT_COLOR = Color.green;
	private static final int DEFAULT_FONT_SIZE = 36;
	
	private static MapMessage currentMessage;
	private static boolean isshowing = true , ishiding = true;
	private static int showed ; 
	
	private static final long REFRESH_PERIOD = 100;
	private static final int FADE_STEP = 15;
	private static final int REMAIN_STEP = 20;
	
	private static final Timer timer = new Timer("MAP_MESSAGE",true);
	

	private String message ;
	private Image image;
	private MapPanel mappanel;
	private Color background = DEFAULT_COLOR;
	private int fontSize = DEFAULT_FONT_SIZE;
	
	private int opacity = 100;
	
	
	public static void show(MapPanel panel , String msg){
		show(panel, msg, DEFAULT_COLOR , DEFAULT_FONT_SIZE);
	}
	
	/**
	 * Show in the map panel asked message
	 * @param panel
	 * @param msg
	 */
	public static void show(MapPanel panel , String msg, Color color , int fontSize){
		MapMessage mm = new MapMessage(panel, msg);
		mm.background = color;
		mm.fontSize = fontSize;
		
		panel.mapmessage = mm;
		if (currentMessage != null){
			mm.opacity = currentMessage.opacity;
		} else {
			mm.opacity = 0;
		}
		currentMessage = mm;
		
		isshowing = true;
		ishiding = false;
		showed = 0;
		
		timer.schedule(new MapMsgWorker(), 100,REFRESH_PERIOD);
		
	}
	

	public MapMessage(MapPanel panel , String msg){
		this.mappanel = panel;
		this.message = msg;
		
	}

	public Image getImage(){
		if (this.image == null) this.image = this.buildImage();
		return this.image;
	}
	
	/**
	 * Draw message image in the map panel
	 * @param g
	 */
	public void draw(Graphics2D g){
		//position of the message in the visible rectangle
		Rectangle r = this.mappanel.getVisibleRect();
		r.x += 25;
		r.y += 25;
		r.width = 200;
		r.height = 200;
		
		//composite
		//System.out.println(this.opacity);
		float fopacity = (this.opacity) / 100f;

		if (fopacity < 0f) fopacity = 0f;
		if (fopacity >1f) fopacity = 1f;
		
        g.setComposite(AlphaComposite.getInstance(
                AlphaComposite.SRC_OVER,fopacity ));

		g.drawImage(this.getImage(),r.x, r.y, this.mappanel);
	}
	
	/**
	 * Build background image
	 */
	private Image buildImage(){
		Image img = new BufferedImage(1000, 60,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = ((BufferedImage) img).createGraphics();

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		
		//		
		StringBuilder sb = new StringBuilder();
		sb.append(this.message);

		
		//composite
        g.setComposite(AlphaComposite.getInstance(
                AlphaComposite.SRC_OVER, 0.8f));
		

		//text metrics
		Font font = new Font(Font.MONOSPACED, Font.BOLD ,this.fontSize);
		FontMetrics fm = g.getFontMetrics(font);
		Rectangle2D rect = fm.getStringBounds(sb.toString(), g);
		
		int width = (int) Math.round(rect.getWidth());
		int height = (int) Math.round(rect.getHeight());
		
		//shadow rect
		g.setColor(Color.darkGray);
		g.fillRoundRect(1,1, width + 9, height + 1, 4, 4);
		
		//draw rect
		g.setColor(this.background);
		g.fillRoundRect(0,0, width + 9, height, 4, 4);
		
		//drawtext
		g.setColor(Color.black);
		g.setFont(font);
		g.drawString(sb.toString(), 4, 42);

		return img;
	}
	
	private static class MapMsgWorker extends TimerTask{

		@Override
		public void run() {
			if (currentMessage == null) return;
			
			//showed
			if (!ishiding && !isshowing){
				showed++;
				if (showed > REMAIN_STEP) ishiding = true;
				 return;
			}
			
			//showing ?
			if (isshowing){
				if (currentMessage.opacity <= 100){
					//still showing
					currentMessage.opacity = currentMessage.opacity + FADE_STEP;
					currentMessage.mappanel.updateUI();
				} else {
					//showed ! 
					currentMessage.opacity = 100;
					isshowing = false;
					ishiding = false;
					showed = 0;
				}
			} 
			
			if (ishiding)
			{
				//hiding
				if (currentMessage.opacity > 0){
					currentMessage.opacity = currentMessage.opacity - FADE_STEP;
					currentMessage.mappanel.updateUI();
				} else {
					//done
					ishiding = false;
					currentMessage.mappanel.mapmessage = null;
					this.cancel();
				}
			}
			
		}
		
	}
}

package diplomacy.gui;

import java.awt.BorderLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;

import diplomacy.gui.ImageProvider.IconType;

public class FrameDocument extends JFrame {

	private static final String URL_HELP_EN = 
		//"http://docs.google.com/View?id=dcpkm2f2_05tgmqmf4";
		//"https://docs.google.com/document/pub?id=1DrFaVlbwrSpToQLCY2sxJy92C1KJfkQzUx1L_Vr_ch4";
		"http://www.jdiplomacy.net/fasthelp.jsp";
	private static final String URL_RULES = "http://wargaming.bismarckslounge.com/diplomacy/diplomacy_rules.html";
	//private static final String URL_WIKI_RULES = "http://en.wikibooks.org/wiki/Diplomacy/Rules";
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 305082191775951470L;

	private static Map<String,Document> urlToDocument = new HashMap<String,Document>();
	
	private static Map<DocType,FrameDocument> typeToFrame = new HashMap<DocType,FrameDocument>();
	
	private JTextPane textpane ;
	
	
	
	public static FrameDocument get(DocType type){
		
		FrameDocument doc = null;
		
		doc = typeToFrame.get(type);
		
		if (doc == null){
		
			switch(type){
				case help:
					doc = new FrameDocument(URL_HELP_EN);
					break;
				case rules:
					doc = new FrameDocument(URL_RULES);
					break;
				}
		
			typeToFrame.put(type, doc);
		}
		
		if (doc != null) return doc;
		
		throw new IllegalArgumentException("doc type not knwown");
	}
	
	private FrameDocument(final String url){
		
		setIconImage(ImageProvider.getInstance().get(IconType.appIcon));
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		this.setBounds(100,100,600,500);
		this.setTitle("JDiplomacy - Help");
		
		textpane = new JTextPane();
		textpane.setContentType("text/html");
		textpane.setEditable(false);
		textpane.setText("<i>Loading document...</i>");
		
		JScrollPane scroll = new JScrollPane(textpane);
		
		panel.add(scroll, BorderLayout.CENTER);
		this.getContentPane().add(panel);
		
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run(){
				loadDocumentation(url);
			}
		});
	}
	

	private void loadDocumentation(final String link){

		Document doc = urlToDocument.get(link);
		
		//document already in cache
		if (doc != null){
			this.textpane.setDocument(doc);
			return;
		}
		
		try {
			
			
			URL u = new URL(link);
			this.textpane.setPage(u);
			
			urlToDocument.put(link, this.textpane.getDocument());
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException ioex){
			ioex.printStackTrace();
		}

		
	}
	
	
	/**
	 * Type of help
	 * @author Jean-Baptiste Vovau
	 *
	 */
	public static enum DocType {
		help,
		rules
	}
}

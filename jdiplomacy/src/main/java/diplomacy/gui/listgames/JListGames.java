/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package diplomacy.gui.listgames;

import diplomacy.Main;
import diplomacy.access.exception.AccessException;
import diplomacy.game.Game;
import diplomacy.game.GameInfo;
import diplomacy.gui.MainGameInterface;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public class JListGames extends JList implements ListCellRenderer {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7410742983737022427L;
	private GamesModel model;

    public JListGames() {
        this.setCellRenderer(this);
        //this.setM

        this.model = new GamesModel();
        this.model.listComponent = this;
        this.setModel(model);
        this.addMouseListener(new ListGamesMouseAdapter(this));
    }



    /**
     * Rendu de la liste
     * @param list
     * @param value
     * @param index
     * @param isSelected
     * @param cellHasFocus
     * @return
     */
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        JPanel p = new PanelDetailGame((GameInfo) value);

        if (isSelected) {
            p.setBackground(Color.YELLOW);
        } else {
            if (index % 2 == 0) {
                p.setBackground(Color.WHITE);
            } else {
                p.setBackground(MainGameInterface.COLOR_LIST_GREY);
            }
        }

        return p;
    }

    /**
     * Affichage d'un jeu s'il est commencé, ou du formulaire d'inscription
     * si le jeu n'est pas complet
     * @param detail
     */
    public void displayGameDetail(GameInfo detail) {
    }

    /**
     * Ecouteur souris pour l'affichage des jeux.<br/>
     *  - Ecoute le double-click pour afficher le jeu ou le détail.<br/>
     *  - Ecoute la fermeture de la fenêtre d'inscription pour rafraichir la liste.
     * */
    private class ListGamesMouseAdapter extends MouseAdapter implements WindowListener {

        private JList list;

        public ListGamesMouseAdapter(JList list) {
            this.list = list;
         
        }

        /**
         * Ecoute du double click sur la liste
         * @param me
         */
        @Override
        public void mouseClicked(MouseEvent me) {
            if (me.getClickCount() > 1) {
                //double click
                GameInfo detail = (GameInfo) getSelectedValue();
                //jeu démarré
                if (detail.isStarted()) {
                    try {
                        Game game = Main.getDBAccess().openGame(detail);
                        Main.getGameFrame().displayGame(game);
                    } catch (AccessException ex) {
                        Main.logException(ex);
                        Logger.getLogger(JListGames.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    FrameGameInscription.show(detail, this);
                }

            }
        }

        @Override
        public void windowOpened(WindowEvent e) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void windowClosing(WindowEvent e) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void windowClosed(WindowEvent e) {
            ((GamesModel)this.list.getModel()).refreshGames();
        }

        @Override
        public void windowIconified(WindowEvent e) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void windowDeiconified(WindowEvent e) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void windowActivated(WindowEvent e) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void windowDeactivated(WindowEvent e) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}

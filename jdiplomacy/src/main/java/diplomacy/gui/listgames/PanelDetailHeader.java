package diplomacy.gui.listgames;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import diplomacy.language.Language;

public class PanelDetailHeader extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5603942534880007188L;

	public PanelDetailHeader() {
		
		this.setBorder(BorderFactory.createEtchedBorder());
		this.setBackground(Color.lightGray);
		this.setPreferredSize(new Dimension(600, 22));
		
		this.setLayout(null);
		
		JLabel ladID = new JLabel("id");
		ladID.setBounds(10,0,50,20);
		this.add(ladID);
		
		JLabel labStatus = new JLabel(Language.getInstance().get("form.games.status"));
		labStatus.setBounds(74,0,80,20);
		this.add(labStatus);
		
		JLabel labTitle = new JLabel(Language.getInstance().get("form.games.title"));
		labTitle.setBounds(176,0,80,20);
		this.add(labTitle);
		
		JLabel labNbPlayers = new JLabel(Language.getInstance().get("form.games.nbplayers"));
		labNbPlayers.setBounds(375,0,80,20);
		this.add(labNbPlayers);
		
		JLabel labTime = new JLabel(Language.getInstance().get("form.games.duration"));
		labTime.setBounds(448,0,100,20);
		this.add(labTime);
		
		JLabel labDateStart = new JLabel(Language.getInstance().get("form.games.datestart"));
		labDateStart.setBounds(554,0,100,20);
		this.add(labDateStart);
	
		Font font = new Font("Tahoma",Font.PLAIN,12);
		for(Component c : this.getComponents()){
			c.setFont(font);
		}
		
		
	}

}

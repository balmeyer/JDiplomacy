/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.gui.listgames;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import diplomacy.Main;
import diplomacy.access.ListGamesProxy;
import diplomacy.access.TypeListGame;
import diplomacy.game.GameInfo;

/**
 * 
 * @author Jean-Baptiste Vovau
 */
public class GamesModel implements ListModel {

	protected Component listComponent = null;

	private List<GameInfo> games;
	private List<ListDataListener> dataListeners;
	private TypeListGame currentType = null;
	private int postponed = 0;

	private Timer modelTimer;

	public GamesModel() {
		this.dataListeners = new ArrayList<ListDataListener>();
		this.currentType = TypeListGame.myRunningGames;

		// timer de mise à jour
		this.modelTimer = new Timer("GAME_MODEL_TIMER", true);
		this.modelTimer.schedule(buildRefresherTask(), 30000, 30000);

		this.refreshGames();
	}

	/**
	 * Force le nouvel affichage des jeux
	 */
	public void refreshGames() {

		// if component not visible : delay net access
		if (this.listComponent != null && !this.listComponent.isFocusOwner()) {
			if (postponed == 0) {
				postponed = 3;
			} else {
				postponed--;
				if (postponed < 0)
					postponed = 0;
			}
		} else {
			this.postponed = 0;
		}

		if (postponed == 0) {
			Main.out(new Date() + " >>refresh games");
			ListGamesProxy.getInstance().clearCache();
			this.displayGame(currentType);

		}
	}

	/**
	 * Execute l'affichage d'un jeu
	 * 
	 * @param type
	 */
	public void displayGame(TypeListGame type) {
		this.currentType = type;
		// find method
		this.games = ListGamesProxy.getInstance().listGames(type);

		for (ListDataListener ldl : this.dataListeners) {
			ldl.contentsChanged(null);
		}
	}

	private TimerTask buildRefresherTask() {
		TimerTask tt = new TimerTask() {
			@Override
			public void run() {
				refreshGames();
			}
		};

		return tt;
	}

	@Override
	public int getSize() {
		return this.games.size();
	}

	@Override
	public Object getElementAt(int index) {
		return this.games.get(index);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		this.dataListeners.add(l);

	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		this.dataListeners.remove(l);
	}

}

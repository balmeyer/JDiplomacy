/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.gui.creagame;

import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public class TimeBetweenTourComboBoxModel implements ComboBoxModel {

        private List<TimeBetweenTour> list = null;
        private TimeBetweenTour selected;

        public TimeBetweenTourComboBoxModel(){
            list = new ArrayList<TimeBetweenTour>();

            //sans limite
            list.add(new TimeBetweenTour(0, 0, 0));
            //jours
            for (int i = 7 ; i >0 ; i--){
                list.add(new TimeBetweenTour(i,0,0));
            }

            //heures
            list.add(new TimeBetweenTour(0, 12, 0));
            list.add(new TimeBetweenTour(0, 8, 0));
            list.add(new TimeBetweenTour(0, 6, 0));
            list.add(new TimeBetweenTour(0, 3, 0));
            list.add(new TimeBetweenTour(0, 2, 0));
            list.add(new TimeBetweenTour(0, 1, 0));

            //minutes
            list.add(new TimeBetweenTour(0, 0, 45));
            list.add(new TimeBetweenTour(0, 0, 30));
            list.add(new TimeBetweenTour(0, 0, 20));
            list.add(new TimeBetweenTour(0, 0, 15));
            list.add(new TimeBetweenTour(0, 0, 10));
            //list.add(new TimeBetweenTour(0, 0, 5));

            selected = list.get(1);
        }

        @Override
		public void setSelectedItem(Object anItem) {
            this.selected = (TimeBetweenTour) anItem;
        }

        @Override
		public Object getSelectedItem() {
            return selected;
        }

        @Override
		public int getSize() {
            return list.size();
        }

        @Override
		public Object getElementAt(int index) {
            return list.get(index);
        }

        @Override
		public void addListDataListener(ListDataListener l) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
		public void removeListDataListener(ListDataListener l) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }
        
}

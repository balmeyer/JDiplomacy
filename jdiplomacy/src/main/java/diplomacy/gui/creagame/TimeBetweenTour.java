/*
 * TimeBetweenTour.java
 * 
 * Created on 9 oct. 2007, 18:57:15
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.gui.creagame;

import diplomacy.language.Language;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public class TimeBetweenTour {

    private int minutes;
    private String tostring = null;
    
    public TimeBetweenTour(int minutes)  {
        this(0,0,minutes);
    }
    
    public TimeBetweenTour(int day , int hour , int nbMinutes){
        
        //réduit les minutes
        while (nbMinutes >= 60){
            hour++;
            nbMinutes -= 60;
        }
        
        while (hour>= 24){
            day++;
            hour -= 24;
        }
        
        this.minutes=nbMinutes;
        
        this.minutes += (hour * 60);
        this.minutes += (day * 24 * 60);
        
        //TEXTE JOUR
        if (day>0){
            tostring = day + " " + Language.getInstance()
                    .get("form.timetour.day");
            if (day>1) tostring += "s";
            return;
        }
        
        //TEXTE HEURE
        if(hour >0){
            tostring = hour + " "+ Language.getInstance()
                    .get("form.timetour.hour");
            if(hour>1) tostring += "s";
            return;
        }
        
        if (minutes>0){
            tostring = minutes + " " + Language.getInstance()
                    .get("form.timetour.minute") + "s";
            return;
        }
        
        tostring = Language.getInstance().get("form.timetour.unlimited");
        
    }
    
    public int getMinutes(){
        return this.minutes;
    }
    
    @Override
    public String toString(){
        return tostring;
    }
    
}

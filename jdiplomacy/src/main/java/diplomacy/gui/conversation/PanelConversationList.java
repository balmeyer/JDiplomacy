package diplomacy.gui.conversation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Box;
import javax.swing.JScrollPane;

import diplomacy.conversation.Conversation;
import diplomacy.conversation.ConversationEvent;
import diplomacy.conversation.ConversationListener;
import diplomacy.game.Game;

public class PanelConversationList extends PanelConversation implements ConversationListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -372056563754703078L;

	private Box box;
	
	public PanelConversationList(){

		this.box = Box.createVerticalBox();
		JScrollPane scroll = new JScrollPane(this.box);
		scroll.setMaximumSize(new Dimension(650,2000));
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		//this.panel.setLayout(new FlowLayout(FlowLayout.LEADING));
		this.setLayout(new BorderLayout());
		this.add(scroll, BorderLayout.CENTER);
	}
	
	@Override
	public void newConversation(ConversationEvent event) {
		List<ConversationDetail> details = new LinkedList<ConversationDetail>();
		for(Conversation c : event.getMainConversationsOrderByDate()){
			ConversationDetail cd = new ConversationDetail(c);
			details.add(0 , cd);
		}
		
		this.box.removeAll();
		for (ConversationDetail cd : details){
			this.box.add(cd);
		}
		this.updateUI();
	}

	public void setGame(Game game){

	}
	
	private void addConversations(List<Conversation> list){

	}
	
}

package diplomacy.gui.conversation;

import javax.swing.JPanel;

import diplomacy.conversation.ConversationListener;
import diplomacy.game.Game;

public abstract class PanelConversation extends JPanel
implements ConversationListener{

	private static final long serialVersionUID = 4694884961480639733L;

	public static PanelConversation getInstance(){
		return new PanelConversationHtml();
		//return new PanelConversationList();
	}

	public abstract void setGame(Game game);
	
	
}

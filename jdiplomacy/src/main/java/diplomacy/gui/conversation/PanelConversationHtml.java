package diplomacy.gui.conversation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkEvent.EventType;
import javax.swing.event.HyperlinkListener;

import diplomacy.Main;
import diplomacy.conversation.Conversation;
import diplomacy.conversation.ConversationEvent;
import diplomacy.game.Game;
import diplomacy.game.User;
import diplomacy.game.board.Country;
import diplomacy.gui.ImageProvider;
import diplomacy.gui.ImageProvider.IconType;
import diplomacy.language.Language;

public class PanelConversationHtml extends PanelConversation 
		implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7899161622951697532L;
	private static final String PATH_MAIN = "Html/conversation.html";
	private static final String PATH_CONV = "Html/conv-quote.html";
	private static final String PATH_CONV_PRIVATE = "Html/conv-private.html";
	private static final String PATH_CONV_FORBIDDEN = "Html/conv-forbidden.html";
	private static final String PATH_REPLY = "Html/reply.html";

	private static final String[] USERNAME_COLORS = new String[] { "green",
			"red", "purple", "orange", "blue", "grey", "black" };

	private static final String BACKGROUND_COLOR_1 = "#FAFAFA";
	private static final String BACKGROUND_COLOR_2 = "#E0E0E0";

	private JPanel panelUserMessage;
	private JTextPane inputTextPane;
	private JButton btnSend;
	private JPanel privateDestPanel;

	private JTextPane textPane;
	private JScrollPane scroll;
	private Game game;

	// patterns
	private Map<String, String> pathToPattern;

	private Map<Long, String> convToColor;

	// user name to color
	private Map<String, String> userToColor;

	private Map<User, String> userToCountry;

	// Check box to user for private conversation
	private final Map<JCheckBox, User> boxToUser = new HashMap<JCheckBox, User>();

	private boolean textLocked = false;
	private String cleanText = null; // text without reply forms

	private int convCount;

	public PanelConversationHtml() {

		this.panelUserMessage = new JPanel();
		this.panelUserMessage.setPreferredSize(new Dimension(600, 120));
		this.panelUserMessage.setMaximumSize(new Dimension(400, 120));

		this.inputTextPane = new JTextPane();
		this.inputTextPane.setBounds(10, 10, 500, 80);
		this.inputTextPane.setBorder(new EtchedBorder());

		Border etchedBorder = BorderFactory
				.createEtchedBorder(EtchedBorder.LOWERED);
		this.privateDestPanel = new JPanel();
		this.privateDestPanel.setBounds(520, 5, 280, 100);
		this.privateDestPanel.setBorder(BorderFactory.createTitledBorder(
				etchedBorder, "private conversation"));

		this.btnSend = new JButton(Language.getInstance().get(
				"conversation.send"), ImageProvider.getInstance()
				.getIcon(IconType.mailSend));
		this.btnSend.setBounds(10, 90, 120, 26);
		this.btnSend.addActionListener(this);

		this.panelUserMessage.setLayout(null);
		this.panelUserMessage.add(this.btnSend);
		this.panelUserMessage.add(this.inputTextPane);
		this.panelUserMessage.add(this.privateDestPanel);



		this.textPane = new JTextPane();
		this.textPane.setContentType("text/html");
		this.textPane.setEditable(false);
		// base url
		URL base = PanelConversationHtml.class
				.getResource("/Html/conversation.html");
		
		try {
			this.textPane.setPage(base);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.textPane.addHyperlinkListener(new ConversationLinkListener());

		this.setLayout(new BorderLayout());

		scroll = new JScrollPane(this.textPane);

		this.add(panelUserMessage, BorderLayout.NORTH);
		this.add(scroll, BorderLayout.CENTER);

	
		this.addComponentListener(new MyComponentListener());
	}

	/**
	 * Attribute the game to the current conversation
	 * 
	 * @param game
	 */
	public void setGame(Game game) {

		// old
		if (this.game != null) {
			// TODO remove listeners
			
		}

		this.userToCountry = null;
		this.game = game;
		
		this.buildHtml(true);
		this.addUserCheckBox(this.privateDestPanel);
	}

	/**
	 * New conversations receided
	 */
	@Override
	public void newConversation(ConversationEvent cEvent) {

		// last conv has no parent
		boolean doScrollUp = true;
		/*
		 * if (list.size() > 0){ doScrollUp = (list.get(list.size() -
		 * 1).getParent() == null) ; }
		 */

		// TODO Auto-generated method stub
		this.buildHtml(doScrollUp);
	}

	private void initDocument(){
		this.buildHtml(true);

	}
	
	private void buildHtml(boolean scrollup) {
		this.convCount = 0;

		// Main frame
		String frame = this.getPattern(PATH_MAIN);

		StringBuilder sb = new StringBuilder(frame);
		// insert conversations
		this.insertConversation(sb);

		this.setConversationText(sb.toString(), scrollup);

	}

	/**
	 * Place the HTML text in the current JTextPane
	 * 
	 * @param text
	 * @param scrollUp
	 */
	private synchronized void setConversationText(final String text, final boolean scrollUp) {
		if (this.textLocked || text == null)
			return;

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {

				try {
					// catch exception for moment
					textPane.setText(text);
					textPane.updateUI();
					
					if (scrollUp) {
						scroll.getViewport().setViewPosition(new Point(0, 0));
						scroll.updateUI();
					}

				} catch (Exception ex) {
					Main.logException(ex);
					textPane.setText("");
				}

			}
		});



	}

	/**
	 * Insert conversations into the main HTML frame
	 * 
	 * @param sb
	 */
	private void insertConversation(StringBuilder sb) {
		StringBuilder conv = new StringBuilder();

		if (game == null)
			return;

		for (Conversation c : game.getConversationManager()
				.getConversationOrderByUpdate()) {
			// conv
			conv.insert(0, getConvHtml(c));

		}

		int start = sb.toString().indexOf("[CONVERSATION]");
		int end = sb.toString().indexOf("]", start) + 1;

		sb.replace(start, end, conv.toString());

	}

	/**
	 * Return next background color for conversation
	 * 
	 * @param c
	 * @return
	 */
	private String getNextBackgroundColor(Conversation c, boolean isPrivate) {

		if (this.convToColor == null) {
			this.convToColor = new HashMap<Long, String>();
		}

		String result = this.convToColor.get(c);
		if (result != null)
			return result;

		if (c.getParentid() == 0) {
			// root conversation : color is modulo
			if (this.convCount++ % 2 == 0) {
				result = BACKGROUND_COLOR_2;
			} else {
				result = BACKGROUND_COLOR_1;
			}

			this.convToColor.put(c.getId(), result);
			return result;
		}

		// child
		String parentColor = this.convToColor.get(c.getParentid());

		if (parentColor == null) {
			parentColor = getNextBackgroundColor(c.getParent(), isPrivate);
		}

		int index = 0;
		for (Conversation child : c.getParent().getChildren()) {
			if (child.equals(c)) {
				break;
			}
			index++;
		}

		if (index % 2 == 0) {
			result = (parentColor.equals(BACKGROUND_COLOR_1)) ? BACKGROUND_COLOR_2
					: BACKGROUND_COLOR_1;
		} else {
			result = parentColor;
		}

		return result;
	}

	private String getPrivateUsers(Conversation c) {
		StringBuilder sb = new StringBuilder();
		String aux = "";

		for (User u : c.getPrivatelist()) {
			sb.append(aux);
			sb.append(u.getUserName());
			aux = ", ";
		}

		return sb.toString();
	}

	/**
	 * Return color code for user
	 * 
	 * @param user
	 * @return
	 */
	private String getColorForUser(User user) {

		if (this.userToColor == null) {
			this.userToColor = new HashMap<String, String>();
		}

		// get color
		String color = this.userToColor.get(user.getUserName());

		if (color == null) {
			// next free color
			String nextColor = "black"; // default
			for (String dispoColor : USERNAME_COLORS) {
				if (!this.userToColor.values().contains(dispoColor)) {
					nextColor = dispoColor;
					break;
				}
			}
			// set the next color
			this.userToColor.put(user.getUserName(), nextColor);
			color = nextColor;
		}

		return color;
	}

	/**
	 * Listen the button conversation
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String msg = this.inputTextPane.getText();

		// avoid empty message
		if (msg == null || msg.trim().length() == 0)
			return;

		// private user ?
		List<User> privateUsers = null;
		if (this.boxToUser.size() > 0) {
			privateUsers = new ArrayList<User>();
			for (JCheckBox box : this.boxToUser.keySet()) {
				if (box.isSelected()) {
					privateUsers.add(this.boxToUser.get(box));
				}
			}
			privateUsers.add(Main.getUser());

			// public ?
			if (privateUsers.size() <= 1
					|| privateUsers.size() == this.game.getGameInfo()
							.getUsers().size()) {
				privateUsers = null;
			}
		}

		this.textLocked = false;
		game.getConversationManager().newConversation(msg, privateUsers);

		// reset
		this.inputTextPane.setText("");
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				// select all user
				for (JCheckBox box : boxToUser.keySet()) {
					box.setSelected(true);
				}
			}
		});

	}

	/**
	 * Listen for document
	 * 
	 * @author JBVovau
	 * 
	 */
	private class ConversationLinkListener implements HyperlinkListener {

		@Override
		public void hyperlinkUpdate(HyperlinkEvent e) {

			if (e.getEventType() != EventType.ACTIVATED)
				return;

			// cancel reply
			if (e.getDescription().startsWith("cancel") && cleanText != null) {
				setConversationText(cleanText, false);
				textLocked = false;
				return;
			}

			// insert text area below conversation
			if (e.getDescription().startsWith("replyto")) {
				int a = e.getDescription().indexOf("=");
				String id = e.getDescription().substring(a + 1);

				// answer to include
				StringBuilder answer = new StringBuilder(getPattern(PATH_REPLY));
				replaceInPattern(answer, "ID", id);

				String text = textPane.getText();
				cleanText = text;
				text = text.replace("<reply" + id + ">", answer);
				StringBuilder sb = new StringBuilder(text);
				replaceInPattern(sb, "SEND", Language.getInstance().get(
						"conversation.send"));
				replaceInPattern(sb, "CANCEL", Language.getInstance().get(
						"conversation.cancel"));

				setConversationText(sb.toString(), false);
				textLocked = true;
				return;
			}

			// send reply to
			if (e.getDescription().startsWith("sendreplyto")) {
				int a = e.getDescription().indexOf("=");
				String id = e.getDescription().substring(a + 1);

				// fetch text
				String text = textPane.getText();
				int start = text.indexOf("textid" + id);
				if (start < 0)
					return;
				// end
				start = text.indexOf(">", start) + 1;

				int end = text.indexOf("</textarea>", start);
				String myReply = text.substring(start, end).trim();

				textLocked = false;
				if (myReply.trim().length() > 0) {
					game.getConversationManager().newConversation(myReply,
							Long.parseLong(id), null);
				}
				return;

			}
		}

	}

	/**
	 * Listen component
	 * @author JBVovau
	 *
	 */
	private class MyComponentListener implements ComponentListener{

		@Override
		public void componentHidden(ComponentEvent e) {
			
		}

		@Override
		public void componentMoved(ComponentEvent e) {
		}

		@Override
		public void componentResized(ComponentEvent e) {
		}

		@Override
		public void componentShown(ComponentEvent e) {
			if (pathToPattern == null){
				SwingUtilities.invokeLater(
						new Runnable(){
							@Override
							public void run(){
								initDocument();
							}
						});
			}
		}
		
	}
	
	private String getConvHtml(Conversation c) {

		// pattern ?

		StringBuilder detail = null;

		if (c.getPrivatelist().size() == 0) {
			detail = new StringBuilder(getPattern(PATH_CONV));
		} else {
			// authorized ?
			if (c.getPrivatelist().contains(Main.getUser())) {
				detail = new StringBuilder(getPattern(PATH_CONV_PRIVATE));
			} else {
				detail = new StringBuilder(getPattern(PATH_CONV_FORBIDDEN));
			}
		}

		String body = c.getBody();
		if (body != null) {
			body = body.replace("\n", "<br/>");
		}

		this.replaceInPattern(detail, "ID", String.valueOf(c.getId()));
		this.replaceInPattern(detail, "NAME", c.getUser().getUserName());
		this.replaceInPattern(detail, "DATE", Language.getInstance().parseDate(
				c.getWriteDate()));
		this.replaceInPattern(detail, "TEXT", body);
		this.replaceInPattern(detail, "BACKGROUND", getNextBackgroundColor(c,
				false));
		this
				.replaceInPattern(detail, "USERCOLOR", getColorForUser(c
						.getUser()));

		this.replaceInPattern(detail, "REPLY", Language.getInstance().get(
				"conversation.reply"));
		this.replaceInPattern(detail, "PUSERS", getPrivateUsers(c));
		this.replaceInPattern(detail, "RECIPIENTS", Language.getInstance().get(
				"conversation.recipients"));
		this.replaceInPattern(detail, "COUNTRY", getIndexCountryFromUser(c
				.getUser()));
		// children ?
		if (c.getChildren().size() > 0) {
			StringBuilder details = new StringBuilder();
			for (Conversation child : c.getChildren()) {
				String subDetail = "<blockquote>" + getConvHtml(child)
						+ "</blockquote>";
				details.insert(0, subDetail);

			}
			this.replaceInPattern(detail, "SUB", details.toString());
		} else {
			this.replaceInPattern(detail, "SUB", "");
		}

		// add separator
		if (c.getParentid() == 0) {
			detail.append("<hr/>");
		}

		return detail.toString();
	}

	private void replaceInPattern(StringBuilder sb, String name, String value) {
		String patToReplace = "[" + name.toUpperCase() + "]";

		while (sb.indexOf(patToReplace) >= 0) {

			int a = sb.indexOf(patToReplace);
			int end = a + patToReplace.length();

			sb.replace(a, end, value);

		}

	}

	private String getPattern(String path) {
		if (this.pathToPattern == null) {
			this.pathToPattern = new HashMap<String, String>();
		}

		String text = this.pathToPattern.get(path);
		if (text != null)
			return text;

		// load pattern
		try {

			URL u = PanelConversationHtml.class.getClassLoader().getResource(path);

			BufferedReader reader = new BufferedReader(new InputStreamReader(u
					.openStream()));
			String line = null;
			StringBuilder sb = new StringBuilder();

			do {
				line = reader.readLine();
				if (line != null)
					sb.append(line);
			} while (line != null);
			reader.close();

			this.pathToPattern.put(path, sb.toString());
			return sb.toString();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Add check box with user name
	 * 
	 * @param panel
	 */
	private void addUserCheckBox(final JPanel panel) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				if (game != null) {

					panel.removeAll();
					boxToUser.clear();
					panel.setLayout(null);

					// current user
					User me = Main.getUser();

					int x = 0;
					int y = 0;

					//

					for (User u : game.getGameInfo().getUsers()) {
						if (u.equals(me))
							continue;

						JCheckBox box = new JCheckBox(u.getUserName(), true);
						box.setBounds(x + 10, y + 20, 140, 20);

						panel.add(box);
						boxToUser.put(box, u);

						y += 25;
						if (y >= 75) {
							y = 0;
							x = 150;
						}
					}

				}

			}

		});

	}

	/**
	 * Return country associated with user (first country if many)
	 * 
	 * @param u
	 * @return
	 */
	private String getIndexCountryFromUser(User u) {

		if (this.userToCountry == null) {
			this.userToCountry = new HashMap<User, String>();
		}

		String c = this.userToCountry.get(u);
		if (c != null)
			return c;

		// fetch country
		for (Country country : Country.getCountries()) {
			if (u.equals(game.getUser(country))) {
				c = String.valueOf(country.getIndex());
				this.userToCountry.put(u, c);
				return c;
			}
		}

		return null;
	}

}

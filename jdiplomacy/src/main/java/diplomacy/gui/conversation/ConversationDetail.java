package diplomacy.gui.conversation;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import diplomacy.conversation.Conversation;

public class ConversationDetail extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8592263559758185997L;

	private JButton label ;
	
	public ConversationDetail(Conversation c){
		super(true);
		this.setBorder(BorderFactory.createLineBorder(Color.lightGray));
		this.setPreferredSize(new Dimension(600,50));
		this.label = new JButton(c.getBody());
		this.label.setPreferredSize(new Dimension(300,40));
		this.add(this.label);
	}
	
}

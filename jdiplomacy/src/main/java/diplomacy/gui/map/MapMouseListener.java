package diplomacy.gui.map;

import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import diplomacy.game.Board;
import diplomacy.game.board.Region;

/**
 * Classe abstraite à l'écoute des mouvements de la souris sur la carte.
 * Prend en charge le déplacement de la carte
 * @author JBVovau
 *
 */
public class MapMouseListener extends MouseAdapter {

    private MapPanel mapPanel;


    private double startX ;
    private double startY;

    public MapMouseListener(MapPanel panel){
        this.mapPanel = panel;
        this.mapPanel.setCursor(Cursor.getDefaultCursor());
    }

    public Board getCurrentBoard(){
        return this.mapPanel.getCurrentBoard();
    }

    @Override
    public void mousePressed(MouseEvent e){
        this.startDragMap(e);
    }

    @Override
    public void mouseDragged(MouseEvent e){
        this.doDragMap(e);


    }

    @Override
    public void mouseReleased(MouseEvent e){
        this.mapPanel.setCursor(Cursor.getDefaultCursor());

    }

    @Override
    public void mouseMoved(MouseEvent e){
    	Region r = mapPanel.getRegionFromCoord(e.getX(), e.getY());
    	if (r != null){
    		this.mapPanel.setRegionName(r);
    	} else{
    		this.mapPanel.setRegionName(null);
    	}
    	
    }
    
    /**
     * Commence un déplacement de carte
     * @param x
     * @param y
     */
    public void startDragMap(MouseEvent e){
        this.startX = e.getX();
        this.startY = e.getY();


    }

    public void doDragMap(MouseEvent e){
        double x = e.getX();
        double y = e.getY();

        double xoffset = x - this.startX ;
        double yoffset = y - this.startY ;

        Rectangle visibleRect = this.mapPanel.getVisibleRect();

        double xrect =  visibleRect.getX();
        double yrect =  visibleRect.getY();

        xrect -= xoffset;
        yrect -= yoffset;

        int left = (int) Math.round(xrect);
        int top = (int) Math.round(yrect);
        int width = (int) Math.round(visibleRect.getWidth());
        int height = (int) Math.round(visibleRect.getHeight());


        Rectangle rect = new Rectangle(
              left,
              top,
              width,
              height);


        this.mapPanel.setCursor(
                Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));

        this.mapPanel.scrollRectToVisible(rect);
        this.mapPanel.updateUI();

    }

}

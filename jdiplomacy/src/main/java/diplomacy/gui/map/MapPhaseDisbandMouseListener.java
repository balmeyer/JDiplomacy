package diplomacy.gui.map;

import java.awt.event.MouseEvent;

import diplomacy.Main;
import diplomacy.game.Unit;
import diplomacy.game.board.Region;
import diplomacy.order.Order;
import diplomacy.order.OrderSet;

/**
 * Ecoute des infos de retraite
 * @author JBVovau
 *
 */
public class MapPhaseDisbandMouseListener extends MapMouseListener {

    private MapPanel panel;

    public MapPhaseDisbandMouseListener(MapPanel panel) {
        super(panel);
        this.panel = panel;
    }

    /**
     * Souris press�e : selectionne l'unit�
     * @param me
     */
    @Override
    public void mousePressed(MouseEvent me) {
        //capte la région de la retraite
        Region disbandRegion = panel.getRegionFromCoord(me.getX(), me.getY());

        panel.typeOrdre = Order.ORDRE_MOUVEMENT;

        Unit u = null;

        if (disbandRegion != null) {
            //recherche l'untié qui retraite
            //u = disbandRegion.getProvince().getDisbandedUnit();
            u = getCurrentBoard()
                    .getDislodgedUnit(disbandRegion.getProvince());

        }

        //is unit and owned unit ? 
        if (u == null || !getCurrentBoard().controls(Main.getUser(), u)) {
            disbandRegion = null;
            this.startDragMap(me);
            u = null;
        } 

        panel.selectedRegion = disbandRegion;
        panel.startRegion = disbandRegion;
        panel.endRegion = null;

        panel.highlightNearRegions(u);
        panel.forceRebuild();
        panel.updateUI();

    }

    @Override
    public void mouseDragged(MouseEvent me) {
        if (panel.startRegion != null) {
            panel.setCursorPosition(me.getX(), me.getY());
            panel.updateUI();
        } else {
            this.doDragMap(me);
        }
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        if (this.panel.getCurrentGame() == null) {
            return;
        }

        //Un ordre complet valable ?
        if (panel.startRegion != null && panel.endRegion != null) {

            //construction del 'ordre
            Unit disbandUnit = //panel.startRegion.getProvince().getDisbandedUnit();
                getCurrentBoard().getDislodgedUnit(panel.startRegion.getProvince());
            if (disbandUnit != null) {
                StringBuilder sb = new StringBuilder();
                sb.append(disbandUnit.getCodeChar());
                sb.append(' ');
                sb.append(disbandUnit.getRegion().getName());
                sb.append(" - ");
                sb.append(panel.endRegion.getName());
                Order dis = Order.create(this.getCurrentBoard(),sb.toString());
                this.panel.getCurrentGame().getCurrentTour().getOrderSet().addOrder(dis);
            }
            panel.selectedRegion = null;

        } else {
            //teste si on "détruit" l'ordre pour la région
            if (panel.selectedRegion != null) {
                OrderSet oSet = panel.getCurrentGame().getCurrentTour().getOrderSet();
                if (oSet != null) {
                    oSet.removeOrder(panel.selectedRegion);
                }
            }
        }

        panel.startRegion = null;
        panel.endRegion = null;
        panel.highlightNearRegions(panel.startRegion);
        panel.forceRebuild();
        panel.updateUI();
    }
}

/*
 * Created on 14 avr. 2007
 *
 *
 */
package diplomacy.gui.map;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import diplomacy.gui.MainGameInterface;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public class MapKeyListener extends KeyAdapter {

	public MapKeyListener(MainGameInterface frame){
		this.map = frame.getMapPan();
		this.map.addKeyListener(this);
		frame.addKeyListener(this);
		//Main.getDesktop().get().addKeyListener(this);
		
	}
	
	@Override
	public void keyTyped(KeyEvent e){
		if (e.getKeyChar() == 8 && map.selectedRegion != null){
			//annule  l'ordre
			//map.userOrderSet.removeOrder(map.selectedRegion);
			map.selectedRegion = null;
		}
		
	}
	
	
	private MapPanel map;
}

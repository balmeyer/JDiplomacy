package diplomacy.gui.map;

import java.awt.event.MouseEvent;

import diplomacy.game.board.Region;
import diplomacy.order.Order;
import diplomacy.game.Ship;
import diplomacy.game.Unit;

/**
 * 
 * Ecoute des mouvements pour l'ajustement
 * @author JBVovau
 *
 */
public class MapPhaseAjustementMouseListener extends MapMouseListener {

    public MapPhaseAjustementMouseListener(MapPanel panel) {
        super(panel);
        this.panel = panel;
    }


    @Override
    public void mousePressed(MouseEvent e){
        this.startDragMap(e);
    }

    @Override
    public void mouseDragged(MouseEvent e){
        this.doDragMap(e);
    }

    /**
	 * Selection
	 */
	@Override
	public void mouseClicked(MouseEvent me) {
	    if (this.panel.getCurrentGame() == null) return;
	    
	    //région clickée
	    Region r = this.panel.getRegionFromCoord(me.getX(), me.getY());
	
	    //teste si région est nulle
		/*
	    if (r == null){
	    Region s = this.panel.getRegionFromCoord(me.getX(), me.getY());
	    if (s != null){
	    r = s.getProvince().getMainRegion();
	    }
	    }*/
	
	    if (r != null) {
	        //ajout ou supression ?
	        //Unit u = r.getProvince().getUnit();
	        Unit u = this.getCurrentBoard().getUnit(r);
	        StringBuilder sb = new StringBuilder();
	        if (u == null) {
	            //AJOUT
	            sb.append("+ ");
	            //type d'unité ?
	            if (me.getButton() == 1 || !r.acceptUnit(Ship.class)) {
	                sb.append("A ");
	            } else {
	                sb.append("F ");
	            }
	            sb.append(r.getProvince().getShortName());
	        } else {
	            //SUPPRESSION
	            sb.append("- ");
	            sb.append(u.getCodeChar());
	            sb.append(' ');
	            sb.append(r.getProvince().getShortName());
	        }
	
	        //met l'ordre
	        Order o = Order.create(this.getCurrentBoard(),sb.toString());
	
	
	
	        //teste si ajout ou suppression
	        //teste s'il faut supprimer ou ajouter l'ordre
	        Order ancienOrdre = this.panel.getCurrentGame()
	                .getCurrentTour().getOrderSet()
	                .getOrderFromBase(r);
	        if (ancienOrdre != null) {
	            //supprime l'ancien ordre
	            this.panel.getCurrentGame().getCurrentTour()
	                    .getOrderSet().removeOrder(r);
	            //determine si l'ordre doit être remplacé par un autre type d'unit� ou si suppression simple
	            if (ancienOrdre.getTypeUnit() == o.getTypeUnit()) {
	
	                return;
	            }
	        }
	
	                    //TODO ajuste les difs
	        //teste avant d'ajouter l'ordre
	        if (o.getOrderType() == Order.ORDRE_ADD_UNIT
	                && this.panel.getAdjustMapValue(o.getCountry()) <= 0) {
	            return;
	        }
	
	        if (o.getOrderType() == Order.ORDER_DEL_UNIT
	                && this.panel.getAdjustMapValue(o.getCountry()) >= 0) {
	            return;
	        }
	
	        //ajoute l'ordre 
	        this.panel.getCurrentGame().getCurrentTour().getOrderSet().addOrder(o);
	    }
	
	}

    @Override
    public void mouseMoved(MouseEvent me) {
    	super.mouseMoved(me);
    	
        panel.startRegion = null;

        //région en évidence
        Region r = this.panel.getRegionFromCoord(me.getX(), me.getY());

        //région non trouvée
        if (r == null) {

            panel.updateUI();
            return;
        }

        //selection de l'ordre
        if (getCurrentBoard().getUnit(r) != null
                && getCurrentBoard().getAdjustDifference(r) < 0) {
            panel.startRegion = r;
            panel.typeOrdre = Order.ORDER_DEL_UNIT;
            panel.updateUI();
            return;
        }

        if (r.getProvince().hasRootSupply()
                && !getCurrentBoard().isOccupiedByUnit(r)) {
            panel.startRegion = r;
            panel.typeOrdre = Order.ORDRE_ADD_UNIT;
            panel.updateUI();
            return;
        }
    }
    
    private MapPanel panel;
}

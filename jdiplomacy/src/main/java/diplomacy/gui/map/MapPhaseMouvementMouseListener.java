package diplomacy.gui.map;

import diplomacy.Main;
import diplomacy.gui.map.MapPanel;
import java.awt.event.MouseEvent;
import diplomacy.game.board.Region;
import diplomacy.order.Order;
import diplomacy.order.OrderSet;
import diplomacy.game.Unit;
import java.awt.event.InputEvent;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 *
 */
//=========================================================
/** Ecouteur de la carte. S'occupe de toutes les actions sur la carte
 * durant les phases de mouvement  */
public final class MapPhaseMouvementMouseListener extends MapMouseListener {

    //déclarations
    private MapPanel mapPanel;
    //private GameFrame myMap;

    //move need a convy
    private boolean moveAsConvoy;
    private boolean forceControllButton ;
    
    /** constructeur */
    public MapPhaseMouvementMouseListener(MapPanel mapPanel) {
        super(mapPanel);
        //this.myMap = myMap;
        this.mapPanel = mapPanel;
    //mapPanel.addMouseListener(this);
    //mapPanel.addMouseMotionListener(this);
    }


    /**
     * Bouton de souris cliqué :
     *
     * <br><br>bouton gauche = unit de base selectionnée
     * <br>bouton droit = support sur unité qui reste en place
     *
     * */
    @Override
    public void mouseClicked(MouseEvent e) {
        //Evite de traiter deux fois la même action
        //if (actionOK) return ;


        //TODO passer le numéro de bouton en un shift / 

        //Selection d'une unité
        if (e.getButton() == 1) {
            //capte la région à l'origine de l'ordre
            mapPanel.selectedRegion = mapPanel.getRegionFromCoord(e.getX(), e.getY());
            mapPanel.startRegion = null;
            mapPanel.endRegion = null;

            //nombre de click et type d'ordre : double-click = convoi
            mapPanel.typeOrdre = Order.ORDRE_MOUVEMENT;
            

            //controle d'unité
            if (mapPanel.selectedRegion != null){
                Unit unit = //mapPanel.selectedRegion.getProvince().getUnit();
                    getCurrentBoard().getUnit(mapPanel.selectedRegion);
                if (!getCurrentBoard().controls(Main.getUser(), unit) ){
                    mapPanel.selectedRegion = null;
                } 
                
                if (e.getClickCount() > 1 
                		&& unit != null 
                		&& unit.canConvoy()){
                	this.mapPanel.typeOrdre = Order.ORDRE_CONVOI;
                	this.forceControllButton = true;
                	
                }
                
                
            }

        } else {
            //création d'un support pour une unité qui ne bouge pas
            if (mapPanel.selectedRegion != null) {
                mapPanel.startRegion = mapPanel.getRegionFromCoord(e.getX(), e.getY());
                //teste si région destination ok
                if (mapPanel.startRegion != null && !mapPanel.selectedRegion.equals(mapPanel.startRegion)) {

                    mapPanel.endRegion = mapPanel.startRegion;
                    mapPanel.typeOrdre = Order.ORDRE_SOUTIEN;
                    this.mouseReleased(e);
                }
            }
        }

        mapPanel.forceRebuild();
        mapPanel.updateUI();
    }

    /**
     * Drag : enregistre le mouvement
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        //teste si unité
        if (mapPanel.startRegion != null) {
            //flag pour convoi
        	boolean newFlagMoveASConvoy = false;
        	
            //teste le type de mouvement
            int ex = e.getModifiers();
            if ((ex & InputEvent.CTRL_MASK) != 0 && mapPanel.typeOrdre != Order.ORDRE_SOUTIEN) {

                Unit startUnit = getCurrentBoard().getUnit(mapPanel.startRegion);
                Unit selectedUnit = getCurrentBoard().getUnit(mapPanel.selectedRegion);
                //unité peut convoyer ?

                    //force un convoi
                newFlagMoveASConvoy = startUnit.canBeConvoyed();
                

                //unit qui convoi ?
                if (selectedUnit.canConvoy() && !mapPanel.selectedRegion.equals(mapPanel.startRegion)) {
                	newFlagMoveASConvoy = true;
                    mapPanel.typeOrdre = Order.ORDRE_CONVOI;
                }

            }
            

            if (!newFlagMoveASConvoy && this.moveAsConvoy){
            	//rebuild near region
            	mapPanel.highlightNearRegions(mapPanel.startRegion);
            	mapPanel.forceRebuild();
            }
            this.moveAsConvoy = newFlagMoveASConvoy || this.forceControllButton;
            
            mapPanel.setCursorPosition(e.getX(), e.getY());

            //si soutien : on affiche les régions proches du curseurs
            if (this.moveAsConvoy || mapPanel.typeOrdre == Order.ORDRE_CONVOI) {
                mapPanel.highlightRegionsCurseurConvoi();
                mapPanel.forceRebuild();
            }
            mapPanel.updateUI();
        } else {

            //drage la map
            this.doDragMap(e);
        }


    }

    /**
     * Bouton de souris pressé : capte le début de mouvement
     */
    @Override
    public void mousePressed(MouseEvent e) {
        //debut de l'action
        this.startDragMap(e);

        //origin region
        mapPanel.startRegion = mapPanel.getRegionFromCoord(e.getX(), e.getY());

        if (mapPanel.startRegion != null) {

            //find the selected unit
            Unit testUnitStart = getCurrentBoard().getUnit(mapPanel.startRegion);
            
            //no unit found : cancel highlight region
            if (testUnitStart == null) {
                mapPanel.startRegion = null;
                mapPanel.highlightNearRegions((Region) null);
                mapPanel.forceRebuild();
                return;
            }

            //controle Convoi / Support
            boolean controlButton = this.forceControllButton || (e.getModifiers() & InputEvent.CTRL_MASK) != 0;
            boolean shiftButton = (e.getModifiers() & InputEvent.SHIFT_MASK) != 0;

            //soutien ? Possible aussi avec bouton deux
            if (e.getButton() >= MouseEvent.BUTTON2) shiftButton = true;

            //test if unit is controller by player
            if (!controlButton && !shiftButton 
                    && !getCurrentBoard().controls(Main.getUser() ,testUnitStart)) {
                mapPanel.startRegion = null;
                mapPanel.selectedRegion = null;
                return;
            }

            //si la region de base est nulle : elle devient la région de départ
            //type = MOUVEMENT
            if (!controlButton && !shiftButton) {
                mapPanel.selectedRegion = mapPanel.startRegion;
                mapPanel.typeOrdre = Order.ORDRE_MOUVEMENT;
                //mise en évidence des régions voisines
                mapPanel.highlightNearRegions(mapPanel.startRegion);
            } else {
                //teste l'untié de départ
                if (mapPanel.selectedRegion == null) {
                    mapPanel.selectedRegion = mapPanel.startRegion;
                }
                
                //when right button is pressed and : start regions
                //and selected regions are identical : transform support to convoy
                if (mapPanel.selectedRegion != null 
                		&& mapPanel.selectedRegion.equals(mapPanel.startRegion)
                		&& shiftButton
                		){
                	shiftButton = false;
                	controlButton = true;
                	this.forceControllButton = true;
                }
                
                //
                Unit unitSelected = //mapPanel.selectedRegion.getProvince().getUnit();
                    getCurrentBoard().getUnit(mapPanel.selectedRegion);
                //force un CONVOI si la touche controle est enfoncé ?
                if (controlButton) {
                    if (unitSelected.canConvoy() && testUnitStart.canBeConvoyed()) {
                        mapPanel.typeOrdre = Order.ORDRE_CONVOI;

                    } else {
                        //teste s'il s'agit d'une armée
                        if (unitSelected.canBeConvoyed()){
                            mapPanel.highlightRegionsCurseurConvoi();
                        } else {
                            mapPanel.startRegion = null;
                            return;
                        }
                    }
                }

                if (shiftButton) {
                    //SUPPORT
                    mapPanel.typeOrdre = Order.ORDRE_SOUTIEN;
                    
                	//to convoy possible : comon region
                	mapPanel.highlightCommonRegions(mapPanel.startRegion, mapPanel.selectedRegion);
                	
                	/*
                	 * code archive
                    if (true || !mapPanel.startRegion.isConvoyStop() || testUnitStart.getType() == Unit.SHIP ) {

                    } else {
                    	//convoy possible
                    	mapPanel.highlightNearRegions(mapPanel.selectedRegion);
                    }
					*/
                }
            }

            mapPanel.setCursorPosition(e.getX(), e.getY());

            //rafraichit la fenetre pour dessiner les flèches
            mapPanel.forceRebuild();
            //mapPanel.updateUI();
        }
    }

    /**
     * Souris relachée : calcule le type de mouvement
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        super.mouseReleased(e);


        //if (actionOK) return;
        //Un ordre complet valable ?
        if (mapPanel.startRegion != null && mapPanel.endRegion != null) {
            mapPanel.pumpOrder();
            mapPanel.selectedRegion = null;
        } else {
            //teste si on "détruit" l'ordre pour la r�gion
            if (mapPanel.selectedRegion != null) {
                OrderSet oSet = this.mapPanel.getCurrentGame().getCurrentTour().getOrderSet();
                if (oSet != null) {
                    oSet.removeOrder(mapPanel.selectedRegion);
                }
            }
        }

        mapPanel.startRegion = null;
        mapPanel.endRegion = null;
        mapPanel.highlightNearRegions(mapPanel.startRegion);
        mapPanel.forceRebuild();
        mapPanel.updateUI();
        
        this.moveAsConvoy = false;
        this.forceControllButton = false;
    }
}
/*
 * Created on 26 mars 2007
 *
 *
 */
package diplomacy.gui.map;

import diplomacy.gui.ImageProvider;
import diplomacy.gui.map.MapPanel;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.List;

import diplomacy.game.board.Region;
import diplomacy.game.Tour;
import diplomacy.order.Order;
import diplomacy.order.OrderSet;
import diplomacy.game.Unit;

/**
 * 
 * Objet chargé de dessiner les ordres dans un contexte graphique.
 * 
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 * 
 *  
 */
public final class OrderDrawer {

    public OrderDrawer() {

        //pinceau soutien
        float[] dashPhase = {8, 3};
        this.soutienStroke = new BasicStroke(5, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_BEVEL, 0, dashPhase, 8);

        //Pinceau unité effacé
        this.delUnitStroke = new BasicStroke(5);

    }

    /**
     * Dessine une série d'ordre.
     * 
     * @param set
     * @param g
     */
    public void drawOrders(final Tour tour, final Graphics2D gMap) {

        //Controle si changement

		/*
         * if (cachedImage != null && set.toString().equals(lastOrderSet)){
         * gMap.drawImage(cachedImage , 0,0,null); return; }
         */

        OrderSet set = tour.getOrderSet();

        if (set == null) {
            return;
        }

        //-------------------
        //Création de la carte
        BufferedImage img = new BufferedImage(MapPanel.MAP_WIDTH,
                MapPanel.MAP_WIDTH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g = img.createGraphics();

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);

        Collection<Order> col = set.getOrders();
        
        synchronized (col) {
            for (Order o : col) {
                //si incorrect : on continue
                if (!o.isVisible() || !o.isCorrect()) {
                    continue;
                }

                //régions de lo'rdre
                Region start = o.getStartRegion();
                Region end = o.getEndRegion();
                Region base = o.getActionRegion();

                //besoin de flèche ?
                boolean drawArrow = true;
                //dessin d'un convoi
                boolean drawConvoi = false;

                //DESSIN DES MOUVEMENTS
                switch (o.getOrderType()) {
                    //-------------------
                    case Order.ORDRE_MOUVEMENT:
                        //Deux cas de mouvement : classique et convoi
                        if (set.getTour().isFinished() && o.getConvoiPath().size() > 0) {
                            g.setColor(COULEUR_CONVOI);
                            drawConvoi = true;
                            drawArrow = false;
                        } else {
                            if (o.isSuccess()){
                                g.setColor(COULEUR_ATTAQUE);
                            } else {
                                g.setColor(COULEUR_ATTAQUE_ECHEC);
                            }
                        }
                        //pinceau selon la force de l'attaque
                        Stroke ms = new BasicStroke(6 + 2 * (o.getForce() - 1));
                        g.setStroke(ms);
                        break;
                     //-------------------
                    case Order.ORDRE_CONVOI:
                        //si le tour est terminé, on ne dessine pas les ordres de convoi
                        if (set.getTour().isFinished()) {
                            drawArrow = false;
                            break;
                        }
                     //-------------------
                    case Order.ORDRE_SOUTIEN:
                        if (o.getOrderType() == Order.ORDRE_CONVOI) {
                            g.setColor(COULEUR_CONVOI);
                        } else {
                            g.setColor(COULEUR_SOUTIEN);
                        }

                        g.setStroke(soutienStroke);
                        //calcul du milieu de la flèche
                        int middleX = start.getX() + ((end.getX() - start.getX()) / 2);
                        int middleY = start.getY() + ((end.getY() - start.getY()) / 2);
                        g.drawLine(base.getX(), base.getY(), middleX, middleY);
                        //soutien d'une unité qui ne bouge pas
                        if (start.equals(end)) {
                            g.fillOval(middleX - 10, middleY - 10, 20, 20);
                        }
                        break;
                    //-------------------
                    case Order.ORDRE_ADD_UNIT:
                        drawArrow = false;
                        Image newUnit = (o.getTypeUnit() == Unit.ARMY) ? o.getCountry().getBigArmyImage() : o.getCountry().getBigFloatImage();
                        Image arrowNewUnit = ImageProvider.getInstance().get("Images/newunit.png");
                        Composite compo = g.getComposite();
                        g.setComposite(AlphaComposite.getInstance(
                                AlphaComposite.SRC_OVER, 0.6f));

                        g.drawImage(newUnit, base.getX() - 18, base.getY() - 18,
                                null);
                        //Restaure l'élément composite par défaut
                        g.setComposite(compo);
                        g.drawImage(arrowNewUnit, base.getX() - 24,
                                base.getY() - 8, null);

                        break;
                     //-------------------
                    case Order.ORDER_DEL_UNIT:
                        //dessin de la suppression
                        g.setColor(COULEUR_ATTAQUE);
                        g.setStroke(delUnitStroke);
                        g.drawLine(base.getX() - 14, base.getY() - 14,
                                base.getX() + 14, base.getY() + 14);
                        g.drawLine(base.getX() - 14, base.getY() + 14,
                                base.getX() + 14, base.getY() - 14);
                        drawArrow = false;
                        break;
                }

                //dessin de la fléche de mouvement
                if (drawArrow && start != null && end != null) {
                    //dessin des retraites
                    int offsetDisband = (tour.getPhase() == Tour.PHASE_FALL_DISBAND || tour.getPhase() == Tour.PHASE_SPRING_DISBAND) ? 8 : 0;
                    //g.setStroke(moveStroke);
                    drawArrow(g, start.getX() + offsetDisband, start.getY() + offsetDisband, end.getX(), end.getY());
                }

                //dessin du convoi
                if (drawConvoi && o.isSuccess()) {
                    List<Region> liste = o.getConvoiPath();
                    start = o.getStartRegion();
                    //cas de l'armée en SPA
                    //start = start.getProvince().getUnit().getRegion();
                    end =  o.getEndRegion();
                    Region nextStep = null;
                    for (int i = 0; i < liste.size() ; i++) {
                        //prochaine région
                        nextStep = liste.get(i);
                        //dessin de l'étape
                        g.drawLine(start.getX(), start.getY(),
                                nextStep.getX(), nextStep.getY());
                        start = nextStep;
                    }
                    //Dessin final : flèche
                    drawArrow(g, start.getX(), start.getY(), end.getX(), end.getY());
                }

            }

        }
        
        //dessine la liste des mouvements sur la carte
        //ajoute la transparence
        Composite oldCompo = gMap.getComposite();
        Composite trans = AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
                0.7f);
        gMap.setComposite(trans);
        gMap.drawImage(img, 0, 0, null);
        gMap.setComposite(oldCompo);

    }

    /**
     * Dessin d'une flèche
     * @param g
     * @param x
     * @param y
     * @param a
     * @param b
     */
    public static final void drawArrow(Graphics2D g, int x, int y, int a, int b) {

    	//gap for shadow
    	final int shadowGap = 1;
    	
        //on sort si les coordonnées sontidentiques
        if (x == a && y == b) {
            return;
        }

        Color current = g.getColor();
        Color shadow = Color.GRAY;
        
        //fin de fl�che
        double xOffset = a - x;
        double yOffset = b - y;

        //longueur trait
        double d = Math.sqrt(Math.pow(xOffset, 2) + Math.pow(yOffset, 2));
        double dAcos = Math.acos(xOffset / d);
        double dAsin = Math.asin(yOffset / d);

        //Calcul de l'angle
        double angle = dAcos;//+ Math.PI / 4.0;
        if (dAsin < 0) {
            angle = (2 * Math.PI) - angle;
        }

        //ligne de la fl�che
        //fin de fl�che : distance - 12
        double xp = x + (Math.cos(angle) * (d - 12));
        double yp = y + (Math.sin(angle) * (d - 12));
        
        //dessin de la t�te de la fl�che
        double angle1 = angle + (Math.PI * 0.9);
        double angle2 = angle + (Math.PI * 1.1);
        double x1 = Math.cos(angle1) * 30;
        double y1 = Math.sin(angle1) * 30;
        double x2 = Math.cos(angle2) * 30;
        double y2 = Math.sin(angle2) * 30;

        
        //-------------------
        //shadow
        g.setColor(shadow);
        g.drawLine(x+shadowGap, y+shadowGap, (int) Math.round(xp) + shadowGap, (int) Math.round(yp) + shadowGap);

        //polygon for shadow
        Polygon polyShadow = new Polygon();
        polyShadow.addPoint(a + shadowGap, b + shadowGap);
        polyShadow.addPoint((int) Math.round(a + x1) + shadowGap, (int) Math.round(b + y1) + shadowGap);
        polyShadow.addPoint((int) Math.round(a + x2) + shadowGap, (int) Math.round(b + y2) + shadowGap);

        g.fillPolygon(polyShadow);
        
        //---------------------
        //arrow
        g.setColor(current);
        g.drawLine(x, y, (int) Math.round(xp), (int) Math.round(yp));


        //polygon
        Polygon poly = new Polygon();
        poly.addPoint(a, b);
        poly.addPoint((int) Math.round(a + x1), (int) Math.round(b + y1));
        poly.addPoint((int) Math.round(a + x2), (int) Math.round(b + y2));
        

        
        //final 
        g.setColor(current);
        g.fillPolygon(poly);

    }
    
    private Stroke soutienStroke;
    private Stroke delUnitStroke;
    private static final Color COULEUR_ATTAQUE = new Color(255, 10, 10);
    private static final Color COULEUR_ATTAQUE_ECHEC = new Color(128,40,255,200);
    private static final Color COULEUR_SOUTIEN = new Color(220, 120, 30);
    //private static final Color COULEUR_DELETE_UNIT = new Color(255, 0, 0, 160);
    private static final Color COULEUR_CONVOI = new Color(0, 0, 255);

}

/*
 * Created on 5 mars 2007
 *
 *
 */
package diplomacy.gui.map;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.JPanel;

import diplomacy.Main;
import diplomacy.game.Board;
import diplomacy.game.Game;
import diplomacy.game.GameListener;
import diplomacy.game.Tour;
import diplomacy.game.Unit;
import diplomacy.game.User;
import diplomacy.game.board.Country;
import diplomacy.game.board.Region;
import diplomacy.game.board.Supply;
import diplomacy.graph.Graph;
import diplomacy.graph.GraphNode;
import diplomacy.gui.MainGameInterface;
import diplomacy.gui.ImageProvider;
import diplomacy.gui.MapMessage;
import diplomacy.gui.ProvinceNameImage;
import diplomacy.gui.ScrollMap;
import diplomacy.order.CountryOrders;
import diplomacy.order.Order;
import diplomacy.order.OrderFetcherListener;
import diplomacy.order.OrderSet;
import diplomacy.order.OrderSetListener;
import diplomacy.tools.MapBuilder;

/**
 * Panel for rendering JDiplomacy map, and listening orders.
 * 
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 */
public class MapPanel extends JPanel implements OrderSetListener, GameListener,
		OrderFetcherListener {

	private static final long serialVersionUID = 1392774901030989445L;
	private Game currentGame;
	private Board board;

	protected double zoomScale = 1.0;
	private OrderSet userOrderSet; //user orders set
	protected Region selectedRegion = null; //regions selected for orders
	protected Region startRegion = null;
	protected Region endRegion = null;

	protected int typeOrdre = 0;

	private int currentX; //cursor position
	private int currentY;
	
	private static Stroke mouvementStroke;
	private static Stroke soutienStroke;
	
	private OrderDrawer orderDrawer; //object drawing orders on the map

	private Collection<Region> regionHighlighted; //region highlighted in map
	
	private Image generalImage = null; //buffered map image, with units, orders, etc.
	private Image emptyMapImage; //board image, without units and orders


	public static final int MAP_WIDTH = 1000;

	private static final Color COLOR_MOVE = new Color(255, 10, 10, 170);
	private static final Color COLOR_SUPPORT = new Color(220, 120, 30, 190);
	private static final Color COLOR_CONVOY = new Color(0, 0, 255, 170);
	private static final Color COLOR_ITEM_SELECTION = new Color(255, 0, 0,
			150);
	private static final Color COLOR_ITEM_CLOSE = new Color(200, 20, 20, 130);
	private static final Color COLOR_DELETE_UNIT = new Color(255, 0, 0, 100);
	private static final Color COLOR_ADD_UNIT = new Color(0, 210, 100, 100);
	// objet map
	private MainGameInterface frame;
	private ImageProvider imageProvider;
	private MapMouseListener myMouseListener;
	private Collection<Region> coastForConvoi;

	private Region regionShowName = null;
	
	public MapMessage mapmessage  ; //for displaying message
	
	public MapPanel(MainGameInterface frame) {
		super(true);

		//orders drawing
		this.orderDrawer = new OrderDrawer();

		this.frame = frame;

		this.loadMapImage();

		this.imageProvider = ImageProvider.getInstance();

		mouvementStroke = new BasicStroke(5);
		float[] dashPhase = { 8, 3 };
		soutienStroke = new BasicStroke(5, BasicStroke.CAP_BUTT,
				BasicStroke.JOIN_BEVEL, 0, dashPhase, 8);
		setLayout(null);

		// liste des régions mises en évidence
		this.regionHighlighted = new ArrayList<Region>(8);

		this.updateMouseListener();
	}

	/**
	 * 
	 * @param game
	 */
	public void setGame(Game game) {
		this.currentGame = game;
		// a l'écoute des changements de jeu
		currentGame.addGameListener(this);

		currentGame.getOrderFetcher().addOrderFetcherListener(this);

		this.tourChanged(currentGame.getCurrentTour());

		this.rebuildTheMap();
	}

	protected ScrollMap getScrollMap() {
		return this.frame.getScrollMap();
	}

	public Board getCurrentBoard() {
		// TODO optimiser
		return this.currentGame.getCurrentTour().getBoard();
	}

	/**
	 * Retourne la valeur d'ajustement avec les ordres en cours compris
	 * 
	 * @param c
	 * @return
	 */
	public int getAdjustMapValue(Country c) {

		int nb = this.board.getAdjustDifference(c);

		for (Order o : this.board.getTour().getOrderSet().getOrders()) {
			if (o.getOrderType() == Order.ORDRE_ADD_UNIT
					&& c.equals(o.getCountry())) {
				nb--;
			}

			if (o.getOrderType() == Order.ORDER_DEL_UNIT
					&& c.equals(o.getCountry())) {
				nb++;
			}

		}

		return nb;

	}

	/**
	 * Force la reconstruction compléte de l'image
	 * 
	 */
	protected void forceRebuild() {
		this.generalImage = null;
	}

	/**
	 * Build 
	 * 
	 */
	private void rebuildTheMap() {

		// fetch current user
		User user = Main.getUser();
		Tour tour = this.currentGame.getCurrentTour();
		// Board board = tour.getBoard();

		// general map image to rebuild
		this.generalImage = new BufferedImage(MAP_WIDTH, MAP_WIDTH,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = ((BufferedImage) generalImage).createGraphics();

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);

		g.drawImage(emptyMapImage, 0, 0, this);

		// Dessin des selections pour les mouvements
		if (regionHighlighted.size() > 0) {
			g.setColor(COLOR_ITEM_SELECTION);
			g.setStroke(mouvementStroke);
			Iterator<Region> its = this.regionHighlighted.iterator();
			while (its.hasNext()) {
				Region r = its.next();
				g.drawOval(r.getX() - 20, r.getY() - 20, 40, 40);
			}
		}

		// -----------------------
		// BOUNCES
		Composite compo = g.getComposite();
		g.setComposite(AlphaComposite
				.getInstance(AlphaComposite.SRC_OVER, 0.6f));
		if (tour.isPhaseDisband()) {
			// TODO bounces for disband ?
		} else {

		}
		g.setComposite(compo);

		// -------------------------------------
		// drawing units
		Iterator<Unit> itu = board.getUnits().iterator();
		while (itu.hasNext()) {
			Unit u = itu.next();
			Region r = u.getRegion();

			// disponibilty halo
			if (tour.isPhaseMouvement()) {
				if (board.controls(Main.getUser(), u)) {
					if (u.getType() == Unit.ARMY) {
						g.drawImage(imageProvider.get("Images/green-halo.png"),
								(r.getX()) - (23), (r.getY()) - (23), this);
					} else {
						g.drawImage(
								imageProvider.get("Images/halo-circle.png"), (r
										.getX()) - (22), (r.getY()) - (18),
								this);
					}
				}
			}

			// aura de selection
			if (r == this.selectedRegion) {
				if (this.typeOrdre == Order.ORDRE_CONVOI) {
					g.setColor(COLOR_CONVOY);
				} else {
					g.setColor(COLOR_MOVE);
				}
				g.fillOval(r.getX() - 24, r.getY() - 24, 44, 44);
			}

			// draw unit
			if (!u.isDisband()) {
				g.drawImage(imageProvider.get(u), (r.getX()) - (18),
						(r.getY()) - (18), this);
			} else {
				Main.out("draw disband !");
				g.drawImage(imageProvider.get("Images/disband.png"),
						(r.getX()) - (24), (r.getY()) - (24), this);
				g.drawImage(imageProvider.get(u), (r.getX()) - (6),
						(r.getY()) - (6), this);
				g.setStroke(mouvementStroke);
				// g.drawLine(r.getX()-12, r.getX, arg2, arg3)
			}
		}

		// -----------------------------
		// Supplies
		Iterator<Supply> itS = Supply.getSupplies().iterator();
		while (itS.hasNext()) {
			Supply s = itS.next();
			// dessin sur le dépot est occupé
			if (board.getOwner(s) == null) {
				continue;
			}
			int x = s.getX();
			int y = s.getY();

			g
					.drawImage(imageProvider.get(board, s), (x) - (9),
							(y) - (9), this);
		}


		
		// ---------------------------------
		// Dessins des choix de selections en cas d'ajustement
		if (tour.getPhase() == Tour.PHASE_FALL_REAJUST
				&& tour.getBoard().needAdjustement()) {

			for (Country c : Country.getCountries()) {
				if (board.needAdjustement(c) && currentGame.controls(user, c)) {

					// EXCEDENT ?
					if (board.getAdjustDifference(c) < 0) {
						Image suppr = imageProvider.get("Images/delete.png");
						Iterator<Unit> it = board.getUnits(c).iterator();
						while (it.hasNext()) {
							Unit u = it.next();
							// halo delete
							g.setColor(COLOR_DELETE_UNIT);
							g.fillRoundRect(u.getRegion().getX() - 24, u
									.getRegion().getY() - 24, 44, 44, 18, 18);
							// image "delete"
							g.drawImage(suppr, u.getRegion().getX() - 28, u
									.getRegion().getY() - 28, null);
						}
					} else {
						// UNITE A AJOUTER ?
						Image add = imageProvider.get("Images/add.png");
						Iterator<Supply> itA = board.getSupplies(c).iterator();
						while (itA.hasNext()) {
							Supply s = itA.next();
							if (c.equals(s.getOriginalOwner())
									&& board.isSupplyHomeFree(s)) {
								// met en évidence les régions
								Iterator<Region> itreg = s.getProvince().getRegions()
										.iterator();
								while (itreg.hasNext()) {
									Region r = itreg.next();
									// halo ADD
									g.setColor(COLOR_ADD_UNIT);
									g.fillRoundRect(r.getX() - 24,
											r.getY() - 24, 44, 44, 18, 18);
									// image add
									g.drawImage(add, r.getX() - 28,
											r.getY() - 28, null);
								}
							}
						}
					}
				}
			}
		}

		// dessins des ordres de l'utilisateur en attente
		this.orderDrawer.drawOrders(this.currentGame.getCurrentTour(), g);
	}

	/**
	 * Mise à jour de l'écouteur de souris selon la phase de jeu
	 * 
	 */
	private void updateMouseListener() {
		if (this.currentGame == null) {
			return;
		}

		// supprime l'ancien Ecouteur
		if (this.myMouseListener != null) {
			this.removeMouseListener(myMouseListener);
			this.removeMouseMotionListener(myMouseListener);
		}

		Tour currentTour = this.currentGame.getCurrentTour();

		//finished tour : no listener
		if (currentTour.canModify()) {

			switch (currentTour.getPhase()) {
			case Tour.PHASE_SPRING:
			case Tour.PHASE_FALL:
				this.myMouseListener = new MapPhaseMouvementMouseListener(this);
				break;
			case Tour.PHASE_FALL_REAJUST:
				this.myMouseListener = new MapPhaseAjustementMouseListener(this);
				break;
			case Tour.PHASE_FALL_DISBAND:
			case Tour.PHASE_SPRING_DISBAND:
				this.myMouseListener = new MapPhaseDisbandMouseListener(this);
				break;
			}
		} else {
			this.myMouseListener = new MapMouseListener(this);
		}

		// ajotue l'écouteur
		if (this.myMouseListener != null) {
			this.addMouseListener(myMouseListener);
			this.addMouseMotionListener(myMouseListener);
		}
	}

	/**
	 * Dessin de la carte
	 */
	@Override
	public void paintComponent(Graphics gr) {
		super.paintComponent(gr);
		if (this.currentGame == null) {
			return;
		}

		// convertit l'object Graphics
		Graphics2D g = (Graphics2D) gr;

		setPreferredSize(new Dimension(1000, 1000));

		// Test
		/*
		 * g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		 * RenderingHints.VALUE_ANTIALIAS_ON);
		 * g.setRenderingHint(RenderingHints.KEY_RENDERING,
		 * RenderingHints.VALUE_RENDER_QUALITY);
		 */

		// gestion du zoom
		g.scale(zoomScale, zoomScale);

		// construction de l'image de fond initiale
		if (generalImage == null) {
			this.rebuildTheMap();
		}
		// dessin de l'image de fond
		g.drawImage(generalImage, 0, 0, null);

		
		//---------------------------------
		//region name
		if (this.regionShowName != null){
			ProvinceNameImage pni = ProvinceNameImage.get(this.regionShowName);
			if (pni != null){
				g.drawImage(pni.img,pni.x,pni.y,this);
			}
		}
		
		// ---------------------------------------------------
		// // ----DESSIN DE L'ORDRE EN COURS DE SAISIE ------
		if (this.startRegion != null) {

			switch (this.typeOrdre) {
			case Order.ORDRE_MOUVEMENT:
				// ---- MOUVEMENT
				g.setColor(COLOR_MOVE);

				break;
			case Order.ORDRE_SOUTIEN:
				g.setColor(COLOR_SUPPORT);
				// dessin de la fl�che interm�diaire
				g.setStroke(soutienStroke);
				int ssX = startRegion.getX()
						+ ((currentX - startRegion.getX()) / 2);
				int ssY = startRegion.getY()
						+ ((currentY - startRegion.getY()) / 2);
				g.drawLine(selectedRegion.getX(), selectedRegion.getY(), ssX,
						ssY);
				break;
			case Order.ORDRE_CONVOI:
				g.setColor(COLOR_CONVOY);
				// dssin de la fléche intermédiaire
				break;
			case Order.ORDER_DEL_UNIT:
				g.setColor(COLOR_DELETE_UNIT);
				// ---> passe au CASE SUIVANT
			case Order.ORDRE_ADD_UNIT:
				// TODO teste si le depot appartient au joueur

				//
				int ax = this.startRegion.getX();
				int bx = this.startRegion.getY();
				g.setStroke(mouvementStroke);
				// peut venir du case "del"
				if (typeOrdre == Order.ORDRE_ADD_UNIT) {
					g.setColor(COLOR_ADD_UNIT);
				}
				g.drawRoundRect(ax - 30, bx - 30, 54, 54, 12, 12);
				// on sort : plus besoin de rien
				return;
			}
			// Dessin de la fléche
			g.setStroke(mouvementStroke);

			OrderDrawer.drawArrow(g, startRegion.getX(), startRegion.getY(),
					currentX, currentY);

			// dessin de destination, si le sommet de la flèche se trouve dans
			// un choix
			Iterator<Region> itr = this.regionHighlighted.iterator();
			boolean choixOk = false;
			while (itr.hasNext()) {
				Region r = itr.next();
				// controle si retraite
				int distX = currentX - r.getX();
				int distY = currentY - r.getY();
				if (Math.abs(distX) < 18 && Math.abs(distY) < 18) {
					// dessin du rond de destination
					g.setColor(COLOR_ITEM_CLOSE);
					g.fillOval(r.getX() - 14, r.getY() - 14, 28, 28);
					choixOk = true;
					endRegion = r;
					break;
				}
			}
			// si pas de choix on désactive l'unité
			if (!choixOk && endRegion != null) {
				endRegion = null;
			}
			

			
		}
		
		if (this.mapmessage != null) {
			this.mapmessage.draw(g);
		}

		
	}

	// //=========================================================
	/** change le mode zoom */
	public void changeZoom(boolean agrandit) {
		if (this.currentGame == null) {
			return;
		}

		// echelle
		if (agrandit) {
			zoomScale += 0.1;
		} else {
			if (zoomScale >= 0.2) {
				zoomScale -= 0.1;
			}
		}

		// type de map
		setPreferredSize(new Dimension(1000, 1000));

		this.rebuildTheMap();
		this.updateUI();
		// this.paint(this.getGraphics());
	}

	@Override
	public void tourChanged(Tour tour) {
		//check if tour is over
		if (tour.canModify()) {
			this.userOrderSet = tour.getOrderSet();
			this.userOrderSet.addOrderSetListener(this);
		} else {
			this.userOrderSet = null;
		}

		// mise à jour de l'écouteur de souris
		this.updateMouseListener();

		// Rafraichit
		this.forceRebuild();
		this.updateUI();
		this.board = tour.getBoard();
	}

	@Override
	public void tourAdded(Tour tour) {
		this.updateMouseListener();
	}

	/**
	 * Met en évidence les voisins d'une région spécifiée
	 * 
	 * @param region
	 */
	public void highlightNearRegions(Region region) {

		// vide la liste des régions mises en évidence
		this.regionHighlighted.clear();

		if (region == null) {
			return;
		}
		// trouve l'unité présente dans la région
		// Unit unit = region.getProvince().getUnit();
		Unit unit = getCurrentBoard().getUnit(region);
		this.highlightNearRegions(unit);
	}

	/**
	 * Met en relief les régions avoisinantes pour l'unité
	 * 
	 * @param u
	 */
	public void highlightNearRegions(Unit u) {
		// vide la liste des régions mises en évidence
		this.regionHighlighted.clear();

		if (u != null) {

			if (u.isDisband()){
				this.regionHighlighted.addAll(u.getPossibleRetreats());
			} else  {
				// retrouve le graphe des régions depuis l'unité
				Graph graph = u.getGraph();
				Iterator<GraphNode> it = graph.getNode(u.getRegion())
						.getSuccessors().iterator();
				while (it.hasNext()) {
					GraphNode node = it.next();
	
					// no disband
					Region region = node.getRegion();
					regionHighlighted.add(region);
				}
				
			}
		}
	}

	/**
	 * Met en Evidence les régions voisines en commun
	 * 
	 * @param r1
	 * @param r2
	 */
	public void highlightCommonRegions(
			Region regionAttack,
			Region regionSupport) {
		
		this.regionHighlighted.clear();
		
		if (regionAttack == null || regionSupport == null) {
			return;
		}

		this.regionHighlighted.addAll(
				Region.getCommonRegionsForSupport(
				this.getCurrentBoard(), 
				regionAttack, 
				regionSupport)
				);


	}

	/** Affiche les régions proches du curseur pour un convoi */
	protected void highlightRegionsCurseurConvoi() {
		this.regionHighlighted.clear();

		// coordonnées de la région itérérée
		int sx;
		int sy;
		for (Region r : getCoastForConvoi()) {
			// la région est une fin de convoi
			if (r.isConvoyStop()) {
				sx = Math.abs(currentX - r.getX());
				sy = Math.abs(currentY - r.getY());
				// suffisement proche ? on ajoute
				if (sx < 150 && sy < 150) {
					regionHighlighted.add(r);
				}
			}
		}
	}

	
	protected void setRegionName(Region r){
		
		//no region to display
		if (this.regionShowName == null && r == null) return;
		
		//same region
		if (this.regionShowName != null && this.regionShowName.equals(r)) return;
		
		this.regionShowName = r;
		//this.forceRebuild();
		this.updateUI();

		
	}
	
	/**
	 * Retourne la collection des côtes pour le convoi
	 * 
	 * @return
	 */
	private Collection<Region> getCoastForConvoi() {
		if (this.coastForConvoi == null) {
			this.coastForConvoi = new ArrayList<Region>(30);

			// ajoute les régions cote pour armée
			for (GraphNode gn : Unit.getGraph(Unit.ARMY).getNodes()) {
				Region r = gn.getRegion();
				if (r.isConvoyStop() && r.getName().length() == 3) {
					this.coastForConvoi.add(r);
				}
			}
		}

		return this.coastForConvoi;
	}

	/**
	 * 
	 * Retourne une province selon les coordonn�es de la carte
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	protected Region getRegionFromCoord(int mouseX, int mouseY) {
		double ax;
		double ay; // coordonn�es approximative
		double x = mouseX / zoomScale;
		double y = mouseY / zoomScale;
		double distance = 18 * zoomScale; // distance acceptable

		// TODO : Affichage des provinces en rech dichotomique
		Iterator<Region> it = Region.getRegions().iterator();
		while (it.hasNext()) {
			Region region = it.next();
			ax = Math.abs(x - (region.getX() - 9));
			ay = Math.abs(y - (region.getY() - 9));
			if (ax < distance && ay < distance) {
				return region;
			}
		}
		return null;
	}

	protected Supply getSupplyFromCoord(int mouseX, int mouseY) {
		double ax;
		double ay; // coordonn�es approximative
		double x = mouseX / zoomScale;
		double y = mouseY / zoomScale;
		double distance = 18 * zoomScale; // distance acceptable
		// TODO : Affichage des provinces en rech dichotomique
		Iterator<Supply> it = Supply.getSupplies().iterator();
		while (it.hasNext()) {
			Supply s = it.next();
			ax = Math.abs(x - (s.getX() - 9));
			ay = Math.abs(y - (s.getY() - 9));
			if (ax < distance && ay < distance) {
				return s;
			}
		}
		return null;
	}

	/** Positionne le curseur sur la carte */
	protected void setCursorPosition(int x, int y) {
		this.currentX = (int) Math.round(x / this.zoomScale);
		this.currentY = (int) Math.round(y / this.zoomScale);
	}

	/**
	 * Génération d'un ordre d'aprés les données internes
	 * 
	 * @return
	 */
	protected Order pumpOrder() {

		if (this.userOrderSet == null) {
			return null;
		}
		Order order = null;

		// vérifie si la région de départ est OK
		if (this.selectedRegion == null || this.endRegion == null
				|| this.startRegion == null) {
			return null;
		}

		// chaine d'ordre
		StringBuffer sb = new StringBuffer();

		switch (this.typeOrdre) {
		case Order.ORDRE_CONVOI:
			// flotte
			sb.append("F ");
			// nom de la flotte qui convoi
			sb.append(this.selectedRegion.getProvince().getShortName());
			// type d'ordre
			sb.append(" C ");
			// r�gion de d�part
			sb.append(this.startRegion.getProvince().getShortName());
			// r�gion d'arriv�e
			sb.append(" - ");
			sb.append(this.endRegion.getProvince().getShortName());
			break;
		case Order.ORDRE_MOUVEMENT:
			// type d'unité
			// Unit u = this.startRegion.getProvince().getUnit();
			Unit u = getCurrentBoard().getUnit(startRegion);
			if (u != null) {
				// type d'unit�
				sb.append(u.getCodeChar());
				sb.append(' ');
				sb.append(u.getRegion().getProvince().getShortName());
				sb.append(" - ");
				sb.append(endRegion.getName());
			} else {
				return null;
			}
			break;
		case Order.ORDRE_SOUTIEN:
			// type d'unité
			// Unit u2 = this.selectedRegion.getProvince().getUnit();
			Unit u2 = this.getCurrentBoard().getUnit(selectedRegion);
			if (u2 != null) {
				// type d'unit�
				sb.append(u2.getCodeChar());
				sb.append(' ');
				sb.append(u2.getRegion().getProvince().getShortName());
				sb.append(" S ");
				sb.append(this.startRegion.getProvince().getShortName());
				sb.append(" - ");
				sb.append(endRegion.getName());
			} else {
				return null;
			}
			break;
		default:
			return null;
		}

		// construit l'ordre
		// TEST
		order = Order.create(this.getCurrentBoard(), sb.toString());

		// ajoute à la liste des ordres
		this.userOrderSet.addOrder(order);

		return order;
	}

	/**
	 * load main map image
	 * 
	 */
	private void loadMapImage() {
		emptyMapImage = ImageProvider.getInstance().get(MapBuilder.getMapImageLocation());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see diplomacy.order.OrderSetListener#orderAdded(diplomacy.order.Order)
	 */
	@Override
	public void orderAdded(Order o) {
		// rafraichit le jeu
		this.forceRebuild();
		this.updateUI();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see diplomacy.order.OrderSetListener#orderRemoved(diplomacy.order.Order)
	 */
	@Override
	public void orderRemoved(Order o) {
		// rafraichit
		this.forceRebuild();
		this.updateUI();
	}

	public Game getCurrentGame() {
		return this.currentGame;
	}

	@Override
	public void newOrders(CountryOrders c) {
		this.updateMouseListener();
	}

	@Override
	public void remainingTime(int minutes) {
	}
}

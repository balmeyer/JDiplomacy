/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.metal.MetalBorders.Flush3DBorder;

import diplomacy.game.Game;
import diplomacy.game.GameListener;
import diplomacy.game.Tour;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public final class InfoPanel extends JPanel implements GameListener{

    /**
	 * 
	 */
	private static final long serialVersionUID = -5410876357786336122L;
	private JLabel label;
    private Tour currentTour;
    private Timer infoTimer;

    public InfoPanel(){
        this.setBackground(Color.LIGHT_GRAY);
        this.setPreferredSize(new Dimension(500,22));
        this.setMaximumSize(new Dimension(1500,22));

        this.setBorder(new Flush3DBorder());
        //this.setBorder(BorderFactory.createEtchedBorder());

        label = new JLabel();
        label.setFont(new Font("SansSerif",Font.BOLD,14));
        this.add(label);

        this.infoTimer = new Timer("INFO_TIMER", true);

    }

    public void displayMessage(String msg){

        this.label.setText(msg);
        
        this.setBackground(Color.YELLOW);
        this.setSize(500, 160);


        TimerTask tt = new TimerTask() {
            @Override
            public void run() {
                //restitue l'objet d'origine
                if (currentTour != null){
                    tourChanged(currentTour);
                }
                this.cancel();
            }
        };
        
        this.infoTimer.schedule(tt, 3000, 30000);

    }

    public void setGame(Game game){
        game.addGameListener(this);
        this.tourChanged(game.getCurrentTour());
    }

    @Override
    public void tourChanged(Tour tour) {
        this.label.setText(tour.toString());
        this.currentTour = tour;

        //couleur de fond
        Color background = Color.LIGHT_GRAY;
        switch(tour.getPhase()){
            case Tour.PHASE_SPRING:
                background = Color.GREEN;
                break;
            case Tour.PHASE_FALL:
                background = Color.ORANGE;
                break;
            case Tour.PHASE_FALL_REAJUST:
                background = Color.YELLOW;
                break;
            case Tour.PHASE_FALL_DISBAND:
            case Tour.PHASE_SPRING_DISBAND:
                background = Color.RED;
                break;
        }

        this.setBackground(background);

    }

    @Override
    public void tourAdded(Tour tour) {

    }

}

/*
 * DocumentLoader.java
 *
 * Created on 1 oct. 2007, 21:00:37
 *
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.xml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import diplomacy.access.exception.AccessException;
import diplomacy.access.exception.ConnectionException;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public class DocumentLoader {

    private static DocumentBuilderFactory dbf;
    
    private static TransformerFactory tf;
    private static Transformer transformer;
    
    /**
     * Create now DOM Document
     * @return
     */
    public static Document create(){
    	return getDocumentBuilder().newDocument();
    }
    
    
    
    /** Chargement d'un document xml */
    public static Document load(URL ur) throws AccessException {
        try {
            //File file = new File(path);
            Document doc = null;
            InputStream istr = ur.openStream();
            doc = getDocumentBuilder().parse(istr);

            
            return doc;
        } catch (SAXException ex) {
            Logger.getLogger(DocumentLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //erreur connexion
            Logger.getLogger(DocumentLoader.class.getName()).log(Level.SEVERE, null, ex);
            throw new ConnectionException();
        }

        return null;
    }
    
    
    // This method writes a DOM document to a file
    public static void writeXmlFile(Document doc, String filename)  {

        // Prepare the DOM document for writing
        Source source = new DOMSource(doc);

        // Prepare the output file
        File file = new File(filename);
        Result result = new StreamResult(file);

        // Write the DOM document to the file
        try {
			getTransformer().transform(source, result);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
    
    private static DocumentBuilder getDocumentBuilder(){
    	if (dbf == null){
        	//init document 
            dbf = DocumentBuilderFactory.newInstance();
            dbf.setValidating(false);
            dbf.setIgnoringElementContentWhitespace(true);
    	}
    	
    	try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			return db;
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		throw new IllegalStateException("cannot create DocumentBuilder");
    }
    
    private static Transformer getTransformer(){
    	if (transformer == null ){
	    	if (tf == null ){
	    		tf = TransformerFactory.newInstance();
	    	}
	    	
	    	try {
				transformer = tf.newTransformer();
			} catch (TransformerConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    	
    	
    	return transformer;
    }
    
}
/*
 * Created on 10 avr. 2004
 *
 * Vovau Jean-Baptiste - balmeyer@dev.java.net
 */
package diplomacy.game;

import diplomacy.game.board.Country;
import diplomacy.game.board.Region;
import diplomacy.graph.Graph;

/**
 * 
 * Created on 10 avr. 2004.
 * 
 * @author Jean-Baptiste Vovau - balmeyer@dev.java.net
 * @version 1.0
 */
public final class Army extends Unit {


	/**
	 * @param country
	 */
	protected Army(Country country) {
		super(country);
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see diplomacy.board.Unit#getType()
	 */
	@Override
	public int getType() {
		
		return Unit.ARMY;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see diplomacy.board.Unit#canGoHere(diplomacy.board.Region)
	 */
	@Override
	public boolean canGoHere(Region region) {
		
		return region.acceptUnit(this);
	}
	
	/**
	 * Une armée ne peut jamais convoyer
	 */
	@Override
	public boolean canConvoy(){
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see diplomacy.board.Unit#getMovementGraph()
	 */
	@Override
	public Graph getMovementGraph() {
		
		return null;
	}

	/* (non-Javadoc)
	 * @see diplomacy.board.Unit#canBeConvoyed()
	 */
	@Override
	public boolean canBeConvoyed() {
		
		return this.getRegion().isConvoyStop();
	}


	/* (non-Javadoc)
	 * @see diplomacy.board.Unit#getCodeChar()
	 */
	@Override
	public char getCodeChar() {
		
		return 'A';
	}



}

/*
 * Created on 24 avr. 2007
 *
 *
 */
package diplomacy.game;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public interface GameListener {

	/** Indique que le tour affiché en cours a changé */
	public void tourChanged(Tour tour);
	
	/** Indique qu'un nouveau tour est disponible */
	public void tourAdded(Tour tour);


}

/*
 * Created on 14 avr. 2007
 *
 *
 */
package diplomacy.game;

import java.util.Date;
import java.util.List;

import diplomacy.Main;
import diplomacy.access.exception.AccessException;
import diplomacy.game.board.Country;
import diplomacy.gui.creagame.TimeBetweenTour;

/**
 * 
 * Information 
 * 
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public final class GameInfo {

    //Identifiant du jeu
    private long gameId;

    //infos
    private String title;
    private short numberOfPlayer;
    private short freePlaces;
    private int minutesBetweenTours;
    private boolean isStarted;
    private boolean isFinished;
    private Date startDate;
    private Date creationDate;
    private Date endDate;
    private int lastViewTour;
    protected String gameCreator;
    private TimeBetweenTour tbt = null;
    private boolean hasplayed;
    private List<User> users;
    private List<Country> winlist;
    
    //info victoire
    private static final int NB_VICTOIRE =  18;
    private static final int NB_VICTOIRE_2_PLAYERS =  24;

    public GameInfo(long id) {
        this.gameId = id;
    }

    @Override
    public boolean equals(Object obj){
        if(obj == null) return false;

        if (!obj.getClass().equals(this.getClass())) return false;

        return this.getGameId() == ((GameInfo) obj).getGameId();
    }

    @Override
    public int hashCode(){
        return (int) this.gameId;
    }

    /**
     * @return Returns the gameId.
     */
    public long getGameId() {
        return gameId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public short getNumberOfPlayer() {
        return numberOfPlayer;
    }

    public void setNumberOfPlayer(short np) {
        this.numberOfPlayer = np;
    }

    public int getMinutesBetweenTours() {
        return this.minutesBetweenTours;
    }

    public void setMinutesBetweenTours(int minute) {
        this.minutesBetweenTours = minute;
        this.tbt = null;
    }

    public TimeBetweenTour getTimeBetweenTour() {
        if (tbt == null) {
            tbt = new TimeBetweenTour(getMinutesBetweenTours());
        }

        return tbt;
    }

    public void setMinutesBetweenTours(TimeBetweenTour tbt) {
        this.setMinutesBetweenTours(tbt.getMinutes());
    }

    public boolean isStarted() {
        return this.isStarted;
    }

    public boolean isFinished() {
        return this.isFinished;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date d) {
        this.startDate = d;
    }

    public Date getEndDate(){
        return this.endDate;
    }

    public void setEndDate(Date d){
        this.endDate = d;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date d) {
        this.creationDate = d;
    }

    public void endGame(){
        this.setFinished(true);
    }

    public boolean containsUser(User user) {
        for (User test : getUsers()) {
            if (test.getUserId() == user.getUserId()) {
                return true;
            }
        }

        return false;
    }

    public List<User> getUsers() {

        //chargement dynamiques des users ?
        if (users == null) {
            try {
                users = Main.getDBAccess().fetchUser(this);
            } catch (AccessException ae) {
                
                Main.logException(ae);
            }
        }

        return users;
    }


    public void addUser(User user) {
        //TODO ajouter l'user dans l'access
        getUsers().add(user);
        this.setFreePlaces((short) (this.getFreePlaces() - 1));
    }

    @Override
    public String toString() {
        return String.valueOf(gameId);
    }

    public String getGameCreator() {
        return this.gameCreator;
    }

    /**
     * @return the freePlaces
     */
    public short getFreePlaces() {
        return freePlaces;
    }

    /**
     * @param freePlaces the freePlaces to set
     */
    public void setFreePlaces(short freePlaces) {
        this.freePlaces = freePlaces;
    }

    /**
     * @param isStarted the isStarted to set
     */
    public void setStarted(boolean isStarted) {
        this.isStarted = isStarted;
    }

    /**
     * @param isFinished the isFinished to set
     */
    public void setFinished(boolean isFinished) {
        this.isFinished = isFinished;
    }

    /**
     * @return the lastViewTour
     */
    public int getLastViewTour() {
        return lastViewTour;
    }

    /**
     * @param lastViewTour the lastViewTour to set
     */
    public void setLastViewTour(int lastViewTour) {
        this.lastViewTour = lastViewTour;
    }

    /**
     * Nombre de dépot pour la victoire
     * @return
     */
    public int getNbSupplyVictory(){
        if (this.getNumberOfPlayer() == 2){
            return NB_VICTOIRE_2_PLAYERS;
        }

        return NB_VICTOIRE;
    }

    /**
     * @return the hasplayed
     */
    public boolean isHasplayed() {
        return hasplayed;
    }

    /**
     * @param hasplayed the hasplayed to set
     */
    public void setHasplayed(boolean hasplayed) {
        this.hasplayed = hasplayed;
    }

	/**
	 * @param winlist the winlist to set
	 */
	public void setWinlist(List<Country> winlist) {
		this.winlist = winlist;
	}

	/**
	 * @return the winlist
	 */
	public List<Country> getWinlist() {
		return winlist;
	}


}

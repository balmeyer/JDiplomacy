/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.game;

import diplomacy.game.board.Country;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public class GameResult {

    private Game game;
    private Map<Country,Integer> countryToSupply;

    public GameResult(Game game){
        this.game = game;
        this.game.setResult(this);
        this.countryToSupply = new HashMap<Country,Integer>();
    }

    public void setResult(Country c , int nbsupply){
        if (countryToSupply.containsKey(c)){
            countryToSupply.remove(c);
        }

        countryToSupply.put(c, nbsupply);
    }

    public int getResult(Country c){
        return countryToSupply.get(c);
    }


}

/*
 * Created on 18 avr. 2007
 *
 *
 */
package diplomacy.game;

import diplomacy.game.board.Country;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public final class OrderData {

	protected long orderDataId = 0;
	protected String orderText = "";
	protected Country country = null;
	protected Game game = null;
	
	protected OrderData(){
		
	}
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package diplomacy.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import diplomacy.game.board.Country;
import diplomacy.game.board.Province;
import diplomacy.game.board.Region;
import diplomacy.game.board.Supply;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public final class Board {

    /** Positions des unités en début de tour. */
    private String startPosition = null;
    private Tour tour;
    private final Map<Province, Unit> provinceToUnit = new HashMap<Province, Unit>();
    private final Map<Province, Unit> provinceToDislodgedUnit = new HashMap<Province, Unit>();
    private final Map<Unit, Region> unitToRegion = new HashMap<Unit, Region>();
    private final List<Unit> units = new ArrayList<Unit>();
    private final Map<Supply, Country> supplyToCountry = new HashMap<Supply, Country>();




    public Board(Tour tour) {
        this.tour = tour;
        this.tour.setBoard(this);
    }

    public Tour getTour() {
        return this.tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public String getStartPosition() {
    	if (this.startPosition == null){
    		throw new IllegalStateException("start position cannot be null");
    	}
        return this.startPosition;
    }

    public void setStartPosition(String value) {
        this.startPosition = value;

        //TODO cache restore
        this.unserializePositions();
    }

    public void reportSupply(Board oldboard) {
        this.clearUnits();
        this.setStartPosition(oldboard.startPosition);
        this.clearUnits();
    }

    private void clearUnits() {
        //this.checkLock();
        provinceToUnit.clear();
        unitToRegion.clear();
        units.clear();
    //supplyToCountry.clear();
    }






    public Country getCountry(Region r) {
        return getCountry(r.getProvince());
    }

    public Country getCountry(Province p) {
        Unit u = getUnit(p);

        if (u != null) {
            return u.getCountry();
        }

        //pas de pays trouvé
        return null;
    }


    public Collection<Unit> getUnits() {
        return this.units;
    }

    public Collection<Unit> getUnits(Country c) {

        List<Unit> list = new ArrayList<Unit>();

        for (Unit u : getUnits()) {
            if (c.equals(u.getCountry())) {
                list.add(u);
            }
        }


        return list;
    }

    public Unit getUnit(Province p) {
        return this.provinceToUnit.get(p);
    }

    public Unit getUnit(Region r) {
        return getUnit(r.getProvince());
    }

    public Unit createUnit(Region r, Country c, int typeOfUnit, boolean dislodged) {
        Unit unit = null;

        switch (typeOfUnit) {
            case Unit.ARMY:
                unit = Unit.createArmy(c);
                break;
            case Unit.SHIP:
                unit = Unit.createShip(c);
                break;
        }

        if (!dislodged){
        this.provinceToUnit.put(r.getProvince(), unit);
        } else{
        	this.provinceToDislodgedUnit.put(r.getProvince() , unit);
        }
        this.unitToRegion.put(unit, r);
        this.units.add(unit);

        unit.setRegion(r);

        return unit;
    }



   
    public Region getRegion(Unit unit) {
        return unitToRegion.get(unit);
    }

    public Country getOwningCountry(Province p) {

        Unit u = getUnit(p);

        if (u == null) {
            return null;
        }

        return u.getCountry();
    }

    public boolean isOccupiedByUnit(Province p) {
        return getUnit(p) != null;
    }

    public boolean isOccupiedByUnit(Region r) {
        return isOccupiedByUnit(r.getProvince());
    }

    public Country getOwner(Supply s) {
        return supplyToCountry.get(s);
    }

    public void setOwner(Supply s, Country c) {
        supplyToCountry.put(s, c);
    }

    public int countSupply(Country c) {
        int n = 0;
        for (Supply s : Supply.getSupplies()) {
            if (this.controls(s, c)) {
                n++;
            }
        }

        return n;
    }

    public Collection<Supply> getSupplies() {
        return supplyToCountry.keySet();
    }

    public Collection<Supply> getSupplies(Country c){
    	List<Supply> list = new ArrayList<Supply>();
    	for(Supply s : getSupplies()){
    		if (c.equals(this.getOwner(s))){
    			list.add(s);
    		}
    	}
    	return list;
    }
    
    public boolean controls(User user, Unit unit) {
    	if (user == null || unit == null) return false;
        return this.tour.getGame().controls(user, unit.getCountry());
    //return controls(user, unit.getCountry());
    }

    public boolean controls(Supply s, Country c) {
        return c.equals(getOwner(s));
    }

    public boolean hasSupply(Supply s, Country c) {
        if (c == null) return false;

        return c.equals(getOwner(s));
    }

    public boolean isSupplyHomeFree(Supply s) {

        return s.getOriginalOwner() != null && getUnit(s.getProvince()) == null;
    }

    public boolean hasUnit(Unit u, Country c) {
        return c.equals(u.getCountry());
    }

    public boolean needAdjustement(Country c) {
        return getAdjustDifference(c) != 0;
    }

    public boolean needAdjustement(){
        for(Country c : Country.getCountries()){
            if (needAdjustement(c)) return true;
        }

        return false;
    }

    public int getAdjustDifference(Country c) {
        int nbSupply = countSupply(c);
        int nbUnit = getUnits(c).size();
        return nbSupply - nbUnit;
    }

    public int getAdjustDifference(Region r) {
        return getAdjustDifference(getCountry(r));
    }

    public boolean needDisband(Country c) {
        if (this.provinceToDislodgedUnit.size() == 0) {
            return false;
        }

        for (Unit u : this.provinceToDislodgedUnit.values()) {
            if (u.getCountry().equals(c)) {
                return true;
            }
        }

        return false;
    }

    public boolean needDisband() {
        for (Country c : Country.getCountries()) {
            if (needDisband(c)) {
                return true;
            }
        }

        return false;
    }

    public Unit getDislodgedUnit(Province p){
    	return provinceToDislodgedUnit.get(p);
    }
    
    /**
     * régions possible pour un support
     * */
    public Collection<Region> getRegionCanSupport(Unit uMove, Unit uSupport) {
        //
        List<Region> list = new ArrayList<Region>(8);

        //erreur d'unités
        if (uMove == null || uSupport == null) {
            return list;
        }

        //regions autour de l'unité
        Collection<Region> aroundMove = uMove.getGraph().getNode(uMove).getRegionsAround();

        //provinces autour du support
        Collection<Province> aroundSupport =
                uSupport.getGraph().getNode(uSupport).getProvincesAround();

        //compare les provinces
        for (Region region : aroundMove) {
            if (aroundSupport.contains(region.getProvince())) {
                list.add(region);
            }
        }

        return list;
    }

    /** Construit la chaine pour sérailiser le jeu */
    /*private void serializePositions() {
    	

    	
        StringBuilder sb = new StringBuilder();

        //---------------
        //Unités
        Iterator<Unit> unitIterator = this.getUnits().iterator();
        while (unitIterator.hasNext()) {
            //unité
            Unit u = (Unit) unitIterator.next();
            //Pays
            sb.append(u.getCountry().getIndex());
            sb.append(":");
            //type d'unité
            sb.append(u.getCodeChar());
            sb.append(":");
            //position
            sb.append(u.getRegion());
            //Retraite ?

            //fin
            sb.append(";");
        }

        //------------
        //dépots
        Iterator<Supply> supplyIterator = Supply.getSupplies().iterator();
        while (supplyIterator.hasNext()) {
            //dépot
            Supply s = supplyIterator.next();
            //pays
            Country c = getOwner(s);
            if (c != null) {
                sb.append(c.getIndex());
                //type : supply
                sb.append(":S:");
                //région
                sb.append(s.getProvince().getShortName());
                //fin
                sb.append(";");
            }
        }

        //-----------------------------------

        //sauvegarde les unités et les dépots dans une chaine texte
        this.startPosition = sb.toString();

    //Ordres
    //sb.setLength(0);
    //Iterator it = this.getTourOrder().getTourOrderSet().get
    }
    */
    /** Etablie un état du jeu d'aprés une chaine */
    
    private void unserializePositions() {
    
        if (startPosition == null) {
            return;
        }

        //vide la liste des unités
        //update région
        //indique que le tour est vue
        //TODO this.tourDiscovered = (this.tourCompleted);
        this.clearUnits();

        String[] words = this.startPosition.split(";");
        for (int i = 0; i < words.length; i++) {

            //bounce
            if (words[i].startsWith("*")) {
                //TODO Province.addBounce(words[i].substring(1));
                continue;
            }

            String[] w = words[i].split(":");
            if (w.length < 3) {
                continue;
            }
            //pays
            Country c = Country.get(Integer.parseInt(w[0]));
            //Région ou province
            String regionName = w[2];
            Region region = Region.get(regionName);
            String marks = null;


            int typeOfUnit = 0;
            //Region base = Region.get(regionName);
            //TODO bug de la restauration : placer le disbander avant la création del'arm�e
            switch (w[1].charAt(0)) {
                //ARMEE
                case 'A':
                    typeOfUnit = Unit.ARMY;
                    break;
                //FLOTTE
                case 'F':
                    typeOfUnit = Unit.SHIP;
                    break;
                //DEPOT
                case 'S':
                    Supply s = Supply.getSupply(region.getProvince());
                    this.setOwner(s, c);
                    break;
            }

            if (w.length >3){
            	marks = w[3];
            }
            
            List<Region> possibleRetreats = null;
            if (w.length > 4){
            	possibleRetreats = new ArrayList<Region>();
            	for (String rn : w[4].split(",")){
            		Region r = Region.get(rn);
            		possibleRetreats.add(r);
            	}
            }
            

            if (typeOfUnit > 0) {
                Unit u = this.createUnit(region, c, typeOfUnit, possibleRetreats != null);
                
                if (marks != null){
                	u.setMarks(marks);
                }
                
                if (possibleRetreats != null){
                	u.getPossibleRetreats().addAll(possibleRetreats);
                }
            }

        }

        if (this.startPosition == null){
        	throw new IllegalStateException("start position have been erased !");
        }

    }
}

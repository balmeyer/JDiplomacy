package diplomacy.game ;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import diplomacy.game.board.Country;
import diplomacy.game.board.Region;
import diplomacy.graph.Graph;
;

/**





@version 1.0 02/04/2004
@author JB Vovau
*/
public abstract class Unit
{

	//constantes statiques
	public final static int INCONNU = 0;
	public final static int ARMY = 1 ;
	public final static int SHIP = 2 ;


	//liste des unités
	
	//graphes de déplacement pour les unités
	private static Graph armyGraph;
	private static Graph shipGraph;
	
	//variables de classe
	private Country country ;
	private Graph movementGraph;
	

    private Region region;
	
    private final List<Region> possibleRetreats = new ArrayList<Region>() ;
    private final List<String> marks = new ArrayList<String>();
    
    /**
     * Get graph associated with unit
     * @param unitType
     * @return
     */
	public static Graph getGraph(int unitType){
		switch (unitType){
			case Unit.ARMY:
				return armyGraph;
			case Unit.SHIP:
				return shipGraph;
			default:
				throw new IllegalArgumentException("Type d'unité inconnu");
		}
		
	}
	
	public static void setGraph(Graph graph, int unitType){
		switch (unitType){
		case Unit.ARMY:
			armyGraph = graph;
			break;
		case Unit.SHIP:
			shipGraph =graph;
			break;
		default:
			throw new IllegalArgumentException("Type d'unit� inconnu");
	}
	}
	
	public static Unit createArmy(Country country){
		Unit u = new Army(country);
		u.setGraph(armyGraph);
		return u;
	}
	
	public static Unit createShip(Country country){
		Unit u = new Ship(country);

		u.setGraph(shipGraph);
		return u;
		
	}
	

	/**
		Constructeur d'unité
	*/
	protected Unit(Country country ){

		this.country = country;

	}


	/**
		retourne le numéro de pays associé à l'unité.
	*/
	public Country getCountry(){
		return this.country ;
	}
	
    public Region getRegion(){
        return this.region;
    }

    protected void setRegion(Region region){
        this.region = region;
    }
	
	public Graph getGraph(){
		return this.movementGraph;
	}
	
	private void setGraph(Graph graph) {
		this.movementGraph = graph;
	}


	public String getMarks(){
		StringBuilder sb = new StringBuilder();
		String aux = "";
		for (String m : this.marks){
			sb.append(aux);
			sb.append(m);
			aux = ",";
		}
		return sb.toString();
	}
	
    public void setMarks(String strMark){
    	this.marks.clear();
    	strMark = strMark.replace("(", "").replace(")","");
    	
    	if (marks == null) return;
    	for(String m : strMark.split(",")){
    		this.marks.add(m);
    	}
    	
    }
	
    public boolean isMarkedAs(String m){
    	return this.marks.contains(m);
    }
    
    public Collection<Region> getPossibleRetreats(){
    	return this.possibleRetreats;
    }
    
    public boolean isDisband(){
    	return this.isMarkedAs("dislodged");// this.possibleRetreats.size() > 0;
    }
    
    @Override
	public String toString(){
		return "[" + this.country +"][" + this.getCodeChar() +"] "
                + getRegion().getName() ;
	}
	
    @Override
	public boolean equals(Object o){
		
		if (o == null || !(o instanceof Unit)) return false;
		
		Unit u = (Unit) o;
		
		if (u == this) return true;
		
		if (u.getType() != this.getType()) return false;
		
		if (!u.getCountry().equals(this.getCountry())) return false;
		
		if (!u.getRegion().equals(this.getRegion())) return false;
		
		return true;

		
	}


	public abstract int getType() ;
	
	public abstract boolean canGoHere(Region region) ;
	
	public abstract Graph getMovementGraph() ;
	
	/** Indique si l'unité est en mesure d'effectuer un convoi, dans sa position. */
	public abstract boolean canConvoy();
	
	/** Indique si une unité est en mesure d'�tre convoy�e */
	public abstract boolean canBeConvoyed();
	
	/** Retourne le code de l'unité */
	public abstract char getCodeChar();
}
/*
 * Created on 15 avr. 2007
 *
 *
 */
package diplomacy.game;

import diplomacy.game.Board;
import diplomacy.language.Language;
import diplomacy.order.OrderSet;

/**
 *
 * Tour de jeu
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 *
 */
public final class Tour {

    //type de phases disponibles
    public static final int PHASE_SPRING = 0;
    public static final int PHASE_SPRING_DISBAND = 1;
    public static final int PHASE_FALL = 2;
    public static final int PHASE_FALL_DISBAND = 3;
    public static final int PHASE_FALL_REAJUST = 4;

    //information sur la date
    private long tourId = 0;
    private int startYear = 0;
    protected int annee = 1901;
    private int phase = 0;
    /** Indique si un tour est achevé ou non */
    private boolean tourCompleted = false;
    /** Indique, si un tour est terminé, si l'utilisateur l'a vu */
    private boolean tourDiscovered = false;
    /** texte de tous les ordres postés*/
    //private String postedOrders;
    /** Liste des ordres sauvegardés */
    //private String persistOrders;
    protected Tour tourBefore;
    protected Tour tourNext;
    private String tostring = null;
    
    private Game game;
    /** Liste d'ordres propres à l'utilisateur. Volatile. **/
    private OrderSet orders;

    private String textOrders;

    /** Etat du jeu en début de tour */
    private Board startBoard ;

    /**
     * Création d'un tour sans ordre établi
     * @param game
     */
    public Tour(Game game) {
        this.game = game;

        //par defaut : valeur du jeu de départ
        //this.orderSet = new OrderSet(this);

        this.startBoard = new Board(this);

        

    }

    public Tour(Game game, String position, String orders) {
        this.game = game;

        if (orders != null)  this.textOrders = orders;

        this.startBoard  = new Board(this);
        this.startBoard.setStartPosition(position);
        //this.setSerializedOrders(orders);
    }

    @Override
    public boolean equals(Object o){
    	if (o == null) return false;
    	
    	if (o == this) return true;
    	
    	if (!(o instanceof Tour))return false;
    	
    	return ((Tour)o).tourId == this.tourId;
    }
    
    public void setBoard(Board board){
        this.startBoard = board;
        if (this.startBoard.getTour() != this){
            this.startBoard.setTour(this);
        }
    }

    /**
     * Retourne le plateau de jeu (position de départ
     * @return
     */
    public Board getBoard() {
        return this.startBoard ;
    }

    public Game getGame() {
        return this.game;
    }

    public String getPositions(){
        return this.startBoard.getStartPosition();
    }

    public void setPositions(String positions){
        this.startBoard.setStartPosition(positions);
    }

    /**
     * Indicate if one can modify this turn
     * @return
     */
    public boolean canModify(){
        if (this.isFinished()) return false;
        if (this.game.getGameInfo().isFinished()) return false;
        if (this.game.getGameInfo().isHasplayed()) return false;

        return true;
    }

    public int getYear() {
        return this.annee;
    }

    public void setYear(int year) {
        this.annee = year;
    }

    public int getPhase() {
        return this.phase;
    }

    public void setPhase(int phase) {
        this.phase = phase;
    }

    /** retourne l'identifiant unique d'une phase dans un jeu. */
    public int getIdPhase() {
        int id = (this.getYear() - getStartYear()) * 5;
        id = id + this.getPhase() + 1;

        return id;
    }

    /** Attribue l'identifiant unique d'une phase dans un jeu */
    public void setIdPhase(int id) {
        int y = (id - 1) / 5;
        y = y + getStartYear();
        int p = ((id - 1) % 5);

        this.setYear(y);
        this.setPhase(p);

    }

    public long getId() {
        return this.tourId;
    }

    public void setId(long id) {
        this.tourId = id;
    }

    public boolean isPhaseDisband() {
        return this.phase == PHASE_FALL_DISBAND || this.phase == PHASE_SPRING_DISBAND;
    }

    public boolean isPhaseMouvement() {
        return this.phase == PHASE_FALL || this.phase == PHASE_SPRING;
    }


    public void setCurrentPhase(Tour tour){
        this.phase = tour.phase;
        this.annee = tour.annee;

    }

    public void moveToNextPhase() {

        this.phase++;
        if (phase > Tour.PHASE_FALL_REAJUST) {
            phase = Tour.PHASE_SPRING;
            this.annee++;
        }
    }

    /**
     * Retourne la collection des ordres choisis par l'utilisateur
     * @return
     */
    public OrderSet getOrderSet() {
        if (orders == null) {
            //nouvel ensemble d'ordres
            orders = new OrderSet(this);
            if (this.textOrders != null){
                orders.unserialize(tostring);
            }
        }
        return this.orders;
    }


    
    /**
     * Indicate if turn is finished
     * @return
     */
    public boolean isFinished() {
        if (this.tourNext != null) {
            tourCompleted = true;
        }
        return this.tourCompleted;
    }

    public void setFinished(boolean b) {
        this.tourCompleted = b;
    //suspend l'écouteur
    }

    /**
     * Indique si le joueur connait le resultat du tour
     * @return
     */
    public boolean isDiscovered() {
        return this.tourDiscovered;
    }

    public void setDiscovered(boolean b) {
        this.tourDiscovered = b;
    }

    public Tour getPreviousTour() {
        return this.tourBefore;
    }

    public Tour getNextTour() {
        return this.tourNext;
    }

    

    @Override
    public String toString() {
        if (tostring == null) {
            switch (this.phase) {
                case PHASE_SPRING:
                    tostring = Language.getInstance().get("tour.spring");
                    break;
                case PHASE_FALL:
                    tostring = Language.getInstance().get("tour.fall");
                    break;
                case PHASE_FALL_REAJUST:
                    tostring = Language.getInstance().get("tour.fall") + " - " + Language.getInstance().get("tour.reajust");
                    break;
                case PHASE_FALL_DISBAND:
                    tostring = Language.getInstance().get("tour.fall") + " + " + Language.getInstance().get("tour.disband");
                    break;
                case PHASE_SPRING_DISBAND:
                    tostring = Language.getInstance().get("tour.spring") + " + " + Language.getInstance().get("tour.disband");
                    break;
            }
            tostring = this.annee + " - " + tostring;
        }

        return tostring;
    }

    /**
     * Créé une copie du Tour
     */
    @Override
    public Object clone() {
        throw new UnsupportedOperationException();
    }

    /**
     * Ajoute le texte d'ordre
     * @param country
     * @param text
     */
    public String getSerializedOrders() {
        return this.getOrderSet().serialize();
    }

    public void setSerializedOrders(String ord) {
        if (ord != null && ord.trim().equals("")) {
            ord = null;
        }


        //initialise la liste des ordres pour la reconstruire
        if (ord != null) {
            this.orders = new OrderSet(this);
            this.orders.unserialize(ord);
        }
    }

    /**
     * OBtient l'année de départ du jeu
     * @return
     */
    private int getStartYear() {
        if (startYear == 0) {
            if (this.game.getGameInfo().getNumberOfPlayer() == 2) {
                startYear = 1914;
            } else {
                startYear = 1901;
            }
        }

        return startYear;
    }
}

/*
 * Created on 15 avr. 2007
 *
 *
 */
package diplomacy.game;

import java.util.Collection;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * Implémente un utilisateur.
 * 
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public final class User {

    private long userID = 0;
    private String userName;
    private long lastGameId = 0;
    private String token = null;
    private static Map<String, User> nameToUser;
    private static Map<Long, User> idToUser;


    static {
        nameToUser = new HashMap<String, User>();
        idToUser = new HashMap<Long, User>();
    }

    public static User get(String name) {
        User u = nameToUser.get(name.trim());

        if (u == null) {
            return (get(name, 0));
        }

        return u;
    }

    public static User get(long id) {
        return idToUser.get(id);
    }

    public static User get(String name, long userId) {

        User u = idToUser.get(userId);
        if (u == null) {

            u = nameToUser.get(name);

            //teste si mise à jour
            if (userId > 0 && u != null) {
                nameToUser.remove(name);
            }

            if (u != null) {
                return u;
            }

            u = new User(name, userId);
            if (userId > 0) {
                idToUser.put(userId, u);
            }
            nameToUser.put(name.toLowerCase(), u);
        }

        return u;
    }

    public static Collection<User> allUsers() {
        return idToUser.values();
    }

    private User(String name, long userId) {
        this();
        this.userName = name.trim();
        this.userID = userId;

    }

    private User() {
    }

    public long getUserId() {
        return userID;
    }

    public String getUserName() {
        return userName;
    }

    public long getLastGameId() {
        return lastGameId;
    }

    public void setLastGameId(long lastid) {
        this.lastGameId = lastid;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    @Override
    public boolean equals(Object user) {
        //cherche si l'objet est de type USEr
        if (!(user instanceof User)) {
            return false;
        }
        //compare l'identifiant
        return ((User) user).userID == (this.userID);

    }

    @Override
    public int hashCode(){
        return (int) this.getUserId();
    }

    @Override
    public String toString() {
        return this.userName;
    }
}

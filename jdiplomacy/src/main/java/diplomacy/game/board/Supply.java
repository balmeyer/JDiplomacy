package diplomacy.game.board;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


public final class Supply {

	static{
		init();
	}
	
	/** Liste des dépots */
	private static Collection<Supply> supplyList ;
	
	/** Correspondance nom de la region - dépot */
	private static Map<Province,Supply> provinceToSupply ;
	
	
	private Country originalOwner;
	
	private Province province;
	
	//location on the map
	private int x,y ;

	public static void init(){
		supplyList = new ArrayList<Supply>(32);
		provinceToSupply = new HashMap<Province,Supply>(32);
	}
	
	public static Collection<Supply> getSupplies(){
		return supplyList;
	}

    public static Supply getSupply(Province province){
        return provinceToSupply.get(province);
    }

    public static Supply create(Province p ,Country originalOwner , int x , int y) {
    	
    	Supply s = new Supply(p,originalOwner,x,y);
		supplyList.add(s);
		provinceToSupply.put(p, s);
    	
    	return s;
    }
    
	/**
	 * 
	 * Constructor with original owner
	 * @param originalOwner
	 */
	private Supply(Province province , Country originalOwner , int x , int y) {
		this.originalOwner = originalOwner;
		this.x = x ;
		this.y = y;
		this.setProvince(province);

	}

	public Province getProvince(){
		return province;
	}

	private void setProvince(Province province){
		this.province = province;
		if (province.getSupply() == null){
			province.setSupply(this);
		}
	}
	
	public Country getOriginalOwner() {
		return this.originalOwner;
	}
	
	public boolean isNeutral() {
		return (this.originalOwner == null);
	}

	public int getX(){
		return this.x;
	}
	
	public int getY(){
		return this.y;
	}
	
    @Override
	public String toString(){
		return "[Supply] "+  this.province.getShortName();
	}
}
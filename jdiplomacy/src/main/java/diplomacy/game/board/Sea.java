package diplomacy.game.board;

import diplomacy.game.Ship;
import diplomacy.game.Unit;


public final class Sea extends Region {

	public Sea(Province province) {
		super(province);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see diplomacy.province.Region#acceptUnit(diplomacy.province.Unit)
	 */
    @Override
	public boolean acceptUnit(Unit unit) {
		//Les régions de type Mer n'acceptent ques les flottes.
		return (unit != null && unit.getType() == Unit.SHIP);
	}

	@Override
	public boolean acceptUnit(Class<? extends Unit> claz){
		return claz.equals(Ship.class);
	}
    
	/**
	 * Retourne FAUX : une MER n'accepte jamais de dépot
	 */
	@Override
	public boolean acceptSupply(){
		return false;
	}

	/* (non-Javadoc)
	 * @see diplomacy.board.Region#isConvoyPath()
	 */
	@Override
	public boolean isConvoyPath() {
		return true;
	}

	/* (non-Javadoc)
	 * @see diplomacy.board.Region#isConvoyStop()
	 */
	@Override
	public boolean isConvoyStop() {
		//mer : arrivée de convoi impossible
		return false;
	}
	


	
}
package diplomacy.game.board ;

import diplomacy.game.Army;
import diplomacy.game.Unit;



public final class Earth extends Region
{

	public Earth(Province province){
		super(province);
	}
	
	/* (non-Javadoc)
	 * @see diplomacy.province.Region#acceptUnit(diplomacy.province.Unit)
	 */
	@Override
	public boolean acceptUnit(Unit unit) {
		//Les region de type Terre n'acceptent que les armées
		return (unit.getType() == Unit.ARMY);
	}

	@Override
	public boolean acceptUnit(Class<? extends Unit> claz){
		return claz.equals(Army.class);
	}
	
	@Override
	public boolean acceptSupply(){
		//
		return getProvince().getSupply() != null;

	}

	/* (non-Javadoc)
	 * @see diplomacy.board.Region#isConvoyPath()
	 */
	@Override
	public boolean isConvoyPath() {
		//terre : convoi impossible
		return false;
	}

	/* (non-Javadoc)
	 * @see diplomacy.board.Region#isConvoyStop()
	 */
	@Override
	public boolean isConvoyStop() {
		return false;
	}


}
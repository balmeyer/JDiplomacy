package diplomacy.game.board ;

import diplomacy.game.Unit;



public final class Coast extends Region
{

	public Coast(Province province){
		super(province);
	}
	
	public Coast(Province province, String specialName){
		super(province, specialName);
	}
	
	/* (non-Javadoc)
	 * @see diplomacy.province.Region#acceptUnit(diplomacy.province.Unit)
	 */
	@Override
	public boolean acceptUnit(Unit unit) {
		//Les côtes acceptent tout type d'unité
		return true ;
	}

	@Override
	public boolean acceptUnit(Class<? extends Unit> claz){
		return true;
	}
	
	@Override
	public boolean acceptSupply(){
		//
		return getProvince().getSupply() != null;

	}

	/* (non-Javadoc)
	 * @see diplomacy.board.Region#isConvoyPath()
	 */
	@Override
	public boolean isConvoyPath() {
		
		return false;
	}

	/* (non-Javadoc)
	 * @see diplomacy.board.Region#isConvoyStop()
	 */
	@Override
	public boolean isConvoyStop() {
		return true;
	}


}
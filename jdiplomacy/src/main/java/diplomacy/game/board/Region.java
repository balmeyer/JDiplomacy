package diplomacy.game.board;

import diplomacy.game.Board;
import java.awt.Shape;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import diplomacy.graph.Graph;
import diplomacy.graph.GraphExplorer;
import diplomacy.graph.GraphNode;
import diplomacy.game.Unit;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Super class for all regions. A Region can be occupied by only one unit.
 *  
 */
public abstract class Region {
    //constantes statiques

    public final static int EARTH = 0;
    public final static int COAST = 1;
    public final static int SEA = 2;
    private static Map<String, Region> nameToRegion = new HashMap<String, Region>(90);
    //private static Graph armyGraph = new Graph();
    //private static Graph shipGraph = new Graph();
    /** Province dans la quelle se situe la région. */
    private Province province = null;
    private Collection<Province> provincesAround;
    //variables
    private String name;

    //shape of the region on the map.
    private Shape shape;
    //posiiton
    int x, y;

    public static void init() {
        nameToRegion = new HashMap<String, Region>(90);
    }

    public static Region build(String name , String type, int x , int y){

    	Region r = null;
    	
    	Province province = Province.get(name.substring(0,3));
    	
    	if (type.equalsIgnoreCase("sea")){
    		r = buildSea(province);
    	}
    	
    	if (type.equalsIgnoreCase("earth")){
    		r = buildEarth(province);
    	}
    	
    	if (type.equalsIgnoreCase("coast")){
    		if (name.length() == 3)
    			r = buildCoast(province);
    		else
    			r = buildCoast(province,name);
    	}
    		
    	r.setPosition(x, y);
    	nameToRegion.put(name.toLowerCase(), r);
    	
    	return r;
    }
    
    private static Region buildEarth(Province province) {
        Region r = new Earth(province);
        return r;
    }

    private static Region buildSea(Province province) {
        Region r = new Sea(province);
        return r;
    }

    private static Region buildCoast(Province province) {
        Region r = new Coast(province);
        return r;
    }

    public static Region buildCoast(Province province, String specialName) {
        Region r = new Coast(province, specialName);
        return r;
    }

    public static Region get(String name) {
        Region r = nameToRegion.get(name.toLowerCase());

        if (r == null){
            throw new IllegalArgumentException("region not found : " + name);
        }

        return r;

    }

    public static Collection<Region> getRegions() {
        return nameToRegion.values();
    }

    /**
     * Ajout des régions existances dans un graph
     *
     */
    public static void completeGraph(Graph graph) {
        GraphExplorer ge = new GraphExplorer(graph);

        //methode 1
        for (GraphNode node = null; ge.hasNext();) {
            node = ge.next();
            Region region = Region.get(node.getName());
            if (region == null) {
            }
            assert (region != null);
            node.setRegion(region);
        }

        //methode 2
        Iterator<Region> it = nameToRegion.values().iterator();
        while (it.hasNext()) {
            graph.addRegion(it.next());
        }
    }

    /**
     * Get common regions for support
     * @param board
     * @param attack
     * @param support
     * @return
     */
    public static Collection<Region> getCommonRegionsForSupport(Board board, Region attack, Region support){
        List<Region> list = new ArrayList<Region>(8);

        Unit uattack = board.getUnit(attack);
        Unit usupport = board.getUnit(support);

        //unit moving can be convoyed ? 
        if (uattack.getType() == Unit.ARMY && uattack.getRegion().isConvoyStop()){
        	list.addAll(usupport.getGraph().getNode(support).getRegionsAround());
        } else {
        
	        //regions around attacking unit
	        Collection<Region> aroundAttack = uattack.getGraph().getNode(attack).getRegionsAround();
	        
	        
	        for (Region r : usupport.getGraph().getNode(support).getRegionsAround()){
	            if (aroundAttack.contains(r)){
	                list.add(r);
	            }
	        }
        
        }
        //purge
        int i = 0;
        while(i < list.size()){
        	Region r = list.get(i);
        	if (!r.acceptUnit(uattack)){
        		list.remove(i);
        	} else {
        		i++;
        	}
        }
        
        return list;
    }

    /**
     *
     */
    protected Region(Province prov, String specialName) {
        this.province = prov;
        this.name = specialName;
    }

    protected Region(Province prov) {
        this.province = prov;
        this.name = this.province.getShortName();
    }

    public Province getProvince() {
        return this.province;
    }

    /**
     * Retourne les provinces adjacentes
     * @return
     */
    public Collection<Province> getProvincesAround() {
        if (this.provincesAround == null) {

            GraphNode gnArmy = Unit.getGraph(Unit.ARMY).getNode(this);
            GraphNode gnFloat = Unit.getGraph(Unit.SHIP).getNode(this);

            this.provincesAround = new ArrayList<Province>(12);

            this.provincesAround.addAll(getProvFromNode(gnArmy));
            this.provincesAround.addAll(getProvFromNode(gnFloat));


        }

        return this.provincesAround;
    }

    private Collection<Province> getProvFromNode(GraphNode node) {
        List<Province> liste = new ArrayList<Province>(8);

        if (node != null) {
            for (GraphNode n : node.getSuccessors()) {
                Province pr = n.getRegion().getProvince();
                if (!liste.contains(n)) {
                    liste.add(pr);
                }


            }
        }
        return liste;

    }

    /**
     *
     */
    public String getName() {
        return name;
    }

    /**
     *
     *
     */
    public Shape getShape() {
        return this.shape;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (!(o instanceof Region)) {
            return false;
        }

        return o.toString().equals(this.toString());
    }


    /**
     * Ce terrain accepte-t-il une unité particuliére ?
     */
    public abstract boolean acceptUnit(Unit unit);

    public abstract boolean acceptUnit(Class<? extends Unit> claz);
    
    /**
     * Cette région peut-elle accepter un dépot ?
     * @return
     */
    public abstract boolean acceptSupply();

    /** Indique si la région est un chemin pour le convoi*/
    public abstract boolean isConvoyPath();

    /** Indique si la région est un départ ou une arrivée de convoi. */
    public abstract boolean isConvoyStop();
}

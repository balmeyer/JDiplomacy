/*
 * Created on 10 avr. 2004
 *
 * Vovau Jean-Baptiste - balmeyer@dev.java.net
 */
package diplomacy.game.board;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import diplomacy.gui.ImageProvider;
import diplomacy.language.Language;


/**
 * Objet pays.
 * 
 * Created on 10 avr. 2004.
 * 
 * @author Jean-Baptiste Vovau - balmeyer@dev.java.net
 * @version 1.0
 */
public final class Country {
	
	private final static Country [] arrayOfCountry = new Country [7];
	private final static Map<String,Country> nameToCountry = new HashMap<String,Country>(7);
	
	private static List<Country> listCountry = null;
	
	/**
	 * Initialisation de la partie statique
	 */

	private Collection<Supply> homeSupplies;
	
	/** Nom du pays. */
	private String name;
	
	private int countryIndex = 0;

	public static void init(){
		nameToCountry.clear();
		listCountry = null;
	}
	
	//private volatile Image bigFlag , smallFlag , bigUnit, smallUnit;

	/**
	 * Cr�ation d'un objet pays d'apr�s son nom
	 * @param name
	 * @return
	 */
	public static Country build(String name , int index){
		Country c = get(name);
		if (c == null){
			c = new Country(name);
			//index
			c.setIndex(index);
			arrayOfCountry[index] = c;
			nameToCountry.put(name.toLowerCase(), c);
			listCountry = null; // force regenerate
		}
		
		
		
		return c;
	}
	
	
	public static Country get(String name){
		return  nameToCountry.get(name.toLowerCase());
	}
	
	/**
	 * Retourne un pays depuis son index
	 * @param index
	 * @return
	 */
	public static Country get(int index){
		return  arrayOfCountry[index];
	}
	
	/**
	 * Liste de tous les pays
	 * @return
	 */
	public static Collection<Country> getCountries(){
		if (listCountry == null){
			listCountry = new ArrayList<Country>();
			for(Country c : arrayOfCountry){
				listCountry.add(c);
			}
		}
		return listCountry;
	}
	

   
	private Country(String name) {
		this.name = name;
	
	}

	/**
	 * Returns original supplies
	 */
	public Collection<Supply> getHomeSupplies(){
		if (this.homeSupplies == null || homeSupplies.size() == 0){
			this.homeSupplies = new ArrayList<Supply>(4);
			Iterator<Supply> it = Supply.getSupplies().iterator();
			while (it.hasNext()){
				Supply s = it.next();
				if (this.equals(s.getOriginalOwner())){
					homeSupplies.add(s);
				}
			}
		}
		
		return homeSupplies;
	}

	/**
	 * Coutnry name
	 * @ret
	 * urn
	 */
	public String getDisplayName() {
		//return this.name;
                
                return Language.getInstance().get("country." 
                        + name.toLowerCase());
	}
        
        public String getGameName(){
            return this.name;
        }

	public Image getSmallFlag(){
		return ImageProvider.getInstance().get("Images/flag"
				+ this.countryIndex + "_s.gif");
	}
	
	public Image getBigFlag(){
		return ImageProvider.getInstance().get("Images/flag"
				+ this.countryIndex + "_b.gif");
	}
	
	public Image getSmallUnitImage() {
		return ImageProvider.getInstance().get("Images/a"
				+ this.countryIndex + "_s.gif");
	}
	
	public Image getBigArmyImage() {
		return ImageProvider.getInstance().get("Images/a"
				+ this.countryIndex + "_b.gif");
	}
	
	public Image getBigFloatImage() {
		return ImageProvider.getInstance().get("Images/f"
				+ this.countryIndex + "_b.gif");
	}
	
	public Image getSupplyImage() {
		return ImageProvider.getInstance().get("Images/c"
				+ this.countryIndex + "_s.gif");
	}
	
    @Override
	public String toString(){
		return this.name;
	}
	

	@Override
	public boolean equals(Object obj){
		if (obj == null) return false;
		
		if (obj instanceof Country
				&& ((Country) obj).getGameName().equals(this.name)){
			return true;
		}
		
		return false;
	}
	
	/**
	 * Identifiant num�rique du pays (0-6)
	 * @return
	 */
	public int getIndex(){
		return countryIndex;
	}
	
	private void setIndex(int i){
		this.countryIndex = i;
	}
	
}

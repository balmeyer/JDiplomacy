package diplomacy.game;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import diplomacy.Main;
import diplomacy.conversation.ConversationManager;
import diplomacy.game.board.Country;
import diplomacy.order.CountryOrders;
import diplomacy.order.NetOrderFetcher;
import diplomacy.order.OrderFetcher;
import diplomacy.tools.MapBuilder;
import diplomacy.tools.MapInfo;

/**
 *	Objet de jeu. Cette objet instancie une partie � un instant
 *	donné, avec la situation de ses unit�s, la phase de jeu, etc.
 *
 *
 */
public final class Game {

	
	public static void initGame(){
		//TODO implements multi map and themes loading
		MapBuilder.loadMap();
	}
	
    /** Constructeur classique. Sans argument, le jeu crée est en mode arbitre,
     * c'est à dire tous les ordres sont entrés par un seul joueur. */
    public Game(GameInfo gid, User player) {
        this.gameId = gid;
        this.tours = new ArrayList<Tour>();

        this.orderFetcher = new NetOrderFetcher(this);
        this.orderFetcher.setGame(this);
        this.conversationManager = new ConversationManager(this);
        
        //TODO add conversation listeners
        this.conversationManager.start();

    }

    public GameInfo getGameInfo() {
        return this.gameId;
    }

    public GameResult getResult(){
        return this.result;
    }

    public void setResult(GameResult result){
        this.result = result;
    }

        /** Etablie un état du jeu d'aprés une chaine */
    public void restoreTour(Tour tour) {
        this.setMyCurrentTour(tour);
    }

    public void sendOrders(CountryOrders co) {
    	
        Tour last = getLastTour();
        //liste les ordres par pays
        Country country = co.getCountry();
        if (!this.countryToTextOrders.containsKey(country)) {
            this.countryToTextOrders.put(country, co);

            User user = this.getUser(country);
            //restaure les ordres du joueur
            if (Main.getUser().equals(user)) {
                last.getOrderSet().addOrder(co);
            }
        }

    }

            /**
     * Retourne l'objet ordre-texte pour un pays
     * @param c
     * @return
     */
    public CountryOrders getOrders(Country c) {
        return (CountryOrders) countryToTextOrders.get(c);
    }



    /**
     * Retourne le synchronisateur d'ordres.
     * @return
     */
    public OrderFetcher getOrderFetcher() {
        return this.orderFetcher;
    }

    public List<Tour> getTours() {
        return this.tours;
    }

    /**
     * Retourne le dernier tour en cours (non termin� ou non d�couvert)
     * @return
     */
    public Tour getLastTour() {
        Tour t = null;
        Iterator<Tour> it = this.getTours().iterator();

        while (it.hasNext()) {
            t = it.next();
            //Tour non découvert ou non terminé : on sort, c'est le dernier tour
            if (!t.isFinished()) {
                break;
            }
        }


        return t;
    }

    public Tour getCurrentTour() {
        //si le tour courrant est null : dernier tour
        if (currentTour == null) {
            currentTour = this.getLastTour();
        }


        return (this.currentTour);
    }

    /**
     * Ajoute un utilisateur et son pays associé
     * @param user
     * @param c
     */
    public void addUserCountry(User user, Country c) {
        //attribue l'utilisateur
        this.countryToUser.put(c, user);
    }

    public boolean controls(User user, Country c){
        return user.equals(getUser(c));
    }

    public User getUser(Country c){
        return countryToUser.get(c);
    }

        /**
     * Indique si le joueur peut entrer ou modifier des ordres pour ce tour.
     * @return
     */
    public boolean canModifyOrder() {

    	//
    	if (this.gameId.isHasplayed()) return false;
    	
        //teste les ordres déjà en cours
        User me = Main.getUser();
        Board board = getLastTour().getBoard();
        Tour lastTour = getLastTour();

        synchronized (countryToTextOrders) {
            //parse les ordres
            for (CountryOrders co : this.countryToTextOrders.values()) {
                User user = this.getUser(co.getCountry());
                if (me.equals(user)) {
                    return false;
                }
            }
        }

        //--------------------------------
        //teste si le joueur a besoin de rentrer des ordres besoin
        if (lastTour.getPhase() == Tour.PHASE_FALL_DISBAND ||
                lastTour.getPhase() == Tour.PHASE_SPRING_DISBAND) {

            for (Country co : Country.getCountries()) {
                User user = this.getUser(co);
                //pays avec retraite dirigé par l'utilisteur
                if (me.equals(user) && board.needDisband(co)) {
                    return true;
                }
            }
            //pas de pays necessitant une retraite
            return false;
        }

        //ok, pas d'objection !
        return true;
    }


    /**
     * Attribut le tour courrant
     * @param tour
     */
    protected void setMyCurrentTour(Tour tour) {
        this.currentTour = tour;
        this.broadcastTourChanged(tour);
    }

    /**
     * Ajoute un écouteur de jeu
     * @param gl
     */
    public void addGameListener(GameListener gl) {
        if (!this.gameListeners.contains(gl)) {
            this.gameListeners.add(gl);
        }
    }

    public void removeGameListener(GameListener gl) {
        if (this.gameListeners.contains(gl)) {
            this.gameListeners.remove(gl);
        }
    }


    /**
     * Déclenche l'évennement changement de tour
     * @param tour
     */
    private void broadcastTourChanged(Tour tour) {
        
        Iterator<GameListener> it = this.gameListeners.iterator();
        while (it.hasNext()) {
            (it.next()).tourChanged(tour);
        }


    }


    /**
     * Affiche un message à l'utilisateur
     * @param msg
     */
    public void userMessage(String msg) {
        Main.getGameFrame().displayMessage(msg);
    }

    public void close(){
    	this.gameListeners.clear();
    	this.orderFetcher.removeAllListeners();
    	this.orderFetcher.close();
    	this.conversationManager.close();
    }
    
    /**
     * Ajout d'un tour à la construction
     * @param tour
     */
    public void addTour(Tour tour) {
    	
    	//tour already exists 
    	for (Tour t : this.getTours()){
    		if (t.equals(tour)){
    			//refresh
    			t.setFinished(true);
    			t.setPositions(tour.getPositions());
    			t.getOrderSet().unserialize(tour.getSerializedOrders());
    			
    			return;
    		}
    	}
    	
        //tours précédents ?
        if (this.tours.size() > 0) {
            Tour before = tours.get(tours.size() - 1);
            tour.tourBefore = before;
            before.tourNext = tour;
        }

        this.tours.add(tour);

        synchronized (this.gameListeners) {
            for (GameListener gl : this.gameListeners) {
                gl.tourAdded(tour);
            }
        }

    }


    
    /**
	 * @param conversationSet the conversationSet to set
	 */
	public void setConversationManager(ConversationManager conversationSet) {
		this.conversationManager = conversationSet;
	}

	/**
	 * @return the conversationSet
	 */
	public ConversationManager getConversationManager() {
		return conversationManager;
	}
	/** retourne l'instance courrant de jeu */
    /*
    public static Game getCurrentGame(){
    return currentGame ;
    } */
    /** FENTETRE d'ordres*/
    //static ConsoleOrdre fen ;
    /** Fenetre de carte */

    private GameInfo gameId;
    private GameResult result;
    private ConversationManager conversationManager;
    
    /** Pour synchroniser les ordres*/
    private OrderFetcher orderFetcher;
    private Tour currentTour;
    //private int an, tour, phase;
    /** Etat de chaque province */
    /** Liste des ordres en attente d'envoi */
    //private UserOrderSet pendingOrders ;
    /** Liste des tours de jeu */
    private List<Tour> tours;
    //Ecouteur de jeu
    private final Collection<GameListener> gameListeners = new ArrayList<GameListener>();

    /** Gestion des ordres pour chaque pays. */
    private final Map<Country, CountryOrders> countryToTextOrders = new HashMap<Country, CountryOrders>(7);

    private final Map<Country,User> countryToUser = new HashMap<Country,User>();



    static final Color COULEUR_PRINTEMPS = new Color(213, 251, 215);
    static final Color COULEUR_AUTOMNE = new Color(250, 209, 120);


    //ici se termine la class Game...
    //17/11/03... vive le bibi !
}


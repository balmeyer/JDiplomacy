package diplomacy.conversation;

import java.util.ArrayList;
import java.util.List;

/**
 * Event thrown when new conversations are received.
 * @author Jean-Baptiste Vovau
 *
 */
public class ConversationEvent {

	private List<Conversation> newConversations;
	private ConversationManager manager ;
	
	protected ConversationEvent(ConversationManager cm){
		this.manager = cm;
		this.newConversations = new ArrayList<Conversation>();
	}
	
	/**
	 * Get new conversations added since last time
	 * @return
	 */
	public List<Conversation> getNewConversations(){
		return newConversations;
	}

	/**
	 * Get all conversation, ordered by date
	 * @return
	 */
	public List<Conversation> getMainConversationsOrderByDate(){
		return this.manager.getConversationOrderByUpdate();
	}

}

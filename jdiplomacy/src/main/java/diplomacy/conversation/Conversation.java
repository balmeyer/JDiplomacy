package diplomacy.conversation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import diplomacy.game.User;

/**
 * Game discussion
 * @author JBVovau
 *
 */
public class Conversation implements Comparable<Conversation> {

	private long id;
	
	private String body;
	
	private User user;
	
	private final List<User> restrictedUsers = new ArrayList<User>();
	
	private Date writeDate;
	
	private Date conversationUpdate;
	
	private long parentid;
	
	private Conversation parent;
	
	private List<Conversation> children = new ArrayList<Conversation>();

	
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @return the excludelist
	 */
	public List<User> getPrivatelist() {
		return restrictedUsers;
	}

	/**
	 * @param writeDate the writeDate to set
	 */
	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	/**
	 * @return the writeDate
	 */
	public Date getWriteDate() {
		return writeDate;
	}


	/**
	 * @param conversationUpdate the conversationUpdate to set
	 */
	public void setConversationUpdate(Date conversationUpdate) {
		this.conversationUpdate = conversationUpdate;
	}

	/**
	 * @return the conversationUpdate
	 */
	public Date getConversationUpdate() {
		return conversationUpdate;
	}

	/**
	 * @return the parent
	 */
	public Conversation getParent() {
		return this.parent;
	}

	public void setParent(Conversation parent){
		this.parent = parent;
	}
	/**
	 * @param parentid the parentid to set
	 */
	public void setParentid(long parentid) {
		this.parentid = parentid;
	}

	/**
	 * @return the parentid
	 */
	public long getParentid() {
		return parentid;
	}


	/**
	 * @return the children
	 */
	public List<Conversation> getChildren() {
		return children;
	}
	
	public void addChild(Conversation conv){
		this.children.add(conv);
		conv.setParent(this);
	}
	
	@Override
	public boolean equals(Object o){
		if (o == null) return false;
		
		if (!(o instanceof Conversation)) return false;
		
		return ((Conversation) o).id == this.id;
	}
	
	@Override
	public String toString(){
		return body;
	}

	@Override
	public int compareTo(Conversation o) {

		//if (!(o instanceof Conversation)) return 0;
		
		return -1 * (o).getConversationUpdate()
			.compareTo(this.getConversationUpdate());
		
	}
}

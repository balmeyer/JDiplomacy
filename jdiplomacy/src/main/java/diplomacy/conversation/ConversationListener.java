package diplomacy.conversation;


/**
 * Listen for conversation manager
 * @author JBVovau
 *
 */
public interface ConversationListener {

	public void newConversation(ConversationEvent event);
	
}

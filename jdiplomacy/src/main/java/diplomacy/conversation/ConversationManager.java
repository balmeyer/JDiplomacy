package diplomacy.conversation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import diplomacy.Main;
import diplomacy.access.exception.AccessException;
import diplomacy.game.Game;
import diplomacy.game.User;

/**
 * Manage, load, send conversations between players.
 * @author JBVovau
 *
 */
public final class ConversationManager {

	private Game game;
	private Timer timer ;
	private final Collection<ConversationListener> listeners = new ArrayList<ConversationListener>();
	
	private final List<Conversation> conversations = new ArrayList<Conversation>();
	private final List<Conversation> orderedDateConversation = new ArrayList<Conversation>();
	
	
	private final Map<Long,Conversation> idToConversation = new HashMap<Long,Conversation>();;
	
	private Conversation lastconversation;
	
	//report request when no activity is detected
	private int maxDelay = 0;
	private int askCount = 0;
	
	public ConversationManager(Game game){
		this.game = game;
		
	}
	
	/**
	 * Start background worker that load new conversations
	 */
	public void start(){
		this.timer = new Timer("TIMER_CONVERSATION",true);
		this.timer.schedule(new ConversationWorker(), 1000, 8000);
	}
	
	public void close(){
		if (this.timer != null){
			this.timer.cancel();
		}
		this.conversations.clear();
		this.idToConversation.clear();
		this.listeners.clear();
	}
	
	
	/**
	 * Send new message
	 * @param msg
	 * @param privateUsers
	 */
	public void newConversation(String msg , List<User> privateUsers){
		this.newConversation(msg, null , privateUsers);
	}
	
	public void newConversation(String msg, Long id , List<User> privateUsers){
		//Fetch conversation reply
		Conversation c = null;
		
		if (id != null){
			c = this.getConversationById(id);
		}
		
		try {
			Main.getDBAccess().getXmlProvider().newConversation(game, c, msg,privateUsers);
			this.maxDelay = 0;
			this.fetchNewConversations();
		} catch (AccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Collection<ConversationListener> getListeners(){
		//TODO add "add method"
		return this.listeners;
	}
	
	public Collection<Conversation> getConversations(){
		return this.conversations;
	}
	
	public List<Conversation> getConversationOrderByUpdate(){
		if (this.orderedDateConversation == null){
			this.buildOrderedConversations();
		}
		
		return this.orderedDateConversation;
	}
	
	public Conversation getLastConversation(){
		return this.lastconversation;
	}
	
	public Conversation getConversationById(long id){
		return this.idToConversation.get(id);
	}
	
	private void fetchNewConversations(){
		
		//new entries
		ArrayList<Conversation> newEntries = new ArrayList<Conversation>();
		
		//delay when no activity ?
		if (this.askCount++ < this.maxDelay) return;
		else this.askCount = 0;
		
		try {
			List<Conversation> list = Main.getDBAccess().fetchConversation(game);
		
			synchronized(this.conversations){
				for(Conversation c : list){
					//conversation already exists
					if (this.idToConversation.containsKey(c.getId())) continue;
					
					//add conversation
					if (c.getParentid() == 0){
						conversations.add(c);
					} else {
						//find parent
						Conversation parent = getConversationById(c.getParentid());
						if (parent != null){
							parent.getChildren().add(c);
							c.setParent(parent);
						}
					}
					idToConversation.put(c.getId(), c);
					lastconversation = c;
					newEntries.add(c);
					
					//put update date
					Conversation actual = c;
					while (actual != null){
						actual.setConversationUpdate(c.getWriteDate());
						actual = actual.getParent();
					}
				}
			}
			
			if (newEntries.size() > 0){
				
				
				
				//sort
				this.buildOrderedConversations();
				
				//send new conversations to listeners
				ConversationEvent event = new ConversationEvent(this);
				event.getNewConversations().addAll(newEntries);
				for(ConversationListener l : listeners){
					l.newConversation(event);
				}
				
				//activity !
				this.maxDelay = 0;
				
			} else {
				//no activity
				this.maxDelay++;
				if (this.maxDelay > 10) this.maxDelay = 10;
				this.askCount = 0;
			}
		
		} catch (AccessException e) {
			e.printStackTrace();
		}
	}
	
	private void buildOrderedConversations(){
		
		Object [] all = this.conversations.toArray();
		
		Arrays.sort(all);
		synchronized(orderedDateConversation){
			this.orderedDateConversation.clear();
			for(Object o : all){
				this.orderedDateConversation.add((Conversation) o);
			}
		}
	}
	
	/**
	 * Fetch new conversations
	 * @author JBVovau
	 *
	 */
	private class ConversationWorker extends TimerTask{

		@Override
		public void run() {
			//
			fetchNewConversations();
		}
		
	}
	
}

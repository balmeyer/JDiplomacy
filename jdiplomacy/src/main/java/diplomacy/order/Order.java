/*
 * Created on 18 mars 2007
 *
 *
 */
package diplomacy.order;

import java.util.ArrayList;
import java.util.List;

import diplomacy.game.Board;
import diplomacy.game.Unit;
import diplomacy.game.board.Country;
import diplomacy.game.board.Province;
import diplomacy.game.board.Region;
import diplomacy.graph.Graph;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 * 
 *  
 */
public final class Order implements Comparable<Order> {

    public static final int ORDRE_MOUVEMENT = 1;
    public static final int ORDRE_SOUTIEN = 2;
    public static final int ORDRE_CONVOI = 3;
    public static final int ORDRE_ADD_UNIT = 4;
    public static final int ORDER_DEL_UNIT = 5;
    public static final int ERREUR_ORDRE_TROP_COURT = 1;
    public static final int ERREUR_TYPE_UNITE_INCONNU = 2;
    public static final int ERREUR_PROVINCE_BASE_INCONNUE = 3;
    public static final int ERREUR_TYPE_ORDRE_INCONNU = 4;
    public static final int ERREUR_PROVINCE_DESTINATION_INCONNUE = 5;
    public static final int ERREUR_UNITE_NON_POSSEDEE = 6;
    public static final int ERREUR_DEPLACEMENT_IMPOSSIBLE = 7;
    public static final int ERREUR_DEPOT_INCORRECT = 8;
    public static final int ERREUR_DEPOT_NON_POSSEDE = 9;
    //-------------
    private String text;
    private String serializedtext ;
    private volatile Country country;
    private int typeOrdre = 0,  typeUnit;
    private int erreur = 0;
    private boolean judged = false;
    private int force = 1;
    
    private String marks;
    
    private Region regionStart,  regionEnd,  regionBase;
    
    
    
    private Unit unit;
    /** Liste des régions empruntées par une armée pour un convoi */
    private List<Region> convoiPath;
    //private Board board ;

    public static Order create(Board board,String text) {
        return new Order(board,text);
    }

    public static Order create(Board board, Country c, String text) {
        return new Order(board, c, text);
    }



    /**
     * Constructeur privé, pour la désérialsation
     *  
     */
    private Order() {
    }

    private Order(Board board ,Country c, String text) {
        this(board, text);
    }

    private Order(Board board , String myOrder) {

    	this.serializedtext = myOrder;
    	
    	String textOrder = myOrder;
    	this.marks = null;
    	String path = null;

    	if (myOrder != null && myOrder.contains(":")){
    		String [] strs = myOrder.split(":");
    		textOrder = strs[0];
    		marks = strs[1];
    	}
    	
    	int a=  textOrder.indexOf("[");
    	if (a > 0){
    		path = textOrder.substring(a).trim();
    		textOrder = textOrder.substring(0,a).trim();
    	}
    	
        //retire le double espace
        StringBuilder sb = new StringBuilder(textOrder.trim());
        int doubleSpace = 0;
        do {
            doubleSpace = sb.toString().indexOf("  ");
            if (doubleSpace >= 0) {
                sb.deleteCharAt(doubleSpace);
            }
        } while (doubleSpace >= 0);

        this.text = sb.toString();

        //analyse l'ordre
        //Main.out("ordre = " + text);
        String[] words = text.toUpperCase().split(" ");
        if (words.length < 2) {
            this.erreur = ERREUR_ORDRE_TROP_COURT;
            return;
        }

        //---------------------------------
        //type d'unité
        if (words[0].equals("A") || words[0].equals("F")) {
            if (words[0].equals("A")) {
                this.typeUnit = Unit.ARMY;
            } else {
                this.typeUnit = Unit.SHIP;
            }
        } else {
            //-------------------
            //ajout ou suppression d'unité
            if (words[0].equals("+") || words[0].equals("-")) {
                //type d'ajout / suppression
                if (words[0].equals("+")) {
                    this.typeOrdre = Order.ORDRE_ADD_UNIT;
                } else {
                    this.typeOrdre = Order.ORDER_DEL_UNIT;
                }


                //----type d'unité
                if (words[1].equals("A") || words[1].equals("F")) {
                    if (words[1].equals("A")) {
                        this.typeUnit = Unit.ARMY;
                    } else {
                        this.typeUnit = Unit.SHIP;
                    }
                }
            }
        }

        //--- verif type unit
        if (this.typeUnit == Unit.INCONNU) {
            //type d'unité inconnu
            this.erreur = ERREUR_TYPE_UNITE_INCONNU;
            return;
        }

        //Région de départ
        String wordBase = (this.typeOrdre == Order.ORDER_DEL_UNIT || this.typeOrdre == Order.ORDRE_ADD_UNIT) ? words[2]
                : words[1];

        this.regionBase = Region.get(wordBase);

        if (regionBase == null) {
            //seconde chance : nom de province ?
            Province pr1 = Province.get(wordBase);
            if (pr1 != null) {
                //récupère la région d'aprés l'unité qui s'y trouve
                Unit u = board.getUnit(pr1);
                if (u != null) {
                    regionBase = u.getRegion();
                }

            }
            if (regionBase == null) {
                this.erreur = ERREUR_PROVINCE_BASE_INCONNUE;
                return;
            }
        }

        //
        this.country = board.getCountry(this.regionBase);

        //teste si ordre ok pour ajustement
        if (this.typeOrdre == Order.ORDRE_ADD_UNIT) {
            //teste si dépot
            if (!this.regionBase.getProvince().isSupply() || regionBase.getProvince().getSupply().isNeutral()) {
                this.erreur = ERREUR_DEPOT_INCORRECT;
                return;
            }

            //pays concerné
            this.country = this.regionBase.getProvince().getSupply().getOriginalOwner();
            this.regionStart = regionBase;
            return;
        }

        if (this.typeOrdre == Order.ORDER_DEL_UNIT) {
            this.unit = board.getUnit(regionBase);

            if (this.unit != null) {
                this.country = this.unit.getCountry();
            } else {
                this.erreur = ERREUR_UNITE_NON_POSSEDEE;
            }
            this.regionStart = regionBase;

            return;
        }

        //région de base connu : d'abord retraite
        this.unit = board.getDislodgedUnit(regionBase.getProvince());
        if (this.unit == null) {
            this.unit = board.getUnit(regionBase);
        }

        if (unit == null) {
            this.erreur = ERREUR_TYPE_UNITE_INCONNU;
            return;
        }

        //transforme la région de base (cas stp e) selon l'unité
        this.regionBase = this.unit.getRegion();

        if (words.length > 2) {
            //peu d'ordre : pas de mouvement
            if (words[2].equals("-")) {
                this.typeOrdre = ORDRE_MOUVEMENT;
            }

            if (words[2].equals("C")) {
                this.typeOrdre = ORDRE_CONVOI;
            }

            if (words[2].equals("S")) {
                this.typeOrdre = ORDRE_SOUTIEN;
            }

            if (this.typeOrdre == 0) {
                this.erreur = ERREUR_TYPE_ORDRE_INCONNU;
                return;
            }

        } else {
            //que deux mots : reste en place
            this.regionStart = this.regionBase;
            this.regionEnd = this.regionBase;
            return;
        }

        //région de départ
        Region rg2 = Region.get(words[3]);
        if (rg2 == null) {
            //2éme chance
            Province pr2 = Province.get(words[3]);
            if (pr2 != null && board.getUnit(pr2) != null) {
                rg2 = board.getUnit(pr2).getRegion();
            }
        }

        //on sort
        if (rg2 == null) {
            this.erreur = Order.ERREUR_PROVINCE_DESTINATION_INCONNUE;
            return;
        }

        //région arrivée
        Region rg3 = null;
        if (words.length >= 6) {
            rg3 = Region.get(words[5]);
            //2 chances
            if (rg3 == null) {
                Province pr3 = Province.get(words[5]);
                if (pr3 != null && board.getUnit(pr3) != null) {
                    rg3 = board.getUnit(pr3).getRegion();
                }
            }
        }

        switch (this.typeOrdre) {
            case ORDRE_MOUVEMENT:
                this.regionStart = this.regionBase;
                this.regionEnd = rg2;
                break;
            case ORDRE_SOUTIEN:
                if (rg3 == null && rg2 != null && regionBase != null) {
                    rg3 = rg2;
                }
            case ORDRE_CONVOI:
                this.regionStart = rg2;
                this.regionEnd = rg3;
                break;
        }

        //région d'arrivée
        //pays emetteur de l'ordre
        if (this.unit != null) {
            this.country = this.unit.getCountry();
        } else {
            this.erreur = Order.ERREUR_TYPE_UNITE_INCONNU;
            return;
        }

        //Controle du type d'unité
        if (this.regionEnd != null && !regionEnd.acceptUnit(this.unit)) {
            this.erreur = Order.ERREUR_DEPLACEMENT_IMPOSSIBLE;
            return;
        }

        //vérifie le déplacement
        if (this.regionEnd != null && this.typeOrdre == Order.ORDRE_SOUTIEN
                && !canSupport(regionBase, regionEnd)) {
            this.erreur = Order.ERREUR_DEPLACEMENT_IMPOSSIBLE;
            return;

        }

        //Déplacement non autorisé si Flotte
        if (this.regionStart != null && this.regionEnd != null
                && this.typeOrdre != ORDRE_CONVOI
                && this.unit.getType() == Unit.SHIP
                && !regionStart.equals(regionEnd) 
                && !this.unit.getGraph().areAdjacent(regionBase, regionEnd)) {
            this.erreur = Order.ERREUR_DEPLACEMENT_IMPOSSIBLE;
        }

        if (this.regionStart == null){
            System.out.println("NON !!!");
        }

        if (this.regionEnd == null){
            System.out.println("NON !!!");
        }

        //Convoy path
        if (path != null){
        	this.convoiPath = new ArrayList<Region>();
        	path = path.replace("[","").replace("]","");
        	for(String c : path.split(",")){
        		Region rec = Region.get(c);
        		this.convoiPath.add(rec);
        	}
        	this.sortConvoyPath();
        }

        
    }

    /**
     * Retourne le texte brut de l'ordre
     * 
     * @return
     */
    public String getText() {
        return this.text;
    }

    public String getSerializedText(){
    	return this.serializedtext;
    }
    
    @Override
    public String toString() {
        return this.text;
    }

    public Country getCountry() {
        return this.country;
    }

    /**
     * Indique si l'ordre a été jugé
     * 
     * @return
     */
    public boolean isJudged() {
        return this.judged || !this.isCorrect();
    }

    /**
     * Indique si l'ordre est r�ussi
     * 
     * @return
     */
    public boolean isSuccess() {
        return this.marks == null;
    }





    /**
     * Indique la force pour un mouvement (déplacement, ou attaque)
     * 
     * @return
     */
    public int getForce() {
        return this.force;
    }

    protected void setForce(int n) {
        this.force = n;
    }



    /**
     * Indique si l'ordre est correct
     * 
     * @return
     */
    public boolean IsParsed() {
        return (regionBase != null && unit != null);
    }

    /**
     * Indique si l'ordre est visible
     * 
     * @return
     */
    public boolean isVisible() {
        return true;
    }

    /**
     * Indique si l'ordre est correct
     * 
     * @return
     */
    public boolean isCorrect() {
        return this.erreur == 0;
    }

    public boolean isOrderOfMouvement(){
        return this.getOrderType() == ORDRE_MOUVEMENT;
    }

    public boolean isSwapOf(Order o){

        if (this.getOrderType() != ORDRE_MOUVEMENT) return false;
        if (o.getOrderType() != ORDRE_MOUVEMENT) return false;

        if (!this.isCorrect() ||!o.isCorrect()) return false;

        if (!this.getStartRegion().equals(o.getEndRegion())) return false;
        if (!o.getStartRegion().equals(this.getEndRegion())) return false;

        return (o.getForce() == this.getForce());

    }

    /**
     * Cherche si l'ordre donnée est un bounce
     * de l'ordre
     * @param o
     * @return
     */
    public boolean isBounceOf(Order o){

        if (this.getOrderType() != ORDRE_MOUVEMENT) return false;
        if (o.getOrderType() != ORDRE_MOUVEMENT) return false;

        if (!this.isCorrect() ||!o.isCorrect()) return false;

        //vérifie si bien mouvement
        if (this.getStartRegion().equals(this.getEndRegion())) return false;
        if (o.getStartRegion().equals(o.getEndRegion())) return false;

        if (!this.getEndRegion().equals(o.getEndRegion())) return false;

        return (o.getForce() == this.getForce());

    }

    /**
     * Indique si le résultat de l'ordre implique un mouvement pour l'unité(
     * changent de région)
     * 
     * @return
     */
    public boolean isMovingOrder() {
        return !(this.getActionRegion().equals(this.getResultRegion()));
    }

    /**
     * Indique que ce mouvement a besoin d'un convoi
     * 
     * @return
     */
    public boolean needConvoi() {
        return this.isMouvement() && this.getUnit().canBeConvoyed() 
                && !this.getUnit().getGraph().areAdjacent(this.getStartRegion(),
                this.getEndRegion());
    //	&& this.unit.getGraph().areNear(this.getEndRegion(),
    // this.getStartRegion());
    }

    /**
     * Indique si cet ordre peut annuler le support donné.
     * @param support
     * @return
     */
    public boolean canCancelThisSupport(Order support){
        //test si mouvement
        if (!this.isOrderOfMouvement()) return false;

        if (!this.isCorrect()) return false;

        //attaque égale action de base
        if (this.getEndRegion().equals(support.getActionRegion())
            && support.getEndRegion().equals(this.getActionRegion()))
        {
            return false;
        }

        return this.getEndRegion().equals(support.getActionRegion())
                && !support.getEndRegion().equals(this.getActionRegion());

    }

    public int getError() {
        return this.erreur;
    }

    /**
     * Retourne le type d'ordre
     * @return
     */
    public int getOrderType() {
        return this.typeOrdre;
    }

    /**
     * Retourne la région du début de l'ordre. (d�part pour un mouvement)
     * 
     * @return
     */
    public Region getStartRegion() {
        return regionStart;
    }

    /**
     * Retourne la région de fin de l'ordre : destination
     * 
     * @return
     */
    public Region getEndRegion() {
        return regionEnd;
    }

    /**
     * Retourne la région d'où part l'ordre.
     */
    public Region getActionRegion() {
        return regionBase;

    }

    /**
     * Retourne la région dans laquelle devrait se trouver l'unit� � la fin de
     * son tour. Utile pour les soutiens / convois
     * 
     * @return
     */
    public Region getResultRegion() {
        Region r = null;

        if (this.isSuccess()) {
            switch (this.getOrderType()) {
                case Order.ORDRE_MOUVEMENT:
                    r = this.getEndRegion();
                    break;
                default:
                    r = this.getActionRegion();
                    break;
            }
        } else {
            r = this.getActionRegion();
        }

        if (r == null) {
            System.out.println("ERREUR ! REGION PEUT PAS ETRE NULL");
            throw new IllegalStateException("REGION PEUT PAS ETRE NULL");
        }

        return r;
    }

    /**
     * Retourne l'unité executant l'ordre
     * 
     * @return
     */
    public Unit getUnit() {
        return this.unit;

    }

    public int getTypeUnit() {
        return this.typeUnit;
    }

    /**
     * Indique si l'ordre provoque un mouvement
     * 
     * @return
     */
    public boolean isMouvement() {
        return this.isSuccess() && this.typeOrdre == Order.ORDRE_MOUVEMENT
                && (!this.regionStart.equals(this.regionEnd));
    }

    @Override
    public int compareTo(Order o) {

        if (o.country.getIndex() < this.country.getIndex()) {
            return 1;
        }
        if (o.country.getIndex() > this.country.getIndex()) {
            return -1;
        }

        return 0;
    }

    /**
     * Liste des régions empruntées pour un convoi
     * 
     * @return
     */
    public List<Region> getConvoiPath() {
        if (this.convoiPath == null) {
            convoiPath = new ArrayList<Region>();
        }

        return convoiPath;
    }
    
    private boolean canSupport(Region start, Region end){

        Province p = end.getProvince();
        return (start.getProvincesAround()
                .contains(p)
                );


    }
    
    
    /**
     * 
     */
    private void sortConvoyPath(){
    	if (!this.isSuccess()) return;
    	
    	Graph graph = Unit.getGraph(Unit.SHIP);
    	List<Region> sorted = new ArrayList<Region>();
    	
    	Region current = this.getStartRegion();
    	
    	int nbtry = 0;
    	
    	//add end
    	
    	while (this.convoiPath.size() > 0 && nbtry++ < 100){
    		//fetch region around
    		Region found = null;
    		for (Region around : graph.getNode(current).getRegionsAround()){
    			if (this.convoiPath.contains(around)){
    				found = around ;
    				break;
    			}
    		}
    		
    		//region found !
    		if (found != null){
    			sorted.add(found);
    			this.convoiPath.remove(found);
    			current = found;
    		} else {
    			//impossible pass
    			return;
    		}
    	}
    	
    	this.convoiPath.clear(); //useless...
    	this.convoiPath.addAll(sorted);
    }
}

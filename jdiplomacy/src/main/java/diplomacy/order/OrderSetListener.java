/*
 * Created on 13 avr. 2007
 *
 *
 */
package diplomacy.order;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public interface OrderSetListener {

	public void orderAdded(Order o);
	
	public void orderRemoved(Order o);
}

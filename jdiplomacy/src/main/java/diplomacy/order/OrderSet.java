/*
 * Created on 26 mars 2007
 *
 *
 */
package diplomacy.order;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import diplomacy.Main;
import diplomacy.game.Board;
import diplomacy.game.Game;
import diplomacy.game.board.Country;
import diplomacy.game.board.Province;
import diplomacy.game.board.Region;
import diplomacy.game.Tour;
import diplomacy.game.User;
import diplomacy.game.Unit;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 * 
 *  
 */
public final class OrderSet {

    /**
     * Construteur
     * 
     * @param tour
     */
    public OrderSet(Tour tour) {
        provinceToOrder = new HashMap<Province, Order>();
        this.orders = new ArrayList<Order>(50);
        this.orderComparator = new OrderComparator();

        this.orderSetListeners = new ArrayList<OrderSetListener>();
        this.tour = tour;
        secured = true;

    }

    public Tour getTour() {
        return this.tour;
    }

    /**
     * Ajoute un écouteur d'ordre
     */
    public void addOrderSetListener(OrderSetListener osl) {
        if (!this.orderSetListeners.contains(osl)) {
            this.orderSetListeners.add(osl);
        }
    }

    /**
     * Supprime un écouteur d'ordre
     */
    public void removeOrderSeListener(OrderSetListener osl) {
        this.orderSetListeners.remove(osl);
    }

    /**
     * Retourne la liste des ordres
     * 
     * @return
     */
    public Collection<Order> getOrders() {
        return this.orders;
    }

    public String getTextOrders(){
    	
    	StringBuilder sb = new StringBuilder();
    	String aux = "";
    	for(Order o : this.getOrders()){
    		sb.append(aux);
    		sb.append(o.getText());
    		aux = ";";
    	}
    	
    	return sb.toString();
    	
    }
    
    public String getTextOrders(Country c){
    	
    	StringBuilder sb = new StringBuilder();
    	String aux = "";
    	for(Order o : this.getOrders()){
    		if (c.equals(o.getCountry())){
    		sb.append(aux);
    		sb.append(o.getText());
    		aux = ";";
    		}
    	}
    	
    	return sb.toString();
    	
    }


    
    /**
     * Liste des ordres depuis un joueur.
     * 
     * @param user
     * @return
     */
    public Collection<Order> getOrdersFromUser(User user){
        List<Order> list = new ArrayList<Order>(30);
        Game game = this.tour.getGame();
        
        for(Order o : getOrders()){
         
            User cUser = game.getUser(o.getCountry());
            if (cUser.equals(user)){
                list.add(o);
            }
        }
        
        return list;
    }
    
    public void addFreeOrder(Order order) {
        secured = false;
        this.addOrder(order);
        secured = true;
    }

    /**
     * Ajoute un ordre à l'ensemble
     */
    public void addOrder(Order order) {
        assert (order != null);
      

        //empeche la modif
        if (this.tour.isFinished() && secured) {
            return;
        }

        Region r = order.getActionRegion();
        if (r == null) {
            return;
        }

        //test si ordre ok par rapport au tour (ajustement)
        if (this.tour.getPhase() != Tour.PHASE_FALL_REAJUST && (order.getOrderType() == Order.ORDER_DEL_UNIT || order.getOrderType() == Order.ORDRE_ADD_UNIT)) {
            //Ordre pas autorisé
            return;
        }

        if (this.tour.getPhase() == Tour.PHASE_FALL_REAJUST && !this.isAdjustPossible(order) && secured) {
            return;
        }

        //enlève l'ordre associée à cette région

        this.removeOrder(r);

        Unit u = null;
        if (tour.isPhaseDisband()){
            u = this.getTour().getBoard().getDislodgedUnit(r.getProvince());
           
        } else {
            u = this.getTour().getBoard().getUnit(r);
        }

        
        
        if (u != null) {

            //Autorisé à commander cette unité ?
            if (!this.canOrderThis(u) && secured) {
                return;
            }

        }

        //ajoute à la table nom => Ordre
        synchronized (provinceToOrder) {
            provinceToOrder.put(r.getProvince(), order);
        }

        synchronized (orders) {
            //ajoute à la liste tri�e des ordres
            orders.add(order);

            //tri les ordres
            Collections.sort(orders, orderComparator);
        }

        this.notifyListeners(order, true);
    }

    /**
     * Ajout un ensemble d'ordre
     * @param co
     */
    public void addOrder(CountryOrders co){
        String [] words = co.getTextOrder().split(";");
        Board board = this.getTour().getBoard();
        for(String o : words){
            Main.out("order = " + o);
            if (o.trim().length() == 0) continue;
            
            this.addOrder(Order.create(board,co.getCountry(), o));
        }
    }



    public Order getOrderFromBase(Province pr) {
        Order o = null;

        if (pr != null) {
            o = this.provinceToOrder.get(pr);
        }
        //Main.dAssert(o != null);
        return o;
    }

    public Order getOrderFromBase(Region r) {
        return getOrderFromBase(r.getProvince());
    }

    /**
     * Supprime un ordre dont l'unité se trouve dans la région donnée
     * 
     * @param region
     */
    public void removeOrder(Region region) {
        if (this.tour.isFinished() && secured) {
            return;
        }

        if (region == null) {
            return;
        }
        Unit u = this.getTour().getBoard().getUnit(region);
        if (u != null) {
            this.removeOrder(u);
        } else {
            Province p = region.getProvince();
            if (this.provinceToOrder.containsKey(p)) {
                Order o = provinceToOrder.remove(p);
                orders.remove(o);
                this.notifyListeners(o, false);
            }
        }
    }

    /**
     * Retire l'ordre assigné à l'unité donnée
     * 
     * @param unit
     */
    public void removeOrder(Unit unit) {
        //impossible de modifier ce tour ?
        if (this.tour.isFinished() && secured) {
            return;
        }

        //Autoris� � commander cette unit� ?
        if (!this.canOrderThis(unit)) {
            return;
        }

        Province p = unit.getRegion().getProvince();

        if (this.provinceToOrder.containsKey(p)) {
            Order o = provinceToOrder.remove(p);
            this.orders.remove(o);
            this.notifyListeners(o, false);
        }
    }

    /**
     * 
     * @deprecated
     */
    @Deprecated
	public void clear() {
        if (this.tour.isFinished()) {
            return;
        }

        provinceToOrder.clear();
        orders.clear();
        this.notifyListeners(null, false);
    }

    /**
     * V�rifie si l'ordre est possible
     * 
     * @param order
     * @return
     */
    private boolean canOrderThis(Unit unit) {
        if (!secured) {
            return true;
        }

        //Capte l'utilisateur
        Game game = this.getTour().getGame();
        User unitUser = game.getUser(unit.getCountry());

        return Main.getUser().equals(unitUser);
    }

    /**
     * Teste si l'ajustement est valabe
     * 
     * @param o
     * @return
     */
    private boolean isAdjustPossible(Order o) {

        if (!o.isCorrect()) return false;

        Country c = o.getCountry();
        Board board = this.getTour().getBoard();
        User user = this.getTour().getGame().getUser(o.getCountry());

        int adjustValue = board.getAdjustDifference(o.getCountry());

        //teste is ordre conforme
        if (o.getOrderType() == Order.ORDRE_ADD_UNIT && adjustValue <= 0) {
            return false;
        }
        if (o.getOrderType() == Order.ORDER_DEL_UNIT && adjustValue >= 0) {
            return false;
        }

        //teste le pays concerné (existe)
        if (o.getCountry() == null) return false;
        if (!Main.getUser().equals(user)) return false;
        
      
        //Calcule le nombre d'unité
        int totalUnit = board.getUnits(c).size();
        int totalCentre = board.getSupplies(c).size();

        //ajoute ou supprime les unités
        Iterator<Order> it = this.getOrders().iterator();
        while (it.hasNext()) {
            Order no = it.next();
            if (c.equals(no.getCountry())) {
                if (no.getOrderType() == Order.ORDRE_ADD_UNIT) {
                    totalUnit++;
                } else {
                    totalUnit--;
                }
            }
        }

        //compare le résultat
        if (totalUnit >= totalCentre && o.getOrderType() == Order.ORDRE_ADD_UNIT) {
            return false;
        }
        if (totalUnit <= totalCentre && o.getOrderType() == Order.ORDER_DEL_UNIT) {
            return false;
        }

        //tous les tests sont passés ? OUF !
        return true;
    }

    /**
     * Retourne la liste d'ordre sous forme de chaine
     */
    public String serialize() {
        StringBuilder sb = new StringBuilder();
        String aux = "";
        for (int i = 0; i < this.orders.size(); i++) {
            Order o = orders.get(i);
            sb.append(aux);
            sb.append(o.getSerializedText());
            aux = ";";
        }


        if (sb.length() == 0) {
            return null;
        }

        return sb.toString();
    }


    public void unserialize(String t) {
    	if (t == null) return;
        //ON fait mine que le tour n'est pas fini pour ajouter des ordres
        boolean saveIsFinished = this.tour.isFinished();
        this.tour.setFinished(false);


        //this.orderSet = new OrderSet(this);
        String[] w = t.split(";");
        for (int i = 0; i < w.length; i++) {
            //ordre
            Order o = Order.create(this.getTour().getBoard(),w[i]);
            this.addFreeOrder(o);
        }
        this.tour.setFinished(saveIsFinished);
    }

    /**
     * Remplit la liste des ordres pour les unit�s qui n'en n'ont pas
     *  
     */
    public void fillMissingOrders() {
        //purge les ordres incorrects
        Iterator<Order> or = this.orders.iterator();
        while (or.hasNext()) {
            if (!(or.next()).isCorrect()) {
                or.remove();
            }
        }

        //teste si phase ajust
        if (this.tour.getPhase() != Tour.PHASE_FALL
                && this.tour.getPhase() != Tour.PHASE_SPRING) {
            return;
        }

        secured = false;
        
        //remplit les ordres manquants
        Board board = this.getTour().getBoard();
        Iterator<Unit> it = board.getUnits().iterator();

        while (it.hasNext()) {
            Unit u = it.next();
            //récupère l'ordre pour l'unité donnée
            Order o = this.getOrderFromBase(u.getRegion());
            //l'ordre n'existe pas : on en crée un !
            if (o == null) {
                String texte = u.getCodeChar() + " " 
                        + u.getRegion().getName() + " - "
                        + u.getRegion().getName();
                o = Order.create(board,u.getCountry(), texte);

                this.addOrder(o);
            }
        }
        //Verrou
        secured = true;
    }

    /**
     * Notifie les �couteurs d'un nouvel �vennement ajout / suppression
     * 
     * @param o
     * @param addAction
     */
    private void notifyListeners(Order o, boolean addAction) {
        Iterator<OrderSetListener> it = this.orderSetListeners.iterator();
        while (it.hasNext()) {
            OrderSetListener osl = it.next();
            if (addAction) {
                osl.orderAdded(o);
            } else {
                osl.orderRemoved(o);
            }
        }
    }

    /**
     * Utilisé pour trier les ordres
     * 
     * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
     * 
     *  
     */
    private class OrderComparator implements Comparator<Order> {

        /**
         * Méthode de comparaison
         */
        @Override
        public int compare(Order o1, Order o2) {
            //tri par pays

            int result = 0;

            if (o1.getError() == Order.ERREUR_UNITE_NON_POSSEDEE){
                return 1;
            }

            //sécurité pour les ordres foireux
            if (o1.getCountry() == null) {
                return -1;
            }
            if (o2.getCountry() == null) {
                return 1;
            }

            if (o1.getCountry().getIndex() < o2.getCountry().getIndex()) {
                result = -1;
            } else {
                if (o1.getCountry().getIndex() > o2.getCountry().getIndex()) {
                    result = 1;
                }
            }

            return result;
        }
    }
    /***************************************************************************
     * 
     * 
     *  
     **************************************************************************/

    //--------
    private Map<Province, Order> provinceToOrder;
    private List<Order> orders;
    private Tour tour;

    //liste des écouteurs
    private Collection<OrderSetListener> orderSetListeners;
    /** Comparaison pour trier les ordres */
    private Comparator<Order> orderComparator;
    /** Gestion d'ajout des ordres */
    private boolean secured;
}

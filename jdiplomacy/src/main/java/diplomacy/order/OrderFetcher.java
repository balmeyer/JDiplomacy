/*
 * Created on 25 avr. 2007
 *
 *
 */
package diplomacy.order;

import org.w3c.dom.Document;

import diplomacy.game.Game;

/**
 * 
 * Synchronizing orders
 * @author : Jean-Baptiste Vovau 
 *
 * 
 */
public interface OrderFetcher {

	/** Envoi la liste des ordres de l'utilisateur */
	public void sendUserOrder(OrderSet set);
	
	/** A l'Ecoute de l'objet et des nouveaux ordres */
	public void addOrderFetcherListener(OrderFetcherListener ofl);

    /** Etabli la liste des ordres en cours*/
    public void setPendingOrders(Document doc);
    
    public void refreshGame();
        
	public void removeOrderFetcherListener(OrderFetcherListener ofl);
	
	public void removeAllListeners();
	
	public Game getGame();
	
	public void setGame(Game game);
	
	public void close();
}

/*
 * Created on 25 avr. 2007
 *
 *
 */
package diplomacy.order;

import java.util.Date;

import diplomacy.game.board.Country;

/**
 * 
 * Liste des ordres pour un pays.
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public class CountryOrders {

	private String textOrder= null;
	private Date datePlayed = null;
	private Country country ;
	//protected Tour tour ;
	
	public CountryOrders(Country c, String text,Date datePlayed){
		//this.tour = tour;
		this.textOrder = text;
		this.country = c;
		this.datePlayed = datePlayed;
	}
	
	public Country getCountry(){
		return this.country;
	}
	
	public Date getDate(){
		return this.datePlayed;
	}
	
	public String getTextOrder(){
		return textOrder;
	}
	
	
}

/*
 * Created on 25 avr. 2007
 *
 *
 */
package diplomacy.order;

/**
 * 
 * A l'ecoute du ramasseur d'ordres
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public interface OrderFetcherListener {

    /** Nouvel ordre pour un pays */
    public void newOrders(CountryOrders c);

    /** Temps restant avant la fin du tour*/
    public void remainingTime(int minutes);
}

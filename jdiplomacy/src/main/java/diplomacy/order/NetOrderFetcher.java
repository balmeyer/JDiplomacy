/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package diplomacy.order;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import diplomacy.Main;
import diplomacy.access.XmlProvider;
import diplomacy.access.exception.AccessException;
import diplomacy.game.Game;
import diplomacy.game.Tour;
import diplomacy.game.board.Country;
import diplomacy.language.Language;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public final class NetOrderFetcher implements OrderFetcher {

    private static final long TIME_REFRESH_SECONDS = 10;

    private XmlProvider access;
    private Timer timer;
    private int timeBetweenTour;

    private int countTest = 0;
    private int maxCountBeforeTest = 0;
    
    private boolean everyOneHasPlayed = false;
    
    private final List<OrderFetcherListener> ofListeners = new ArrayList<OrderFetcherListener>(8);
    private Game game;
    
    
    public NetOrderFetcher(Game game) {
        super();
        this.access = Main.getDBAccess().getXmlProvider();
        this.setGame(game);
        this.timeBetweenTour = game.getGameInfo().getMinutesBetweenTours();
        this.timer = new Timer("REFRESH_ORDER", true);
        this.start();
    }

    /**
     * Check if new tour is available
     */
    @Override
    public void refreshGame(){
    	
    	//
    	try {
    		
    		//check if player has played (if not, tour should not be finished)
    		Tour currentTour = this.getGame().getLastTour();
    		if (currentTour != null){
    			if (currentTour.canModify() && !everyOneHasPlayed) return;
    		}
    		
			boolean newtour = Main.getDBAccess().refreshGame(this.getGame());
		
			if (newtour){
				everyOneHasPlayed = false;
				String msg = Language.getInstance().get("game.newtouravailable");
				this.getGame().userMessage(msg);
			}
    	
    	} catch (AccessException e) {
			Main.logException(e);
		}
    	
    }
    
    @Override
    public void setPendingOrders(Document doc) {
        this.analyseOrders(doc);
    }

    @Override
    public void sendUserOrder(OrderSet set) {
        try {
            //disable
            this.getGame().getLastTour().setFinished(true);
            Document doc = this.access.sendOrder(set);
            this.analyseOrders(doc);

        //analyse
        } catch (AccessException ex) {
            Logger.getLogger(NetOrderFetcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Start listening
     * */
    private  void start() {

        TimerTask tt = new TimerTask() {

            @Override
			public void run() {
            	if (canSendRequest()){
	                refreshOrders();
	                refreshGame();
            	}
            }
        };

        this.timer.schedule(tt, 1000, TIME_REFRESH_SECONDS * 1000);
    }

    private void refreshOrders() {
        try {
            //this.triggerUselessOrders();
            Document doc = this.access.downloadOrders(getGame().getLastTour());
            this.analyseOrders(doc);
        } catch (AccessException ex) {
            Logger.getLogger(NetOrderFetcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    /**
     * Determine if usefull to send request
     * @return
     */
    private boolean canSendRequest(){
    	//every one has played ?
    	if (everyOneHasPlayed) return true;
    	
    	if (this.countTest++ >= this.maxCountBeforeTest){
    		this.maxCountBeforeTest= 6 ;
    		this.countTest = 0;
    		return true;
    	}
    	
    	return false;
    }
    
    /**
     * Analyse le documents ordres reçus
     * @param doc
     */
    private synchronized void analyseOrders(Document doc) {
        if (doc == null) {
            return;
        }

        //retard minimal

        //list order
        final List<CountryOrders> listOfOrder = new ArrayList<CountryOrders>(8);

        //retard
        String delay = doc.getElementsByTagName("pending").item(0).getAttributes().getNamedItem("delay").getNodeValue();

        int minuteDelay = Integer.parseInt(delay);


        //retrieve node
        NodeList list = doc.getElementsByTagName("order");
        for (int i = 0; i < list.getLength(); i++) {
            //pays
            int c = Integer.parseInt(list.item(i).getAttributes().getNamedItem("country").getNodeValue());
            String order = list.item(i).getTextContent();

            //date
            String sDate = list.item(i).getAttributes().getNamedItem("date").getNodeValue();

            //Country order
            Date date = Calendar.getInstance().getTime();
            date = Language.getInstance().parseDate(sDate);

            Country country = Country.get(c);
            CountryOrders co = new CountryOrders(country, order, date);

            listOfOrder.add(co);

        }

        //dispatch to listener
        Tour tour = this.getGame().getLastTour();
        
        short numberOfOrder = 0;
        
        for (CountryOrders co : listOfOrder) {
            this.broadCastNewOrders(co);
            numberOfOrder++;

            //orders to tour
            if (tour != null && !tour.isFinished()) {
                this.getGame().sendOrders(co);
            }

            //DELAY
            int minute = getRemainingTime(minuteDelay);
            this.triggerRemainingTime(minute);

        }
        everyOneHasPlayed = (numberOfOrder >= 7);
    }


    private int getRemainingTime(int nextDelay) {

        if (this.timeBetweenTour == 0) {
            return 0;
        }
        int result = timeBetweenTour - nextDelay;

        if (result < 0) {
            return 0;
        }
        return result;

    }
    
    @Override
    public void close(){
    	this.timer.cancel();
    }
    

    @Override
    public void addOrderFetcherListener(OrderFetcherListener ofl) {

        synchronized (this.ofListeners) {
            this.ofListeners.add(ofl);
        }
    }

    @Override
	public void removeOrderFetcherListener(OrderFetcherListener ofl) {

        synchronized (this.ofListeners) {
            this.ofListeners.remove(ofl);
        }
    }

    @Override
	public void removeAllListeners() {
        //System.out.println("REMOVE_ALL_LISTENER");
        synchronized (this.ofListeners) {
            this.ofListeners.clear();
        }
    }

    @Override
	public Game getGame() {
        return this.game;
    }

    @Override
	public void setGame(Game game) {
        this.game = game;
    }

    public void broadCastNewOrders(CountryOrders co) {

        synchronized (this.ofListeners) {
            for (OrderFetcherListener ofl : this.ofListeners) {
                ofl.newOrders(co);
            }
        }

    }

    protected void triggerRemainingTime(int minutes) {

        synchronized (this.ofListeners) {
            for (OrderFetcherListener ofl : this.ofListeners) {
                ofl.remainingTime(minutes);
            }

        }

    }
}

/*
 * Language.java
 *
 * Created on 1 oct. 2007, 20:58:42
 *
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.language;

import diplomacy.Main;
import diplomacy.access.exception.AccessException;
import diplomacy.xml.DocumentLoader;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public class Language {

    //instance of Language
    private static Language instance;

    private List<String> availableLanguages;
    private Map<String, Map<String, String>> keyToWords;
    private Map<String,String> codeToLibelle;
    private String langue = "en";
    private DateFormat displayDateFormat ;
    private DateFormat convertDateFormat;
    
    /**
     * Get public instance of language
     * @return
     */
    public static Language getInstance() {
        if (instance == null){
            instance = new Language();
        }
        
        return instance;
    }
    
    private Language() {
        try {
            //Chargement des langues
            this.init();
            //current culture
            this.setLanguage(this.getCurrentLanguage());
        } catch (AccessException ex) {
            Logger.getLogger("ACCESS").log(Level.SEVERE, null, ex);
        }
    }

    public String getLanguage() {
        return langue;
    }

    /**
     * Set the current Language for user
     * @param langue
     */
    public void setLanguage(String langue) {

        if (langue == null) langue = "";

        langue = langue.toLowerCase();
        //check language
        if (!availableLanguages.contains(langue)){
            this.langue = "en";
        } else {
            this.langue = langue.trim().toLowerCase();
        }

        
        this.convertDateFormat = null;
        this.displayDateFormat = null;
    }

    /**
     * Retourne un terme international
     * */
    public String get(String code) {
        Map<String, String> map = keyToWords.get(code);

        if (map != null) {
            if (map.containsKey(langue)) {
                return map.get(langue);
            }
            //si non trouvé : premier mot dispo
            if (map.size()>0){
                return map.values().toArray()[0].toString();
            }
        }

        return "[NOT FOUND]";
    }

    /**
     *
     * Get ISO2 to Libelle
     * @return
     */
    public Map<String,String> getCodeToLibelle(){
        return this.codeToLibelle;
    }

    /**
     * Parse date to string format in choosen culture
     * @param date
     * @return
     */
    public String parseDate(Date date){
        return getDisplayDateFormat().format(date);
    }

    public Date parseDate(String sdate) {

        //date null
        if (sdate == null || sdate.length() == 0) return null;

        //gestion du cas rarissime où l'heure est un nombre pile
        if (sdate != null && sdate.length() > 19) {
            sdate = sdate.substring(0,19);
        }
        try {
            Date d = getConvertDateFormat().parse(sdate);
            return d;
        } catch (ParseException ex) {
            Logger.getLogger(Language.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    private DateFormat getConvertDateFormat(){
        if (convertDateFormat == null){
            convertDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }

        return convertDateFormat;
    }

    /**
     * Retourne le type de format à afficher
     * @return
     */
    private DateFormat getDisplayDateFormat(){
        if (displayDateFormat == null){

            String format = "MM/dd/yyyy hh:mm:ss";
            if (this.getLanguage().equalsIgnoreCase("fr")){
                format = "dd/MM/yyyy HH:mm:ss";
            }
            
            displayDateFormat = new SimpleDateFormat(format);
        }
        
        return displayDateFormat;
    }
    

    /**
     * Chargement du langage
     */
    private void init() throws AccessException {
        //nouvelle ref
        keyToWords = new HashMap<String, Map<String, String>>();
        availableLanguages = new ArrayList<String>();
        
        //XML des langues
        URL u = Main.class.getClassLoader().getResource("language.xml");
        
       
        Document doc = DocumentLoader.load(u);

        //all language
        codeToLibelle = new HashMap<String,String>();
        NodeList langs = doc.getElementsByTagName("lang");
        for(int i = 0 ; i < langs.getLength(); i++){
            Node nlang = langs.item(i);

            String code = nlang.getAttributes().getNamedItem("code").getNodeValue();
            String libelle = nlang.getAttributes().getNamedItem("libelle").getNodeValue();
            codeToLibelle.put(code, libelle);
        }

        //parse
        Node key = doc.getElementsByTagName("key").item(0);
        while (key != null) {
            //il s'agit bien d'un noeud key
            if (key.getNodeName().equals("key")) {

                //récupère la clef
                String code = key.getAttributes().getNamedItem("name").getNodeValue();

                //element should not exists
                if (this.keyToWords.containsKey(code)){
                	throw new IllegalArgumentException("language : code '" + code + "' already exists");
                }
                
                //ajout des valeurs internes
                for (int i = 0; i < key.getChildNodes().getLength(); i++) {
                    Node v = key.getChildNodes().item(i);
                    if (!v.getNodeName().equals("val")) {
                        continue;
                    }
                    String myLangue = v.getAttributes().getNamedItem("lang").getNodeValue();
                    String valeur = v.getTextContent();
                    this.addWord(code, valeur, myLangue);

                    //add langue
                    if (!this.availableLanguages.contains(myLangue)){
                        this.availableLanguages.add(myLangue);
                    }
                }
            }
            //au suivant
            key = key.getNextSibling();
        }



    }

    /**
     * Get the language of current culture
     * @return
     */
    private String getCurrentLanguage(){

        Locale locale = Locale.getDefault();

        return locale.getLanguage();
        //return "en";
    }

    /** Ajout d'un terme */
    private void addWord(String key, String value, String langue) {
        Map<String, String> map = keyToWords.get(key);

        //n'existepas : on la créé
        if (map == null) {
            map = new HashMap<String, String>();
            keyToWords.put(key, map);
        }

        //ajoute le terme
        map.put(langue, value);
    }
}
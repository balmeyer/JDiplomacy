package diplomacy;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import diplomacy.access.NetXmlProvider;

public class Config {

	private static Properties prop = null;
	
	/**
	 * Get custon properties (file : config.properties)
	 * @param key
	 * @return
	 */
	public static String get(String key){
		if (prop == null){
			try {
				init();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return prop.getProperty(key);
	}
	
	private static void init() throws IOException{
    	prop = new Properties();
    	
    	URL url = NetXmlProvider.class.getResource("/config.properties");
    	prop.load(url.openStream());
	}
	
}

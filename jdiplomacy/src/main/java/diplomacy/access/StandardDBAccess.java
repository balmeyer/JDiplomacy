/*
 * Created on 19 avr. 2007
 *
 *
 */
package diplomacy.access;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import diplomacy.Main;
import diplomacy.access.exception.AccessException;
import diplomacy.access.exception.AccountNoConfirmerdException;
import diplomacy.access.exception.BadLoginException;
import diplomacy.access.exception.EmailAlreadyExistsException;
import diplomacy.access.exception.LoginAlreadyExistsException;
import diplomacy.access.exception.MissingFieldException;
import diplomacy.access.exception.PasswordException;
import diplomacy.access.exception.UserException;
import diplomacy.conversation.Conversation;
import diplomacy.game.Game;
import diplomacy.game.GameInfo;
import diplomacy.game.Tour;
import diplomacy.game.User;
import diplomacy.game.board.Country;
import diplomacy.language.Language;

/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 *
 */
public final class StandardDBAccess implements DBAccess {

	/*
    private static final int LIST_MY_GAMES = 1;
    private static final int LIST_OPEN_GAMES = 2;
    private static final int LIST_ONE_GAME = 3;
    private static final int LIST_ALL_GAMES = 4;*/
    
    public StandardDBAccess() throws IOException {
        //objet pour charger les docs XML
        this.xmlAccess = new NetXmlProvider();
    }

    /* (non-Javadoc)
     * @see diplomacy.game.DBAccess#login()
     */
    @Override
    public User login(String login, char[] password) throws AccessException {

        if (login == null || login.equals("")) {
            throw new UserException();
        }

        Document doc = this.xmlAccess.login(login, password);

        return parseLoginResponse(doc);

    }

    
    
    @Override
    public User login(String token) throws AccessException {
        Document doc = this.xmlAccess.login(token);
        return parseLoginResponse(doc);
    }

    /** Ask forgotten password */
    @Override
    public void askPassword(String login) throws AccessException{
    	Document doc = this.xmlAccess.askPassword(login);
    
    	NodeList list = doc.getElementsByTagName("msg");
    	if (list.getLength() > 0) {
    		Node node = list.item(0);
    		String id = node.getAttributes()
    			.getNamedItem("id")
    			.getNodeValue();
    		
    		if (id != null && id.equals("0")) return;
    	} 
    
    	//bad login
    	throw new BadLoginException();
    	
    }
    
    
    private User parseLoginResponse(Document doc) throws AccessException{

        User myuser = null;

        if (doc != null) {
            //recupere l'utilisateur
            NodeList list = doc.getElementsByTagName("user");
            if (list.getLength() > 0) {
                Node elem = list.item(0);
                //identifiant
                long userID = Long.parseLong(elem.getAttributes().getNamedItem("id").getNodeValue());
                //nom
                String userName = elem.getAttributes().getNamedItem("name").getNodeValue();
                //chaine d'authentification ?
                String token = elem.getAttributes().getNamedItem("token").getNodeValue();
                myuser = User.get(userName, userID);
                myuser.setToken(token);
                //dernier jeu ?
                try {
                    myuser.setLastGameId(Long.parseLong(elem.getAttributes().getNamedItem("lastgame").getNodeValue()));
                } catch (NumberFormatException nfe) {
                    //impossible de lire le dernier jeu !
                }




            } else {

                //parse l'erreur
                NodeList listError = doc.getElementsByTagName("error");

                if (listError.getLength() >0){
                    String error = listError.item(0).getTextContent();
                    int noError = Integer.valueOf(error);
                    switch(noError){
                        case 1:
                            //utilisateur inconnu
                            throw new UserException();
                        case 2:
                            //mot de passe
                            throw new PasswordException();
                        case 3:
                            //compte pas confirmé
                            throw new AccountNoConfirmerdException();
                        default:
                            //erreur utilisateur non traitée
                            throw new UserException();
                    }
                }


                throw new AccessException("UNKOWN ERROR");
            }
        }
        return myuser;
    }

    /* (non-Javadoc)
     * @see diplomacy.game.DBAccess#getMyGames(diplomacy.board.User)
     */
    @Override
    public List<GameInfo> listGames(TypeListGame param) {
        return loadListGame(param, 0);
    }

    /* (non-Javadoc)
     * @see diplomacy.game.DBAccess#getAvailableGame()
     */
    private List<GameInfo> loadListGame(TypeListGame typeList, long idgame) {
        //list of games
        List<GameInfo> l = new ArrayList<GameInfo>();

        //document XML
        Document doc = null;

        try {
            if (typeList == null && idgame > 0){
                //load one
                doc = this.xmlAccess.listGame(idgame);
            } else{
                //load list
                doc = this.xmlAccess.listGames(typeList);
            }


        } catch (AccessException ae) {
            Main.logException(ae);
            return l;
        }

        //parse XML file
        NodeList list = doc.getElementsByTagName("game");

        for (int i = 0; i < list.getLength(); i++) {
            //retrieve info
            long id = 0;
            int minutes = 0;
            short places = 0, player = 0;
            boolean started = false;
            boolean finished = false;
            boolean hasplayed = false;
            Date startDate = null, creaDate = null , endDate = null;
            String title = null;

            Node child = list.item(i).getFirstChild();
            while (child != null) {
                //
                String key = child.getNodeName();
                String value = child.getTextContent();

                //---------------
                if (key.equals("id")) {
                    id = Long.parseLong(value);
                }

                if (key.equals("started")) {
                    started = value.equals("1");
                }

                if (key.equals("hasplayed")) {
                    hasplayed = value.equals("1");
                }


                if (key.equals("title")) {
                    title = value;
                }

                if (key.equals("minute")) {
                    minutes = Integer.parseInt(value);
                }

                if (key.equals("player")) {
                    player = Short.parseShort(value);
                }

                if (key.equals("place")) {
                    places = Short.parseShort(value);
                }

                if (key.equals("finished")){
                    finished = value.equals("1");
                }

                if (key.equals("creadate")) {
                        creaDate = Language.getInstance()
                                .parseDate(value);
                }

                if (key.equals("enddate")) {
                        if (value.length() >0)
                            endDate = Language.getInstance()
                                    .parseDate(value);

                }

                if (key.equals("startdate")) {
                        startDate = Language.getInstance()
                                .parseDate(value);

                }



                //next
                child = child.getNextSibling();
            }

            GameInfo g = new GameInfo(id);
            g.setTitle(title);
            g.setMinutesBetweenTours(minutes);
            g.setFreePlaces(places);
            g.setNumberOfPlayer(player);
            g.setStarted(started);
            g.setFinished(finished);
            g.setHasplayed(hasplayed);
            g.setStartDate(startDate);
            g.setEndDate(endDate);
            g.setCreationDate(creaDate);
            l.add(g);

        }


        return l;
    }

    @Override
    public Game openGame(long id) throws AccessException {
        //liste 
        List<GameInfo> list = loadListGame(null, id);

        if (list.size() == 0) {
            return null;
        }
        Document doc = this.xmlAccess.openGame(id);
        return this.fetchGame(list.get(0), doc);

    }

    @Override
    public Game openGame(GameInfo info) throws AccessException {
        Document doc = this.xmlAccess.openGame(info.getGameId());
        return this.fetchGame(info, doc);
    }
    
    /** Refresh information for one game*/
    @Override
    public boolean refreshGame(Game game) throws AccessException{

    	Document doc = this.xmlAccess.fetchNewTours(game);
    	
    	if (doc == null) return false;
    	
    	NodeList tours = doc.getElementsByTagName("tour");
    	this.parseTours(game, tours);

    	return (tours.getLength() > 0);
    }
    
    
    @Override
    public List<User> fetchUser(GameInfo inf) throws AccessException {
        //TODO mettre le chagement des users dans le listing
        List<User> users = new ArrayList<User>(7);

        Document doc = this.xmlAccess.loadUsers(inf);

        NodeList list = doc.getElementsByTagName("user");

        for (int i = 0; i < list.getLength(); i++) {
            Node n = list.item(i);
            String login = n.getAttributes().getNamedItem("login").getNodeValue();
            long id = Long.parseLong(n.getAttributes().getNamedItem("id").getNodeValue());
            users.add(User.get(login, id));
        }

        return users;
    }

    @Override
	public void createGame(GameInfo info) throws AccessException {
        this.xmlAccess.newGame(info);

    }

    /* (non-Javadoc)
     * @see diplomacy.game.DBAccess#setXMLAccess(diplomacy.game.XMLAccess)
     */
    @Override
	public void setXmlProvider(XmlProvider xmla) {
        this.xmlAccess = xmla;
    }

    /**
     * Objet de chargement des docs XML
     * @see diplomacy.game.DBAccess#getXMLAccess()
     */
    @Override
	public XmlProvider getXmlProvider() {
        return xmlAccess;
    }

    /**
     * rejoint le jeu
     * @param info
     * @throws diplomacy.access.exception.AccessException
     */
    @Override
    public void joinGame(GameInfo info) throws AccessException {
        Document doc = this.xmlAccess.joinGame(info);

        NodeList list = doc.getElementsByTagName("ok");
        if (list.getLength() > 0) {
            info.addUser(Main.getUser());
        }


    }


    @Override
    public void newAccount(String email, String login, char[] password)
            throws AccessException {
        //requêtes
        Document doc = this.xmlAccess.newAccount(email, login, password);

        //capte message d'erreur
        NodeList nlist = doc.getElementsByTagName("error");

        if (nlist.getLength() > 0) {
            Node node = nlist.item(0);
            String attrId = node.getAttributes().getNamedItem("id").getNodeValue();

            int code = Integer.parseInt(attrId);

            switch (code) {
                case -1:
                    throw new MissingFieldException();
                case 1:
                    throw new EmailAlreadyExistsException();
                case 2:
                    throw new LoginAlreadyExistsException();
                default:
                    throw new UnknownError("Erreur création de compte");
            }
        }
    }

    private Game fetchGame(GameInfo inf, Document doc) {
        Game g = new Game(inf, Main.getUser());

        //Utilisateurs
        NodeList users = doc.getElementsByTagName("country");
        for (int i = 0; i < users.getLength(); i++) {
            //recupere le node du pays
            Node node = users.item(i);

            String name = node.getAttributes().getNamedItem("username").getNodeValue();
            long userId = Long.parseLong(node.getAttributes().getNamedItem("userid").getNodeValue());

            User u = User.get(name, userId);
            //Récupère le pays
            int idcountry = Integer.parseInt(node.getAttributes().getNamedItem("index").getNodeValue());
            Country c = Country.get(idcountry);
            g.addUserCountry(u, c);
        }


        //TOURS
        NodeList tours = doc.getElementsByTagName("tour");
        this.parseTours(g, tours);

        //Game over ?
        NodeList winners = doc.getElementsByTagName("winner");
        this.parseWinners(g, winners);
        
        //ordres en cours
        g.getOrderFetcher().setPendingOrders(doc);

        return g;
    }
    
    private void parseTours(Game game, NodeList tours){
    	for (int i = 0; i < tours.getLength(); i++) {
            Node node = tours.item(i);
            Tour t = new Tour(game , null , null);
            t.setIdPhase(Integer.parseInt(node.getAttributes().getNamedItem("idphase").getNodeValue()));
            t.setId(Long.parseLong(node.getAttributes().getNamedItem("id").getNodeValue()));

            t.setDiscovered(Boolean.parseBoolean(node.getAttributes().getNamedItem("discovered").getNodeValue()));
            t.setFinished(Boolean.parseBoolean(node.getAttributes().getNamedItem("finished").getNodeValue()));

            for (int j = 0; j < node.getChildNodes().getLength(); j++) {
                Node child = node.getChildNodes().item(j);
                //parse les positions
                if (child.getNodeName().toLowerCase().equals("position")) {
                    t.setPositions(child.getTextContent());
                }

                //parse les ordres
                if (child.getNodeName().toLowerCase().equals("orders")) {
                    t.setSerializedOrders(child.getTextContent());
                    t.setFinished(t.getSerializedOrders() != null);
                }
            }


            //ajouteu le tour
            game.addTour(t);
        }
    }
    
    private void parseWinners(Game game, NodeList winners){
    	if (winners.getLength() == 0) return;
    	
    	List<Country> list = new ArrayList<Country>(7);
    	
    	for (int i = 0; i < winners.getLength() ; i++){
    		Node node = winners.item(i);
    		
    		int index = Integer.parseInt(node.getAttributes().getNamedItem("index").getNodeValue());
    		int position = Integer.parseInt(node.getAttributes().getNamedItem("position").getNodeValue());
    		
    		Country c = Country.get(index);
    		list.add(position - 1, c);
    		
    	}
    	
    	game.getGameInfo().setWinlist(list);
    	
    }
    /**
     * Returns last conversation
     */
    @Override
	public List<Conversation> fetchConversation(Game game) throws AccessException{

    	Document doc = this.xmlAccess.fetchConversations(game);
    	
    	
    	List<Conversation> list = new ArrayList<Conversation>();
    	
    	if (doc == null) return list; //Empty list
    	
    	doc.normalizeDocument();
    	
    	//
    	NodeList nodelist = doc.getElementsByTagName("conv");
    	for (int i = 0; i < nodelist.getLength() ; i++){
    		//
    		Conversation c = new Conversation();
    		
    		//parse attributes
    		Node node = nodelist.item(i);
    		//Conv id
    		long attrId = Long.parseLong(node.getAttributes().getNamedItem("id").getNodeValue());
    		c.setId(attrId);
    		
    		//write Date
    		String strDate = node.getAttributes().getNamedItem("date").getNodeValue();
    		c.setWriteDate(Language.getInstance().parseDate(strDate));
    		//parent
    		if (node.getAttributes().getNamedItem("parent") != null){
    			long parentid = Long.parseLong(node.getAttributes().getNamedItem("parent").getNodeValue());
    			c.setParentid(parentid);
    		}
    		
    		
    		//CHILDREN
    		Node child = node.getFirstChild();
    		while (child != null){
    			if (child.getNodeName().equalsIgnoreCase("author")){
    				//TODO author
    				String name = child.getAttributes().getNamedItem("name").getNodeValue();
    				long userid = Long.parseLong(child.getAttributes().getNamedItem("id").getNodeValue());
    				User user = User.get(name, userid);
    				if (user != null){
    					c.setUser(user);
    				}
    			}
    			
    			//restricted people
    			if (child.getNodeName().equalsIgnoreCase("restricted")){
    				//child
    			 
    				
    				for (int a = 0 ; a < child.getChildNodes().getLength() ; a ++){
    					Node nodeUser = child.getChildNodes().item(a);
    					if (!nodeUser.getNodeName().equalsIgnoreCase("user")) continue;
    					String usid = nodeUser.getAttributes().getNamedItem("id").getNodeValue();
    					User u = User.get(Long.parseLong(usid));
    					if (u != null) c.getPrivatelist().add(u);
    					nodeUser = nodeUser.getNextSibling();
    				}
    			}
    			
    			//BODY
    			if (child.getNodeName().equalsIgnoreCase("body")){
    				String body = child.getTextContent();
    				c.setBody(body);
    			}
    			
    			child = child.getNextSibling();
    		}
    		
    		list.add(c);
    		
    	}
    	
    	return list;
    }

    /** Options */
    @Override
	public Map<String,String> loadOrModifyOptions(Map<String,String> options) 
    	throws AccessException{
    	
    	Document doc = this.xmlAccess.loadOrModifyOptions(options);
    	
    	Map<String,String> result = new HashMap<String,String>();
    	
    	NodeList list = doc.getElementsByTagName("option");
    	for(int i = 0 ; i < list.getLength() ; i++){
    		Node n = list.item(i);
    		String key = n.getAttributes().getNamedItem("key").getNodeValue();
    		String value = n.getAttributes().getNamedItem("value").getNodeValue();
    		
    		result.put(key, value);
    	}
    	
    	return result;
    	
    }
    
    
    private XmlProvider xmlAccess = null;
}
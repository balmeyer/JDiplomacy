package diplomacy.access;

/**
 * 
 * @author Jean-Baptiste Vovau
 *
 */
public enum TypeListGame {

	myRunningGames,
	openGames,
	allGames,
	fastJoinGames
	
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.access;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import diplomacy.Main;
import diplomacy.game.GameInfo;

/**
 * Liste des jeux en cache
 * @author Jean-Baptiste Vovau
 */
public class ListGamesProxy {

	private static ListGamesProxy instance;
	
    private Map<TypeListGame,List<GameInfo>> cachedLists;

    public static ListGamesProxy getInstance(){
    	if (instance == null) instance = new ListGamesProxy();
    	return instance;
    }
    
    
    private ListGamesProxy() {
        cachedLists = new HashMap<TypeListGame,List<GameInfo>>();
    }

    /**
     * Retourne une liste de jeu gérée en cache
     * @param listType
     * @return
     */
    public List<GameInfo> listGames(TypeListGame listType){


        List<GameInfo> result = cachedLists.get(listType);

        if (result == null){
            result = Main.getDBAccess().listGames(listType);
            cachedLists.put(listType, result);
        }

        return result;
    }

    /**
     * Vide le cache des listes
     */
    public void clearCache(){
        this.cachedLists.clear();
    }

}

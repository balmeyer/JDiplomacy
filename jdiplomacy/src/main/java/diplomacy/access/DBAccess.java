/*
 * Created on 18 avr. 2007
 *
 *
 */
package diplomacy.access;

import java.util.List;
import java.util.Map;

import diplomacy.access.exception.AccessException;
import diplomacy.conversation.Conversation;
import diplomacy.game.Game;
import diplomacy.game.GameInfo;
import diplomacy.game.User;

/**
 * Manipulation en base
 * 
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public interface DBAccess {

    public XmlProvider getXmlProvider();

    public void setXmlProvider(XmlProvider xmlp);

    /** Demande de login */
    public User login(String login, char[] password )
            throws AccessException;

    /** Ask forgotten password */
    public void askPassword(String login) throws AccessException;
    
    /** login avec un token */
    public User login(String token)throws AccessException  ;

    /** Liste les parties pour un joueur */
    public List<GameInfo> listGames(TypeListGame typelist);

    /** Obtient un objet donné. */
    //public Object fetch(Class c, Object key) throws AccessException;
    /** Chargements dynamique des utilisateurs d'une partie */
    public List<User> fetchUser(GameInfo gif) throws AccessException;


    /** Création d'une nouvelle partie */
    public void createGame(GameInfo info) throws AccessException;

    /** Inscription du joueur à une partie */
    public void joinGame(GameInfo info) throws AccessException;

    public Game openGame(GameInfo info) throws AccessException;

    public Game openGame(long id) throws AccessException;

    /** Création d'un nouveau compte */
    public void newAccount(String email, String login, char[] password)
            throws AccessException;
    
    /** Refresh information for one game*/
    public boolean refreshGame(Game game) throws AccessException;

    public List<Conversation> fetchConversation(Game game) throws AccessException;
    
    /** Options */
    public Map<String,String> loadOrModifyOptions(Map<String,String> options) throws AccessException;
    
}

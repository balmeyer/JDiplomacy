/*
 * PasswordException.java
 * 
 * Created on 13 oct. 2007, 13:41:10
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.access.exception;


/**
 *
 * @author Jean-Baptiste Vovau
 */
public class PasswordException extends AccessException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3676619751125377212L;

	public PasswordException(){
        super("form.error.badpassword");
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.access.exception;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public class AccountNoConfirmerdException extends AccessException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6704215465502099194L;

	public AccountNoConfirmerdException(){
        super("form.error.noconfirmed");
    }
}

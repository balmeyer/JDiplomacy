/*
 * UserException.java
 * 
 * Created on 13 oct. 2007, 13:43:10
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.access.exception;


/**
 *
 * @author Jean-Baptiste Vovau
 */
public class UserException extends AccessException{
    /**
	 * 
	 */
	private static final long serialVersionUID = 3357180387331641772L;

	public UserException(){
        super("form.error.baduser");
    }
}

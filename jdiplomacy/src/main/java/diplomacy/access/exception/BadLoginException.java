package diplomacy.access.exception;

public class BadLoginException extends AccessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6234018441897275729L;

	public BadLoginException() {
		super("form.login.badlogin");
	}

}

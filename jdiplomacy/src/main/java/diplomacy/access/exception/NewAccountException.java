/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.access.exception;

/**
 * Exception à la création de compte
 * @author Jean-Baptiste Vovau
 */
public abstract class NewAccountException extends AccessException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -482532691742703675L;

	public NewAccountException(String msg){
        super(msg);
    }
    
    public NewAccountException(){
        super(null);
    }
    
}

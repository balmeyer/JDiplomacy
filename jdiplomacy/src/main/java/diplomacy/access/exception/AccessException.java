/*
 * AccessException.java
 * 
 * Created on 9 oct. 2007, 19:53:51
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.access.exception;

/**
 * Exception d'accès
 * @author Jean-Baptiste Vovau
 */
public class AccessException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2608391556253714660L;

	public AccessException(String msg){
        super(msg);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.access.exception;

/**
 *
 * Aucune connexion au serveur possible.
 * 
 * @author Jean-Baptiste Vovau
 */
public class ConnectionException extends AccessException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7195481771776728092L;

	public ConnectionException(){
        super("exception.connection");
    }
    
}

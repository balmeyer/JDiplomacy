/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diplomacy.access.exception;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public class BadEmailException extends NewAccountException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -7473330994263158529L;

	public BadEmailException(){
        super("form.newaccount.error.bademail");
    }
}

/*
 * Created on 18 avr. 2007
 *
 *
 */
package diplomacy.access;

import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import diplomacy.access.exception.AccessException;
import diplomacy.conversation.Conversation;
import diplomacy.game.Game;
import diplomacy.game.GameInfo;
import diplomacy.game.Tour;
import diplomacy.game.User;
import diplomacy.order.OrderSet;

/**
 * 
 * Methods for exchanging data with server : Fetch XML files from exchange server.
 * 
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 *
 * 
 */
public interface XmlProvider {

    /** Identification */
    public Document login(String user, char[] password) throws AccessException;

    /** */
    public Document askPassword(String login) throws AccessException;
    
    public Document login(String token)  throws AccessException;

    /** Get XML information for one game. */
    public Document listGame(long id) throws AccessException;

    /** Load several games */
    public Document listGames(TypeListGame param) throws AccessException;

    /** Chargement des utilisateurs d'une partie */
    public Document loadUsers(GameInfo info) throws AccessException;

    /** Load one game. */
    public Document openGame(long id) throws AccessException;

    public Document joinGame(GameInfo info) throws AccessException;

    /** Création d'un compte */
    public Document newAccount(String email, String login, char[] password)
            throws AccessException;

    public Document newGame(GameInfo info) throws AccessException;

    public Document sendOrder(OrderSet set) throws AccessException;

    public Document downloadOrders(Tour tour) throws AccessException;

    public Document fetchNewTours(Game game) throws AccessException;
    
    public Document newConversation(Game game, Conversation parent, String text, List<User> privateUsers) throws AccessException;
    
    public Document fetchConversations(Game game) throws AccessException;

    /** Load options information */
    public Document loadOrModifyOptions(Map<String,String> options) throws AccessException;
    
}
        

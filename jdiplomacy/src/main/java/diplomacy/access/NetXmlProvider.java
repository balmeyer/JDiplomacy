/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package diplomacy.access;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.Document;

import diplomacy.Config;
import diplomacy.Main;
import diplomacy.access.exception.AccessException;
import diplomacy.conversation.Conversation;
import diplomacy.game.Game;
import diplomacy.game.GameInfo;
import diplomacy.game.Tour;
import diplomacy.game.User;
import diplomacy.game.board.Country;
import diplomacy.order.OrderSet;
import diplomacy.xml.DocumentLoader;

/**
 *
 * @author Jean-Baptiste Vovau
 */
public final class NetXmlProvider implements XmlProvider {

    private static String JDIP_HOST = null;
    
    private static final String PAGE_LOGIN = "login";
    private static final String PAGE_LIST_GAMES = "listgames";
    private static final String PAGE_CREATE_ACCOUNT = "createaccount";
    private static final String PAGE_CREATE_GAME = "creategame";
    private static final String PAGE_JOIN_GAME = "joingame";
    private static final String PAGE_GAME = "game";
    private static final String PAGE_ORDERS = "orders";
    private static final String PAGE_NEW_TOURS = "tour";
    private static final String PAGE_CONVERSATION = "conversation";
    private static final String PAGE_OPTIONS ="options";

    public NetXmlProvider() throws IOException{
    	JDIP_HOST = Config.get("website");
    }


    @Override
    public Document login(String user, char[] password) throws AccessException {
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_LOGIN);
        sb.append("?login=");
        sb.append(user);
        sb.append("&password=");
        sb.append(password);

        return this.load(sb.toString());
    }

    @Override
    public Document login(String token) throws AccessException {
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_LOGIN);
        sb.append("?token=");
        sb.append(token);


        return this.load(sb.toString());
    }

    /**
     * Ask password
     */
    @Override
	public Document askPassword(String login) throws AccessException {
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_LOGIN);
        sb.append("?login=");
        sb.append(login);
        sb.append("&action=askpassword");
        
        return load(sb.toString());
    }

    
    @Override
    public Document listGames(TypeListGame param) throws AccessException {
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_LIST_GAMES);

        sb.append("?");

        switch(param){
            case myRunningGames:
                sb.append("my=1");
                break;
            case openGames:
                sb.append("open=1");
                break;
            case allGames:
                sb.append("all=1");
                break;
            case fastJoinGames:
            	sb.append("fastjoin=1");
            	break;
        }

        return this.load(sb.toString());
    }

    @Override
    public Document listGame(long id) throws AccessException {
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_LIST_GAMES);
        sb.append("?idgame=");
        sb.append(id);

        return this.load(sb.toString());
    }

    @Override
    public Document openGame(long id) throws AccessException {
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_GAME);
        sb.append("?idgame="); //
        sb.append(id);
        
        return this.load(sb.toString());
    }

    @Override
    public Document joinGame(GameInfo info) throws AccessException {
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_JOIN_GAME);
        sb.append("?idgame="); //
        sb.append(info.getGameId());

        return this.load(sb.toString());
    }

    /**
     * Nouveau compte 
     * @param email
     * @param login
     * @param password
     * @return
     */
    @Override
    public Document newAccount(String email, String login, char[] password)
            throws AccessException {
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_CREATE_ACCOUNT);
        sb.append("?email=");
        sb.append(email);
        sb.append("&login=");
        sb.append(login);
        sb.append("&password=");
        sb.append(password);

        return this.load(sb.toString());
    }

    /**
     * 
     * 
     * @param info
     * @return
     */
    @Override
    public Document newGame(GameInfo info) throws AccessException {

        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_CREATE_GAME);
        sb.append("?player=");
        sb.append(info.getNumberOfPlayer());
        sb.append("&minutes=");
        sb.append(info.getMinutesBetweenTours());
        sb.append("&title=");

        try {
            sb.append(URLEncoder.encode(info.getTitle(), "UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(NetXmlProvider.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }


        return this.load(sb.toString());

    }

    @Override
    public Document loadUsers(GameInfo info) throws AccessException {
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_LIST_GAMES);
        sb.append("?idgame=");
        sb.append(info.getGameId());
        sb.append("&users=1");

        return load(sb.toString());
    }

    @Override
    public Document downloadOrders(Tour tour) throws AccessException {
        if (tour == null) {
            return null;
            
        }
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_ORDERS);
        sb.append("?idtour=");
        sb.append(tour.getId());

        return load(sb.toString());
    }

    @Override
    public Document sendOrder(OrderSet set) throws AccessException {
        User me = Main.getUser();
        Game game = set.getTour().getGame();

        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_ORDERS);
        sb.append("?action=send&idtour=");
        sb.append(set.getTour().getId());

        //
        for (Country c : Country.getCountries()) {
            try {
                

                //joueur ou neutre
                if (game.getUser(c)!= null 
                        && !game.getUser(c).equals(me)) {
                    continue;
                    
                }
                String o = set.getTextOrders(c);
                if (o == null) {
                    o = "";
                }
                o = URLEncoder.encode(o, "UTF-8");
                sb.append("&country[");
                sb.append(c.getIndex());
                sb.append("]=");
                sb.append(o);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(NetXmlProvider.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //sb.append(set.serialize());

        return load(sb.toString());
    }


    @Override
	public Document fetchNewTours(Game game) throws AccessException{

    	if (game.getLastTour() == null) return null;
    	
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_NEW_TOURS);
        sb.append("?game=");
        sb.append(game.getGameInfo().getGameId());
        sb.append("&fromtour=");
        //max id tour

        sb.append(game.getLastTour().getId());


        return load(sb.toString());
    	
    	
    }
    
    @Override
    public Document newConversation(Game game, Conversation parent, 
    		String text , List<User> privateUsers) throws AccessException{
        
    	try {
	    	StringBuilder sb = new StringBuilder(JDIP_HOST);
	        sb.append('/');
	        sb.append(PAGE_CONVERSATION);
	        sb.append("?game=");
	        sb.append(game.getGameInfo().getGameId());
	        sb.append("&action=send");
	        
	        if (parent != null){
	        	sb.append("&parent=");
	        	sb.append(parent.getId());
	        }
	        
	        if (privateUsers != null && privateUsers.size() > 0){
	        	sb.append("&filter=");
	        	String aux = "";
	        	for (User user : privateUsers){
	        		if (user.getUserId() > 0 ){
	        			sb.append(aux);
	        			sb.append(user.getUserId());
	        			aux = ",";
	        		}
	        	}
	        }
	        
	        String body = URLEncoder.encode(text, "UTF-8");
	        sb.append("&text=");
	        sb.append(body);
	        //max id tour
	
	
	        return load(sb.toString());
        
    	} catch (UnsupportedEncodingException ex){
    		ex.printStackTrace();
    	}
    	//no document
    	return null;
    }
    
    @Override
	public Document fetchConversations(Game game) throws AccessException{
    	
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_CONVERSATION);
        sb.append("?game=");
        sb.append(game.getGameInfo().getGameId());
        sb.append("&action=list");
        
        if(game.getConversationManager().getLastConversation() != null){
        	sb.append("&from=");
        	sb.append(game.getConversationManager().getLastConversation().getId());
        }
        //max id tour


        return load(sb.toString());
    }
    
    /** Load options information */
    @Override
	public Document loadOrModifyOptions(Map<String,String> options) 
    	throws AccessException
    {
        StringBuilder sb = new StringBuilder(JDIP_HOST);
        sb.append('/');
        sb.append(PAGE_OPTIONS);
        sb.append("?action=op");

        if (options != null){
	        for(String key : options.keySet()){
	        	sb.append("&key:");
	        	sb.append(key);
	        	sb.append("=");
	        	sb.append(options.get(key));
	        }
        }


        return load(sb.toString());
    	
    }
    
    /**
     * Load document
     * @param url
     * @return
     */
    private Document load(String url) throws AccessException {

        //add URL options
        char sign = '&';
        if (url.indexOf('?') < 0) {
            sign = '?';

            //language options
            
        }
        url += sign + "lang=" + diplomacy.language.Language.getInstance().getLanguage();

        //security
        if (Main.getUser() != null) {
            url += "&token=" + Main.getUser().getToken();
        }
        Main.out(url);
        Document doc = null;

        try {
            URL u = new URL(url);
            doc = DocumentLoader.load(u);
        } catch (MalformedURLException ex) {
            Logger.getLogger(NetXmlProvider.class.getName()).log(Level.SEVERE, null, ex);
        }

        return doc;
    }
}

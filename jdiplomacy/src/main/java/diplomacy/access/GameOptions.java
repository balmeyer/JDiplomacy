package diplomacy.access;

import java.util.HashMap;
import java.util.Map;

import diplomacy.Main;
import diplomacy.access.exception.AccessException;

/**
 * game options.
 * @author JBVovau
 *
 */
public class GameOptions {

	//key / Value
	private Map<String,String> options = new HashMap<String,String>();
	
	private static GameOptions instance;
	
	public static GameOptions getInstance(){
		if (instance == null) instance = new GameOptions();
		return instance;
	}
	
	private GameOptions(){ }
	
	/**
	 * Load options from server
	 * @throws AccessException 
	 */
	public void load() throws AccessException{
		this.options = Main.getDBAccess().loadOrModifyOptions(null);

	}
	
	/**
	 * Save options to server
	 * @throws AccessException 
	 */
	public void save() throws AccessException{
		Main.getDBAccess().loadOrModifyOptions(this.options);
	}
	
	public String getOption(String key){
		return options.get(key);
	}
	
	public void setOption(String key, String value){
		this.options.put(key,value);
	}
	
}

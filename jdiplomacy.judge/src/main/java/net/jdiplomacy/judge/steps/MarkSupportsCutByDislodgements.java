/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.order.Order;

/**
 *
 * @author Jean-Baptiste
 */
@AdjudicationIndex(index=9)
public class MarkSupportsCutByDislodgements implements AdjudicationStep{

    @Override
	public String adjucate(SupportingDataStructure structure) {

        boolean anyUnitMarked = false;


        for(Unit u : structure.getUnits()){

            Order o = structure.getOrder(u);

            if (o == null) continue;

            if (u.getMarks() == null && o.getOrderType() == OrderType.movement){
                if (JudgeTools.cutSupport(structure,o)){
                    anyUnitMarked = true;
                }
            }
            
        }

        if (anyUnitMarked){
            return "MarkBouncesCausedbyInabilitytoSwapPlaces";
        }
        //no repeat needed
        return null;
    }

}

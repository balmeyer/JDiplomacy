/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Province;
import net.jdiplomacy.judge.order.Order;

/**
 *
 * @author Jean-Baptiste
 */
@AdjudicationIndex(index = 3)
public class CalculateInitialCombatStrengths implements AdjudicationStep {

    @Override
	public String adjucate(SupportingDataStructure structure) {
        //throw new UnsupportedOperationException("Not supported yet.");

        for (Order o : structure.getCorrectOrders()) {

            //move ?
            if (o.getOrderType() != OrderType.movement) {
                continue;
            }

            //non convoyed move : cut support
            if (!o.isConvoyedMove()){
                //cut support for the destination region
                JudgeTools.cutSupport(structure, o);
            }

        }

        //For every space on the board:
        //Create a "combat list" containing all units that are attempting
        //to either move to or remain in that space
        for (Unit u : structure.getBoard().getUnits()) {
            Order o = structure.getOrder(u);

            Province p = null;

            if (o != null) {
                p = o.getExpectedResultRegion().getProvince();
            } else {
                p = u.getRegion().getProvince();
            }

            structure.addToCombatList(p, u);
        }

        //no repeat needed
        return null;

    }
}

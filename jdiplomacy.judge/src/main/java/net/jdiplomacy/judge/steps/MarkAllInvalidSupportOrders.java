/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.JudgeEngine;
import net.jdiplomacy.judge.JudgeOptions;
import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.order.Order;

/**
 * Step 2. Mark All Invalid Support Orders
 * @author Jean-Baptiste
 */
@AdjudicationIndex(index = 2)
public class MarkAllInvalidSupportOrders implements AdjudicationStep {

    @Override
	public String adjucate(SupportingDataStructure structure) {
    	

        //For all units issuing support orders:
        for (Order o : structure.getCorrectOrders()) {
            //is support ?
            if (o.getOrderType() != OrderType.support) {
                continue;
            }

            String failMessage = null;
            
            //cannot support itself
            if (o.getBaseRegion().equals(o.getStartRegion())) {
                failMessage = ("cannot support itself");
            }

            //test supported unit existence
            Unit supported = structure.getBoard().getUnit(o.getStartRegion());
            if (supported == null) {
                failMessage = ("void supported unit does not exist" );
            }

            //test matching order
            if (!Order.areSupportOrConvoyMatch(o, structure.getOrder(supported))) {
                failMessage = ("void supported no match");
            }

            if (failMessage != null){
            	o.fail(failMessage);
            	if (JudgeEngine.options == JudgeOptions.failingSupportOrConvoyHold){
            		o.getUnit().markAs(Mark.hold);
            	}
            	continue;
            }
            
            //support ok
            //Increment support count of supported unit
            supported.addCountSupport();

            //test no help list
            Unit attackedUnit = structure.getBoard().getUnit(o.getEndRegion());
            if (attackedUnit != null) {
                Country attackedCountry = attackedUnit.getCountry();
                if (attackedCountry.equals(o.getCountry())) {
                    supported.addToNohelpList(o.getUnit());
                }
            }

        }

        //no repeat needed
        return null;
    }
}

package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.game.board.Supply;
import net.jdiplomacy.judge.order.Order;

@AdjudicationIndex(index=1)
public class CheckAdjustment implements AdjudicationStep {

	@Override
	public String adjucate(SupportingDataStructure structure) {

		// clear beginning marks
		structure.getBoard().clearMarks();
		
		//test if occupied region
		for (Order o : structure.getCorrectOrders()){
			
			
			if (o.getOrderType() != OrderType.add && o.getOrderType() != OrderType.del){
				o.fail("not allowed in adjust phase");
				continue;
			}
				
			Region r = o.getBaseRegion();
			
			if (r == null){
				o.fail("unknown region");
				continue;
			}
			
			Supply s = Supply.getSupply(r.getProvince());
			
			if (s == null && o.getOrderType() == OrderType.add){
				o.fail("no supply here");
				continue;
			}
			
			Unit u = structure.getBoard().getUnit(r);
			
			//cannot add
			if (u != null && o.getOrderType() == OrderType.add) {
				o.fail("unit already present");
				continue;
			}
			
			//cannot del
			if (u == null && o.getOrderType() == OrderType.del) {
				o.fail("no unit to delete here");
				continue;
			}
			
			if (s != null && s.getOriginalOwner() == null){
				o.fail("neutral power");
				continue;
			}
			
			assert o.getCountry() != null;
			if(s != null && !s.getOriginalOwner().equals(o.getCountry())){
				o.fail("not good owner");
				continue;
			}
			
			//test unit type
			if (o.getOrderType() == OrderType.add && !o.getBaseRegion().acceptUnit(o.getUnitType())){
				o.fail("unit not allowed here ");
				continue;
			}
			
			
            
			int unitDif = structure.getBoard().getAdjustDifference(o.getCountry());
			
			if (o.getOrderType() == OrderType.add && unitDif <= 0){
				o.fail("cannot add unit for country");
				continue;
			}
			
			if (o.getOrderType() == OrderType.del && unitDif >= 0){
				o.fail("cannot del unit for country");
				continue;
			}
			
			//ok !
            //commit add
            if (o.getOrderType() == OrderType.add){
            	structure.getBoard().createUnit(o.getBaseRegion(), o.getCountry(), o.getUnitType());
            	o.validateMove();
            }
            
            if (o.getOrderType() == OrderType.del){
            	structure.getBoard().deleteUnit(u);
            	o.validateMove();
            }
			
		}
		
		//remove units if not del order enough
		for(Country c : Country.getCountries()){
			//no units ?
			if (structure.getBoard().getUnits(c).size() == 0) continue;
			
			int dif = structure.getBoard().getAdjustDifference(c);
			int i = 0;
			while (i > dif){
				//get first unit
				Object [] units = structure.getBoard().getUnits(c).toArray();
				//delete first unit found
				structure.getBoard().deleteUnit((Unit) units[0]);
				i--;
			}
		}
		
		return null;
	}

}

package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.order.Order;

@AdjudicationIndex(index=1)
public class DisbandPurgeOrders implements AdjudicationStep {

	@Override
	public String adjucate(SupportingDataStructure structure) {
		

		//clean not move order
		for (Order o : structure.getCorrectOrders()){
			if (o.getOrderType() != OrderType.movement){
				o.fail("not allowed in disband phase");
				continue;
			}
			
			Unit dislodged = structure.getBoard()
				.getUnit(o.getBaseRegion().getProvince(), true);
			
			//no disband
			if (dislodged == null){
				o.fail("no unit");
				continue;
			}
			
			//
			if (!dislodged.isMarkedAs(Mark.dislodged)){
				o.fail("unit is not dislodged");
				continue;
			}
			
			//check disband from
			if (!dislodged.getPossibleRetreats().contains(o.getEndRegion())){
				o.fail("bounce");
				continue;
			}

			
			//success !
			o.validateMove();
		}
		return null;
	}

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.order.Order;

/**
 *
 * @author Jean-Baptiste
 */
@AdjudicationIndex(index=5)
public class MarkConvoyDisruptionsAndSupportCutsMadebySuccessfulConvoys implements AdjudicationStep{

    @Override
	public String adjucate(SupportingDataStructure structure) {

        int lastSize = structure.getConvoySucceededListSize();

        //For every army in the "convoying armies list":
        for(Unit u : structure.getConvoyedArmies()){
            Order o = structure.getOrder(u);
            
            //check disruption
            JudgeTools.checkDisruptions(structure, u);



            if (u.isMarkedAs(Mark.convoy_endangered)){
                o.fail("no convoy");
                //no more support
                u.setCountSupport(0);

                //Mark as "no convoy" all unmarked units that are offering support to this unit.
                for (Order supporting : structure.getCorrectOrders()){
                    if (supporting.getOrderType() != OrderType.support) continue;
                    
                    if (Order.areSupportOrConvoyMatch(supporting, o)){
                        supporting.fail("no convoy");
                    }
                }
            } else {


            if (u.isMarkedAs(Mark.convoy_under_attack)){

                //remove mark
                u.clearMarks();

                //cut support (?)
                JudgeTools.cutSupport(structure, o);

                //add to succeed list
                structure.addToConvoySuccededList(u);

            }
            }

        }

        if (lastSize != structure.getConvoySucceededListSize()) {
            //If the "convoy succeeded list" grew during this step, then return to step 4.
            return "MarkSupportCutsMadebyConvoyersandMarkEndangeredConvoys";
        }


        //no repeat needed
        return null;
    }
}

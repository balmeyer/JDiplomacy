package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Supply;

/**
 * Add supplies
 * @author JBVovau
 *
 */
@AdjudicationIndex(index=12)
public class AddNewSupplies implements AdjudicationStep {

	@Override
	public String adjucate(SupportingDataStructure structure) {

		for (Unit u : structure.getUnits()){
			
			//TODO ask if any mark disable new supply
			//if (u.getMarks() != null) continue;
			if (u.isMarkedAs(Mark.dislodged)) continue;
			
			//get supply
			Supply s = Supply.getSupply(u.getRegion().getProvince());
			
			//no supply
			if (s == null) continue;
			
			structure.getBoard().setOwner(s, u.getCountry());
		}
		
		return null;
	}

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge.steps;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;



/**
 * Give to "step" an index to sort them.
 * @author Jean-Baptiste
 */

@Retention(RetentionPolicy.RUNTIME)

public @interface AdjudicationIndex {

    public int index();


}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.order.Order;

/**
 *
 * @author Jean-Baptiste
 */
@AdjudicationIndex(index = 4)
public class MarkSupportCutsMadebyConvoyersandMarkEndangeredConvoys implements AdjudicationStep {

    @Override
	public String adjucate(SupportingDataStructure structure) {

        int initialListSize = structure.getConvoySucceededListSize();

        //For every army in the "convoying armies list":
        for (Unit army : structure.getConvoyedArmies()) {

            //check disruptions
            JudgeTools.checkDisruptions(structure, army);

            //get order
            Order o = structure.getOrder(army);
            assert o != null;

            //unit not marked
            if (army.getMarks() == null) {
                //cut procedure
                JudgeTools.cutSupport(structure, o);

                //convoy succeed
                structure.addToConvoySuccededList(army);
            } else {
                //assert army.isMarkedAs(Mark.convoy_endangered);
                if (army.isMarkedAs(Mark.convoy_endangered)){
                army.markAs(Mark.convoy_under_attack);
                }
            }
        }

        //If the "convoy succeeded list" grew during this step, then repeat this step.
        if (structure.getConvoySucceededListSize() != initialListSize) {
            //ask for repetition
            return this.getClass().getSimpleName();
        }

        return null;
    }
}

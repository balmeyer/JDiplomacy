/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Province;
import net.jdiplomacy.judge.order.Order;

/**
 *
 * @author Jean-Baptiste
 */
@AdjudicationIndex(index=8)
public class MarkBouncesCausedbyInabilitytoSelfDislodge implements AdjudicationStep{

    @Override
	public String adjucate(SupportingDataStructure structure) {
        
        boolean anyUnitMarkedAsBounce = false;


        for(Province p : Province.getProvinceList()){

            Unit greaterSupport = structure.getGreatestFromCombatList(p);

            if (greaterSupport == null) continue;

            Unit victim = null;

            Order o = structure.getOrder(greaterSupport);
            if (greaterSupport.getMarks() == null && o != null
                    && o.getOrderType() == OrderType.movement){
                    victim = structure.getBoard().getUnit(p);
            }

            //If there is a "victim," but the "victim" was issued a move order
            //and is not marked, then there is no "victim."
            if(victim != null){
                Order victimOrder = structure.getOrder(victim);

                if (victimOrder != null && victim.getMarks() == null){
                    victim = null;
                }
            }

            //If there is still a "victim," but if the "victim" is owned
            //by the same power that owns the moving unit, there is no "victim."
            //Execute the bounce procedure for the moving unit.
            if (victim != null){
                if (victim.getCountry().equals(greaterSupport.getCountry())){
                	assert greaterSupport.equals(o.getUnit());
                    JudgeTools.bounce(structure, greaterSupport);
                    victim = null;
                    anyUnitMarkedAsBounce = true;
                }
            }

            //If there is still a "victim", then subtract the number of units
            //listed in the moving unit's "no help" list from its "support count."
            //If this result is no longer larger than the "support count" for all
            //other units in the "combat list" for the space,
            //execute the bounce procedure for the moving unit.
            if (victim != null){
                int totalMovingSupport = greaterSupport.getCountSupport() 
                        - greaterSupport.getNohelpList().size();

                int otherGreaterSupport = structure.getSecondGreatestSupportFromCombalist(p,greaterSupport);
                
                if (totalMovingSupport <= otherGreaterSupport){
                	assert greaterSupport.equals(o.getUnit());
                    JudgeTools.bounce(structure, greaterSupport);
                    anyUnitMarkedAsBounce = true;
                }
            }
            
        }

        if(anyUnitMarkedAsBounce){
            return "MarkBouncesCausedbyInabilitytoSwapPlaces";
        }

        //no repeat needed
        return null;
    }

}

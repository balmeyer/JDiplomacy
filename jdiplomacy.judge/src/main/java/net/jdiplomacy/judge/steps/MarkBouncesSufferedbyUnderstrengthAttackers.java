/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge.steps;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Province;
import net.jdiplomacy.judge.order.Order;

/**
 *
 * @author Jean-Baptiste
 */
@AdjudicationIndex(index=7)
public class MarkBouncesSufferedbyUnderstrengthAttackers implements AdjudicationStep {

    @Override
	public String adjucate(SupportingDataStructure structure) {

        boolean anyUnitMarkedAsBounce = false;

        for (Province p : Province.getProvinceList()){

            //get the greater support count for this combat list
            Unit greaterSupportUnit = structure.getGreatestFromCombatList(p) ;

            Iterator<Unit> it = structure.getCombatList(p);

            List<Unit> myCombatList = new ArrayList<Unit>();
            while(it.hasNext()){
                myCombatList.add(it.next());
            }

            for(Unit u : myCombatList) {

                Order o = structure.getOrder(u);
                
                if (o == null) continue ;

                if (u.getMarks() == null && o.getOrderType() == OrderType.movement){
                    //test greater support

                    boolean isNotTheGreatestSupport = false ;
                    if (greaterSupportUnit == null){
                        //no greatest support : equality
                        isNotTheGreatestSupport = true;
                    } else {
                        //check if the greatest is not the unit itself
                        isNotTheGreatestSupport = (!greaterSupportUnit.equals(u)
                                && u.getCountSupport() <= greaterSupportUnit.getCountSupport());
                    }
                    

                    if (isNotTheGreatestSupport){
                        JudgeTools.bounce(structure, u );
                        anyUnitMarkedAsBounce = true;
                    }
                }
            }
            
        }

        if (anyUnitMarkedAsBounce){
            //If any unit was marked "bounce" during this step, repeat this step.
            return "MarkBouncesCausedbyInabilitytoSwapPlaces";
        }

        //no repeat needed
        return null;
    }
}

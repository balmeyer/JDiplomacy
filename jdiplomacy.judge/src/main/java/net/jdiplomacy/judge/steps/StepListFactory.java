package net.jdiplomacy.judge.steps;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.jdiplomacy.judge.Phase;


/**
 * Create list of Adjudication Step class.
 * 
 * Phases : - move spring - spring disband - move autumn - autumn disband (check
 * supplies) - adjust
 * 
 * @author JBVovau
 * 
 */
public class StepListFactory {

	// Adjudications class to be instanced : movement
	private static final Class<?>[] MOVEMENT_STEPS_SPRING = new Class<?>[] {
			MarkAllInvalidConvoyOrders.class,
			MarkAllInvalidSupportOrders.class,
			CalculateInitialCombatStrengths.class,
			MarkSupportCutsMadebyConvoyersandMarkEndangeredConvoys.class,
			MarkConvoyDisruptionsAndSupportCutsMadebySuccessfulConvoys.class,
			MarkBouncesCausedbyInabilitytoSwapPlaces.class,
			MarkBouncesSufferedbyUnderstrengthAttackers.class,
			MarkBouncesCausedbyInabilitytoSelfDislodge.class,
			MarkSupportsCutByDislodgements.class,
			MoveUnitsThatDidNotBounce.class, CalculateValidityOrders.class,
			CommitMovement.class };

	private static final Class<?>[] MOVEMENT_STEPS_AUTUMN = new Class<?>[] {
			MarkAllInvalidConvoyOrders.class,
			MarkAllInvalidSupportOrders.class,
			CalculateInitialCombatStrengths.class,
			MarkSupportCutsMadebyConvoyersandMarkEndangeredConvoys.class,
			MarkConvoyDisruptionsAndSupportCutsMadebySuccessfulConvoys.class,
			MarkBouncesCausedbyInabilitytoSwapPlaces.class,
			MarkBouncesSufferedbyUnderstrengthAttackers.class,
			MarkBouncesCausedbyInabilitytoSelfDislodge.class,
			MarkSupportsCutByDislodgements.class,
			MoveUnitsThatDidNotBounce.class, CalculateValidityOrders.class,
			CommitMovement.class, AddNewSupplies.class };

	private static final Class<?>[] ADJUST_STEPS = new Class[] { CheckAdjustment.class };

	private static final Class<?>[] DISBAND_STEPS = new Class[] {
			DisbandPurgeOrders.class, CalculateValidityOrders.class,
			CommitDisband.class };

	/**
	 * Create adjudication steps pour current phase
	 * 
	 * @param phase
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public static List<AdjudicationStep> createList(Phase phase) {

		Class<?>[] clazz = getClasses(phase);

		// Create instance

		List<AdjudicationStep> steps = new ArrayList<AdjudicationStep>();

		try {

			for (Class<?> c : clazz) {
				steps.add((AdjudicationStep) c.newInstance());
			}
		} catch (IllegalAccessException iaex) {
			Logger.getLogger("StepListFactory").log(Level.SEVERE,
					"error in adjudication step", iaex);
			throw new IllegalStateException("error in building judge list",
					iaex);
		} catch (InstantiationException ex) {
			Logger.getLogger("StepListFactory").log(Level.SEVERE,
					"error in adjudication step", ex);
			throw new IllegalStateException("error in building judge list", ex);
		}

		return steps;
	}

	/**
	 * Get class for instance
	 * 
	 * @param phase
	 * @return
	 */
	private static Class<?>[] getClasses(Phase phase) {
		switch (phase) {
		case movementSpring:
			return MOVEMENT_STEPS_SPRING;
		case movementFall:
			return MOVEMENT_STEPS_AUTUMN;
		case disbandFall:
		case disbandSpring:
			return DISBAND_STEPS;
		case adjust:
			return ADJUST_STEPS;
		default:
				throw new IllegalArgumentException("Phase unknown : " + phase);
		}

	}

}

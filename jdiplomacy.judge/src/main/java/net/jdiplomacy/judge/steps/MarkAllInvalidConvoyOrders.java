/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge.steps;

import java.util.ArrayList;
import java.util.List;

import net.jdiplomacy.judge.JudgeEngine;
import net.jdiplomacy.judge.JudgeOptions;
import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.UnitType;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.graph.Graph;
import net.jdiplomacy.judge.graph.GraphNode;
import net.jdiplomacy.judge.order.Order;

/**
 * <li>For all convoying armies:</li> <blockquote> If trying to convoy through a
 * non-existent fleet, either change the army's order to HOLD or mark the army
 * "no convoy" (the decision as to which should be done is based on a game
 * option).
 * 
 * Otherwise, if trying to convoy through a fleet that did not order a matching
 * convoy order, mark the army "no convoy."
 * 
 * Otherwise, include this army in a "convoying armies list." </blockquote>
 * 
 * For all fleets issuing convoy orders:
 * 
 * If the army being convoyed does not exist, either change the fleet's order to
 * HOLD or mark the fleet "void" (the decision as to which should be done is
 * based on a game option).
 * 
 * Otherwise, if the army being convoyed did not issue the matching order, mark
 * the fleet "void."
 * 
 * 
 * @author Jean-Baptiste
 */
@AdjudicationIndex(index = 1)
public class MarkAllInvalidConvoyOrders implements AdjudicationStep {

	@Override
	public String adjucate(SupportingDataStructure structure) {

		// clear beginning marks
		structure.getBoard().clearMarks();

		// For all convoying armies:
		for (Order o : structure.getCorrectOrders()) {
			if (o.isConvoyedMove()) {

				// convoy helper : fill convoy path when empty
				if (o.getConvoiPath().size() == 0) {
					o.getConvoiPath().addAll(this.fillConvoyPath(o, structure));
				}

				// check fleets
				for (Region r : o.getConvoiPath()) {
					Unit fleet = structure.getBoard().getUnit(r);
					if (fleet == null) {
						// non existing fleet
						o.fail("no convoy");
						if (JudgeEngine.options == JudgeOptions.failingSupportOrConvoyHold)
							o.getUnit().markAs(Mark.hold);
						break;
					} else {
						// test matching convoy
						Order convoying = structure.getOrder(fleet);
						if (!Order.areSupportOrConvoyMatch(convoying, o)) {
							o.fail("no convoy");
							if (JudgeEngine.options == JudgeOptions.failingSupportOrConvoyHold)
								o.getUnit().markAs(Mark.hold);
							break;
						} else {
							// add to convoying list
							structure.addConvoyingArmy(convoying.getUnit());
							structure.addConvoyedArmy(o.getUnit());
						}
					}
				}

				// check if convoy path is complete

			}
		}

		// For all fleets issuing convoy orders:
		for (Order o : structure.getCorrectOrders()) {

			// is convoy ?
			if (o.getOrderType() != OrderType.convoy)
				continue;

			// test non existant fleet
			Unit fleet = structure.getBoard().getUnit(o.getBaseRegion());
			if (fleet == null) {
				o.fail("void");
				continue;
			}
			assert fleet.getType() == UnitType.FLEET;
			// convoy match
			if (!Order.areSupportOrConvoyMatch(o, structure.getOrderFromBase(o
					.getStartRegion()))) {
				o.fail("void (no match)");
				continue;
			}
		}

		// test if convoy path is complete
		for (Order o : structure.getCorrectOrders()) {
			if (!o.isConvoyedMove())
				continue;

			if (!this.testConvoyPath(o)) {
				o.fail("no convoy");

			}
		}
		// no repeat needed
		return null;

	}

	/**
	 * Get convoy path regarding fleet
	 * 
	 * @param o
	 * @return
	 */
	private List<Region> fillConvoyPath(Order o,
			SupportingDataStructure structure) {
		List<Region> result = new ArrayList<Region>();

		for (Order c : structure.getCorrectOrders()) {

			if (c.getOrderType() == OrderType.convoy) {
				// starting regions are similar ?
				if (c.getStartRegion().equals(o.getBaseRegion())) {
					// add fleet region as part of convoy path
					result.add(c.getBaseRegion());
				}

			}
		}

		return result;
	}

	/**
	 * Test if the convoy path is complete for this order
	 * @param o
	 * @return
	 */
	private boolean testConvoyPath(Order o) {

		if (!o.isSuccess())
			return false;

		//get start and end region
		Region start = o.getBaseRegion();
		Region end = o.getEndRegion();

		//Copy the convoy path (not modify original one)
		List<Region> path = new ArrayList<Region>();
		path.addAll(o.getConvoiPath());

		//try to reach start to end
		if (reachByThisWay(start, end, path)) {
			return true;
		}

		return false;
	}

	/**
	 * Test if one can join end region from start with specified list path
	 * @param start
	 * @param end
	 * @param path
	 * @return
	 */
	private boolean reachByThisWay(Region start, Region end, List<Region> path) {

		Graph graph = Unit.getGraph(UnitType.FLEET);
		GraphNode node = graph.getNode(start);

		if (graph.getNode(end).isNear(node)) {
			return true;
		}

		for (Region r : path) {
			if (graph.getNode(r).isNear(node)) {
				// node adjacent
				List<Region> newPath = new ArrayList<Region>();
				newPath.addAll(path);
				newPath.remove(r);
				boolean ok = reachByThisWay(r, end, newPath);
				if (ok)
					return true;
			}
		}

		return false;
	}

}

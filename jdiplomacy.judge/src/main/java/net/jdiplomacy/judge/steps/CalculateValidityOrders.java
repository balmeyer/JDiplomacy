/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.UnitType;
import net.jdiplomacy.judge.graph.Graph;
import net.jdiplomacy.judge.graph.GraphExplorer;
import net.jdiplomacy.judge.order.Order;

/**
 * Test validity of movements
 * @author Jean-Baptiste
 */
@AdjudicationIndex(index = 0)
public class CalculateValidityOrders implements AdjudicationStep {

    @Override
	public String adjucate(SupportingDataStructure structure) {
    	

    	
        for (Order o : structure.getCorrectOrders()) {
        	try {
            testMovement(o);
        	} catch (NullPointerException npex){
        		//TODO temp remove this
        		throw new IllegalStateException("order problem : "
        				+ o.getText() , npex);
        	}
        }

                //no repeat needed
        return null;
    }

    private void testMovement(Order o) {
        //unit available ?
        if (o.getUnit() == null) {
            o.fail("no unit here");
            return;
        }

        //get the unit and test validity movement
        Unit u = o.getUnit();

        //job already done in "Order" constructor
        //unit not allowed in a specific territory (army in sea ?)
        if (!o.getEndRegion().acceptUnit(u)){
            o.fail("impossible movement");
            return;
        }

        
        boolean testMovement = (u.getType() == UnitType.FLEET);

        testMovement |= !o.getBaseRegion().isConvoyStop();

        testMovement |= !o.getEndRegion().isConvoyStop();

        //test basic move (sure convoy is not possible)
        if (testMovement) {
            Graph g = o.getUnit().getGraph();
            GraphExplorer exp = new GraphExplorer(g);

            if (!exp.canJoin(o.getBaseRegion(), o.getEndRegion())) {
                o.fail("impossible movement for ship");
            }
        }



    }
}

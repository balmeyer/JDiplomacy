package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.order.Order;


/**
 * Modify current board by moving unit with validated moves.
 * @author JBVovau
 *
 */
@AdjudicationIndex(index=11)
public class CommitMovement implements AdjudicationStep {

	@Override
	public String adjucate(SupportingDataStructure structure) {
		
        for (Order o : structure.getCorrectOrders()) {
        	
        	//commit moves
            if (o.getOrderType() == OrderType.movement && o.isMoveValidated()) {
                structure.getBoard().moveUnit(o.getUnit(), o.getResultRegion());
            }
            
        }
		
		return null;
	}

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.order.Order;

/**
 *
 * @author Jean-Baptiste
 */
@AdjudicationIndex(index = 10)
public class MoveUnitsThatDidNotBounce implements AdjudicationStep {

    @Override
	public String adjucate(SupportingDataStructure structure) {
        //throw new UnsupportedOperationException("Not supported yet.");

        for (Order o : structure.getCorrectOrders()) {

            if (o.getOrderType() == OrderType.movement && o.getUnit().getMarks() == null) {

                Unit dislodged = structure.getBoard().getUnit(o.getEndRegion());

                if (dislodged != null) {

                    Order dislodegedOrderUnit = structure.getOrder(dislodged);
                    boolean notMoving = dislodegedOrderUnit == null
                            || dislodegedOrderUnit.getResultRegion().equals(dislodged.getRegion());

                    if (dislodged.getMarks() != null || notMoving) {

                    	//Depending game option : mark or hold
                    	if (dislodged.isMarkedAs(Mark.hold)) continue;
                    	
                        dislodged.markAs(Mark.dislodged);
                        

                        //If the dislodged unit had attempted a non-convoyed move into
                        //the space from which its attacker came, and if the attacker
                        //also was not convoyed, then remove the dislodged unit from
                        //the "combat list" for the space from which the attacker came.
                        if (dislodegedOrderUnit != null) {
                            if (dislodegedOrderUnit.isConvoyedMove()) {
                                continue;
                            }
                            if (o.isConvoyedMove()) {
                                continue;
                            }
                            if (o.getBaseRegion().equals(dislodegedOrderUnit.getEndRegion())
                                    && o.getEndRegion().equals(dislodegedOrderUnit.getBaseRegion())) {
                                structure.removeFromCombatList(o.getBaseRegion().getProvince(), dislodged);
                            }
                        }

                        //Create the "possible retreats" list for the dislodged unit
                        //by listing all spaces on the board to which the unit could move
                        //without convoying, but do not list the space from which
                        //the dislodged unit's attacker came, nor any space with a "combat list."
                        for (Region r : dislodged.getRoundingRegions()) {
                            if (r.equals(o.getBaseRegion())) {
                                continue;
                            }
                            if (structure.getCombatListSize(r.getProvince()) > 0) {
                                continue;
                            }
                            //add to dislodging
                            dislodged.addToPossibleRetreats(r);
                        }
                    }
                }

                //
                JudgeTools.unbounce(structure, o.getBaseRegion().getProvince());

                //move ?
                o.validateMove();
            }
        }

        //no repeat needed
        return null;
    }
}

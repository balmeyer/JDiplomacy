package net.jdiplomacy.judge.steps;

import java.util.ArrayList;
import java.util.List;

import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.order.Order;


@AdjudicationIndex(index=2)
public class CommitDisband implements AdjudicationStep {

	@Override
	public String adjucate(SupportingDataStructure structure) {
		
		List<Unit> toPreserve = new ArrayList<Unit>();
		
		for (Order o : structure.getCorrectOrders()){
			//dislodged unit
			Unit u = structure.getBoard()
				.getUnit(o.getBaseRegion().getProvince(),true);
			assert u != null;
			
			//move
			if (o.isMoveValidated()){
				structure.getBoard().moveUnit(u, o.getEndRegion());
				toPreserve.add(u);
			} else {
				//delete unit
				structure.getBoard().deleteUnit(u);
			}
			
		}
		
		//clean
		//important ! concurent modif when delete
		List<Unit> units = new ArrayList<Unit>();
		units.addAll(structure.getUnits());
		
		for (Unit u : units){
			if (u.isMarkedAs(Mark.dislodged) && !toPreserve.contains(u)){
				structure.getBoard().deleteUnit(u);
			}
		}
		
		return null;
	}

}

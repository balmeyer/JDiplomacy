/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.order.Order;

/**
 *
 * @author Jean-Baptiste
 */
@AdjudicationIndex(index = 6)
public class MarkBouncesCausedbyInabilitytoSwapPlaces implements AdjudicationStep {

    @Override
	public String adjucate(SupportingDataStructure structure) {

        boolean anyUnitMarkedAsBounce = false;

        //For all moving units that are not convoying and are not marked:
        for (Order o : structure.getCorrectOrders()) {
            //no move
            if (o.getOrderType() != OrderType.movement) {
                continue;
            }
            if (o.isConvoyedMove() || o.getUnit().getMarks() != null) {
                continue;
            }

            Region dest = o.getEndRegion();
            Unit unitDest = structure.getBoard().getUnit(dest);
            Unit swapper = null;
            Unit unit = o.getUnit();

            //test swapping unit
            if (unitDest != null) {
                //Region swapRegion = null;
                boolean destUnitConvoying = false;

                Order swappingOrder = structure.getOrder(unitDest);
                boolean swapping = false;
                
                if (swappingOrder != null) {
                    //swapRegion = swappingOrder.getEndRegion();
                    destUnitConvoying = swappingOrder.isConvoyedMove();
                    swapping = o.getEndRegion().equals(swappingOrder.getBaseRegion())
                            && o.getBaseRegion().equals(swappingOrder.getEndRegion());
                }

                

                if (unitDest.getMarks() == null && swapping && !destUnitConvoying) {
                    swapper = unitDest;
                }
            }

            //manage what to do with the "swapper"
            if (swapper != null) {
                boolean bothOwnedBySamePlayer = unit.getCountry().equals(swapper.getCountry());

                int unitSupportCountWithoutHelp = unit.getCountSupport() - unit.getNohelpList().size();
                int swapperSupportCount = swapper.getCountSupport();

                int swapperSupportCountWithoutHelp = swapper.getCountSupport() - swapper.getNohelpList().size();
                int unitSupportCount = unit.getCountSupport();
                
                if (bothOwnedBySamePlayer || unitSupportCountWithoutHelp <= swapperSupportCount){
                    //add to bounce list
                    JudgeTools.bounce(structure, unit );
                    anyUnitMarkedAsBounce = true;
                }


                if (bothOwnedBySamePlayer || swapperSupportCountWithoutHelp <= unitSupportCount){
                    //add to bounce
                    JudgeTools.bounce(structure, unit );
                    JudgeTools.bounce(structure, swapper );
                    anyUnitMarkedAsBounce = true;
                }
                
            }

        }


        if (anyUnitMarkedAsBounce){
            //repeat the same step
            return "MarkBouncesCausedbyInabilitytoSwapPlaces";
        }

        //no repeat needed
        return null;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.game.SupportingDataStructure;

/**
 *
 * @author Jean-Baptiste
 */
public interface AdjudicationStep {


    /**
     * Returns the step to repeat. Null if no repeat needed
     * @param structure
     * @return
     */
    public String adjucate(SupportingDataStructure structure);

}

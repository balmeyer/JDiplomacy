/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge.steps;

import net.jdiplomacy.judge.JudgeEngine;
import net.jdiplomacy.judge.JudgeOptions;
import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.UnitType;
import net.jdiplomacy.judge.game.board.Province;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.order.Order;

/**
 *
 * @author Jean-Baptiste
 */
public class JudgeTools {


    public static boolean cutSupport(SupportingDataStructure structure , Order cuttingMove){
        //TODO cut support subroutine
        Region supportingRegion = cuttingMove.getEndRegion();

        Unit supportingUnit = structure.getBoard().getUnit(supportingRegion);
        if (supportingUnit == null) return false;

        Order supportOrder = structure.getOrder(supportingUnit);

        if (supportOrder == null) return false ;
        
        if (supportOrder.getOrderType() != OrderType.support) return false;

        //Unit movingUnit = cuttingMove.getUnit();
        
        //1. is not already marked "cut" or "void,"
        if(supportingUnit.isMarkedAs(Mark.cut) || supportingUnit.isMarkedAs(Mark.empty)) return false;

        //2. is not owned by the same power as the moving unit,
        if (supportingUnit.getCountry().equals(cuttingMove.getCountry())) return false;
            
        //TODO 3. if (and only if) the moving unit is a convoyed army, 
        //the supporting unit is not offering support for or against 
        //any convoying fleet (whose order is not marked "void"), and...
        boolean b3 = false;
        if (cuttingMove.isConvoyedMove()){
        	
        }
        
        	
        //4. either we are executing Step 9 of the main algorithm or 
        //the supporting unit is not offering support for a move into 
        //the space from which the moving unit originated.	
        //TODO terminate the cutSupport subroutine
        		
        boolean step9 = structure.getCurrentStepInstance().getClass()
        	.equals(MarkSupportsCutByDislodgements.class);
        		
        boolean supportOnCuttingStartRegion = supportOrder.getEndRegion().equals(cuttingMove.getStartRegion());
       
        boolean doIt = true ;
        
        if (b3) doIt =(step9 || supportOnCuttingStartRegion);
        
        if (!doIt) return false;
        
        
        //Cut !
        //boolean cutted = false;

        Unit supportedUnit = structure.getBoard().getUnit(supportOrder.getStartRegion());
        
        assert supportedUnit != null;
        
        if (supportedUnit != null && supportedUnit.isMarkedAs(Mark.cut)) return false;
        
        supportOrder.fail("cut");
        supportedUnit.setCountSupport(supportedUnit.getCountSupport() - 1);
        supportedUnit.getNohelpList().remove(supportingUnit);

        if (JudgeEngine.options == JudgeOptions.failingSupportOrConvoyHold){
        	supportingUnit.markAs(Mark.hold);
        }
        
        return true;

    }


    /**
     * 
     * @param structure
     * @param armyMoving
     */
    public static void checkDisruptions(SupportingDataStructure structure , Unit armyMoving){


        if (armyMoving == null) return;
        assert(armyMoving.getType() == UnitType.ARMY);
        if (armyMoving.getType() != UnitType.ARMY) throw new IllegalArgumentException ("convoyed unit must be an army : " + armyMoving);
        Order o = structure.getOrder(armyMoving);

        if (o == null) return;
        
        //get convoy path
        
        //convoyed order
        Order convoyedOrder = structure.getOrder(armyMoving);
        
        if (convoyedOrder == null) return;
        
        for (Region r : convoyedOrder.getConvoiPath()){
        	//greater support for this region
        	Unit greater = structure.getGreatestFromCombatList(r.getProvince());

        	Unit fleet = structure.getBoard().getUnit(r);
        	
        	//If the "combat list" for the space occupied by the fleet contains 
        	//a single unit with a higher support count than all others, 
        	//and if that single unit is not owned by the same power 
        	//that owns the convoying fleet, then mark the convoying 
        	//army as "convoy endangered."
        	
        	//not same country that convoying
        	if (greater != null && !greater.getCountry().equals(fleet.getCountry())){
        		armyMoving.markAs(Mark.convoy_endangered);
        		break;
        	}
        	
        	//http://en.wikibooks.org/wiki/Diplomacy/Rules
        	//"Note that convoys are not "broken" as easily as support; 
        	//a convoying fleet that is attacked but not dislodged will 
        	//successfully carry out its convoy order."
        	
        	//TODO (If the RULE SAFE_CONVOYS is in use, an army is also marked "convoy engangered" if the combat list for the space occupied by the fleet has more than one unit tied with an equal greatest support count, and if at least one of these units is not owned by the same power that owns the convoying fleet, and if the convoying fleet is not one of these units.)
        	
        }
    }


    /**
     * Bounce subroutine
     * @param structure
     * @param bouncedUnit
     */
    public static void bounce(SupportingDataStructure structure, Unit bouncedUnit){
        // bounce subroutine
        bouncedUnit.markAs(Mark.bounce);
        bouncedUnit.setCountSupport(0);
        bouncedUnit.getNohelpList().clear();
       
        
        //order
        Order o = structure.getOrder(bouncedUnit);
        o.fail("bounce");
        //bouncedUnit.bounceOn(o.getEndRegion());

        assert o != null;
        structure.addToCombatList(o.getEndRegion() , bouncedUnit) ;

    }

    /***
     * 
     * @param structure
     * @param p
     */
    public static void unbounce(SupportingDataStructure structure, Province p){

        Unit maxUnitBounce = structure.getGreatestFromCombatList(p);

        //If there is a single unit in the "combat list" of the space
        //which has more strength than all others, and if this unit
        //is marked "bounce," then
        if (maxUnitBounce != null && maxUnitBounce.isMarkedAs(Mark.bounce)){

            maxUnitBounce.removeMark(Mark.bounce);

            if (maxUnitBounce.isMarkedAs(Mark.dislodged)){
                maxUnitBounce.removeMark(Mark.dislodged); 
            } else {
                structure.removeFromCombatList(p , maxUnitBounce);
                //do recursively
                unbounce(structure, p);
            }
        }

    }

}

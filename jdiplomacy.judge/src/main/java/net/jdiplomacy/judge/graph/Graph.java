package net.jdiplomacy.judge.graph;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Collection;

import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Region;

/**
 * 
 * Cet objet est represente un Graphe(Sommets & Arcs) pour mod�liser la carte du
 * Diplomacy. Le graphe utilis� est non orient�, et chaque arc indique un
 * d�placement possible entre deux provinces. <br>
 * <br>
 * 
 * Le diplomacy utilisera deux graphes : un pour les flottes, un pour les
 * arm�es.
 * 
 * @version 1.0 05/04/2004
 * @author JB Vovau - balmeyer@dev.java.net
 */
public class Graph {

    //declarations
    private final Map<String, GraphNode> nodes = new HashMap<String, GraphNode>();

    /**
     * Ajoute un sommet en le é�signant par son nom (exemple : "Spa").
     *
     * @param name
     *            nom du sommet
     * @param succ
     *            liste des successeurs, séparés par une virgule
     *
     */
    public GraphNode addNode(String firstNode, String successeur) {

        //cherche les successeurs du sommet
        //String s;
        GraphNode node = null;

        //ajoute le premier sommet
        node = this.addNode(firstNode);
        String[] sommets = successeur.split(" ");
        assert (sommets.length > 0);
        for (int i = 0; i < sommets.length; i++) {
            //ajoute le node suivant au node pr�cedent
            node.addSuccessor(this.addNode(sommets[i]));
        }

        return node;

    }

    /**
     * Ajoute un sommet au graphe d'apr�s son nom.
     *
     */
    public GraphNode addNode(String name) {
        if (getNode(name) == null) {
            GraphNode node = new GraphNode(name.toLowerCase());
            this.addNode(node);
        }

        return getNode(name);

    }

    /**
     * Ajoute un sommet au graphe.
     */
    public void addNode(GraphNode node) {
        if (!this.nodes.containsValue(node)) {
            this.nodes.put(node.getName(), node);
        }
    }

    public GraphNode addRegion(Region region) {
        GraphNode node = this.getNode(region.getName());
        //assert (node != null);
        if (node != null) {
            node.setRegion(region);
        }
        return node;
    }

    /**
     * Retourne un objet GraphNode (sommet) d'aprés son nom.
     */
    public GraphNode getNode(String name) {
        GraphNode result = nodes.get(name.toLowerCase());

        //si le résultat est null : tente de trouver un équivalent
        if (result == null && name != null && name.length() == 3) {
            Iterator<GraphNode> it = nodes.values().iterator();
            while (it.hasNext()) {
                GraphNode test = it.next();
                if (test.getName().substring(0, 3).equals(name.toLowerCase())) {
                    result = test;
                    break;
                }
            }
        }

        return result;

    }

    public GraphNode getNode(Region region) {
        GraphNode result = getNode(region.getName());

        if (result == null) {
            //plusieurs test
            //Iterator it = region.getProvince().getRegions()
        }

        return result;
    }

    public GraphNode getNode(Unit u) {
        return getNode(u.getRegion());
    }

    public GraphNode getFirstNode() {
        Iterator<GraphNode> it = getNodes().iterator();
        if (it.hasNext()) {
            return it.next();
        }
        return null;
    }

    /**
     * Retourne dans un Iterator l'ensemble des sommets.
     */
    public Collection<GraphNode> getNodes() {
        return nodes.values();
    }

    public void checkRegions() throws Exception {
        Iterator<GraphNode> it = this.getNodes().iterator();
        while (it.hasNext()) {
            Region region = it.next().getRegion();
            if (region.getProvince() == null) {
                throw new Exception("Region sans province : "
                        + region.getName());
            }
        }
    }

    /**
     * Indique si deux sommets se touchent.
     */
    public boolean areAdjacent(String node1, String node2) {
        GraphNode n1, n2;
        n1 = getNode(node1);
        n2 = getNode(node2);
        return areAdjacent(n1, n2);
    }

    public boolean areAdjacent(Region r1, Region r2) {
        return areAdjacent(r1.getName(), r2.getName());
    }

    public boolean areAdjacent(GraphNode n1, GraphNode n2) {
        if (n1 != null) {
            return n1.isNear(n2);
        }
        return false;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge.graph;

/**
 *
 * Pile utilisé par GraphExplorer pour le parcours d'un graphe.
 *
 * @see diplomacy.graph.GraphExplorer
 */

public class GraphStack {
	private int nb;

	private GraphNode[] nodes;

	public GraphStack() {
		this.nb = 0;
		nodes = new GraphNode[128];
	}

	public void push(GraphNode node) {
		nodes[nb++] = node;
	}

	public GraphNode pop() {
		nb--;
		return nodes[nb];

	}

	public GraphNode peek() {
		return nodes[nb - 1];
	}

	public boolean isEmpty() {
		return (this.nb == 0);
	}

}
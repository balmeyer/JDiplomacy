/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jdiplomacy.judge.game.Board;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.UnitType;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.game.board.Province;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.game.board.Supply;
import net.jdiplomacy.judge.graph.Graph;
import net.jdiplomacy.judge.order.Order;
import net.jdiplomacy.judge.steps.AdjudicationIndex;
import net.jdiplomacy.judge.steps.AdjudicationStep;
import net.jdiplomacy.judge.steps.StepListFactory;


/**
 * JDiplomacy Judge
 * @author Jean-Baptiste
 */
public class JudgeEngine {

	public static final String VERSION = "0.9.1";
	public static final int MAX_SUPPLY_GENERIC = 18;
	
	public static JudgeOptions options = JudgeOptions.failingSupportOrConvoyHold;
	
	
	/**
	 * Init game data (as province, supply, etc.)
	 */
	static {
		initGame();
	}

	/**
	 * Judge a diplomacy turn
	 * @param positions
	 * @param orders
	 * @param phase
	 * @return
	 */
	public JudgeResult judge(String positions, String orders, Phase phase) {

		List<String> listPositions = new ArrayList<String>();
		List<String> listOrders = new ArrayList<String>();

		for (String pos : positions.split(";")) {
			listPositions.add(pos);
		}

		for (String ord : orders.split(";")) {
			listOrders.add(ord);
		}

		return judge(listPositions.toArray(), listOrders.toArray(), phase);
	}

	/**
	 * 
	 * @param positions
	 * @param orders
	 * @param phase
	 * @return
	 */
	public JudgeResult judge(Object[] positions, Object[] orders, Phase phase) {

		// build the initial board

		Board b = new Board(positions) ;


		List<Order> ordersList = new ArrayList<Order>();

		// decompose orders
		for (Object o : orders) {
			Order order = Order.create(b, o.toString() 
					, phase == Phase.disbandFall || phase == Phase.disbandSpring);
			ordersList.add(order);
		}

		// judge orders
		return judge(b, ordersList, phase);

	}

	/**
		Static init of the game.
	 * 
	 */
	public static void initGame() {

		//map init

		// nouveau graphe de carte
		Graph graphForArmy = new Graph();
		Graph graphForShip = new Graph();

		//clear 
		Country.clear();
		Region.clear();
		Province.clear();
		Supply.clear();
		
		// Pays
		Country austria = Country.build("Austria");
		Country england = Country.build("England");
		Country france = Country.build("France");
		Country germany = Country.build("Germany");
		Country italy = Country.build("Italy");
		Country russia = Country.build("Russia");
		Country turkey = Country.build("Turquey");

		// ----------------
		// AUSTRIA
		// 1. Provinces
		Province.build("Boh", "Bohemia");
		Province.build("Bud", "Budapest");
		Province.build("Gal", "Galicia");
		Province.build("Tri", "Trieste");
		Province.build("Tyr", "Tyrolia");
		Province.build("Vie", "Vienna");
		// 2.Region
		Region.buildEarth(Province.get("Boh"));
		Region.buildEarth(Province.get("Bud"));
		Region.buildEarth(Province.get("Gal"));
		Region.buildCoast(Province.get("Tri"));
		Region.buildEarth(Province.get("Tyr"));
		Region.buildEarth(Province.get("Vie"));
		Region.get("Boh").setPosition(516, 576);
		Region.get("Bud").setPosition(646, 654);
		Region.get("Gal").setPosition(640, 582);
		Region.get("Tri").setPosition(528, 710);
		Region.get("Tyr").setPosition(464, 642);
		Region.get("Vie").setPosition(524, 630);
		// 3. Arrows (flèches)
		// 3.1 = for army
		graphForArmy.addNode("Boh", "Sil Gal Vie");
		graphForArmy.addNode("Bud", "Tri Vie Gal Rum Ser");
		graphForArmy.addNode("Gal", "Boh Sil War Ukr Rum Bud Vie");
		graphForArmy.addNode("Tri", "Vie Bud Ser Alb Tyr");
		graphForArmy.addNode("Tyr", "Pie Ven Mun Boh Vie Tri");
		graphForArmy.addNode("Vie", "Boh Gal Bud Tri Tyr");
		// 3.2 = for ship
		graphForShip.addNode("Tri", "Alb Adr Ven");
		// 4 supply
		new Supply(Province.get("Bud"), austria);
		new Supply(Province.get("Tri"), austria);
		new Supply(Province.get("Vie"), austria);

		/*
		 * 08/03/2007 ! p[0] = new Province(0, "Boh", "Bohemia", "Mun Sil Gal
		 * Vie Tyr", 516, 576); p[1] = new Province(0, "Bud", "Budapest", "Vie
		 * Gal Rum Ser Tri", 646, 654); p[2] = new Province(0, "Gal", "Galicia",
		 * "War Ukr Rum Bud Vie Boh Sil", 640, 582); p[3] = new Province(1,
		 * "Tri", "Trieste", "Tyr Vie Bud Ser Alb Adr Ven", 528, 710); p[4] =
		 * new Province(0, "Tyr", "Tyrolia", "Mun Boh Vie Tri Ven Pie", 464,
		 * 642); p[5] = new Province(0, "Vie", "Vienna", "Boh Gal Bud Tri Tyr",
		 * 524, 630); p[1].setSupply(0, 594, 648); p[3].setSupply(0, 486, 684);
		 * p[5].setSupply(0, 550, 612);
		 */

		// England
		Province.build("Cly", "Clyde");
		Province.build("Edi", "Edinburgh");
		Province.build("Lvp", "Liverpool");
		Province.build("Lon", "London");
		Province.build("Wal", "Wakes");
		Province.build("Yor", "Yorkshire");
		Region.buildCoast(Province.get("Cly"));
		Region.buildCoast(Province.get("Edi"));
		Region.buildCoast(Province.get("Lvp"));
		Region.buildCoast(Province.get("Lon"));
		Region.buildCoast(Province.get("Wal"));
		Region.buildCoast(Province.get("Yor"));
		Region.get("Cly").setPosition(252, 324);
		Region.get("Edi").setPosition(288, 330);
		Region.get("Lvp").setPosition(252, 384);
		Region.get("Lon").setPosition(280, 470);
		Region.get("Wal").setPosition(232, 452);
		Region.get("Yor").setPosition(288, 412);
		new Supply(Province.get("Edi"), england);
		new Supply(Province.get("Lvp"), england);
		new Supply(Province.get("Lon"), england);
		graphForArmy.addNode("Cly", "Edi Lvp");
		graphForArmy.addNode("Edi", "Cly Yor");
		graphForArmy.addNode("Lvp", "Cly Edi Yor Wal");
		graphForArmy.addNode("Lon", "Yor Wal");
		graphForArmy.addNode("Wal", "Lon Yor Lvp");
		graphForArmy.addNode("Yor", "Edi Lvp Wal Lon");
		graphForShip.addNode("Cly", "NAt Nrg Edi Lvp");
		graphForShip.addNode("Edi", "Cly Nrg Nth Yor");
		graphForShip.addNode("Yor", "Edi Nth Lon");
		graphForShip.addNode("Lon", "Yor Nth Eng Wal");
		graphForShip.addNode("Wal", "Lon Eng Iri Lvp");
		graphForShip.addNode("Lvp", "Wal Iri NAt Cly");
		/*
		 * p[6] = new Province(1, "Cly", "Clyde", "NAt Nrg Edi Lvp", 252, 324);
		 * p[7] = new Province(1, "Edi", "Edinburgh", "Nrg Nth Yor Lvp Cly",
		 * 288, 330); p[8] = new Province(1, "Lvp", "Liverpool", "Cly Edi Yor
		 * Wal Iri NAt", 252, 384); p[9] = new Province(1, "Lon", "London", "Yor
		 * Nth Eng Wal", 280, 470); p[10] = new Province(1, "Wal", "Wales", "Lvp
		 * Yor Lon Eng Iri", 232, 452); p[11] = new Province(1, "Yor",
		 * "Yorkshire", "Edi Nth Lon Wal Lvp", 288, 412); p[7].setSupply(1, 280,
		 * 358); p[8].setSupply(1, 248, 414); p[9].setSupply(1, 294, 486);
		 */

		// FRANCE
		Province.build("Bre", "Brest");
		Province.build("Bur", "Burgundy");
		Province.build("Gas", "Gascony");
		Province.build("Mar", "Marseilles");
		Province.build("Par", "Paris");
		Province.build("Pic", "Picardy");
		Region.buildCoast(Province.get("Bre")).setPosition(232, 574);
		Region.buildEarth(Province.get("Bur")).setPosition(332, 610);
		Region.buildCoast(Province.get("Gas")).setPosition(240, 670);
		Region.buildCoast(Province.get("Mar")).setPosition(322, 686);
		Region.buildEarth(Province.get("Par")).setPosition(278, 590);
		Region.buildCoast(Province.get("Pic")).setPosition(310, 530);
		new Supply(Province.get("Bre"), france);
		new Supply(Province.get("Mar"), france);
		new Supply(Province.get("Par"), france);
		graphForArmy.addNode("Bre", "Pic Par Gas");
		graphForArmy.addNode("Bur", "Par Gas Mar Mun Ruh Bel Pic");
		graphForArmy.addNode("Gas", "Spa Bre Par Bur Mar");
		graphForArmy.addNode("Mar", "Spa Gas Bur Pie");
		graphForArmy.addNode("Par", "Bre Gas Bur Pic");
		graphForArmy.addNode("Pic", "Bre Par Bur Bel");
		graphForShip.addNode("Bre", "Eng Mid Gas Pic");
		graphForShip.addNode("Pic", "Eng Bre Bel");
		graphForShip.addNode("Gas", "Bre Mid Spa-N");
		graphForShip.addNode("Mar", "Spa-S Pie GoL");

		/*
		 * p[12] = new Province(1, "Bre", "Brest", "Eng Pic Par Gas Mid", 232,
		 * 574); p[13] = new Province(0, "Bur", "Burgundy", "Par Pic Bel Ruh Mun
		 * Mar Gas", 332, 610); p[14] = new Province(1, "Gas", "Gascony", "Bre
		 * Par Bur Mar Spa Mid", 240, 670); p[15] = new Province(1, "Mar",
		 * "Marseilles", "Bur Pie GoL Spa Gas", 322, 686); p[16] = new
		 * Province(0, "Par", "Paris", "Pic Bur Gas Bre", 278, 590); p[17] = new
		 * Province(1, "Pic", "Picardy", "Eng Bel Bur Par Bre", 310, 530);
		 * 
		 * p[12].setSupply(2, 194, 540); p[15].setSupply(2, 318, 716);
		 * p[16].setSupply(2, 286, 564);
		 */

		// GERMANY
		Province.build("Ber", "Berlin");
		Province.build("Kie", "Kiel");
		Province.build("Mun", "Munich");
		Province.build("Pru", "Prussia");
		Province.build("Ruh", "Ruhr");
		Province.build("Sil", "Silesia");
		Region.buildCoast(Province.get("Ber")).setPosition(486, 474);
		Region.buildCoast(Province.get("Kie")).setPosition(422, 486);
		Region.buildEarth(Province.get("Mun")).setPosition(440, 570);
		Region.buildCoast(Province.get("Pru")).setPosition(558, 476);
		Region.buildEarth(Province.get("Ruh")).setPosition(392, 550);
		Region.buildEarth(Province.get("Sil")).setPosition(532, 532);
		new Supply(Province.get("Ber"), germany);
		new Supply(Province.get("Mun"), germany);
		new Supply(Province.get("Kie"), germany);
		graphForArmy.addNode("Ber", "Kie Pru Sil Mun");
		graphForArmy.addNode("Kie", "Ber Hol Den Ruh Mun");
		graphForArmy.addNode("Mun", "Ruh Kie Ber Sil Tyr Bur Boh");
		graphForArmy.addNode("Pru", "Ber Sil War Lvn");
		graphForArmy.addNode("Ruh", "Hol Kie Mun Bur Bel");
		graphForArmy.addNode("Sil", "Mun Ber Pru War Gal Boh");
		graphForShip.addNode("Ber", "Bal Pru Kie");
		graphForShip.addNode("Kie", "Hol Hel Den Bal Ber");
		graphForShip.addNode("Pru", "Ber Bal Lvn");

		/*
		 * p[18] = new Province(1, "Ber", "Berlin", "Bal Pru Sil Mun Kie", 486,
		 * 474); p[19] = new Province(1, "Kie", "Kiel", "Hel Den Bal Ber Mun Ruh
		 * Hol", 422, 486); p[20] = new Province(0, "Mun", "Munich", "Kie Ber
		 * Sil Boh Tyr Bur Ruh", 440, 570); p[21] = new Province(1, "Pru",
		 * "Prussia", "Bal Lvn War Sil Ber", 558, 476); p[22] = new Province(0,
		 * "Ruh", "Ruhr", "Hol Kie Mun Bur Bel", 392, 550); p[23] = new
		 * Province(0, "Sil", "Silesia", "Ber Pru War Gal Boh Mun", 532, 532);
		 * p[18].setSupply(3, 480, 502); p[20].setSupply(3, 446, 610);
		 * p[19].setSupply(3, 450, 460);
		 */

		// ITALY
		Province.build("Apu", "Apulia");
		Province.build("Nap", "Naples");
		Province.build("Pie", "Piedmont");
		Province.build("Rom", "Rome");
		Province.build("Tus", "Tuscany");
		Province.build("Ven", "Venice");
		Region.buildCoast(Province.get("Apu")).setPosition(502, 790);
		Region.buildCoast(Province.get("Nap")).setPosition(512, 844);
		Region.buildCoast(Province.get("Pie")).setPosition(376, 684);
		Region.buildCoast(Province.get("Rom")).setPosition(456, 780);
		Region.buildCoast(Province.get("Tus")).setPosition(420, 730);
		Region.buildCoast(Province.get("Ven")).setPosition(424, 686);
		new Supply(Province.get("Nap"), italy);
		new Supply(Province.get("Rom"), italy);
		new Supply(Province.get("Ven"), italy);
		graphForArmy.addNode("Apu", "Ven Rom Nap");
		graphForArmy.addNode("Nap", "Rom Apu");
		graphForArmy.addNode("Pie", "Mar Tus Ven Tyr");
		graphForArmy.addNode("Rom", "Tus Ven Apu Nap");
		graphForArmy.addNode("Tus", "Pie Ven Rom");
		graphForArmy.addNode("Ven", "Pie Tus Rom Apu Tyr Tri");
		graphForShip.addNode("Apu", "Nap Ven Adr Ion");
		graphForShip.addNode("Nap", "Apu Ion Tyn Rom");
		graphForShip.addNode("Pie", "Mar GoL Tus");
		graphForShip.addNode("Rom", "Tus Tyn Nap ");
		graphForShip.addNode("Tus", "Rom Tyn GoL Pie");
		graphForShip.addNode("Ven", "Apu Tri Adr");
		/*
		 * p[24] = new Province(1, "Apu", "Apulia", "Ven Adr Ion Nap Rom", 502,
		 * 790); p[25] = new Province(1, "Nap", "Naples", "Apu Ion Tyn Rom",
		 * 512, 844); p[26] = new Province(1, "Pie", "Piedmont", "Tyr Ven Tus
		 * GoL Mar", 376, 684); p[27] = new Province(1, "Rom", "Rome", "Tus Ven
		 * Apu Nap Tyn", 456, 780); p[28] = new Province(1, "Tus", "Tuscany",
		 * "Pie Ven Rom Tyn GoL", 420, 730); p[29] = new Province(1, "Ven",
		 * "Venice", "Tyr Tri Adr Apu Rom Tus Pie", 424, 686);
		 * p[25].setSupply(4, 476, 806); p[27].setSupply(4, 440, 778);
		 * p[29].setSupply(4, 448, 694);
		 */

		// RUSSIA
		Province.build("Lvn", "Livonia");
		Province.build("Mos", "Moscow");
		Province.build("Sev", "Sevastopol");
		Province.build("StP", "St. Petersbourg");
		Province.build("Ukr", "Ukraine");
		Province.build("War", "Warsaw");
		Region.buildCoast(Province.get("Lvn")).setPosition(664, 388);
		Region.buildEarth(Province.get("Mos")).setPosition(782, 440);
		Region.buildCoast(Province.get("Sev")).setPosition(826, 620);
		Region.buildCoast(Province.get("StP")).setPosition(798, 266);
		Region.buildCoast(Province.get("StP"), "StP-N").setPosition(782, 212);
		Region.buildCoast(Province.get("StP"), "StP-S").setPosition(726, 342);
		Region.buildEarth(Province.get("Ukr")).setPosition(730, 590);
		Region.buildEarth(Province.get("War")).setPosition(592, 540);
		new Supply(Province.get("Mos"), russia);
		new Supply(Province.get("Sev"), russia);
		new Supply(Province.get("StP"), russia);
		new Supply(Province.get("War"), russia);
		graphForArmy.addNode("Lvn", "Pru StP Mos War");
		graphForShip.addNode("Lvn", "Pru Bal Bot Stp-S");
		graphForArmy.addNode("Mos", "Lvn StP War Ukr Sev");
		graphForArmy.addNode("Sev", "Ukr Mos Rum Arm");
		graphForShip.addNode("Sev", "Rum Bla Arm");
		graphForArmy.addNode("StP", "Mos Lvn Fin Nwy");
		graphForShip.addNode("Stp-N", "Bar Nwy");
		graphForShip.addNode("Stp-S", "Fin Bot Lvn");
		graphForArmy.addNode("Ukr", "War Gal Rum Sev Mos");
		graphForArmy.addNode("War", "Sil Gal Ukr Mos Lvn Pru");
		/*
		 * p[30] = new Province(1, "Lvn", "Livonia", "Bot StP Mos War Pru Bal",
		 * 664, 388); p[31] = new Province(0, "Mos", "Moscow", "StP Sev Ukr War
		 * Lvn", 782, 440); p[32] = new Province(1, "Sev", "Sevastopol", "Ukr
		 * Mos Arm Bla Rum", 826, 620); // //p[33] = new Province(1 , "StP" ,
		 * "St. Petersbourg" , "Bar Mos Lvn Bot // Fin Nwy", 786, 238) ; p[33] =
		 * new Province(1, "StP", "St. Petersbourg", "Fin Mos Lvn Bot", "Bar
		 * Nwy", 798, 266, 726, 342, 782, 212);
		 * 
		 * p[34] = new Province(0, "Ukr", "Ukraine", "Mos Sev Rum Gal War", 730,
		 * 590); p[35] = new Province(0, "War", "Warsaw", "Pru Lvn Mos Ukr Gal
		 * Sil", 592, 540); p[31].setSupply(5, 870, 400); p[32].setSupply(5,
		 * 830, 684); p[33].setSupply(5, 732, 318); p[35].setSupply(5, 608,
		 * 514);
		 */

		// TURKEY
		Province.build("Ank", "Ankara");
		Province.build("Arm", "Armenia");
		Province.build("Con", "Constantinople");
		Province.build("Smy", "Smyrna");
		Province.build("Syr", "Syria");
		Region.buildCoast(Province.get("Ank")).setPosition(860, 798);
		Region.buildCoast(Province.get("Arm")).setPosition(966, 834);
		Region.buildCoast(Province.get("Con")).setPosition(758, 824);
		Region.buildCoast(Province.get("Smy")).setPosition(782, 892);
		Region.buildCoast(Province.get("Syr")).setPosition(942, 916);
		new Supply(Province.get("Ank"), turkey);
		new Supply(Province.get("Con"), turkey);
		new Supply(Province.get("Smy"), turkey);
		graphForArmy.addNode("Ank", "Con Arm Smy");
		graphForShip.addNode("Ank", "Con Bla Arm");
		graphForArmy.addNode("Arm", "Sev Ank Smy Syr");
		graphForShip.addNode("Arm", "Sev Bla Ank");
		graphForArmy.addNode("Con", "Bul Smy Ank");
		graphForShip.addNode("Con", "Bul-E Bul-S Bla Ank Aeg Smy");
		graphForArmy.addNode("Smy", "Con Ank Arm Syr");
		graphForShip.addNode("Smy", "Con Syr Aeg Eas");
		graphForArmy.addNode("Syr", "Smy Arm");
		graphForShip.addNode("Syr", "Smy Eas");

		// TURKEY
		/*
		 * p[36] = new Province(1, "Ank", "Ankara", "Bla Arm Smy Con", 860,
		 * 798); p[37] = new Province(1, "Arm", "Armenia", "Bla Sev Syr Smy
		 * Ank", 966, 834); p[38] = new Province(1, "Con", "Constantinople",
		 * "Bla Ank Smy Aeg Bul", 758, 824); p[39] = new Province(1, "Smy",
		 * "Smyrna", "Con Ank Arm Syr Eas Aeg", 782, 892); p[40] = new
		 * Province(1, "Syr", "Syria", "Arm Eas Smy", 942, 916);
		 * p[36].setSupply(6, 832, 832); p[38].setSupply(6, 730, 802);
		 * p[39].setSupply(6, 738, 898);
		 */

		// NEUTRALS
		Province.build("Alb", "Albania");
		Region.buildCoast(Province.get("Alb")).setPosition(588, 810);
		graphForArmy.addNode("Alb", "Tri Ser Gre");
		graphForShip.addNode("Alb", "Tri Adr Ion Gre");
		Province.build("Bel", "Belgium");
		Region.buildCoast(Province.get("Bel")).setPosition(330, 506);
		graphForArmy.addNode("Bel", "Pic Hol Bur Ruh");
		graphForShip.addNode("Bel", "Pic Eng Nth Hol");
		/*
		 * p[41] = new Province(1, "Alb", "Albania", "Tri Ser Gre Ion Adr", 588,
		 * 810); p[42] = new Province(1, "Bel", "Belgium", "Nth Hol Ruh Bur Pic
		 * Eng", 330, 506);
		 */
		//
		// p[43] = new Province(1 , "Bul" , "Bulgaria" , "Rum Bla Con Aeg Gre
		// Ser",680, 770) ;
		Province.build("Bul", "Bulgaria");
		Province.build("Den", "Denmark");
		Province.build("Fin", "Finland");
		Province.build("Gre", "Greece");
		Province.build("Hol", "Holland");
		Region.buildCoast(Province.get("Bul"), "Bul-E").setPosition(720, 760);
		Region.buildCoast(Province.get("Bul"), "Bul-S").setPosition(694, 806);
		Region.buildCoast(Province.get("Bul")).setPosition(680, 770);
		Region.buildCoast(Province.get("Den")).setPosition(444, 394);
		Region.buildCoast(Province.get("Fin")).setPosition(682, 218);
		Region.buildCoast(Province.get("Gre")).setPosition(626, 848);
		Region.buildCoast(Province.get("Hol")).setPosition(380, 474);
		graphForArmy.addNode("Bul", "Gre Ser Rum Con");
		graphForShip.addNode("Bul-S", "Gre Aeg Con");
		graphForShip.addNode("Bul-E", "Con Rum Bla");
		graphForArmy.addNode("Den", "Kie Swe");
		graphForShip.addNode("Den", "Ska Nth Hel Kie Swe Bal");
		graphForArmy.addNode("Fin", "Nwy Swe StP");
		graphForShip.addNode("Fin", "Swe Bot Stp-S");
		graphForArmy.addNode("Gre", "Alb Ser Bul");
		graphForShip.addNode("Gre", "Alb Bul-S Ion Aeg");
		graphForArmy.addNode("Hol", "Kie Bel Ruh");
		graphForShip.addNode("Hol", "Bel Nth Hel Kie");
		/*
		 * p[43] = new Province(1, "Bul", "Bulgaria", "Con Aeg Gre Ser", "Rum
		 * Bla Con", 680, 770, 694, 806, 720, 760);
		 * 
		 * p[44] = new Province(1, "Den", "Denmark", "Ska Swe Bal Kie Hel Nth",
		 * 444, 394); p[45] = new Province(1, "Fin", "Finland", "Nwy StP Bot
		 * Swe", 682, 218); p[46] = new Province(1, "Gre", "Greece", "Ser Bul
		 * Aeg Ion Alb", 626, 848); p[47] = new Province(1, "Hol", "Holland",
		 * "Nth Hel Kie Ruh Bel", 380, 474);
		 */
		Province.build("Nwy", "Norway");
		Province.build("NAf", "North Africa");
		Province.build("Por", "Portugal");
		Province.build("Rum", "Rumania");
		Province.build("Ser", "Serbia");
		Region.buildCoast(Province.get("Nwy")).setPosition(482, 246);
		Region.buildCoast(Province.get("NAf")).setPosition(144, 932);
		Region.buildCoast(Province.get("Por")).setPosition(64, 714);
		Region.buildCoast(Province.get("Rum")).setPosition(710, 700);
		Region.buildEarth(Province.get("Ser")).setPosition(614, 768);
		graphForArmy.addNode("Nwy", "Swe Fin");
		graphForShip.addNode("Nwy", "Swe Ska Nth Nrg Bar Stp-N");
		graphForArmy.addNode("NAf", "Tun");
		graphForShip.addNode("NAf", "Tun Wes Mid");
		graphForArmy.addNode("Por", "Spa");
		graphForShip.addNode("Por", "Spa-N Spa-S Mid");
		graphForArmy.addNode("Rum", "Bul Ser Bud Gal Ukr Sev");
		graphForShip.addNode("Rum", "Bul-E Sev Bla");
		graphForArmy.addNode("Ser", "Tri Alb Gre Bud Rum Bul");
		/*
		 * p[48] = new Province(1, "Nwy", "Norway", "Nrg Bar StP Fin Swe Ska
		 * Nth", 482, 246); p[49] = new Province(1, "NAf", "North Africa", "Wes
		 * Tun Mid", 144, 932); p[50] = new Province(1, "Por", "Portugal", "Mid
		 * Spa", 64, 714); p[51] = new Province(1, "Rum", "Rumania", "Bud Gal
		 * Ukr Sev Bla Bul Ser", 710, 700); p[52] = new Province(0, "Ser",
		 * "Serbia", "Bud Rum Bul Gre Alb Tri", 614, 768);
		 */
		// p[53] = new Province(1 , "Spa" , "Spain" , "Gas Mar GoL Wes Mid Por",
		// 130, 780) ;
		// ESPAGNE PROVINCE SPE
		Province.build("Spa", "Spain");
		Province.build("Swe", "Sweden");
		Province.build("Tun", "Tunis");
		Region.buildCoast(Province.get("Spa")).setPosition(130, 780);
		Region.buildCoast(Province.get("Spa"), "Spa-N").setPosition(140, 690);
		Region.buildCoast(Province.get("Spa"), "Spa-S").setPosition(146, 828);
		Region.buildCoast(Province.get("Swe")).setPosition(536, 266);
		Region.buildCoast(Province.get("Tun")).setPosition(362, 942);
		graphForArmy.addNode("Spa", "Por Gas Mar");
		graphForShip.addNode("Spa-N", "Por Mid Gas");
		graphForShip.addNode("Spa-S", "Por Mid Wes GoL Mar");
		graphForArmy.addNode("Swe", "Nwy Fin Den");
		graphForShip.addNode("Swe", "Ska Nwy Den Bal Bot Fin");
		graphForArmy.addNode("Tun", "NAf");
		graphForShip.addNode("Tun", "NAf Wes Tyn Ion");
		/*
		 * 
		 * p[53] = new Province(1, "Spa", "Spain", "Mar GoL Wes Mid Por", "Mid
		 * Gas Por", 130, 780, 146, 828, 140, 690);
		 * 
		 * p[54] = new Province(1, "Swe", "Sweden", "Nwy Fin Bot Bal Den Ska",
		 * 536, 266); p[55] = new Province(1, "Tun", "Tunis", "Wes Tyn Ion NAf",
		 * 362, 942);
		 */
		new Supply(Province.get("Bel"), 354, 518);
		new Supply(Province.get("Bul"), 650, 768);
		new Supply(Province.get("Den"), 474, 404);
		new Supply(Province.get("Gre"), 652, 888);
		new Supply(Province.get("Hol"), 360, 480);
		new Supply(Province.get("Nwy"), 490, 298);
		new Supply(Province.get("Por"), 40, 748);
		new Supply(Province.get("Rum"), 676, 720);
		new Supply(Province.get("Ser"), 602, 726);
		new Supply(Province.get("Spa"), 136, 730);
		new Supply(Province.get("Swe"), 548, 304);
		new Supply(Province.get("Tun"), 378, 914);
		/*
		 * p[42].setSupply(-1, 354, 518); p[43].setSupply(-1, 650, 768);
		 * p[44].setSupply(-1, 474, 404); p[46].setSupply(-1, 652, 888);
		 * p[47].setSupply(-1, 360, 480); p[48].setSupply(-1, 490, 298);
		 * p[50].setSupply(-1, 40, 748); p[51].setSupply(-1, 676, 720);
		 * p[52].setSupply(-1, 602, 726); p[53].setSupply(-1, 136, 730);
		 * p[54].setSupply(-1, 548, 304); p[55].setSupply(-1, 378, 914);
		 */

		// SEAS
		Province.build("Adr", "Adriactic Sea");
		Province.build("Aeg", "Aegean Sea");
		Province.build("Bal", "Baltic Sea");
		Province.build("Bar", "Barents Sea");
		Region.buildSea(Province.get("Adr")).setPosition(500, 756);
		Region.buildSea(Province.get("Aeg")).setPosition(688, 894);
		Region.buildSea(Province.get("Bal")).setPosition(560, 430);
		Region.buildSea(Province.get("Bar")).setPosition(736, 44);
		graphForShip.addNode("Adr", "Ven Apu Tri Alb Ion");
		graphForShip.addNode("Aeg", "Ion Gre Eas Smy Con Bul-S");
		graphForShip.addNode("Bal", "Pru Lvn Ber Den Swe Bot Kie");
		graphForShip.addNode("Bar", "Nrg Nwy StP-N");
		/*
		 * p[56] = new Province(2, "Adr", "Adriatic Sea", "Tri Alb Ion Apu Ven",
		 * 500, 756); p[57] = new Province(2, "Aeg", "Aegean Sea", "Gre Bul Con
		 * Smy Eas Ion", 688, 894); p[58] = new Province(2, "Bal", "Baltic Sea",
		 * "Bot Lvn Pru Ber Kie Den Swe", 560, 430); p[59] = new Province(2,
		 * "Bar", "Barents Sea", "StP Nwy Nrg", 736, 44);
		 */
		Province.build("Bla", "Black Sea");
		Province.build("Eas", "Eastern Mediterranean");
		Province.build("Eng", "English Channel");
		Province.build("Bot", "Gulf of Bothnia");
		Region.buildSea(Province.get("Bla")).setPosition(792, 732);
		Region.buildSea(Province.get("Eas")).setPosition(770, 958);
		Region.buildSea(Province.get("Eng")).setPosition(256, 514);
		Region.buildSea(Province.get("Bot")).setPosition(604, 330);
		graphForShip.addNode("Bla", "Sev Arm Ank Con Bul-E Rum");
		graphForShip.addNode("Eas", "Aeg Smy Ion Syr");
		graphForShip.addNode("Eng", "Mid Iri Bre Pic Wal Lon Nth Bel");
		graphForShip.addNode("Bot", "Swe Bal Fin Lvn Stp-S");
		/*
		 * p[60] = new Province(2, "Bla", "Black Sea", "Sev Arm Ank Con Bul
		 * Rum", 792, 732); p[61] = new Province(2, "Eas", "Eastern
		 * Mediterranean", "Smy Syr Ion Aeg", 770, 958); p[62] = new Province(2,
		 * "Eng", "English Channel", "Wal Lon Nth Bel Pic Bre Mid Iri", 256,
		 * 514); p[63] = new Province(2, "Bot", "Gulf of Bothnia", "Swe Fin StP
		 * Lvn Bal", 604, 330);
		 */
		Province.build("GoL", "Gulf of Lyon");
		Province.build("Hel", "Helgoland Bight");
		Province.build("Ion", "Ionian Sea");
		Province.build("Iri", "Irish Sea");
		// Province.build("")
		Region.buildSea(Province.get("GoL")).setPosition(318, 762);
		Region.buildSea(Province.get("Hel")).setPosition(408, 438);
		Region.buildSea(Province.get("Ion")).setPosition(536, 902);
		Region.buildSea(Province.get("Iri")).setPosition(168, 456);
		graphForShip.addNode("GoL", "Wes Spa-S Mar Pie Tus Tyn");
		graphForShip.addNode("Hel", "Nth Hol Kie Den");
		graphForShip.addNode("Ion", "Tun Tyn Nap Apu Adr Alb Gre Aeg Eas");
		graphForShip.addNode("Iri", "Nat Mid Eng Wal Lvp");
		/*
		 * p[64] = new Province(2, "GoL", "Gulf of Lyon", "Mar Pie Tus Tyn Wes
		 * Spa", 318, 762); p[65] = new Province(2, "Hel", "Helgoland Bight",
		 * "Nth Den Kie Hol", 408, 438); p[66] = new Province(2, "Ion", "Ionian
		 * Sea", "Tyn Nap Apu Adr Alb Gre Aeg Eas Tun", 536, 902); p[67] = new
		 * Province(2, "Iri", "Irish Sea", "NAt Lvp Wal Eng Mid", 168, 456);
		 */
		Province.build("Mid", "Mid-Atlantic Ocean");
		Province.build("NAt", "North-Atlantic Ocean");
		Province.build("Nth", "North Sea");
		Province.build("Nrg", "Norwegian Sea");
		Province.build("Ska", "Skagerrak");
		Province.build("Tyn", "Tyrrhenian Sea");
		Province.build("Wes", "Western Mediterranean");
		Region.buildSea(Province.get("Mid")).setPosition(58, 576);
		Region.buildSea(Province.get("NAt")).setPosition(134, 300);
		Region.buildSea(Province.get("Nth")).setPosition(360, 344);
		Region.buildSea(Province.get("Nrg")).setPosition(388, 96);
		Region.buildSea(Province.get("Ska")).setPosition(462, 350);
		Region.buildSea(Province.get("Tyn")).setPosition(424, 818);
		Region.buildSea(Province.get("Wes")).setPosition(204, 848);
		graphForShip.addNode("Mid", "Por Spa-N Spa-S Gas Eng Iri NAt NAf Wes");
		graphForShip.addNode("NAt", "Iri Mid Nrg Cly Lvp");
		graphForShip.addNode("Nth", "Edi Yor Lon Nrg Eng Nwy Hol Bel Hel");
		graphForShip.addNode("Nrg", "Nat Cly Edi Nwy Bar");
		graphForShip.addNode("Ska", "Nth Nwy Swe Den");
		graphForShip.addNode("Wes", "NAf Tun Tyn GoL Spa-S Mid");
		graphForShip.addNode("Tyn", "GoL Wes Tun Ion Nap Rom Tus");

		// complétion des graphes

		Region.completeGraph(graphForArmy);
		Region.completeGraph(graphForShip);
		Unit.setGraph(graphForArmy, UnitType.ARMY);
		Unit.setGraph(graphForShip, UnitType.FLEET);

	}

	/**
	 * 
	 * @param board
	 * @param orders
	 * @return
	 */
	public JudgeResult judge(Board board, List<Order> orders, Phase phase) {
		//stop if game over
		if (phase == Phase.gameOver){
			throw new IllegalArgumentException("cannot juge finished game");
		}
		
		// supporting data structure
		SupportingDataStructure structure = new SupportingDataStructure(board);

		structure.setPhase(phase);
		structure.setOrders(orders);

		List<AdjudicationStep> steps = StepListFactory.createList(phase);

		// save name to id
		int id = 0;
		int minIndex = 999999999;
		int maxIndex = -1;

		Map<String, Integer> stepToIndex = new HashMap<String, Integer>();
		Map<Integer, AdjudicationStep> indexToStep = new HashMap<Integer, AdjudicationStep>();

		for (AdjudicationStep step : steps) {
			// index
			id = step.getClass().getAnnotation(AdjudicationIndex.class).index();
			// check
			if (stepToIndex.containsValue(id)) {
				throw new IllegalStateException(
						"cannot contains same adjudication step index : "
								+ step.getClass().getSimpleName());
			}

			stepToIndex.put(step.getClass().getSimpleName(), id);
			indexToStep.put(id, step);

			if (id < minIndex)
				minIndex = id;
			if (id > maxIndex)
				maxIndex = id;
		}

		// launching all steps
		int i = minIndex;
		int nbSteps = 0;

		assert minIndex <= maxIndex;
		//System.out.println("--- start adjucation");
		while (i <= maxIndex) {

			nbSteps++;

			if (nbSteps > 200) {
				throw new IllegalStateException(
						"infinite loop in JudgeEngine !");
			}

			AdjudicationStep step = indexToStep.get(i);

			/*System.out.println(nbSteps + " > [" + i + "] "
					+ step.getClass().getSimpleName().toString()); */

			// set current step instance
			structure.setCurrentStepInstance(step);
			
			//launch step
			String stepToRepeat = step.adjucate(structure);

			if (stepToRepeat != null) {
				if (!stepToIndex.containsKey(stepToRepeat)) {
					throw new IllegalArgumentException("step name unknown :"
							+ stepToRepeat);
				}

				int newIndex = stepToIndex.get(stepToRepeat);

				if (newIndex > i) {
					throw new IllegalStateException("cannot jump from step "
							+ i + " to step " + newIndex);
				}
				i = newIndex;
			} else {
				i++;
			}

		}

		// rebuild positions
		structure.getBoard().refreshPositions();

		// set next phase
		switch (phase) {
		case movementSpring:
			if (structure.getBoard().needDisband()) {
				structure.setNextPhase(Phase.disbandSpring);
			} else {
				structure.setNextPhase(Phase.movementFall);
			}
			break;
		case disbandSpring:
			structure.setNextPhase(Phase.movementFall);
			break;
		case movementFall:
			if (structure.getBoard().needDisband()) {
				structure.setNextPhase(Phase.disbandFall);
			} else {
				if (structure.getBoard().needAdjust()) {
					structure.setNextPhase(Phase.adjust);
				} else{
					structure.setNextPhase(Phase.movementSpring);
				}
			}
			break;
		case disbandFall:
			if (structure.getBoard().needAdjust())
				structure.setNextPhase(Phase.adjust);
			else
				structure.setNextPhase(Phase.movementSpring);
			break;
		case adjust:
			structure.setNextPhase(Phase.movementSpring);
			break;
		}
		
		//test game ended
		List<Country> winlist = testGameOver(structure);
		
		
		/*System.out.println("old phase : "+ structure.getPhase()
				+ ", new phase : " + structure.getNextPhase()); */

		// returns result from structure
		JudgeResult result = JudgeResult.buildResult(structure);
		result.setWinList(winlist);
		
		return result;
	}

	/**
	 * test game over
	 * @param structure
	 */
	private List<Country> testGameOver(SupportingDataStructure structure){
		
		List<Country> winlist = new ArrayList<Country>(7);
		
		boolean winner = false;
		
		//check if winner
		for(Country c : Country.getCountries()){
			if (structure.getBoard().countSupply(c) >= MAX_SUPPLY_GENERIC){
				winner = true;
				break;
			}
		}
		
		//if not any winner return empty list
		if (!winner) return winlist;
		
		while (winlist.size() < 7){
			int max = -1;
			Country current = null;
			for(Country c : Country.getCountries()){
				int nbSupplyr = structure.getBoard().countSupply(c);
				if (nbSupplyr > max && !winlist.contains(c)){
					current = c;
					max = nbSupplyr;
				}
				
			}
			winlist.add(current);
		}
		
		structure.setNextPhase(Phase.gameOver);
		return winlist;
		
	}
	
	
}

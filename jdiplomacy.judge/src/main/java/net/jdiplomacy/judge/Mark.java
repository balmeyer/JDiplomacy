package net.jdiplomacy.judge;

public enum Mark {
	bounce,
	dislodged,
	cut,
	empty,
	no_convoy,
	convoy_endangered,
	convoy_under_attack,
	hold,
	fail
	
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge;

import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import net.jdiplomacy.judge.game.Board;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.order.Order;

import org.w3c.dom.Node;

/**
 * Result of the Judge Engine.
 * @author Jean-Baptiste
 */
@XmlRootElement(name="result")
public class JudgeResult {

    //strucutre of data for adjucation
    private SupportingDataStructure structure;

    private List<Country> winlist = new ArrayList<Country>();

    /**
     * Build result of adjucation from structure
     * @param structure
     * @return
     */
    public static JudgeResult buildResult(SupportingDataStructure structure){

        JudgeResult r = new JudgeResult();
        r.structure = structure;
        return r;
    }

    /**
     * Flush result to an xml file
     * @param r
     * @param file
     * @throws JAXBException
     * @throws IOException
     */
    public static void toXmlFile(JudgeResult r , String file) throws JAXBException, IOException{
        createMarshaller().marshal(r, new FileWriter(file));
    }
    
    /**
     * Flush result to the specified stream
     * @param r
     * @param stream
     * @throws JAXBException
     */
    public static void toXmlStream(JudgeResult r , OutputStream stream) throws Exception{
    	createMarshaller().marshal(r, stream);
    }
    
    /**
     * Return result as a DOM Node.
     * @param result
     * @return
     */
    public static Node getResultAsNode(JudgeResult result){

        Node node = null;
        try {
        	
			node = createMarshaller().getNode(result);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
		return node;
    }
    
    /**
     * Create new instance of marshaller
     * @return
     * @throws JAXBException
     */
    private static Marshaller createMarshaller() throws JAXBException{
        JAXBContext context = JAXBContext.newInstance(JudgeResult.class);

        return context.createMarshaller();
    }
    
    /*
     * No instance
     */
    private JudgeResult(){
    
    }

    /**
     * Returns the version of Judge Engine
     * @return
     */
    @XmlAttribute(name="version")
    public String getVersion(){
    	return JudgeEngine.VERSION;
    	
    }
    
    /**
     * Returns the initial position of units, before judgement
     * @return
     */
    @XmlElement(name="initialPosition")
    public String getInitialPosition(){
        return structure.getInitialPosition();
    }

    /**
     * Get phase before adjudication
     * @return
     */
    @XmlAttribute(name="phase")
    public Phase getPhase() {
    	return this.structure.getPhase();
    }
    
    @XmlAttribute(name="nextphase")
    public Phase getNextPhase(){
    	return this.structure.getNextPhase();
    }
    /**
     * Indicate that a dislodged units exist in result and that a disband phase must come next
     * @return
     */
    @XmlAttribute(name="isdisband")
    public boolean isDislodgedUnit(){
    	for(Unit u : structure.getUnits()){
    		if (u.isMarkedAs(Mark.dislodged)) return true;
    	}
    	
    	return false;
    }
    
    public Board getBoard(){
        return this.structure.getBoard();
    }

    /**
     * Returns the new positions of units, when judgement is done.
     * @return
     */
    @XmlElement(name="finalPosition")
    public String getFinalPosition(){
        if (this.structure.getBoard() != null) return this.structure.getBoard().getStartPosition();
        return null;
    }


    /**
     * Returns orders
     * @return
     */
    @XmlElementWrapper(name="orders")
    public Collection<Order> getOrders(){
        return structure.getOrders();
    }

    public String getTextOrders(boolean withmark){
    	StringBuilder sb = new StringBuilder();
    	String aux = "";
    	
    	for(Order o : getOrders()){
    		sb.append(aux);
    		if (!withmark){
    		sb.append(o.getText());
    		}
    		else sb.append(o.getTextWithMarks());
    		aux = ";";
    	}
    	
    	return sb.toString();
    }
    
    @XmlElementWrapper(name="units")
    public Collection<Unit> getUnits(){
    	return this.structure.getUnits();
    }
    
    /**
     * When game is over, return list of player
     * @return
     */
    @XmlTransient
    public List<Country> getWinList(){
    	return this.winlist;
    }
    
    public void setWinList(List<Country> winlist){
    	this.winlist = winlist;
    }
    
}

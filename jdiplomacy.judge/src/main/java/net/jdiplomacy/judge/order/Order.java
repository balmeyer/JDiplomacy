/*
 * Created on 18 mars 2007
 *
 *
 */
package net.jdiplomacy.judge.order;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.game.Board;
import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.UnitType;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.game.board.Province;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.graph.Graph;



/**
 * @author : Jean-Baptiste Vovau - balmeyer@dev.java.net
 * 
 *  
 */
@XmlRootElement(name = "order")
public final class Order implements Comparable<Order> {

    public static final int ERREUR_ORDRE_TROP_COURT = 1;
    public static final int ERREUR_TYPE_UNITE_INCONNU = 2;
    public static final int ERREUR_PROVINCE_BASE_INCONNUE = 3;
    public static final int ERREUR_TYPE_ORDRE_INCONNU = 4;
    public static final int ERREUR_PROVINCE_DESTINATION_INCONNUE = 5;
    public static final int ERREUR_UNITE_NON_POSSEDEE = 6;
    public static final int ERREUR_DEPLACEMENT_IMPOSSIBLE = 7;
    public static final int ERREUR_DEPOT_INCORRECT = 8;
    public static final int ERREUR_DEPOT_NON_POSSEDE = 9;
    //-------------
    //private Board board;
    private String text;
    private volatile Country country;
    private OrderType typeOrdre;
    private UnitType typeUnit;
    private int erreur = 0;
    private boolean success = true;
    private boolean moveValidated = false;
    private Region regionStart, regionEnd, regionBase;
    private Unit unit;
    private String failmessage;

    //list of path when convoy
    private List<Region> convoiPath;
    //private Board board ;

    public static Order create(Board board, String text, boolean disband) {
        if (text.contains(":")) {
            return unserialize(board, text);
        }

        return new Order(board, text,disband);
    }

    public static Order create(Board board, Country c, String text ,boolean disband) {
        if (text.contains(":")) {
            Order o = unserialize(board, text);
            o.country = c;
            return o;
        }
        return new Order(board, c, text,disband);
    }

    /**
     * Return Order Object
     * */
    /*
    @Deprecated
    private static String serializex(Order o) {
        StringBuilder sb = new StringBuilder();

        sb.append(o.country.getIndex());
        sb.append(':');
        sb.append(o.text);
        sb.append(':');
        sb.append(o.typeOrdre);
        sb.append(':');
        sb.append(o.typeUnit);
        sb.append(':');
        sb.append(o.erreur);
        sb.append(':');
        sb.append(o.success);

        //chemin de convoi
        if (o.convoiPath != null) {
            sb.append(":");
            String aux = "";
            for (int i = 0; i < o.convoiPath.size(); i++) {
                Region r = o.convoiPath.get(i);

                if (r == null) {
                    continue;
                }
                sb.append(aux);
                sb.append(r.getName());
                aux = "-";
            }

        }

        return sb.toString();
    }
    */
    /**
     * Transforme un texte en ordre
     * */
    private static Order unserialize(Board board, String s) {

        if (board == null) {
            throw new IllegalArgumentException("board ne peut etre null !");
        }

        if (s == null) {
            return null;
        }


        //bounce
        if (s.startsWith("X:")) {
            return null;
        }

        String[] w = s.split(":");
        //ordre vide
        if (w.length < 2) {
            return null;
        }

        Order o = new Order(board, w[1],false);

        o.country = Country.get(Integer.parseInt(w[0]));
        //o.typeOrdre = OrderType.valueOf(w[2]);
        //o.typeUnit = UnitType.valueOf(w[3]);
        //o.erreur = Integer.parseInt(w[4]);
        //o.success = Boolean.parseBoolean(w[5]);
        //o.force = Integer.parseInt(w[6]);




        return o;
    }

    /**
     * Test if a convoying fleet order matches a convoyer army
     * @param fleetOrder
     * @param armyOrder
     * @return
     */
    public static boolean areSupportOrConvoyMatch(Order supporting, Order moving) {
        Province p1 = null;
        Province p2 = null;

        if (supporting == null || moving == null){
        	return false;
        }
        
        if (moving.getOrderType() != OrderType.movement) {
            return false;
        }

        if (supporting.getOrderType() != OrderType.convoy && supporting.getOrderType() != OrderType.support) {
            return false;
        }

        p1 = supporting.getStartRegion().getProvince();
        p2 = moving.getBaseRegion().getProvince();

        if (p1 == null || p2 == null) {
            return false;
        }

        if (!p1.equals(p2)) {
            return false;
        }

        p1 = supporting.getEndRegion().getProvince();
        p2 = moving.getEndRegion().getProvince();

        return p1.equals(p2);
    }

    /**
     * Empty constructor, used for serialization
     *  
     */
    private Order() {
    }

    private Order(Board board, Country c, String text, boolean disband) {
        this(board, text, disband);

        //test if the contry actually owns the unit
        if (c == null || !c.equals(this.country)) {
            this.setError(Order.ERREUR_UNITE_NON_POSSEDEE);
        }
    }

    private Order(Board board, String textOrder , boolean disband) {

        //this.board = board;
        this.text = textOrder;
        
        //first, set the convoy path
        if (textOrder.contains("[")){
        	int a = textOrder.indexOf('[');
        	int end = textOrder.indexOf(']');
        	
        	String myPath = textOrder.substring(a , end + 1);
        	//remove path from order
        	textOrder = textOrder.replace(myPath, "");
        	myPath = myPath.replace("[", "").replace("]", "");
        	
        	this.convoiPath = new ArrayList<Region>();
        	for (String name : myPath.split(",")){
        		Region r = Region.get(name);
        		this.convoiPath.add(r);
        	}
        }
        
        //espace pour add et del
        if (textOrder.startsWith("+") || textOrder.startsWith("-")){
        	StringBuilder s = new StringBuilder(textOrder);
        	s.insert(1, ' ');
        	textOrder = s.toString();
        }
        
        //retire le double espace
        StringBuilder sb = new StringBuilder(textOrder.trim());
        int doubleSpace = 0;
        do {
            doubleSpace = sb.toString().indexOf("  ");
            if (doubleSpace >= 0) {
                sb.deleteCharAt(doubleSpace);
            }
        } while (doubleSpace >= 0);

        this.text = sb.toString();

        //analyse l'ordre
        String[] words = text.toUpperCase().split(" ");
        if (words.length < 2) {
            this.erreur = ERREUR_ORDRE_TROP_COURT;
            return;
        }

        //---------------------------------
        //Unit type
        if (words[0].equals("A") || words[0].equals("F")) {
            if (words[0].equals("A")) {
                this.typeUnit = UnitType.ARMY;
            } else {
                this.typeUnit = UnitType.FLEET;
            }
        } else {
            //-------------------
            //ajout ou suppression d'unité
            if (words[0].equals("+") || words[0].equals("-")) {
                //type d'ajout / suppression
                if (words[0].equals("+")) {
                    this.typeOrdre = OrderType.add;
                } else {
                    this.typeOrdre = OrderType.del;
                }


                //----type d'unité
                if (words[1].equals("A") || words[1].equals("F")) {
                    if (words[1].equals("A")) {
                        this.typeUnit = UnitType.ARMY;
                    } else {
                        this.typeUnit = UnitType.FLEET;
                    }
                }
            }
        }

        //--- verif type unit
        if (this.typeUnit == null) {
            //type d'unité inconnu
            this.erreur = ERREUR_TYPE_UNITE_INCONNU;
            return;
        }

        //Région de départ
        String wordBase = (this.typeOrdre == OrderType.del
                || this.typeOrdre == OrderType.add) ? words[2]
                : words[1];

        try {
            this.regionBase = Region.get(wordBase);

        } catch (IllegalArgumentException iae) {

            this.erreur = ERREUR_PROVINCE_BASE_INCONNUE;
            return;
        }

        //
        this.country = board.getOwningCountry(this.regionBase);

        //teste si ordre ok pour ajustement
        if (this.typeOrdre == OrderType.add) {


            //pays concerné
        	if (this.regionBase.getProvince().getSupply() != null){
            this.country = this.regionBase.getProvince().getSupply().getOriginalOwner();
        	}
            this.regionStart = regionBase;
            return;
        }

        if (this.typeOrdre == OrderType.del) {
        
            this.unit = board.getUnit(regionBase.getProvince(),disband);

            if (this.unit != null) {
                this.country = this.unit.getCountry();
            } else {
                this.erreur = ERREUR_UNITE_NON_POSSEDEE;
            }
            this.regionStart = regionBase;

            return;
        }

        //région de base connu : d'abord retraite
        this.unit = board.getUnit(regionBase);


        if (unit == null) {
            this.erreur = ERREUR_TYPE_UNITE_INCONNU;
            return;
        }

        //transforme la région de base (cas stp e) selon l'unité
        this.regionBase = this.unit.getRegion();

        if (words.length > 2) {
            //peu d'ordre : pas de mouvement
            if (words[2].equals("-")) {
                this.typeOrdre = OrderType.movement;
            }

            if (words[2].equals("C")) {
                this.typeOrdre = OrderType.convoy;
            }

            if (words[2].equals("S")) {
                this.typeOrdre = OrderType.support;
            }

            if (this.typeOrdre == null) {
                this.erreur = ERREUR_TYPE_ORDRE_INCONNU;
                return;
            }

        } else {
            //que deux mots : reste en place
            this.regionStart = this.regionBase;
            this.regionEnd = this.regionBase;
            return;
        }

        //région de départ
        Region rg2 = null;
        try {
            rg2 = Region.get(words[3]);
            if (rg2 == null) {
                //2éme chance
                Province pr2 = Province.get(words[3]);
                if (pr2 != null && board.getUnit(pr2) != null) {
                    rg2 = board.getUnit(pr2).getRegion();
                }
            }
        } //on sort
        catch (IllegalArgumentException ex) {
            this.erreur = Order.ERREUR_PROVINCE_DESTINATION_INCONNUE;
            return;
        }

        //région arrivée
        Region rg3 = null;
        if (words.length >= 6) {
            rg3 = Region.get(words[5]);
            //2 chances
            if (rg3 == null) {
                Province pr3 = Province.get(words[5]);
                if (pr3 != null && board.getUnit(pr3) != null) {
                    rg3 = board.getUnit(pr3).getRegion();
                }
            }
        }

        switch (this.typeOrdre) {
            case movement:
                this.regionStart = this.regionBase;
                this.regionEnd = rg2;
                break;
            case support:
                if (rg3 == null && rg2 != null && regionBase != null) {
                    rg3 = rg2;
                }
            case convoy:
                this.regionStart = rg2;
                this.regionEnd = rg3;
                break;
        }

        //région d'arrivée
        //pays emetteur de l'ordre
        if (this.unit != null) {
            this.country = this.unit.getCountry();
        } else {
            this.erreur = Order.ERREUR_TYPE_UNITE_INCONNU;
            return;
        }

        //Controle du type d'unité
        if (this.regionEnd != null && !regionEnd.acceptUnit(this.unit)) {
            this.erreur = Order.ERREUR_DEPLACEMENT_IMPOSSIBLE;
            return;
        }

        //vérifie le déplacement
        if (this.regionEnd != null && this.typeOrdre == OrderType.support
                && !canSupport(regionBase, regionEnd)) {
            this.erreur = Order.ERREUR_DEPLACEMENT_IMPOSSIBLE;
            return;

        }

        //Déplacement non autorisé si Flotte
        if (this.regionStart != null && this.regionEnd != null
                && this.typeOrdre != OrderType.convoy
                && this.unit.getType() == UnitType.FLEET
                && !regionStart.equals(regionEnd)
                && !this.unit.getGraph().areAdjacent(regionBase, regionEnd)) {
            this.erreur = Order.ERREUR_DEPLACEMENT_IMPOSSIBLE;
        }

        if (this.regionStart == null) {
            System.out.println("NON !!!");
        }

        if (this.regionEnd == null) {
            System.out.println("NON !!!");
        }

    }

    /**
     * Original instruction for this order
     * 
     * @return
     */
    @XmlAttribute(name = "text")
    public String getText() {
    	
    	//add convoy path
    	if (this.text != null && !this.text.contains("[") 
    			&& this.getConvoiPath().size() > 0){
    		StringBuilder sb = new StringBuilder(this.text);
    		sb.append(" [");
    		String aux = "";
    		for(Region r : this.getConvoiPath()){
    			sb.append(aux);
    			sb.append(r.getName());
    			aux = ",";
    		}
    		sb.append("]");
    		this.text = sb.toString();
    	}
    	
        return this.text;
    }

    /**
     * Returns instructions with marks
     * @return
     */
    @XmlAttribute(name="textmarked")
    public String getTextWithMarks(){
    	StringBuilder sb = new StringBuilder(this.getText());
    	
    	if (this.unit != null && this.unit.getMarks() != null){
    		sb.append(":");
    		sb.append(this.unit.getMarks());
    		if (this.unit.getTextPossibleRetreats() != null){
    			sb.append(":");
    			sb.append(unit.getTextPossibleRetreats());
    		} 
    	}
    	return sb.toString();
    }
    
    @Override
    public String toString() {
    	StringBuilder sb = new StringBuilder(this.text);
    	
    	if (this.unit != null && this.unit.getMarks() != null){
    		sb.append(" [");
    		sb.append(this.unit.getMarks());
    		sb.append("]");
    	}
    	
        return sb.toString();
    }

    @XmlAttribute(name = "country")
    public String getCountryName() {
    	if (country == null) return null;
    	
        return this.country.getGameName();
    }

    public Country getCountry() {
        return this.country;
    }


    /**
     * 
     * 
     * @return
     */
    @XmlAttribute(name = "success")
    public boolean isSuccess() {
        return this.success && this.erreur == 0;
    }

    public boolean isMoveValidated(){
        return this.moveValidated;
    }

    /**
     * Indique que l'ordre échoue
     *  
     */
    public void fail() {

        this.success = false;
        
    }


    public void fail(String message) {
        this.fail();
        this.failmessage = message;
        
        Mark mark = Mark.fail;
        try {
        	
        	mark = Mark.valueOf(message.replace(" ", "_"));
        } catch (IllegalArgumentException ex){
        	//message not understood
        }
        
        if (this.getUnit() != null) this.getUnit().markAs(mark);
    }

    public void validateMove(){
        //assert this.getUnit().getMarks() == null;
        if (this.failmessage != null){
        	throw new IllegalStateException("cannot valide failed :" + failmessage);
        }
        this.success = true;
        this.moveValidated = true;
    }

    /**
     * Indique si l'ordre est visible
     * 
     * @return
     */
    public boolean isVisible() {
        return true;
    }

    /**
     * Indique si l'ordre est correct
     * 
     * @return
     */
    public boolean isCorrect() {
        return this.erreur == 0;
    }

    @XmlAttribute(name="msg")
    public String getMessage(){
    	return this.failmessage;
    }
    

    @XmlTransient
    public int getError() {
        return this.erreur;
    }

    public void setError(int e) {
        this.erreur = e;
        if (e != 0) {
            this.fail();
        }
    }

    /**
     * Retourne le type d'ordre
     * @return
     */
    public OrderType getOrderType() {
        return this.typeOrdre;
    }

    /**
     * Retourne la région du début de l'ordre. (départ pour un mouvement)
     * 
     * @return
     */
    public Region getStartRegion() {
        return regionStart;
    }

    /**
     * Returns the destination region
     * 
     * @return
     */
    public Region getEndRegion() {
        return regionEnd;
    }

    /**
     * 
     */
    public Region getBaseRegion() {
        return regionBase;

    }

    /**
     * Get the expeced Region to move (or remain) if order is successfull
     * @return
     */
    public Region getExpectedResultRegion() {

        Region r = null;

        switch (this.getOrderType()) {
            case movement:
                r = this.getEndRegion();
                break;
            default:
                r = this.getBaseRegion();
                break;
        }

        assert r != null;
        return r;
    }

    /**
    returns resulting region when order is complete. Used for move.
     * @return
     */
    public Region getResultRegion() {
        Region r = null;

        if (this.isSuccess()) {
            r = getExpectedResultRegion();
        } else {
            r = this.getBaseRegion();
        }

        if (r == null) {
            System.out.println("ERREUR ! REGION PEUT PAS ETRE NULL");
            throw new IllegalStateException("REGION PEUT PAS ETRE NULL");
        }

        return r;
    }

    /**
     * Retourne l'unité executant l'ordre
     * 
     * @return
     */
    public Unit getUnit() {
        return this.unit;

    }

    public UnitType getUnitType() {
        return this.typeUnit;
    }

    /**
     * Indique si l'ordre provoque un mouvement
     * 
     * @return
     */
    public boolean isMouvement() {
        return this.success && this.typeOrdre == OrderType.movement
                && (!this.regionStart.equals(this.regionEnd));
    }


    /**
     * Determines if the order is a convoyed move
     * @return
     */
    public boolean isConvoyedMove(){
        //order is correct ?
        if (!this.isCorrect()) return false;

        //order is move ?
        if (this.getOrderType() != OrderType.movement) return false ;
        
        //fleet are not convoyed
        if (this.getUnit() != null && this.getUnit().getType() == UnitType.FLEET) return false;

        //destination is not a coast ?
        if (this.getEndRegion() != null && !this.getEndRegion().isConvoyStop()) return false;

        //contains path ?
        if (this.getConvoiPath().size() > 0) return true ;
        
        //near ?
        Graph graph = Unit.getGraph(UnitType.ARMY);
        return (!graph.areAdjacent(this.getBaseRegion(), this.getEndRegion()));
        
    }

    @Override
    public int compareTo(Order o) {

        if (o.country.getIndex() < this.country.getIndex()) {
            return 1;
        }
        if (o.country.getIndex() > this.country.getIndex()) {
            return -1;
        }

        return 0;
    }

    /**
     * 
     * 
     * @return
     */
    public List<Region> getConvoiPath() {
        if (this.convoiPath == null) {
            convoiPath = new ArrayList<Region>();
        }

        return convoiPath;
    }

    private boolean canSupport(Region start, Region end) {

        Province p = end.getProvince();
        return (start.getProvincesAround().contains(p));


    }
}

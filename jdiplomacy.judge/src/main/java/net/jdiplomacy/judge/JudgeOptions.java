package net.jdiplomacy.judge;

/**
 * Determine specials options
 * @author JBVovau
 *
 */
public enum JudgeOptions {

	/** When Support or Convoy fails, unit HOLD and is not marked, not dislodged */
	failingSupportOrConvoyHold ,
	/** When support or convoy fails, they can be dislodeg */
	failingSupportOrConvoyAreMarked
}

package net.jdiplomacy.judge.game.board;

import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.UnitType;




public final class Coast extends Region
{

	public Coast(Province province){
		super(province);
	}
	
	public Coast(Province province, String specialName){
		super(province, specialName);
	}
	
	/* (non-Javadoc)
	 * @see diplomacy.province.Region#acceptUnit(diplomacy.province.Unit)
	 */
	@Override
	public boolean acceptUnit(Unit unit) {
		//coast accepts all unit types
		return true ;
	}

	/*
	public boolean acceptUnit(Class<> unitClass){
		return true;
	} */
	@Override
	public boolean acceptUnit(UnitType type){
		return true;
	}
	
	@Override
	public boolean acceptSupply(){
		//
		return getProvince().getSupply() != null;

	}

	/* (non-Javadoc)
	 * @see diplomacy.board.Region#isConvoyPath()
	 */
	@Override
	public boolean isConvoyPath() {
		
		return false;
	}

	/* (non-Javadoc)
	 * @see diplomacy.board.Region#isConvoyStop()
	 */
	@Override
	public boolean isConvoyStop() {
		return true;
	}


}
package net.jdiplomacy.judge.game.board;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.jdiplomacy.judge.JudgeEngine;
import net.jdiplomacy.judge.game.Board;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.UnitType;
import net.jdiplomacy.judge.graph.Graph;
import net.jdiplomacy.judge.graph.GraphExplorer;
import net.jdiplomacy.judge.graph.GraphNode;




/**
 * 
 * Super class for all regions. A Region can be occupied by only one unit.
 *  
 */
public abstract class Region {
    //constantes statiques

    public final static int EARTH = 0;
    public final static int COAST = 1;
    public final static int SEA = 2;

    private static final Map<String, Region> nameToRegion = new HashMap<String, Region>(90);
    //private static Graph armyGraph = new Graph();
    //private static Graph shipGraph = new Graph();
    /** Province dans la quelle se situe la région. */
    private Province province = null;
    private Collection<Province> provincesAround ;
    //variables
    private String name;


    public static void clear(){
    	nameToRegion.clear();
    }
    
    public static Region buildEarth(Province province) {
        Region r = new Earth(province);
        nameToRegion.put(r.getName().toLowerCase(), r);
        return r;
    }

    public static Region buildSea(Province province) {
        Region r = new Sea(province);
        nameToRegion.put(r.getName().toLowerCase(), r);
        return r;
    }

    public static Region buildCoast(Province province) {
        Region r = new Coast(province);
        nameToRegion.put(r.getName().toLowerCase(), r);
        return r;
    }

    public static Region buildCoast(Province province, String specialName) {
        Region r = new Coast(province, specialName);
        nameToRegion.put(r.getName().toLowerCase(), r);
        return r;
    }

    public static Region get(String name) {
        Region r = nameToRegion.get(name.trim().toLowerCase());

        if (r == null){
            throw new IllegalArgumentException("region non trouvée : " + name);
        }

        return r;

    }

    public static Collection<Region> getRegions() {
    	
    	if (nameToRegion.size() == 0) JudgeEngine.initGame();
    	
        return nameToRegion.values();
    }

    /**
     * Ajout des régions existances dans un graph
     *
     */
    public static void completeGraph(Graph graph) {
        GraphExplorer ge = new GraphExplorer(graph);

        //methode 1
        for (GraphNode node = null; ge.hasNext();) {
            node = ge.next();
            Region region = Region.get(node.getName());
            if (region == null) {
            }
            assert (region != null);
            node.setRegion(region);
        }

        //methode 2
        Iterator<Region> it = nameToRegion.values().iterator();
        while (it.hasNext()) {
            graph.addRegion(it.next());
        }
    }

    public static Collection<Region> getCommonRegionsForSupport(Board board, Region attack, Region support){
        List<Region> list = new ArrayList<Region>(8);

        Unit uattack = board.getUnit(attack);
        Unit usupport = board.getUnit(support);


        Collection<Region> aroundAttack = uattack.getGraph().getNode(attack).getRegionsAround();
        for (Region r : usupport.getGraph().getNode(support).getRegionsAround()){
            if (aroundAttack.contains(r)){
                list.add(r);
            }
        }

        return list;
    }

    /**
     *
     */
    protected Region(Province prov, String specialName) {
        this.province = prov;
        this.name = specialName;
    }

    protected Region(Province prov) {
        this.province = prov;
        this.name = this.province.getShortName();
    }

    @SuppressWarnings("unused")
	private Region() {}
    
    public Province getProvince() {
        return this.province;
    }

    /**
     * Retourne les provinces adjacentes
     * @return
     */
    public Collection<Province> getProvincesAround() {
        if (this.provincesAround == null) {

            GraphNode gnArmy = Unit.getGraph(UnitType.ARMY).getNode(this);
            GraphNode gnFloat = Unit.getGraph(UnitType.FLEET).getNode(this);

            this.provincesAround = new ArrayList<Province>(12);

            this.provincesAround.addAll(getProvFromNode(gnArmy));
            this.provincesAround.addAll(getProvFromNode(gnFloat));


        }

        return this.provincesAround;
    }

    private Collection<Province> getProvFromNode(GraphNode node) {
        List<Province> liste = new ArrayList<Province>(8);

        if (node != null) {
            for (GraphNode n : node.getSuccessors()) {
                Province pr = n.getRegion().getProvince();
                if (!liste.contains(pr)) {
                    liste.add(pr);
                }


            }
        }
        return liste;

    }

    /**
     *
     */
    public String getName() {
        return name;
    }


    public void setPosition(int x, int y) {

    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (!(o instanceof Region)) {
            return false;
        }

        return o.toString().equals(this.toString());
    }


    /**
     * Ce terrain accepte-t-il une unité particuliére ?
     */
    public abstract boolean acceptUnit(Unit unit);

    public abstract boolean acceptUnit(UnitType type);
    
    /**
     * Cette région peut-elle accepter un dépot ?
     * @return
     */
    public abstract boolean acceptSupply();

    /** Indique si la région est un chemin pour le convoi*/
    public abstract boolean isConvoyPath();

    /** Indique si la région est un départ ou une arrivée de convoi. */
    public abstract boolean isConvoyStop();
}

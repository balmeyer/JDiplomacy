/*
 * Created on 10 avr. 2004
 *
 * Vovau Jean-Baptiste - balmeyer@dev.java.net
 */
package net.jdiplomacy.judge.game;

import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.graph.Graph;

/**
 * 
 * Created on 10 avr. 2004.
 * 
 * @author Jean-Baptiste Vovau - balmeyer@dev.java.net
 * @version 1.0
 */
public final class Fleet extends Unit {

    /**
     * @param country
     */
    protected Fleet(Country country) {
        super(country);
    }

    /*
     * (non-Javadoc)
     *
     * @see diplomacy.board.Unit#getType()
     */
    @Override
    public UnitType getType() {

        return UnitType.FLEET;
    }

    /*
     * (non-Javadoc)
     *
     * @see diplomacy.board.Unit#canGoHere(diplomacy.board.Region)
     */
    @Override
    public boolean canGoHere(Region region) {

        return region.acceptUnit(this);
    }

    @Override
    public boolean canConvoy() {
        return this.getRegion().isConvoyPath();
    }

    /*
     * (non-Javadoc)
     *
     * @see diplomacy.board.Unit#getMovementGraph()
     */
    @Override
	public Graph getMovementGraph() {

        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see diplomacy.board.Unit#canBeConvoyed()
     */
    @Override
	public boolean canBeConvoyed() {

        return false;
    }

    /* (non-Javadoc)
     * @see diplomacy.board.Unit#getCodeChar()
     */
    @Override
	public char getCodeChar() {

        return 'F';
    }
}

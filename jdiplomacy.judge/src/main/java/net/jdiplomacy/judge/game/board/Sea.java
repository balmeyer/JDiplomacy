package net.jdiplomacy.judge.game.board;

import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.UnitType;

public final class Sea extends Region {

    public Sea(Province province) {
        super(province);
    }

    /*
     * (non-Javadoc)
     *
     * @see diplomacy.province.Region#acceptUnit(diplomacy.province.Unit)
     */
    @Override
    public boolean acceptUnit(Unit unit) {
        //Les régions de type Mer n'acceptent ques les flottes.
        return (unit != null && unit.getType() == UnitType.FLEET);
    }

    @Override
	public boolean acceptUnit(UnitType type){
		return UnitType.FLEET == type;
	}
    
    /**
     * Retourne FAUX : une MER n'accepte jamais de dépot
     */
    @Override
    public boolean acceptSupply() {
        return false;
    }



    /* (non-Javadoc)
     * @see diplomacy.board.Region#isConvoyPath()
     */
    @Override
    public boolean isConvoyPath() {
        return true;
    }

    /* (non-Javadoc)
     * @see diplomacy.board.Region#isConvoyStop()
     */
    @Override
    public boolean isConvoyStop() {
        //mer : arrivée de convoi impossible
        return false;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge.game;

/**
  Type of army
 * @author Jean-Baptiste
 */
public enum UnitType {

    ARMY,
    FLEET
}

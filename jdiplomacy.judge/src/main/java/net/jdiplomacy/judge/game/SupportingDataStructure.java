/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.jdiplomacy.judge.Phase;
import net.jdiplomacy.judge.game.board.Province;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.order.Order;
import net.jdiplomacy.judge.steps.AdjudicationStep;


/**
 *
 * @author Jean-Baptiste
 */
public class SupportingDataStructure {

    private Board board;
    //private Board finalBoard;

    //Phase
    private Phase phase = null;
    private Phase nextPhase = null;
    
    private String initialPosition;

    private List<Order> orders;
    private Map<Province, List<Unit>> combatList = new HashMap<Province, List<Unit>>();
    private List<Unit> convoyingArmiesList = new ArrayList<Unit>();
    private List<Unit> convoySucceededList = new ArrayList<Unit>();

    private List<Unit> convoyedArmiesList = new ArrayList<Unit>();
    
    private AdjudicationStep currentStepInstance;
    
    public SupportingDataStructure(Board board) {

        this.board = board;
        this.initialPosition = board.getStartPosition();
        //duplicate final board for adjucation result
        //this.finalBoard = new Board(board.getStartPosition());

        //init the combat list for all Province
        for (Province p : Province.getProvinceList()) {
            combatList.put(p, new ArrayList<Unit>());
        }
    }

    /**
	 * @param phase the phase to set
	 */
	public void setPhase(Phase phase) {
		this.phase = phase;
	}

	/**
	 * @return the phase
	 */
	public Phase getPhase() {
		return phase;
	}

	public Phase getNextPhase(){
		return this.nextPhase;
	}
	
	public void setNextPhase(Phase phase){
		this.nextPhase = phase;
	}
	
	public String getInitialPosition(){
    	return this.initialPosition;
    }
    
    /**
	 * @param currentStepInstance the currentStepInstance to set
	 */
	public void setCurrentStepInstance(AdjudicationStep currentStepInstance) {
		this.currentStepInstance = currentStepInstance;
	}

	/**
	 * @return the currentStepInstance
	 */
	public AdjudicationStep getCurrentStepInstance() {
		return currentStepInstance;
	}

	/**
     *  list of all units attempting to enter (or remain in) the space.
     * Successful attacks and defenses are decided using this list and
     * the data concerning supports that is maintained for each of the units.
     * @param province
     */
    public Iterator<Unit> getCombatList(Province province) {

        return combatList.get(province).iterator();
    }

    public int getCombatListSize(Province p) {
        return combatList.get(p).size();
    }


    /**
     * Get the greatest and unique Unit with the max count support
     * @param province
     * @return
     */
    public Unit getGreatestFromCombatList(Province province) {
        Unit greater = null;
        int maxSupport = 0;

        Iterator<Unit> it = getCombatList(province);

        while (it.hasNext()) {
            Unit u = it.next();
            assert u != null;

            if (greater == null) {
                greater = u;
                maxSupport = u.getCountSupport();
            } else {
                if (u.getCountSupport() > maxSupport) {
                    greater = u;
                    maxSupport = u.getCountSupport();
                } else {
                    if (u.getCountSupport() == maxSupport) {
                        assert u != greater;
                        greater = null;
                    }
                }
            }
        }

        return greater;
    }

    public int getSecondGreatestSupportFromCombalist(Province province, Unit exceptThisUnit){

        int maxSupport = 0;

        Iterator<Unit> it = getCombatList(province);

        while (it.hasNext()) {
            Unit u = it.next();
            assert u != null;

            if (u.equals(exceptThisUnit))continue;
            
            if (u.getCountSupport() > maxSupport) {
                maxSupport = u.getCountSupport();
            }
        }
        
        return maxSupport;
    }
    
    public void addToCombatList(Region r, Unit u) {
        this.addToCombatList(r.getProvince(), u);
    }

    public void addToCombatList(Province p, Unit u) {

        //purge
        List<Unit> list = this.combatList.get(p);

        if (list.contains(u)) {
            list.remove(u);
        }

        list.add(u);
    }

    public void removeFromCombatList(Province p, Unit u) {
        List<Unit> list = this.combatList.get(p);
        list.remove(u);
    }

    /**
     * a list (initially empty) that will contain all units that have been issued a valid convoy order.
     * @return the convoyingArmiesList
     */
    public Collection<Unit> getConvoyingArmies() {
        return convoyingArmiesList;
    }

    public Collection<Unit> getConvoyedArmies() {
        return convoyedArmiesList;
    }
    
    public void addConvoyingArmy(Unit u){
    	if (u.getType() != UnitType.FLEET)
    		throw new IllegalArgumentException("convoying unit must be a fleet");
    	if (!this.convoyingArmiesList.contains(u))
    		this.convoyingArmiesList.add(u);
    }
    
    public void addConvoyedArmy(Unit u){
    	if (u.getType() != UnitType.ARMY)
    		throw new IllegalArgumentException("convoyied unit must be an army");
    	if (!this.convoyedArmiesList.contains(u))
    		this.convoyedArmiesList.add(u);
    }
    
    
    /**
     * a list (initially empty) to contain all units whose convoy orders succeeded.
     *
     * @return the convoySucceededList
     */
    public Iterator<Unit> getConvoySucceededList() {
        return convoySucceededList.iterator();
    }

    public int getConvoySucceededListSize() {
        return this.convoySucceededList.size();
    }

    public void addToConvoySuccededList(Unit u) {
    	if(u.getType() != UnitType.ARMY)
    		throw new IllegalArgumentException("unit must be an army");
    		
        if (!this.convoySucceededList.contains(u)) {
            this.convoySucceededList.add(u);
        }
    }

    /**
     * @return the board
     */
    /*
    public Board getBoard() {
        return board;
    }*/

    public Board getBoard() {
        return this.board;
    }

    /**
     * @return the orders
     */
    public List<Order> getOrders() {
        return orders;
    }

    public Order getOrder(Unit u) {
        if (u == null) {
            return null;
        }
        return getOrderFromBase(u.getRegion());
    }

    public Order getOrderFromBase(Region r) {
        return getOrderFromBase(r.getProvince());
    }

    public Order getOrderFromBase(Province p) {
        for (Order o : this.getCorrectOrders()) {
            if (o.getBaseRegion().getProvince().equals(p)) {
                return o;
            }
        }
        return null;
    }

    /**
     * Get correct orders
     * @return
     */
    public Collection<Order> getCorrectOrders() {
        List<Order> list = new ArrayList<Order>(this.orders.size());

        for (Order o : this.getOrders()) {
            if (o.isCorrect()) {
                list.add(o);
            }
        }

        return list;
    }

    /**
     * @param orders the orders to set
     */
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public Collection<Unit> getUnits() {
        return this.board.getUnits();
    }
    

}

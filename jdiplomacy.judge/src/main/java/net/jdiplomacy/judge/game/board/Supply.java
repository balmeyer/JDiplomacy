package net.jdiplomacy.judge.game.board;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class Supply {


    /** Liste des dépots */
    private static final Collection<Supply> supplyList = new ArrayList<Supply>(34);
    /** Correspondance nom de la region - dépot */
    private static final Map<Province, Supply> provinceToSupply  = new HashMap<Province, Supply>(34);
    private Country originalOwner;
    private Province province;

    public static void clear(){
    	supplyList.clear();
    	provinceToSupply.clear();
    }
    
    public static Collection<Supply> getSupplies() {
        return supplyList;
    }

    public static Supply getSupply(Province province) {
        return provinceToSupply.get(province);
    }

    public static Supply getSupply(String provinceName){
    	Province p = Province.get(provinceName);
    	return getSupply(p);
    }
    
    /**
     * Constructor without original supply
     *
     */
    public Supply(Province province, int x, int y) {
        this.originalOwner = null;
        this.setProvince(province);
        supplyList.add(this);
        provinceToSupply.put(province, this);
    }

    /**
     *
     * Constructor with original owner
     * @param originalOwner
     */
    public Supply(Province province, Country originalOwner) {
        this.originalOwner = originalOwner;
        this.setProvince(province);
        supplyList.add(this);
        provinceToSupply.put(province, this);
    }

    public Province getProvince() {
        return province;
    }

    private void setProvince(Province province) {
        this.province = province;
        if (province.getSupply() == null) {
            province.setSupply(this);
        }
    }

    public Country getOriginalOwner() {
        return this.originalOwner;
    }

    public boolean isNeutral() {
        return (this.originalOwner == null);
    }


    @Override
    public String toString() {
        return "[Supply] " + this.province.getShortName();
    }
}

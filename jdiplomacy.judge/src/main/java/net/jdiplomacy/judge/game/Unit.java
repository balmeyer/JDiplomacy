package net.jdiplomacy.judge.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.graph.Graph;


/**


Unit of Diplomacy


@version 1.0 02/04/2004
@author JB Vovau
 */
@XmlRootElement(name="unit")
public abstract class Unit {

    //liste des unités
    //graphes de déplacement pour les unités
    private static Graph armyGraph;
    private static Graph shipGraph;

    //variables de classe
    private Country country;
    private Graph movementGraph;

    //Unit id
    private long id;
    private Region region;
    private Region startRegion ;


    private List<Mark> marks = new ArrayList<Mark>();
    private int countSupport = 0;
    private List<Unit> noHelpList = new ArrayList<Unit>();

    //when unit disband, possible retreats
    private List<Region> possibleRetreats = new ArrayList<Region>();

    /**
     * Get a specified graph for an unit type
     * @param unitType
     * @return
     */
    public static Graph getGraph(UnitType unitType) {
        switch (unitType) {
            case ARMY:
                return armyGraph;
            case FLEET:
                return shipGraph;
            default:
                throw new IllegalArgumentException("Unknown Unit Type");
        }

    }

    public static void setGraph(Graph graph, UnitType unitType) {
        switch (unitType) {
            case ARMY:
                armyGraph = graph;
                break;
            case FLEET:
                shipGraph = graph;
                break;
            default:
                throw new IllegalArgumentException("Unknown Unit Type");
        }
    }

    public static Unit createArmy(Country country) {
        Unit u = new Army(country);
        u.setGraph(armyGraph);
        return u;
    }

    public static Unit createShip(Country country) {
        Unit u = new Fleet(country);

        u.setGraph(shipGraph);
        return u;

    }

    /**
    Constructeur d'unité
     */
    protected Unit(Country country) {

        this.country = country;

    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(this.getType());
        sb.append(" from ");
        sb.append(this.getCountry());

        if (this.getRegion() != null){
            sb.append(" in ");
            sb.append(this.getRegion().getName());
        }

        String m = this.getMarks();
        if (m != null){
            sb.append('[');
            sb.append(m);
            sb.append(']');
        }

        return sb.toString();
    }

    @SuppressWarnings("unused")
	private Unit(){}
    
    /**
    Get country
     */
    public Country getCountry() {
        return this.country;
    }

    /**
     * Return text starting position
     * @return
     */
    @XmlAttribute(name="position")
    public String getPosition(){
    	return this.region.getName();
    }
    
    @XmlAttribute(name="initial")
    public String getFirstPosition(){
    	return this.startRegion.getName();
    }
    
    public Region getRegion() {
        return this.region;
    }

    protected void setRegion(Region region) {
    	
    	//mark first position
    	if (this.region == null){
    		this.startRegion = region;
    	}
    	
        this.region = region;
    }

    /**
     * Get the region around this unit, accessible without convoy
     */
    public Collection<Region> getRoundingRegions(){
        return this.getGraph().getNode(this.getRegion()).getRegionsAround();
    }

    public Graph getGraph() {
        return this.movementGraph;
    }

    private void setGraph(Graph graph) {
        this.movementGraph = graph;
    }

    /**
     * When unit is dislodged, list of possible region
     * @param r
     */
    public void addToPossibleRetreats(Region r){
        this.possibleRetreats.add(r);
    }

    @Override
    public boolean equals(Object o) {


        if (o == null || !(o instanceof Unit)) {
            return false;
        }

        if (this == o ) return true;
        
        Unit u = (Unit) o;
        
        if (!u.country.equals(this.country)) return false;
        
        if (!u.getRegion().equals(this.getRegion())) return false;
        
        if (!u.getType().equals(this.getType())) return false;
        
        return true;
        //return u.id == this.id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @XmlAttribute(name="type")
    public abstract UnitType getType();

    public abstract boolean canGoHere(Region region);

    public abstract Graph getMovementGraph();

    /** Indique si l'unité est en mesure d'effectuer un convoi, dans sa position. */
    public abstract boolean canConvoy();

    /** Indique si une unité est en mesure d'�tre convoy�e */
    public abstract boolean canBeConvoyed();

    /** Retourne le code de l'unité */
    public abstract char getCodeChar();

    /**
     *
     * A textual mark (initially none) which indicates the status of the order.
     * The order for any unit without a mark will succeed
     * @return the mark
     */
    public boolean isMarkedAs(Mark mark) {
        return (this.marks.contains(mark));

    }

    /**
     * @param mark the mark to set
     */
    public void markAs(Mark mark) {
        if (!marks.contains(mark)){
            marks.add(mark);
        }
    }

    public void removeMark(Mark mark){
        marks.remove(mark);
    }

    /**
     * Remove all marks
     */
    public void clearMarks(){
        this.marks.clear();
    }

    @XmlAttribute(name="marks")
    public String getMarks(){
        StringBuilder sb = new StringBuilder();
        String aux = "";

        for (Mark mark : this.marks){
            sb.append(aux);
            sb.append(mark.toString());
            aux =",";
        }

        if (sb.length() == 0) return null;
        return sb.toString();
    }

    /**
     * When unit is dislodged, get the possible retreats
     * @return
     */
    public Collection<Region> getPossibleRetreats(){
    	return this.possibleRetreats;
    }
    
    @XmlAttribute(name="possibleRetreats")
    public String getTextPossibleRetreats(){
    	
    	if (this.possibleRetreats.size() == 0) return null;
    	
    	StringBuilder sb = new StringBuilder();
    	String aux = "";
    	
    	for(Region r : this.getPossibleRetreats()){
    		sb.append(aux);
    		sb.append(r.getName());
    		aux = ",";
    	}
    	return sb.toString();
    }
    
    /**
     * the number of valid supports given to this unit's order (initially zero).
     * This number determines what is sometimes in the algorithm below called the unit's "strength."
     * @return the countSupport
     */
    @XmlAttribute(name="support")
    public int getCountSupport() {
        return countSupport;
    }

    /**
     * @param countSupport the countSupport to set
     */
    public void setCountSupport(int countSupport) {
        this.countSupport = countSupport;
    }

    public void addCountSupport(){
        this.countSupport++;
    }

    /**
     * a list of units owned by the same power as the piece being attacked
     * which are offering support to the unit in its attempt to move into an occupied space.
     * @return the noHelpList
     */
    public List<Unit> getNohelpList() {
        return noHelpList;
    }

    public void addToNohelpList(Unit u){
        if (!noHelpList.contains(u)) {
            noHelpList.add(u);
        }
    }



}

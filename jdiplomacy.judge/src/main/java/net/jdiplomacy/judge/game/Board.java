/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.game.board.Province;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.game.board.Supply;


/**
 * A board contains relationships betweens static structures (province, regions, supplies)
 * and temporary items (units)
 * @author Jean-Baptiste Vovau
 */
public final class Board {

    /** serialized start position */
    private String startPosition = null;
    //private final Map<Province, Unit> provinceToUnit = new HashMap<Province, Unit>();
    private final Map<Unit, Region> unitToRegion = new HashMap<Unit, Region>();
    private final List<Unit> units = new ArrayList<Unit>();
    private final Map<Supply, Country> supplyToCountry = new HashMap<Supply, Country>();
    //private boolean locked = false;

    public Board() {
    }

    public Board (Object [] positions){
    	StringBuilder sb = new StringBuilder();
    	
    	String aux = "";
    	
    	for(Object obj : positions){
    		sb.append(aux);
    		sb.append(obj.toString());
    		aux = ";";
    	}

    	this.setStartPosition(sb.toString());
    }
    
    public Board(String startPosition) {
        this.setStartPosition(startPosition);
    }

    public String getStartPosition() {
        if (this.startPosition == null) {
            this.serializePositions();
        }

        return this.startPosition;
    }

    public void setStartPosition(String value) {
        this.startPosition = value;
        this.unserializePositions();
    }

    /**
     * Clear old marks for units
     */
    public void clearMarks(){
    	for (Unit u : this.getUnits()){
    		u.clearMarks();
    	}
    	this.refreshPositions();
    }
    
    public void reportSupply(Board oldboard) {
        this.clearUnits();
        //this.locked = false;
        this.setStartPosition(oldboard.startPosition);
        this.clearUnits();
    }

    private void clearUnits() {
        //this.checkLock();
        //provinceToUnit.clear();
        unitToRegion.clear();
        units.clear();
        //supplyToCountry.clear();
        
    }

    public Country getOwningCountry(Region r) {
        return getOwningCountry(r.getProvince());
    }

    public Country getOwningCountry(Province p) {
        Unit u = getUnit(p);

        if (u != null) {
            return u.getCountry();
        }

        //pas de pays trouvé
        return null;
    }


    public Collection<Unit> getUnits() {
        return this.units;
    }

    public Collection<Unit> getUnits(Country c) {

        List<Unit> list = new ArrayList<Unit>();

        for (Unit u : getUnits()) {
            if (c.equals(u.getCountry())) {
                list.add(u);
            }
        }


        return list;
    }

    public Unit getUnit(Province p){
    	return getUnit(p , false);
    }
    
    public Unit getUnit(String provinceName, boolean dislodged){
    	return getUnit(Province.get(provinceName),dislodged);
    }
    
    public Unit getUnit(Province p, boolean dislodged) {
    	
    	for (Unit u : this.getUnits()){
    		if (u.getRegion().getProvince().equals(p)){
    			if (!dislodged && !u.isMarkedAs(Mark.dislodged)){
    				return u;
    			}
    			if (dislodged && u.isMarkedAs(Mark.dislodged)){
    				return u;
    			}
    		}
    	}
    	
    	return null;
    }

    public Unit getUnit(Region r) {
        return getUnit(r.getProvince() , false);
    }

    public Unit getUnit(String provinceName) {
        return getUnit(Province.get(provinceName) , false);
    }


    public Unit createUnit(Region r, Country c, UnitType typeOfUnit) {
        Unit unit = null;

        switch (typeOfUnit) {
            case ARMY:
                unit = Unit.createArmy(c);
                break;
            case FLEET:
                unit = Unit.createShip(c);
                break;
            default:
                throw new IllegalArgumentException("type of unit unknown !");
        }

        this.unitToRegion.put(unit, r);
        this.units.add(unit);

        this.startPosition = null;
        unit.setRegion(r);


        return unit;
    }

    public Unit createDisbandUnit(Region r, Country c, UnitType typeOfUnit, Province disbandFrom) {
        Unit unit = null;

        switch (typeOfUnit) {
            case ARMY:
                unit = Unit.createArmy(c);
                break;
            case FLEET:
                unit = Unit.createShip(c);
                break;
        }


        this.unitToRegion.put(unit, r);
        this.units.add(unit);

        this.startPosition = null;
        unit.setRegion(r);


        return unit;
    }

    /**
     * Remove unit
     * @param u
     */
    public void deleteUnit(Unit u){
    	
    	this.units.remove(u);
    	Region r = this.unitToRegion.remove(u);
    	assert r != null;
    	
    	
    	this.refreshPositions();
    }
    
    public Region getRegion(Unit unit) {
        return unitToRegion.get(unit);
    }

    public boolean isOccupiedByUnit(Province p) {
        return getUnit(p) != null;
    }

    public boolean isOccupiedByUnit(Region r) {
        return isOccupiedByUnit(r.getProvince());
    }

    public Country getOwner(Supply s) {
        return supplyToCountry.get(s);
    }

    public void setOwner(Supply s, Country c) {
        supplyToCountry.put(s, c);
        //force refresh
        this.startPosition = null;
    }

    public int countSupply(int index){
    	return countSupply(Country.get(index));
    }
    
    /**
     * Count actual supplies owned by a power in the game.
     * @param c
     * @return
     */
    public int countSupply(Country c) {
        int n = 0;
        for (Supply s : Supply.getSupplies()) {
            if (this.controls(s, c)) {
                n++;
            }
        }

        return n;
    }
    
    public int countUnit(Country c){
    	int nb = 0;
    	for(Unit u : this.getUnits()){
    		if (u.getCountry().equals(c)){
    			nb++;
    		}
    	}
    	return nb;
    }
    
    public boolean needAdjust(Country c){
    	return (countUnit(c) != countSupply(c));
    }

    /**
     * Check if this board needs unit adjust
     * @return
     */
    public boolean needAdjust(){
    	for (Country c : Country.getCountries()){
    		if(needAdjust(c)) return true;
    	}
    	
    	return false;
    }
    public Collection<Supply> getSupplies(Country c) {
        return supplyToCountry.keySet();
    }


    /**
     * Test if a COuntyr
     * @param c
     * @param u
     * @return
     */
    public boolean controls(Country c, Unit u) {
        return u.getCountry().equals(c);
    }

    public boolean controls(Supply s, Country c) {
        return c.equals(getOwner(s));
    }

    public boolean hasSupply(Supply s, Country c) {
        if (c == null) {
            return false;
        }

        return c.equals(getOwner(s));
    }

    public boolean isSupplyHomeFree(Supply s) {

        return s.getOriginalOwner() != null && getUnit(s.getProvince()) == null;
    }

    public boolean hasUnit(Unit u, Country c) {
        return c.equals(u.getCountry());
    }

    public boolean needAdjustement(Country c) {
        return getAdjustDifference(c) != 0;
    }

    public boolean needAdjustement() {
        for (Country c : Country.getCountries()) {
            if (needAdjustement(c)) {
                return true;
            }
        }

        return false;
    }

    public int getAdjustDifference(Country c) {
        int nbSupply = countSupply(c);
        int nbUnit = getUnits(c).size();
        return nbSupply - nbUnit;
    }

    public int getAdjustDifference(Region r) {
        return getAdjustDifference(getOwningCountry(r));
    }

    public boolean needDisband(){
    	for (Unit u : this.getUnits()){
    		if (u.isMarkedAs(Mark.dislodged)) return true;
    	}
    	return false;
    }
    
    /**
     * 
     * */
    public Collection<Region> getRegionCanSupport(Unit uMove, Unit uSupport) {
        //
        List<Region> list = new ArrayList<Region>(8);

        //erreur d'unités
        if (uMove == null || uSupport == null) {
            return list;
        }

        //regions autour de l'unité
        Collection<Region> aroundMove = uMove.getGraph().getNode(uMove).getRegionsAround();

        //provinces autour du support
        Collection<Province> aroundSupport =
                uSupport.getGraph().getNode(uSupport).getProvincesAround();

        //compare les provinces
        for (Region region : aroundMove) {
            if (aroundSupport.contains(region.getProvince())) {
                list.add(region);
            }
        }

        return list;
    }

    public void refreshPositions(){
    	this.startPosition = null;
    	this.serializePositions();
    }
    
    /**
     * Move a unit from start to end.
     *
     * @param start
     * @param end
     */
    public void move(String start, String end) {
        Unit u = this.getUnit(start);
        Region dest = Region.get(end);
        this.moveUnit(u, dest);
    }

    public void moveUnit(Unit unit, Region dest) {
    	
        if (unit == null) {
            throw new IllegalArgumentException("unit cannot be null !");
        }
        if (dest == null) {
            throw new IllegalArgumentException("region cannot be null");
        }

        //no move
        if (dest.equals(unit.getRegion())) {
            return;
        }

        Region oldregion = this.unitToRegion.remove(unit);
        assert oldregion != null;

        assert !oldregion.equals(dest);

        //move
        //this.provinceToUnit.put(dest.getProvince(), u2move);
        this.unitToRegion.put(unit, dest);
        unit.setRegion(dest);
        unit.clearMarks();
        //force calculation of initial position
        this.startPosition = null;
    }

    
    /** Construit la chaine pour sérailiser le jeu */
    private void serializePositions() {
        StringBuilder sb = new StringBuilder();

        //---------------
        //Unités
        Iterator<Unit> unitIterator = this.getUnits().iterator();
        while (unitIterator.hasNext()) {
            //unité
            Unit u = unitIterator.next();
            //Pays
            sb.append(u.getCountry().getIndex());
            sb.append(":");
            //type d'unité
            sb.append(u.getCodeChar());
            sb.append(":");
            //position
            sb.append(u.getRegion());
            //mark
            if (u.getMarks() != null) {
                sb.append(":(");
                sb.append(u.getMarks());
                sb.append(")");
            }
            //disbandregion
            if (u.getPossibleRetreats().size() > 0 && u.isMarkedAs(Mark.dislodged)){
            	sb.append(":");
            	sb.append(u.getTextPossibleRetreats());
            }

            sb.append(";");
        }

        //------------
        //dépots
        Iterator<Supply> supplyIterator = Supply.getSupplies().iterator();
        while (supplyIterator.hasNext()) {
            //dépot
            Supply s = supplyIterator.next();
            //pays
            Country c = getOwner(s);
            if (c != null) {
                sb.append(c.getIndex());
                //type : supply
                sb.append(":S:");
                //région
                sb.append(s.getProvince().getShortName());
                //fin
                sb.append(";");
            }
        }

        //-----------------------------------

        //save units and supply
        this.startPosition = sb.toString();

        if (this.startPosition.endsWith(";")) {
            this.startPosition = this.startPosition.substring(0, this.startPosition.length() - 1);
        }

        //Ordres
        //sb.setLength(0);
        //Iterator it = this.getTourOrder().getTourOrderSet().get
    }

    /** Etablie un état du jeu d'aprés une chaine */
    private void unserializePositions() {

        if (startPosition == null) {
            return;
        }

        this.clearUnits();

        String[] words = this.startPosition.split(";");
        for (int i = 0; i < words.length; i++) {

            words[i] = words[i].trim();

            try {

                String[] w = words[i].split(":");
                if (w.length < 3) {
                    continue;
                }
                //pays
                Country c = Country.get(Integer.parseInt(w[0]));
                //region
                String regionName = w[2];
                Region region = Region.get(regionName);

                //unit type
                UnitType typeOfUnit = null;
                //Region base = Region.get(regionName);

                switch (w[1].charAt(0)) {
                    //ARMEE
                    case 'A':
                        typeOfUnit = UnitType.ARMY;
                        break;
                    //FLOTTE
                    case 'F':
                        typeOfUnit = UnitType.FLEET;
                        break;
                    //DEPOT
                    case 'S':
                        Supply s = Supply.getSupply(region.getProvince());
                        this.setOwner(s, c);
                        break;
                }


                if (typeOfUnit != null) {
                    //unit is not a supply
                    Unit u = this.createUnit(region, c, typeOfUnit);
                    
                    if (w.length >3 && w[3].length() > 2){
                        String marks = w[3];
                        marks = marks.substring(1,marks.length() - 1);
                        String [] ms = marks.split(",");
                        for(String m : ms){
                        	Mark mark = Mark.valueOf(m);
                        	u.markAs(mark);
                        }
                    }
                    
                    if (w.length>4){
                    	u.getPossibleRetreats().clear();
                    	for(String rn : w[4].split(",")){
                    	Region r = Region.get(rn);
                    	assert r != null;
                    	u.getPossibleRetreats().add(r);
                    	}
                    }
                }



            } catch (Exception ex) {
                throw new IllegalArgumentException(
                        "Error in creating unit : " + words[i], ex);
            }

        }

        /*
        if (this.getOrderSet().getOrders().size() == 0
        && this.persistOrders != null){
        this.getOrderSet().unserialize(persistOrders);
        } */

        this.serializePositions();
        //this.locked = true;
    }
}

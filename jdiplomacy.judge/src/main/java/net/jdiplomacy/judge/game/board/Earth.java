package net.jdiplomacy.judge.game.board;


import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.UnitType;


/**
 * 
 * @author JBVovau
 *
 */
public final class Earth extends Region
{

	public Earth(Province province){
		super(province);
	}
	
	/* (non-Javadoc)
	 * @see diplomacy.province.Region#acceptUnit(diplomacy.province.Unit)
	 */
	@Override
	public boolean acceptUnit(Unit unit) {
		//Les region de type Terre n'acceptent que les armées
		return (unit.getType() == UnitType.ARMY);
	}

	@Override
	public boolean acceptSupply(){
		//
		return getProvince().getSupply() != null;

	}
	
	@Override
	public boolean acceptUnit(UnitType type){
		return type == UnitType.ARMY;
	}

	/* (non-Javadoc)
	 * @see diplomacy.board.Region#isConvoyPath()
	 */
	@Override
	public boolean isConvoyPath() {
		//terre : convoi impossible
		return false;
	}

	/* (non-Javadoc)
	 * @see diplomacy.board.Region#isConvoyStop()
	 */
	@Override
	public boolean isConvoyStop() {
		return false;
	}


}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge.game;

/**
 * Kind of phase to judge.
 * @author Jean-Baptiste
 */
public enum PhaseType {

    movement,
    disband,
    adjust
}

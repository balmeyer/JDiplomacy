package net.jdiplomacy.judge.game.board;

/*
 * Created on 10 avr. 2004
 *
 * Vovau Jean-Baptiste - balmeyer@dev.java.net
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import net.jdiplomacy.judge.JudgeEngine;

/**
 * Objet pays.
 * 
 * Created on 10 avr. 2004.
 * 
 * @author Jean-Baptiste Vovau - balmeyer@dev.java.net
 * @version 1.0
 */
@XmlRootElement(name="country")
public final class Country {

    private final static List<Country> countryList = new ArrayList<Country>(7);
    private final static Map<String, Country> nameToCountry = new HashMap<String, Country>(7);
    /**
     * Initialisation de la partie statique
     */
    private Collection<Supply> homeSupplies;
    
    /** Nom du pays. */
    private String name;
    private int countryIndex = 0;

    public static void clear(){
    	countryList.clear();
    	nameToCountry.clear();

    }
    

    /**
     * Création d'un objet pays d'après son nom
     * @param name
     * @return
     */
    public static Country build(String name) {
        Country c = get(name);
        if (c == null) {
            c = new Country(name);
            //index
            c.setIndex(countryList.size());
            countryList.add(c);
            nameToCountry.put(name.toLowerCase(), c);

        }

        return c;
    }

    public static Country get(String name) {
        return nameToCountry.get(name.toLowerCase());
    }

    /**
     * Retourne un pays depuis son index
     * @param index
     * @return
     */
    public static Country get(int index) {
    	
    	if (countryList.size() == 0) {
    		JudgeEngine.initGame();    	
    	}
    	
    	if (index >= countryList.size()){
    		throw new IndexOutOfBoundsException("Countries has not been initialized");
    	
    	}
    	
        return countryList.get(index);
    }

    /**
     * Liste de tous les pays
     * @return
     */
    public static Collection<Country> getCountries() {
        return countryList;
    }

    private Country(String name) {
        this.name = name;

    }

    /**
     * Retourne les dépots originels.
     */
    public Collection<Supply> getHomeSupplies() {
        if (this.homeSupplies == null || homeSupplies.size() == 0) {
            this.homeSupplies = new ArrayList<Supply>(4);
            Iterator<Supply> it = Supply.getSupplies().iterator();
            while (it.hasNext()) {
                Supply s = it.next();
                if (this.equals(s.getOriginalOwner())) {
                    homeSupplies.add(s);
                }
            }
        }

        return homeSupplies;
    }

    @XmlAttribute(name="name")
    public String getGameName() {
        return this.name;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof Country
                && ((Country) obj).getGameName().equals(this.name)) {
            return true;
        }

        return false;
    }

    /**
     * Identifiant num�rique du pays (0-6)
     * @return
     */
    public int getIndex() {
        return countryIndex;
    }

    private void setIndex(int i) {
        this.countryIndex = i;
    }
}

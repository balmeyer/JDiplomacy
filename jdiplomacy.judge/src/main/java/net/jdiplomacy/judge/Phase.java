/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge;

/**
 *
 * @author Jean-Baptiste
 */
public enum Phase {
    movementSpring,
    disbandSpring,
    movementFall,
    disbandFall,
    adjust,
    gameOver //when game is finished
}

package net.jdiplomacy.judge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import net.jdiplomacy.judge.game.board.Country;

import org.junit.Test;

public class TestGameOver {

	@Test
	public void testGameOver(){
		
		String start = "0:F:Adr;1:F:Nwy;2:A:Ber;2:A:Den;2:F:Aeg;3:F:Bal;"
			+ "4:A:Bud;4:F:Nap;5:A:Lvn;5:F:StP-S;5:F:Sev;6:A:Con;6:A:Smy;"
			+ "6:F:Ank;2:A:Kie;2:A:Tri;1:F:Nth;2:A:Mun;2:A:Lon;2:A:Tyr;"
			+ "2:A:Bel;2:A:Cly;2:F:GoL;2:A:Gas;2:A:Pic;4:S:Bud;2:S:Tri;"
			+ "0:S:Vie;1:S:Edi;2:S:Lvp;2:S:Lon;2:S:Bre;2:S:Mar;2:S:Par;"
			+ "2:S:Ber;2:S:Mun;2:S:Kie;4:S:Nap;4:S:Rom;2:S:Ven;5:S:Mos;"
			+ "5:S:Sev;5:S:StP;3:S:War;6:S:Ank;6:S:Con;6:S:Smy;2:S:Bel;"
			+ "2:S:Den;2:S:Hol;1:S:Nwy;2:S:Por;2:S:Spa;2:S:Tun";
		
		String orders = "A Tyr - Vie ; F Aeg - Gre";
		
		JudgeEngine je = new JudgeEngine();
		
		JudgeResult r = je.judge(start, orders, Phase.movementFall);
		
		assertNotSame(0, r.getWinList());
		assertEquals(7, r.getWinList().size());
		assertEquals(Country.get(2), r.getWinList().get(0));
		assertEquals(Phase.gameOver, r.getNextPhase());
		
		try {
			r = je.judge(r.getFinalPosition(), "", r.getNextPhase());
			assertFalse("must throw exception",true);
		} catch (Exception ex){
			
		}
	}
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import net.jdiplomacy.judge.game.Board;
import net.jdiplomacy.judge.game.SupportingDataStructure;
import net.jdiplomacy.judge.game.board.Province;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
/**
 *
 * @author Jean-Baptiste
 */
public class TestSupportingDataStructure {

    public TestSupportingDataStructure() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    @Test
    public void testInitialStructure(){

        //JudgeEngine je = new JudgeEngine();

        Board b = new Board();

        SupportingDataStructure sds = new SupportingDataStructure(b);

        for (Province p : Province.getProvinceList()){
            assertNotNull(sds.getCombatList(p));
            assertEquals(0 , sds.getCombatListSize(p));
        }

        assertNotNull(sds.getConvoySucceededList());
        assertNotNull(sds.getConvoyingArmies());
        assertEquals(0, sds.getConvoySucceededListSize());
        assertEquals(0, sds.getConvoyingArmies().size());

    }

}
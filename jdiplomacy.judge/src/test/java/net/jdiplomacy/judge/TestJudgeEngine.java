/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.xml.bind.JAXBException;


import net.jdiplomacy.judge.JudgeEngine;
import net.jdiplomacy.judge.JudgeResult;
import net.jdiplomacy.judge.Mark;
import net.jdiplomacy.judge.Phase;
import net.jdiplomacy.judge.game.Board;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.game.board.Province;
import net.jdiplomacy.judge.order.Order;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Jean-Baptiste
 */
public class TestJudgeEngine {

    public static final String FIRST_POSITION = "0:A:Vie;0:A:Bud;0:F:Tri;1:A:Lvp;1:F:Lon;1:F:Edi;"
            + "2:A:Par;2:A:Mar;2:F:Bre;3:A:Mun;3:A:Ber;3:F:Kie;4:A:Rom;4:A:Ven;"
            + "4:F:Nap;5:A:Mos;5:A:War;5:F:Stp-S;5:F:Sev;6:A:Con;6:A:Smy;6:F:Ank;"
            + "0:S:Bud;0:S:Tri;0:S:Vie;1:S:Edi;1:S:Lvp;1:S:Lon;2:S:Bre;2:S:Mar;"
            + "2:S:Par;3:S:Ber;3:S:Mun;3:S:Kie;4:S:Nap;4:S:Rom;4:S:Ven;5:S:Mos;"
            + "5:S:Sev;5:S:StP;5:S:War;6:S:Ank;6:S:Con;6:S:Smy;";

    public TestJudgeEngine() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testStartingBoard(){
    	Board b = new Board(FIRST_POSITION);
    	
        assertFalse(b.needAdjust());
        assertEquals(3, b.countSupply(Country.get(0)));
        assertEquals(3, b.countSupply(Country.get(1)));
        assertEquals(3, b.countSupply(Country.get(2)));
        assertEquals(3, b.countSupply(Country.get(3)));
        assertEquals(3, b.countSupply(Country.get(4)));
        assertEquals(4, b.countSupply(Country.get(5)));
        assertEquals(3, b.countSupply(Country.get(6)));
        
        assertEquals(3,b.countUnit(Country.get(0)));
        assertEquals(3,b.countUnit(Country.get(1)));
        assertEquals(3,b.countUnit(Country.get(2)));
        assertEquals(3,b.countUnit(Country.get(3)));
        assertEquals(3,b.countUnit(Country.get(4)));
        assertEquals(4,b.countUnit(Country.get(5)));
        assertEquals(3,b.countUnit(Country.get(6)));
    }
    
    @Test
    public void testBadConvoy(){
        JudgeEngine engine = new JudgeEngine();

        JudgeResult result = engine.judge(FIRST_POSITION, "A mar - gre", Phase.movementSpring);
        
        Unit gre = result.getBoard().getUnit("gre");
        Unit mar = result.getBoard().getUnit("mar");
        
        assertNotNull(mar);
        assertNull(gre);
        
        for( Order o : result.getOrders()){
        	assertFalse(o.isSuccess());
        }
        
        
        /////////////////
        String posi = "0:A:Mar;0:F:Gol";
        String orders = "A Mar - Naf ; F Gol C Mar - Naf";

        result = engine.judge(posi, orders, Phase.movementSpring);
        gre = result.getBoard().getUnit("naf");
        mar = result.getBoard().getUnit("mar");
        
        assertNotNull(mar);
        assertNull(gre);
        
        for( Order o : result.getOrders()){
        	if (!o.isConvoyedMove()) continue;
        	assertFalse(o.isSuccess());
        }
        
        ///////////////////
        posi = "0:A:Mar;0:F:Gol;0:F:Wes";
        orders = "A Mar - Naf ; F Gol C Mar - Naf ; F Wes C Mar - Bur";

        result = engine.judge(posi, orders, Phase.movementSpring);
        gre = result.getBoard().getUnit("naf");
        mar = result.getBoard().getUnit("mar");
        
        assertNotNull(mar);
        assertNull(gre);
        
        for( Order o : result.getOrders()){
        	if (!o.isConvoyedMove()) continue;
        	assertFalse(o.isSuccess());
        }
        
        ///////////////////
        //Good order
        posi = "0:A:Mar;0:F:Gol;0:F:Wes;1:F:Tyn";
        orders = "A Mar - Naf ; F Gol C Mar - Naf ; F Wes C Mar - NAf ; F Tyn C Mar - Par";

        result = engine.judge(posi, orders, Phase.movementSpring);
        gre = result.getBoard().getUnit("naf");
        mar = result.getBoard().getUnit("mar");
        
        assertNull(mar); //success
        assertNotNull(gre);
        
        for( Order o : result.getOrders()){
        	if (!o.isConvoyedMove()) continue;
        	assertTrue(o.isSuccess());
        }
        
        ///////////////////
        //Good order
        posi = "0:A:Mar;0:F:Gol;0:F:Wes;1:F:Tyn;2:F:Mid";
        orders = "A Mar - Naf ; F Mid C Mar - Naf ; F Wes C Mar - NAf ; "
        	+ "F Tyn C Mar - Naf; F Gol C Mar - Naf";

        result = engine.judge(posi, orders, Phase.movementSpring);
        gre = result.getBoard().getUnit("naf");
        mar = result.getBoard().getUnit("mar");
        
        assertNull(mar); //success
        assertNotNull(gre);
        
        for( Order o : result.getOrders()){
        	if (!o.isConvoyedMove()) continue;
        	assertTrue(o.isSuccess());
        }
    }
    
    @Test
    public void testSimpleMove() {

        JudgeEngine engine = new JudgeEngine();


        JudgeResult result = engine.judge(FIRST_POSITION, "A mar - spa ; F Bre - eng", Phase.movementSpring);

        assertNotNull(result);


        //same units

        Board board = result.getBoard();

        assertNotNull(board);
        assertNotNull(result.getOrders());

        assertNotNull(board.getUnit("spa"));
        assertNull(board.getUnit("mar"));

        //orders
        for (Order o : result.getOrders()) {
            assertTrue(o.isCorrect());
            assertNotNull(o.getResultRegion());
            assertEquals(o.getEndRegion(), o.getResultRegion());
        }
    }

    @Test
    public void testXmlResult() throws JAXBException, IOException {

        JudgeEngine engine = new JudgeEngine();
        JudgeResult result = engine.judge(FIRST_POSITION, "A mar - spa ; F Bre - eng", Phase.movementSpring);
        
        JudgeResult.toXmlFile(result, "C:\\item.xml");




    }

    @Test
    public void testBounces1() {

        JudgeEngine engine = new JudgeEngine();

        String firstPosition = "0:A:Mar ; 0:A:Bur";
        String orders = "A Mar - Bur ; A Bur - Mar";

        JudgeResult result = engine.judge(firstPosition, orders, Phase.movementSpring);

        for (Order o : result.getOrders()) {
            assertFalse(o.isSuccess());
            assertFalse(o.isMoveValidated());
            assertEquals(o.getBaseRegion(), o.getResultRegion());
            Unit u = o.getUnit();
            assertTrue(u.isMarkedAs(Mark.bounce));
            assertNotSame(0, u.getPossibleRetreats());
        }

        String resultOrders = result.getTextOrders(true);
        for(String no : resultOrders.split(";")){
        	String [] ws = no.split(":");
        	assertEquals(2,ws.length);
        }

    }

    @Test
    public void testBounces2() {

        JudgeEngine engine = new JudgeEngine();

        String firstPosition = "0:A:Mar ; 0:A:Gas";
        String orders = "A Mar - Spa ; A Gas - Spa";

        JudgeResult result = engine.judge(firstPosition, orders, Phase.movementSpring);

        for (Order o : result.getOrders()) {
            assertFalse(o.isSuccess());
            assertFalse(o.isMoveValidated());
            assertEquals(o.getBaseRegion(), o.getResultRegion());
            Unit u = o.getUnit();
            assertTrue(u.isMarkedAs(Mark.bounce));

        }


    }

    @Test
    public void testBouncesNoSupport() {

        JudgeEngine engine = new JudgeEngine();

        String firstPosition = "0:A:Mar ; 0:A:Gas";
        String orders = "A Mar - Spa ; A Gas - Spa";

        JudgeResult result = engine.judge(firstPosition, orders, Phase.movementSpring);

        for (Order o : result.getOrders()) {
            assertFalse(o.isSuccess());
            assertFalse(o.isMoveValidated());
            assertEquals(o.getBaseRegion(), o.getResultRegion());
            Unit u = o.getUnit();
            assertTrue(u.isMarkedAs(Mark.bounce));
        }


        //same with different country
        firstPosition = "0:A:Mar ; 1:A:Gas";
        orders = "A Mar - Spa ; A Gas - Spa";


        result = engine.judge(firstPosition, orders, Phase.movementSpring);

        for (Order o : result.getOrders()) {
            assertFalse(o.isSuccess());
            assertFalse(o.isMoveValidated());
            assertEquals(o.getBaseRegion(), o.getResultRegion());
            Unit u = o.getUnit();
            assertTrue(u.isMarkedAs(Mark.bounce));
        }


    }

    @Test
    public void testBounceOnNonMovingUnit() throws Exception, IOException {
        JudgeEngine engine = new JudgeEngine();

        String firstPosition = "0:A:Mar;3:A:Spa";
        String orders = "A Mar - Spa";

        JudgeResult result = engine.judge(firstPosition, orders, Phase.movementSpring);

        for (Order o : result.getOrders()) {
            assertFalse(o.isSuccess());
            assertFalse(o.isMoveValidated());
            assertEquals(o.getBaseRegion(), o.getResultRegion());
            Unit u = o.getUnit();
            assertTrue(u.isMarkedAs(Mark.bounce));
        }

        assertNotNull(result.getBoard().getUnit("spa"));
        assertNotNull(result.getBoard().getUnit("mar"));
        assertEquals(Country.get(0), result.getBoard().getUnit("mar").getCountry());
        assertEquals(Country.get(3), result.getBoard().getUnit("spa").getCountry());

        assertNotSame(result.getInitialPosition(), result.getFinalPosition());
        
        JudgeResult.toXmlFile(result, "C:\\bounce.xml");
        
    }

    @Test
    public void testBounceWithOneSupport() throws JAXBException, IOException {
        JudgeEngine engine = new JudgeEngine();

        String firstPosition = "0:A:Gas;3:A:Mar;4:A:Por";
        String orders = "A Mar - Spa ; A Gas - Spa ; A Por S Mar - Spa";

        JudgeResult result = engine.judge(firstPosition, orders, Phase.movementSpring);

        Unit bounced = result.getBoard().getUnit("Gas");
        assertNotNull(bounced);
        assertTrue(bounced.isMarkedAs(Mark.bounce));

        Unit moving = result.getBoard().getUnit("Spa");
        assertNotNull(moving);
        assertNull(result.getBoard().getUnit("Mar"));
        assertNull(result.getBoard().getUnit("Spa").getMarks());
        assertEquals(Country.get(3), moving.getCountry());

        assertNotNull(result.getBoard().getUnit("Por"));
        assertNull(result.getBoard().getUnit("Por").getMarks());
        assertNotNull(result.getBoard().getStartPosition());

        //test persistence of marks
        Board newBoard = new Board(result.getBoard().getStartPosition());
        assertNotNull(newBoard.getStartPosition());
        assertEquals(result.getBoard().getStartPosition() , newBoard.getStartPosition());
        Unit newBounced = newBoard.getUnit("Gas");
        assertTrue(newBounced.isMarkedAs(Mark.bounce));
        

        assertFalse(result.isDislodgedUnit());
        
        JudgeResult.toXmlFile(result, "C:\\bounce2.xml");
        
    }

    @Test
    public void testDislodge(){
        JudgeEngine engine = new JudgeEngine();

        String firstPosition = "0:A:Gas;3:A:Mar;4:A:Spa";
        String orders = "A Gas - Spa ; A Mar S Gas - Spa";
        
        JudgeResult r = engine.judge(firstPosition, orders, Phase.movementSpring);
        
        assertTrue(r.isDislodgedUnit());
        assertEquals(Phase.disbandSpring, r.getNextPhase());
        
        String pos = r.getFinalPosition();
        assertNotNull(pos);
        
        Unit supporitng = r.getBoard().getUnit("mar");
        Unit moving = r.getBoard().getUnit("gas");
        assertNotNull(supporitng);
        assertNull(moving);
        
        Unit spaNew = r.getBoard().getUnit("spa");
        Unit spaOld = r.getBoard().getUnit(Province.get("spa"), true);
        assertTrue(spaOld.isMarkedAs(Mark.dislodged));
        assertFalse(spaNew.isMarkedAs(Mark.dislodged));
        
        
    }
    
    /**
     * Test move
     */
    @Test
    public void testQueue(){
    	JudgeEngine engine = new JudgeEngine();
    	
    	String start = "0:A:Rum;6:A:Bul";
    	String orders = "A Rum - Bul ; A Bul - Gre";
    	
    	JudgeResult result = engine.judge(start, orders, Phase.movementSpring);
    	
    	Unit rum = result.getBoard().getUnit("rum");
    	Unit bul = result.getBoard().getUnit("bul");
    	Unit gre = result.getBoard().getUnit("gre");
    	
    	assertNull(rum);
    	assertNotNull(bul);
    	assertNotNull(gre);
    	
    	assertEquals(Country.get(0) , bul.getCountry());
    	assertEquals(Country.get(6), gre.getCountry());
    }
    

    @Test
    public void testBigBounce(){
    	/*
    	 * Autriche
    	 * A TRI-BUD
			A SER S A TRI-BUD
			
			Allemagne & Turquie
			A GAL-BUD (A)
			A ROU S A GAL-BUD (T)
			
			Italie & France
			A BUD-TRI (I)
			A VIE S A BUD-TRI (F)
    	 */
    	String pos = "0:A:Tri;0:A:Ser;3:A:Gal;6:A:Rum;3:A:Bud;2:A:Vie";
    	String orders = "A Tri - Bud ; A Ser S Tri - Bud ; A Gal - Bud;"
    		+ "A Rum S Gal - Bud ; A Bud - Tri ; A Vie S Bud - Tri";
    	
    	JudgeEngine je = new JudgeEngine();
    	JudgeResult r = je.judge(pos, orders, Phase.movementSpring);
    	
    	for (Order o : r.getOrders()){
    		Unit u = o.getUnit();
    		assertNotNull(u);
    	
    	}
    	Unit aTri = r.getBoard().getUnit("tri", false);
    	Unit aBud = r.getBoard().getUnit("bud", false);
    	Unit aGalGermany = r.getBoard().getUnit("gal", false);
    	assertNotNull(aTri);
    	assertNotNull(aBud);
    	assertNotNull(aGalGermany);
    	//assertEquals("",r.getFinalPosition());
    	
    	assertEquals(Country.get(3), aGalGermany.getCountry());
    	
    	//tricky...
    	assertEquals(Country.get(3) , aBud.getCountry());
    	assertEquals(Country.get(0), aTri.getCountry());
    }
}

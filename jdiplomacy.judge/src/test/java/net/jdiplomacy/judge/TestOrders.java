/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import net.jdiplomacy.judge.game.Board;
import net.jdiplomacy.judge.game.OrderType;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.game.board.Province;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.order.Order;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
/**
 *
 * @author Jean-Baptiste
 */
public class TestOrders {

    public TestOrders() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    @SuppressWarnings("unused")
	@Test
    public void testSingleOrder(){
        JudgeEngine je = new JudgeEngine();

        String startPosition = "2:A:Vie";
        String singleOrder = "A Vie - Pie";

        Board b = new Board();
        b.setStartPosition(startPosition);

        //test unit
        Unit u = b.getUnit(Region.get("Vie"));
        assertNotNull(u);

        //Test order
        Order o = Order.create(b, singleOrder,false);

        assertNotNull(o);
        assertEquals(singleOrder, o.getText());
        assertEquals(u , o.getUnit());
        assertEquals(Region.get("Vie"), o.getStartRegion());
        assertEquals(Region.get("Vie"), o.getBaseRegion());
        assertEquals(Region.get("Pie"), o.getEndRegion());

        assertEquals(Country.get(2), o.getCountry());

        assertEquals(OrderType.movement,o.getOrderType());

        assertTrue(o.isCorrect());


        //bad orders
        Order bad1 = Order.create(b, "",false);
        assertFalse(bad1.isCorrect());

        Order bad = Order.create(b, "DUMMY ORDER",false);
        assertFalse(bad.isCorrect());

        Order badRegion = Order.create(b,"F tru - adz",false);
        assertFalse(bad.isCorrect());
        assertNull(badRegion.getBaseRegion());

    }

    @Test
    public void testIncorrectOrder() {
        //JudgeEngine je = new JudgeEngine();

        String startPosition = "0:A:Vie";

        //
        String singleOrder = "A Vie - ZZZ";

        Board b = new Board();
        b.setStartPosition(startPosition);

        Order o = Order.create(b , singleOrder,false);

        //assertFalse(o.isCorrect());

        o = Order.create(b, "Z Vie - Bre",false);
        assertFalse(o.isCorrect());

        o = Order.create(b, "F ZZZ - Vie",false);
        assertFalse(o.isCorrect());

        o = Order.create(b, "F ZZZ - ZZZ",false);
        assertFalse(o.isCorrect());

    }


    @Test
    public void testFailConvoyOrder(){
        JudgeEngine je = new JudgeEngine();

        //keep marks
        JudgeEngine.options = JudgeOptions.failingSupportOrConvoyAreMarked;
        
        String startPosition = "0:A:Gas";
        String order = "A Gas - Por [Mid]" ;
        
        JudgeResult result = je.judge(startPosition, order, Phase.movementSpring);
        
        for (Order o : result.getOrders()){
        	assertTrue(o.isConvoyedMove());
        	assertEquals(1 , o.getConvoiPath().size());
        	assertEquals(Region.get("mid") , o.getConvoiPath().get(0));
        	assertEquals(Mark.no_convoy.toString() , o.getUnit().getMarks());
        	assertTrue(o.getUnit().isMarkedAs(Mark.no_convoy));
        }
    }

    @Test
    public void testConvoy(){
        JudgeEngine je = new JudgeEngine();

        //keep marks
        JudgeEngine.options = JudgeOptions.failingSupportOrConvoyAreMarked;
        
        String startPosition = "0:A:Mar;2:F:Gol;2:F:Wes";
        String order = "A Mar - Naf [Gol,Wes]; F Gol C Mar - Naf ; F Wes C Mar - Naf" ;
        
        JudgeResult result = je.judge(startPosition, order, Phase.movementSpring);
        Unit mar = result.getBoard().getUnit("mar");
        Unit naf = result.getBoard().getUnit("naf");
        
        assertNull(mar);
        assertNotNull(naf);
        for (Order o : result.getOrders()){
        	assertTrue(o.isSuccess());
        }
    }
    

    @Test
    public void testConvoyWithoutPath(){
        JudgeEngine je = new JudgeEngine();

        //keep marks
        JudgeEngine.options = JudgeOptions.failingSupportOrConvoyAreMarked;
        
        
        //
        
        String startPosition = "0:A:Mar;2:F:Gol;2:F:Wes";
        String order = "A Mar - Naf; F Gol C Mar - Naf ; F Wes C Mar - Naf" ;
        
        JudgeResult result = je.judge(startPosition, order, Phase.movementSpring);
        Unit mar = result.getBoard().getUnit("mar");
        Unit naf = result.getBoard().getUnit("naf");
        
        assertNull(mar);
        assertNotNull(naf);
        for (Order o : result.getOrders()){
        	assertTrue(o.isSuccess());
        	
        	if (o.getBaseRegion().equals(Region.get("mar"))){
        		assertTrue(o.isConvoyedMove());
        	}
        	
        	if (o.getResultRegion().equals(naf.getRegion())){
        		assertTrue(o.getConvoiPath().size() > 0);
        	}
        }
    }
    
    
    @Test
    public void testNotCutConvoy(){
        JudgeEngine je = new JudgeEngine();

        //keep marks
        JudgeEngine.options = JudgeOptions.failingSupportOrConvoyAreMarked;
        
        String startPosition = "0:A:Mar;2:F:Gol;2:F:Wes;3:F:Pie";
        //support not dislodged
        String order = "A Mar - Naf [Gol,Wes]; F Gol C Mar - Naf ; F Wes C Mar - Naf;F Pie - Gol" ;
        
        JudgeResult result = je.judge(startPosition, order, Phase.movementSpring);
        Unit mar = result.getBoard().getUnit("mar");
        Unit naf = result.getBoard().getUnit("naf");
        Unit pie = result.getBoard().getUnit("pie");
        
        assertNull(mar);
        assertNotNull(naf);
        assertNotNull(pie);
        
        for (Order o : result.getOrders()){
        	if (o.getUnit().equals(pie)) continue;
        	assertTrue( o.toString() ,o.isSuccess());
        }
    }
    

    @Test
    public void testRealCutConvoy(){
        JudgeEngine je = new JudgeEngine();

        //keep marks
        JudgeEngine.options = JudgeOptions.failingSupportOrConvoyAreMarked;
        
        String startPosition = "0:A:Mar;2:F:Gol;2:F:Wes;3:F:Pie;4:F:Tyn";
        //support not dislodged
        String order = "A Mar - Naf [Gol,Wes]; F Gol C Mar - Naf ; F Wes C Mar - Naf;F Pie - Gol" 
        	+ ";F Tyn S Pie - Gol";
        
        JudgeResult result = je.judge(startPosition, order, Phase.movementSpring);
        
        Unit mar = result.getBoard().getUnit("mar");
        Unit naf = result.getBoard().getUnit("naf");
        Unit pie = result.getBoard().getUnit("pie");
        
        Unit golDis = result.getBoard().getUnit(Province.get("Gol"), true);
        Unit gol =  result.getBoard().getUnit(Province.get("Gol"), false);
        
        assertNotNull(mar);
        assertNull(naf);
        assertNull(pie);
        assertNotNull(golDis);
        assertNotNull(gol);
        assertEquals(Country.get(2), golDis.getCountry());
        assertEquals(Country.get(3), gol.getCountry());
        

    }
    
    @Test
    public void testBounceConvoy(){
        JudgeEngine je = new JudgeEngine();

        //keep marks
        JudgeEngine.options = JudgeOptions.failingSupportOrConvoyAreMarked;
        
        String startPosition = "0:A:Mar;2:F:Gol;2:F:Wes;4:F:Tyn;6:A:Tun";
        //support not dislodged
        String order = "A Mar - Naf [Gol,Wes]; F Gol C Mar - Naf ; "
        	+ "F Wes C Mar - Naf;A Tun - Naf" ;
        
        JudgeResult result = je.judge(startPosition, order, Phase.movementSpring);
        
        Unit mar = result.getBoard().getUnit("mar");
        Unit naf = result.getBoard().getUnit("naf");
        Unit tun = result.getBoard().getUnit("tun");
        

        assertNotNull(mar);
        assertNull(naf);
        assertNotNull(tun);


        

    }
    
    @Test
    public void testBounces(){
        JudgeEngine je = new JudgeEngine();

        //keep marks
        JudgeEngine.options = JudgeOptions.failingSupportOrConvoyAreMarked;
        
        String startPosition = "2:A:Par:(bounce);2:A:Mar;3:A:Ruh";
        String order = "A Par - Bur;A Mar S Par - Bur;A Ruh - Bur" ;
        
        JudgeResult result = je.judge(startPosition, order, Phase.movementSpring);
        
        Unit par = result.getBoard().getUnit("par");
        Unit mar = result.getBoard().getUnit("mar");
        Unit ruh = result.getBoard().getUnit("ruh");
        Unit bur = result.getBoard().getUnit("bur");
        
        assertNull(par);
        assertNotNull(mar);
        assertNotNull(ruh);
        assertNotNull(bur);
        assertTrue(ruh.isMarkedAs(Mark.bounce));
        assertEquals(Phase.movementFall, result.getNextPhase());
        
    }

}
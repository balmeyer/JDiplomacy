package net.jdiplomacy.judge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import net.jdiplomacy.judge.game.Board;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.UnitType;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.game.board.Supply;
import net.jdiplomacy.judge.order.Order;

import org.junit.Test;

public class TestAdjust {

	@Test
	public void testAddSupply(){
		JudgeEngine engine = new JudgeEngine();
		
		String startPositions = "2:A:Spa;2:S:Par";
		String orders = "A Spa - Por";
	
		//before
		Board bEfore = new Board(startPositions);
		Supply s = Supply.getSupply("Por");
		assertNull(s.getOriginalOwner());
		assertNull(bEfore.getOwner(s));
		assertEquals(1, bEfore.countSupply(Country.get(2)));
		assertFalse(bEfore.needAdjust());
		
		JudgeResult result = engine.judge(startPositions, orders, Phase.movementFall);
		
		assertEquals(2, result.getBoard().countSupply(Country.get(2)));
		assertEquals(Country.get(2), result.getBoard().getOwner(s));
		assertTrue(result.getBoard().needAdjust());
		
		assertEquals(Phase.adjust, result.getNextPhase());
		
		//order
		String addOrder = "+ A Par";
		JudgeResult r2 = engine.judge(result.getBoard().getStartPosition(),
				addOrder, Phase.adjust);
		assertNotNull(r2.getBoard().getUnit("par"));
		for(Order o : r2.getOrders()){
			assertTrue(o.isSuccess());
			assertEquals(addOrder, o.getText());
		}
		assertEquals(2, r2.getBoard().countUnit(Country.get(2)));
		
	
	}
	
	@Test
	public void testAdjdOrder(){
		JudgeEngine engine = new JudgeEngine();
		
		String startPositions = "2:A:Par;2:S:Par;2:S:Mar;2:S:Bre";
		String orders = "+ A Mar;+ F Bre;+A Mid;+ F War;+A Par;+A Por";
		
		JudgeResult result = engine.judge(startPositions, orders, Phase.adjust);
		
		//3 units successfully added
		assertEquals(3 , result.getUnits().size());
		
		Unit armyCreated = result.getBoard().getUnit("mar");
		Unit fleetCreated = result.getBoard().getUnit("bre");
		
		assertNotNull(armyCreated);
		assertNotNull(fleetCreated);
				
		assertEquals(UnitType.ARMY , armyCreated.getType());
		assertEquals(UnitType.FLEET, fleetCreated.getType());
		
		assertEquals(Country.get(2) , armyCreated.getCountry());
		assertEquals(Country.get(2) , fleetCreated.getCountry());
		
		//test fails
		
		try {
			JudgeResult.toXmlFile(result , "C:\\adjust.xml");
		} catch (JAXBException e) {
			e.printStackTrace();
			assertNotNull(null);
		} catch (IOException e) {
			e.printStackTrace();
			assertNotNull(null);
		}
		
	}
	
	@Test
	public void testDelOrder(){
		JudgeEngine engine = new JudgeEngine();
		
		String positions = "2:A:Par;2:A:Bre;2:S:Par";
		
		String orders = "+ A Mar";
		
		//
		JudgeResult r = engine.judge(positions, orders, Phase.adjust);
		for(Order o : r.getOrders()){
			assertFalse(o.isSuccess());
		}
		
		assertEquals(1, r.getBoard().getUnits().size());
		
		
		//
		orders = "- A Mar";
		
		r = engine.judge(positions, orders, Phase.adjust);
		for(Order o : r.getOrders()){
			assertFalse(o.isSuccess());
		}
		
		assertEquals(1, r.getBoard().getUnits().size());
		
		//
		orders = "- A Par";
		
		r = engine.judge(positions, orders, Phase.adjust);
		for(Order o : r.getOrders()){
			assertTrue(o.isSuccess());
		}
		
		assertEquals(1, r.getBoard().getUnits().size());
		Unit mar = r.getBoard().getUnit("mar");
		Unit bre = r.getBoard().getUnit("bre");
		Unit par = r.getBoard().getUnit("par");
		assertNull(mar);
		assertNull(par);
		assertNotNull(bre);
		
		//
		positions = "2:A:Par;2:A:Bre;2:F:Mar;2:A:Spa;2:S:Par";
		orders = "- A Spa ; + A Por ; + A War ; + A Mos";
		
		r = engine.judge(positions, orders, Phase.adjust);
		for(Order o : r.getOrders()){
			assertFalse(o.isSuccess());
		}
		
		assertEquals(1, r.getBoard().getUnits().size());

		
		positions = "2:A:Par;2:A:Bre;2:F:Mar;2:A:Spa;2:S:Par";
		orders = "- A Par";
		
		r = engine.judge(positions, orders, Phase.adjust);
		for(Order o : r.getOrders()){
			assertTrue(o.isSuccess());
		}
		
		assertEquals(1, r.getBoard().getUnits().size());
	}
	
	@Test
	public void testStolenSupply(){
		String positions = "2:A:Par;2:S:Par;2:S:Mar;5:S:War;5:S:SeV";
		
		String orders = "";
		JudgeEngine e = new JudgeEngine();
		JudgeResult r = e.judge(positions, orders, Phase.movementFall);
	
		assertEquals(2, r.getBoard().countSupply(2));
		assertEquals(2, r.getBoard().countSupply(5));
		
		
	
	}
	
}

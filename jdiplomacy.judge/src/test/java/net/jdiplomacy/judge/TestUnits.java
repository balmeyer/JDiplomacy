/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import net.jdiplomacy.judge.game.Board;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.UnitType;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.game.board.Region;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Jean-Baptiste
 */
public class TestUnits {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testBoard() {
        //JudgeEngine engine = new JudgeEngine();

        String startPosition = "0:A:Vie";

        Board b = new Board();
        b.setStartPosition(startPosition);

        //
        assertEquals(startPosition, b.getStartPosition());

        //test the only unit provided
        assertEquals(b.getUnits().size(), 1);

        for (Unit u : b.getUnits()) {
            assertEquals(UnitType.ARMY, u.getType());
            assertNotNull(u.getRegion());
            assertEquals("Vie", u.getRegion().getName());
            assertEquals(Country.get(0), u.getCountry());
        }

        //test board from Country
        Collection<Unit> units = b.getUnits(Country.get(0));
        assertNotNull(units);
        assertEquals(1, units.size());


        //Region
        Region vienna = Region.get("Vie");
        assertNotNull(vienna);
        assertEquals("Vie", vienna.getName());

        //Test the unit in Vienna
        Unit u = b.getUnit(vienna);
        assertNotNull(u);
        assertEquals(vienna, u.getRegion());
        assertEquals(vienna, b.getRegion(u));
        assertEquals(vienna.getProvince(), b.getRegion(u).getProvince());

        assertEquals(u, b.getUnit(vienna.getProvince()));

        //
        b.controls(Country.get(0), u);
        assertTrue(b.isOccupiedByUnit(vienna));
        assertTrue(b.isOccupiedByUnit(vienna.getProvince()));

        //
        assertFalse(u.canConvoy());
        assertFalse(u.canBeConvoyed());

    }

    @Test
    public void testUnitType() {
        //JudgeEngine engine = new JudgeEngine();

        String startPosition = "0:A:Vie;2:F:Bre";

        Board b = new Board();
        b.setStartPosition(startPosition);

        assertNotNull(b.getUnits());
        assertEquals(2, b.getUnits().size());

        Unit a = b.getUnit("Vie");
        Unit f = b.getUnit("Bre");

        assertNotNull(a);
        assertNotNull(f);

        assertEquals(UnitType.ARMY, a.getType());
        assertEquals(UnitType.FLEET, f.getType());

    }

    @Test
    public void testBoardMove() {
        //JudgeEngine engine = new JudgeEngine();

        String startPosition = "0:A:Vie;2:A:Bre";

        Board b = new Board();
        b.setStartPosition(startPosition);

        //start
        Unit uStart = b.getUnit("Bre");
        assertNotNull(uStart);

        assertEquals(1, b.getUnits(Country.get(0)).size());
        assertEquals(1, b.getUnits(Country.get(2)).size());

        b.move("Bre", "Par");

        //end
        Unit uEnd = b.getUnit("Par");
        assertNotNull(uEnd);
        assertEquals(uStart, uEnd);

        assertNull(b.getUnit("Bre"));
        assertEquals(2, b.getUnits().size());

        assertEquals(1, b.getUnits(Country.get(0)).size());
        assertEquals(1, b.getUnits(Country.get(2)).size());

        assertEquals(Region.get("Par"), uStart.getRegion());
        assertEquals(Region.get("Par"), uEnd.getRegion());

        String endPosition = b.getStartPosition();
        assertEquals("0:A:Vie;2:A:Par", endPosition);
    }
}

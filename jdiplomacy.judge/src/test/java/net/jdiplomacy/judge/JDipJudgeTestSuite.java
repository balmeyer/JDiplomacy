/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jdiplomacy.judge;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * 
 * @author Jean-Baptiste
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { net.jdiplomacy.judge.TestInitialBoard.class,
		net.jdiplomacy.judge.TestJudgeEngine.class,
		net.jdiplomacy.judge.TestUnits.class, net.jdiplomacy.judge.TestOrders.class,
		net.jdiplomacy.judge.TestSupportingDataStructure.class,
		net.jdiplomacy.judge.TestSupport.class,
		net.jdiplomacy.judge.TestAdjust.class ,
		net.jdiplomacy.judge.TestDisband.class,
		net.jdiplomacy.judge.TestGameOver.class})
public class JDipJudgeTestSuite {

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jdiplomacy.judge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import net.jdiplomacy.judge.game.Board;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.UnitType;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.game.board.Province;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.game.board.Supply;
import net.jdiplomacy.judge.graph.Graph;
import net.jdiplomacy.judge.graph.GraphExplorer;
import net.jdiplomacy.judge.graph.GraphNode;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test the inital board game
 * @author Jean-Baptiste
 */
public class TestInitialBoard {

    public TestInitialBoard() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    @Test
    public void testGraph(){
        //JudgeEngine je = new JudgeEngine();


    	//test regions
    	assertNotNull(Province.getProvinceList());
    	assertTrue(Province.getProvinceList().size() > 0);
    	assertNotNull(Province.get("par"));
    	assertNotNull(Region.get("par"));
    	assertTrue(Region.getRegions().size() > 0);
         //test if graph exist
        assertNotNull(Unit.getGraph(UnitType.ARMY));
        assertNotNull(Unit.getGraph(UnitType.FLEET));

        //explore something
        GraphExplorer ge = new GraphExplorer(Unit.getGraph(UnitType.ARMY));
        ge.startAt("Par");

        List<String> explored = new ArrayList<String>();

        while (ge.hasNext()){
            GraphNode node = ge.next();
            assertNotNull(node);
            assertFalse(explored.contains(node.getName()));
            explored.add(node.getName());
        }

    }

    @Test
    public void testProvince(){
        @SuppressWarnings("unused")
		JudgeEngine je = new JudgeEngine();

        assertNotNull(Province.getProvinceList());
        assertEquals(Province.getProvinceList().size(), 75);

        for(Province p : Province.getProvinceList()){
            assertNotNull(p.getName());
            assertNotNull(p.getRegions());

            //fetch region
            Region r = Region.get(p.getShortName());
            assertNotNull(r);
        }

    }

    @SuppressWarnings("unused")
	@Test
    public void testRegions(){
        JudgeEngine je = new JudgeEngine();

        assertNotNull(Region.getRegions());

        for(Region r : Region.getRegions()){
            assertNotNull(r.getName());
            assertNotNull(r.getProvince());
            assertNotNull(r.getProvince().getRegions());
            assertNotNull(r.getProvincesAround());

            //name
            String regionName = r.getName().substring(0,3);
            assertEquals(regionName, r.getProvince().getShortName());

            //fetch Province by name
            Province p = Province.get(r.getName());
            assertNotNull(p);

            
        }
    }

    @Test
    public void testSupplies(){
        //JudgeEngine je = new JudgeEngine();

        assertNotNull(Supply.getSupplies());
        assertEquals(34,Supply.getSupplies().size());

        for(Supply s : Supply.getSupplies()){
            assertNotNull(s.getProvince());
            assertTrue(s.isNeutral() || s.getOriginalOwner() != null);
        }

        assertEquals(Country.get(0), Supply.getSupply(Province.get("Vie")).getOriginalOwner());


    }


    @Test
    public void testCountry(){
        //JudgeEngine je = new JudgeEngine();

        assertNotNull(Country.getCountries());
        assertEquals(Country.getCountries().size(), 7);

        for(Country c : Country.getCountries()){
            assertNotNull(c.getGameName());
        }
    }


    @Test
    public void testEmptyBoard(){
        //JudgeEngine je = new JudgeEngine();

        Board b = new Board();
        int nbSupply = b.countSupply(Country.get(0));
        //no supply
        assertEquals(0,nbSupply);




    }

    @Test
    public void testJudgeConstructors(){
    	JudgeEngine engine = new JudgeEngine();
    	
    	String startPositions1 = "0:A:Bre;1:F:Mar";
    	String [] positions2 = new String[] {"0:A:Bre" ,"1:F:Mar"};
    	
    	Board b1 = new Board(startPositions1);
    	Board b2 = new Board(positions2);
    	
    	assertEquals(b1.getStartPosition() , b2.getStartPosition());
    	
    	String [] orders = new String [] {"F Mar - Spa" , "A Bre - Par"};
    	
    	JudgeResult result = engine.judge(positions2, orders, Phase.movementSpring);
    	
    	assertNotNull(result.getBoard().getUnit("Spa"));
    	assertNotNull(result.getBoard().getUnit("Par"));
    }
    
    /**
     * Test static inits
     */
    @SuppressWarnings("unused")
	@Test
    public void testGameInit(){
    	//test multiple inits
    	JudgeEngine.initGame();
    	assertEquals(75, Province.getProvinceList().size());
    	assertEquals(81, Region.getRegions().size());
    	assertEquals(7,Country.getCountries().size());
    	assertEquals(34, Supply.getSupplies().size());
    	
    	Graph armyGraph = Unit.getGraph(UnitType.ARMY);
    	Graph fleetGraph = Unit.getGraph(UnitType.FLEET);
    	assertNotNull(armyGraph);
    	assertNotNull(fleetGraph);
    	assertEquals(56, armyGraph.getNodes().size());
    	assertEquals(64, fleetGraph.getNodes().size());
    	assertEquals(34, Supply.getSupplies().size());
    	
    	JudgeEngine.initGame();
    	JudgeEngine.initGame();
    	assertEquals(75, Province.getProvinceList().size());
    	assertEquals(81, Region.getRegions().size());
    	assertEquals(7,Country.getCountries().size());
    	assertEquals(34, Supply.getSupplies().size());
    	
    	armyGraph = Unit.getGraph(UnitType.ARMY);
    	fleetGraph = Unit.getGraph(UnitType.FLEET);
    	assertNotNull(armyGraph);
    	assertNotNull(fleetGraph);
    	assertEquals(56, armyGraph.getNodes().size());
    	assertEquals(64, fleetGraph.getNodes().size());
    	assertEquals(34, Supply.getSupplies().size());
    	
    	JudgeEngine engine = new JudgeEngine();
    	assertEquals(75, Province.getProvinceList().size());
    	assertEquals(81, Region.getRegions().size());
    	assertEquals(7,Country.getCountries().size());
    	assertEquals(34, Supply.getSupplies().size());
    }
    
}
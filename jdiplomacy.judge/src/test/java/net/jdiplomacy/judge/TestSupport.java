package net.jdiplomacy.judge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.order.Order;

import org.junit.Test;


public class TestSupport {

	
	@Test
	public void testHoldSupport(){
    	JudgeEngine engine = new JudgeEngine();
    	
    	JudgeEngine.options = JudgeOptions.failingSupportOrConvoyHold;

        String firstPosition = "2:A:Par;0:A:Bur;0:A:Pic;2:A:Bre";
        
        String orders1 = "A Bre S Par ; A Pic S A Bur - Par ; A Bur - Par";
        String orders2 = "A Bre S Par - Par ; A Pic S A Bur - Par ; A Bur - Par";
        String orders3 = "A Bre S Par H ; A Pic S A Bur - Par ; A Bur - Par; A Par H";
        
        String [] orders = new String[]{orders1 , orders2 ,orders3};
        
        for (String ord : orders){
        	JudgeResult jr = engine.judge(firstPosition, ord, Phase.movementFall);
        	Unit par = jr.getBoard().getUnit("par");
        	Unit bur = jr.getBoard().getUnit("bur");
        	assertEquals(Country.get(2), par.getCountry());
        	assertFalse(par.isMarkedAs(Mark.dislodged));
        	assertNotNull(bur);
        	assertTrue(bur.isMarkedAs(Mark.bounce));
        	
        }
	}
	
	/**
     * Attack which cut support
     */
    @Test
    public void testBounceWithOneSupportCutted() {
    	
    	JudgeEngine engine = new JudgeEngine();
    	
    	JudgeEngine.options = JudgeOptions.failingSupportOrConvoyHold;

        String firstPosition = "0:A:Gas;3:A:Mar;4:A:Por;1:F:Mid";
        String orders = "A Mar - Spa ; A Gas - Spa ; A Por S Mar - Spa ; F Mid - Por";

        JudgeResult result = engine.judge(firstPosition, orders, Phase.movementSpring);

        //result
        String finalPos = result.getFinalPosition();
        //bounced mark
        assertNotSame(firstPosition, finalPos);
        
        //nobody move !
        assertNotNull(result.getBoard().getStartPosition());
        assertNotNull(result.getBoard().getUnit("gas"));
        assertNotNull(result.getBoard().getUnit("por"));
        assertNotNull(result.getBoard().getUnit("mid"));
        assertNotNull(result.getBoard().getUnit("mar"));
        
        //no Spanish holidays
        assertNull(result.getBoard().getUnit("spa"));
        
        //test marks
        for(Unit u : result.getBoard().getUnits()){
        	assertNotNull(u.getMarks());
        }
        
        for(Order o : result.getOrders()){
        	assertNotNull(o);
        }
    
    }

	@Test
	public void testFleetSupport(){
    	JudgeEngine engine = new JudgeEngine();
    	
    	JudgeEngine.options = JudgeOptions.failingSupportOrConvoyHold;

    	String positions = "0:A:Tyr;0:F:Adr";
    	String orders = "A Tyr - Ven ; F Adr S Tyr - Ven";
    	
    	JudgeResult r = engine.judge(positions, orders, Phase.movementSpring);
    	
    	Unit ven = r.getBoard().getUnit("Ven");
    	Unit exTyr = r.getBoard().getUnit("tyr");
    	Unit fAdr = r.getBoard().getUnit("adr");
    	
    	assertNull(exTyr);
    	assertNotNull(ven);
    	assertNotNull(fAdr);
    	
	}
    
    
}

package net.jdiplomacy.judge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import net.jdiplomacy.judge.game.Board;
import net.jdiplomacy.judge.game.Unit;
import net.jdiplomacy.judge.game.board.Country;
import net.jdiplomacy.judge.game.board.Province;
import net.jdiplomacy.judge.game.board.Region;
import net.jdiplomacy.judge.order.Order;

import org.junit.Test;

public class TestDisband {

	@Test
	public void testDisband(){
		JudgeEngine je = new JudgeEngine();
		
		//burgundy dislodge paris
		String start = "2:A:Par:(dislodged):Bre,Pic;1:A:Par";
		
		
		Board b = new Board(start);
		assertEquals(2, b.getUnits().size());
		Unit france = b.getUnit(Province.get("par") , true);
		Unit italy = b.getUnit(Province.get("par") , false);
		
		assertNotNull(france);
		assertNotNull(italy);
		
		assertEquals(Country.get(2) , france.getCountry());
		assertEquals(Country.get(1), italy.getCountry());

		assertNotSame(0, france.getPossibleRetreats().size());
		assertTrue(france.getPossibleRetreats().contains(Region.get("bre")));
		assertTrue(france.getPossibleRetreats().contains(Region.get("pic")));

		
		//no orders
		String orders = "";
		
		JudgeResult jr = je.judge(start, orders, Phase.disbandSpring);
		
		assertEquals(1, jr.getBoard().getUnits().size());
		italy = jr.getBoard().getUnit("par");
		assertNotNull(italy);
		assertEquals(Country.get(1), italy.getCountry());
		
		b = null;
		
		//disband in forbidden region
		orders = "A Par - Bur"; //impopossible retreats !
		
		jr = je.judge(start, orders, Phase.disbandSpring);
		
		assertEquals(1, jr.getBoard().getUnits().size());
		france = jr.getBoard().getUnit(Province.get("par") , true);
		italy = jr.getBoard().getUnit("par");
		
		assertNotNull(italy);
		assertNull(france);
		assertEquals(Country.get(1), italy.getCountry());
		
		//ok orders
		orders = "A Par - Bre";
		
		jr = je.judge(start, orders, Phase.disbandSpring);
		
		assertEquals(2, jr.getBoard().getUnits().size());
		france = jr.getBoard().getUnit(Province.get("bre") , false);
		italy = jr.getBoard().getUnit("par");
		assertNull(jr.getBoard().getUnit(Province.get("par") , true));
		assertEquals(italy, jr.getBoard().getUnit(Province.get("par") ,false));
		assertNotNull(italy);
		assertNotNull(france);
		assertEquals(Country.get(1), italy.getCountry());
		assertEquals(Country.get(2), france.getCountry());
		
		
	}
	
	
	@Test
	public void testDislodgeAndDisband(){
		JudgeEngine je = new JudgeEngine();
		
		//burgundy dislodge paris
		String start = "4:A:Pie;4:A:Tus;3:A:Ven";
		String orders = "A Pie - Ven ; A Tus S Pie - Ven";
		JudgeResult r = je.judge(start, orders, Phase.movementSpring);
		
		for(Order o : r.getOrders()){
			assertTrue(o.isSuccess());
		}
		Unit itFromPie = r.getBoard().getUnit(Province.get("ven"), false);
		Unit gerPie = r.getBoard().getUnit(Province.get("ven"), true);
		Unit tus = r.getBoard().getUnit(Province.get("tus"), false);
		
		assertNotNull(itFromPie);
		assertNotNull(gerPie);
		assertNotNull(tus);
		assertEquals(Phase.disbandSpring,r.getNextPhase());
		assertTrue(gerPie.isMarkedAs(Mark.dislodged));
		
		//Disband
		orders = "A Ven - Tyr";
		r = je.judge(r.getFinalPosition(), orders, Phase.disbandFall);
		Unit disband = r.getBoard().getUnit(Province.get("tyr"), false);
		assertNotNull(disband);
		for(Order o : r.getOrders()){
			assertTrue(o.isSuccess());
		}
		disband = r.getBoard().getUnit(Province.get("ven"), true);
		Unit moved = r.getBoard().getUnit(Province.get("ven"), false);
		assertNull(disband);
		assertNotNull(moved);
		
	}
	
	@Test
	public void testBounceDisband(){
		JudgeEngine je = new JudgeEngine();
		
		//burgundy dislodge paris
		String start = "2:A:Par:(dislodged):Bre,Bur;4:A:Bre";
		String orders = "A Par - Bre";
		JudgeResult r = je.judge(start, orders, Phase.disbandSpring);
		
		assertEquals(Phase.movementFall, r.getNextPhase());
		for(Order o : r.getOrders()){
			//assertTrue(o.getUnit().isMarkedAs(Mark.bounce));
			assertFalse(o.isSuccess());
		}
		assertEquals(1,r.getUnits().size());
		Unit dis = r.getBoard().getUnit("par", true);
		Unit notdis = r.getBoard().getUnit("par", false);
		Unit bre = r.getBoard().getUnit("bre",false);
		Unit bredis = r.getBoard().getUnit("bre",true);
	
		assertNull(dis);
		assertNull(notdis);
		assertNotNull(bre);
		assertNull(bredis);
		
		assertEquals("4:A:Bre", r.getFinalPosition());
	}
}
